//
//  Home_LastCell.m
//  SteelonCall
//
//  Created by INDOBYTES on 29/07/17.
//  Copyright © 2017 com.indobytes. All rights reserved.
//

#import "Home_LastCell.h"
#import "SDWebImageManager.h"

@implementation Home_LastCell
{
    NSArray * tetimonialArr,*blogArr,*brandArr;
    CGRect cellFrameInSuperview,blogcellFrameInSuperview;
    double tempValue,tempvalue1;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
//    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
//    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    
    _brandLabel.layer.borderWidth = .5;
    _brandLabel.layer.borderColor = [UIColor whiteColor].CGColor;
    _brandLabel.layer.cornerRadius = _brandLabel.frame.size.height / 2;
    _brandLabel.clipsToBounds = YES;
    
    
    UINib *cellNib = [UINib nibWithNibName:@"testimonialCell" bundle:nil];
    [self.testimonialCollection registerNib:cellNib forCellWithReuseIdentifier:@"testimonialCell"];
   // self.testimonialCollection.pagingEnabled = YES;
    UINib *cellNib1 = [UINib nibWithNibName:@"blogCell" bundle:nil];
    [self.blogUpdateCollection registerNib:cellNib1 forCellWithReuseIdentifier:@"blogCell"];
    
    //brandsCell
    UINib *cellNib2 = [UINib nibWithNibName:@"brandsCell" bundle:nil];
    [self.brandCollectionView registerNib:cellNib2 forCellWithReuseIdentifier:@"brandsCell"];
    
    self.testimonialCollection.delegate = self;
    self.testimonialCollection.dataSource = self;
    
    self.blogUpdateCollection.delegate = self;
    self.blogUpdateCollection.dataSource = self;
    
    self.brandCollectionView.delegate = self;
    self.brandCollectionView.dataSource = self;

    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.testimonialView.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(20.0, 20.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.testimonialView.bounds;
    maskLayer.path  = maskPath.CGPath;
  //  self.testimonialView.layer.mask = maskLayer;
    
    UIBezierPath *maskPath1 = [UIBezierPath bezierPathWithRoundedRect:self.blogView.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(20.0, 20.0)];
    
    CAShapeLayer *maskLayer1 = [[CAShapeLayer alloc] init];
    maskLayer1.frame = self.blogView.bounds;
    maskLayer1.path  = maskPath1.CGPath;
   // self.blogView.layer.mask = maskLayer1;
    [self blogUpdate];
    [self clientTestimonial];
    [self loadAdds];
}
-(void)loadAdds{
    
    [[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString:@"getBrandsList" requestNumber:Cart_Count showProgress:NO withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             if (data) {
                 brandArr = [[NSArray alloc]init];
                 brandArr = data;
                 [_brandCollectionView reloadData];
//                 for (NSDictionary *dic in data) {
//                     
//                     NSArray *Ar = [[dic valueForKey:@"brand_image"] componentsSeparatedByString:@"/" ];
//                     if ([Ar containsObject:@"tata.png"]) {
////                         [ad_Array setValue:[dic valueForKey:@"brand_image"] forKey:@"img1"];
////                         [ad_Array setValue:[dic valueForKey:@"description"] forKey:@"desc1"];
//                         
//                     }
//                     if ([Ar containsObject:@"vizag_2.png"]) {
//                         
////                         [ad_Array setValue:[dic valueForKey:@"brand_image"] forKey:@"img2"];
////                         [ad_Array setValue:[dic valueForKey:@"description"] forKey:@"desc2"];
//                         
//                     }
//                     
//                 }
                 
                 //[_testtableView reloadData];
                 
                 //archive
                 
             }
             
         }
         
         
     }];
    
    
}

-(void)clientTestimonial
{
    [[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString:@"calculate/service/getTestimonials" requestNumber:WS_Banners showProgress:NO withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             if (data) {
                 
                tetimonialArr = [[NSArray alloc]init];
                 tetimonialArr = data;
                 [_testimonialCollection reloadData];
             }
         }
     }];
}
-(void)blogUpdate
{
    [[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString:@"calculate/service/displayBlogPosts" requestNumber:WS_Banners showProgress:NO withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             if (data) {
                 
                 blogArr = [[NSArray alloc]init];
                 blogArr = data;
                 [_blogUpdateCollection reloadData];
             }
         }
     }];
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == _brandCollectionView) {
        return brandArr.count;
    }
    else if (collectionView == _testimonialCollection) {
        return tetimonialArr.count;
    }
    else
    {
        return blogArr.count;
    }
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:  (UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == _brandCollectionView) {
        return CGSizeMake(100, 70);
    }
   else if (collectionView == _testimonialCollection ) {
         return CGSizeMake(280, 190);
    }
    else
    {
         return CGSizeMake(300, 180);
    }
   
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == _brandCollectionView) {
        brandsCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"brandsCell" forIndexPath:indexPath];
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        
        
        
                [manager downloadImageWithURL:[NSURL URLWithString:[[brandArr valueForKey:@"brand_image"]objectAtIndex:indexPath.item]] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize)
        
                 {
        
        
                 } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        
                     if(image){
                         
                         UIImage *img = [self convertToGreyscale:image];
        
                         cell.brandImage.image = img;
                         //   NSLog(@"image=====%@",image);
                     }
                 }];
       // cell.brandImage.image =
        
        [_brandCollectionView setShowsHorizontalScrollIndicator:YES];
       
        //[_brandCollectionView setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        
        
        return cell;
    }
   else if (collectionView == _testimonialCollection) {
        testimonialCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"testimonialCell" forIndexPath:indexPath];
        cell.descriptionLabel.text = [[tetimonialArr valueForKey:@"testimonial"]objectAtIndex:indexPath.item];
        cell.emailLabel.text =[[tetimonialArr valueForKey:@"email"]objectAtIndex:indexPath.item];
cell.websiteLabel.text =[[tetimonialArr valueForKey:@"website"]objectAtIndex:indexPath.item];
        cell.nameLabel.text =[NSString stringWithFormat:@"%@ , %@",[[tetimonialArr valueForKey:@"name"]objectAtIndex:indexPath.item],[[tetimonialArr valueForKey:@"company"]objectAtIndex:indexPath.item]];
      //  cell.companyLabel.text =[[tetimonialArr valueForKey:@"company"]objectAtIndex:indexPath.item];
        cell.addressLabel.text =[[tetimonialArr valueForKey:@"address"]objectAtIndex:indexPath.item];
        
        return cell;
    }
    else
    {
        blogCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"blogCell" forIndexPath:indexPath];
        cell.titleLabel.text =[[blogArr valueForKey:@"title"]objectAtIndex:indexPath.item];
      //  cell.desc.text =[[blogArr valueForKey:@"post_short_desc"]objectAtIndex:indexPath.item];
        cell.dateLabel.text =[NSString stringWithFormat:@"%@ %@",[[blogArr valueForKey:@"day"]objectAtIndex:indexPath.item],[[blogArr valueForKey:@"month_year"]objectAtIndex:indexPath.item]];
        //cell.linkbtn.text =[[blogArr valueForKey:@"address"]objectAtIndex:indexPath.item];
      //  [ cell.linkbtn setTitle:[[blogArr valueForKey:@"link"]objectAtIndex:indexPath.item] forState:UIControlStateNormal];
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        
        
        
        [manager downloadImageWithURL:[NSURL URLWithString:[[blogArr valueForKey:@"post_image_path"]objectAtIndex:indexPath.item]] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize)
         
         {
             
             
         } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
             
             if(image){
                 
                 cell.imageOutlet.image = image;
                 //   NSLog(@"image=====%@",image);
             }
         }];
        
        [cell.linkAction addTarget:self action:@selector(gotoLink:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
}

- (UIImage *) convertToGreyscale:(UIImage *)i {
    
    int kRed = 1;
    int kGreen = 2;
    int kBlue = 4;
    
    int colors = kGreen | kBlue | kRed;
    int m_width = i.size.width;
    int m_height = i.size.height;
    
    uint32_t *rgbImage = (uint32_t *) malloc(m_width * m_height * sizeof(uint32_t));
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(rgbImage, m_width, m_height, 8, m_width * 4, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaNoneSkipLast);
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    CGContextSetShouldAntialias(context, NO);
    CGContextDrawImage(context, CGRectMake(0, 0, m_width, m_height), [i CGImage]);
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    // now convert to grayscale
    uint8_t *m_imageData = (uint8_t *) malloc(m_width * m_height);
    for(int y = 0; y < m_height; y++) {
        for(int x = 0; x < m_width; x++) {
            uint32_t rgbPixel=rgbImage[y*m_width+x];
            uint32_t sum=0,count=0;
            if (colors & kRed) {sum += (rgbPixel>>24)&255; count++;}
            if (colors & kGreen) {sum += (rgbPixel>>16)&255; count++;}
            if (colors & kBlue) {sum += (rgbPixel>>8)&255; count++;}
            m_imageData[y*m_width+x]=sum/count;
        }
    }
    free(rgbImage);
    
    // convert from a gray scale image back into a UIImage
    uint8_t *result = (uint8_t *) calloc(m_width * m_height *sizeof(uint32_t), 1);
    
    // process the image back to rgb
    for(int i = 0; i < m_height * m_width; i++) {
        result[i*4]=0;
        int val=m_imageData[i];
        result[i*4+1]=val;
        result[i*4+2]=val;
        result[i*4+3]=val;
    }
    
    // create a UIImage
    colorSpace = CGColorSpaceCreateDeviceRGB();
    context = CGBitmapContextCreate(result, m_width, m_height, 8, m_width * sizeof(uint32_t), colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaNoneSkipLast);
    CGImageRef image = CGBitmapContextCreateImage(context);
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    UIImage *resultUIImage = [UIImage imageWithCGImage:image];
    CGImageRelease(image);
    
    free(m_imageData);
    
    // make sure the data will be released by
    
    [NSData dataWithBytesNoCopy:result length:m_width * m_height];
    
    return resultUIImage;
}
-(void)gotoLink:(id)sender
{
    int tag =[sender tag];
    NSString * linkUrl = [[blogArr valueForKey:@"link"]objectAtIndex:tag];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: linkUrl]];
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    //tempValue =0;
    for (UICollectionViewCell *cell in [self.testimonialCollection visibleCells]) {
        NSIndexPath *indexPath = [self.testimonialCollection indexPathForCell:cell];
        
        UICollectionViewLayoutAttributes *attributes = [self.testimonialCollection layoutAttributesForItemAtIndexPath:indexPath];
        
        CGRect cellRect = attributes.frame;
        
        cellFrameInSuperview = [self.testimonialCollection convertRect:cellRect toView:[self.testimonialCollection superview]];
        tempvalue1 = cellFrameInSuperview.origin.x+100;
        break;
    }
    for (UICollectionViewCell *cell in [self.blogUpdateCollection visibleCells]) {
        NSIndexPath *indexPath = [self.blogUpdateCollection indexPathForCell:cell];
        
        UICollectionViewLayoutAttributes *attributes = [self.blogUpdateCollection layoutAttributesForItemAtIndexPath:indexPath];
        
        CGRect cellRect = attributes.frame;
        
        blogcellFrameInSuperview = [self.blogUpdateCollection convertRect:cellRect toView:[self.blogUpdateCollection superview]];
        tempValue = blogcellFrameInSuperview.origin.x+100;
        break;
    }

}

- (IBAction)gotoLeftAction:(id)sender {
    if (tempvalue1 == 0) {
        tempvalue1 = 0;
    }
    else if (tempvalue1 == cellFrameInSuperview.origin.x)
    {
        tempvalue1 -= 100;
    }
    else if (tempvalue1 < 0)
    {
        tempvalue1 = 0;
    }
    else
    {
        tempvalue1 -= 100;
    }
    CGPoint offset = CGPointMake(tempvalue1, 0.0);
    [self.testimonialCollection setContentOffset:offset animated:YES];

}

- (IBAction)gotoRightAction:(id)sender {
    if (tempvalue1 == 0) {
        tempvalue1 = blogcellFrameInSuperview.origin.x+100;
    }
    else if (tempvalue1 == blogcellFrameInSuperview.origin.x)
    {
        tempvalue1 += 100;
    }
    else if (tempvalue1 > (tetimonialArr.count-2)*170)
    {
        tempvalue1 = (tetimonialArr.count-1)*170;
    }
    else
    {
        tempvalue1 += 100;
    }
    CGPoint offset = CGPointMake(tempvalue1, 0.0);
    [self.testimonialCollection setContentOffset:offset animated:YES];
   }

- (IBAction)blogleftAction:(id)sender {

    if (tempValue == 0) {
        tempValue = blogcellFrameInSuperview.origin.x;
    }
    else if (tempValue == blogcellFrameInSuperview.origin.x)
    {
        tempValue -= 100;
    }
    else if (tempValue < 0)
    {
        tempValue = 0;
    }
    else
    {
        tempValue -= 100;
    }
    CGPoint offset = CGPointMake(tempValue, 0.0);
    [self.blogUpdateCollection setContentOffset:offset animated:YES];
}


- (IBAction)blogRightAction:(id)sender {

    if (tempValue == 0) {
        tempValue = blogcellFrameInSuperview.origin.x+100;
    }
    else if (tempValue == blogcellFrameInSuperview.origin.x)
    {
        tempValue += 100;
    }
    else if (tempValue > (blogArr.count-2)*170)
    {
        tempValue = (blogArr.count-2)*170;
    }
    else
    {
        tempValue += 100;
    }
   CGPoint offset = CGPointMake(tempValue, 0.0);
    [self.blogUpdateCollection setContentOffset:offset animated:YES];
    
}
@end
