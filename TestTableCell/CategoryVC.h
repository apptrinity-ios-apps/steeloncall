//
//  CategoryVC.h
//  SteelonCall
//
//  Created by INDOBYTES on 05/11/18.
//  Copyright © 2018 com.indobytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryVC : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *categorytableview;

@end
