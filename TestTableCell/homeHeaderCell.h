//
//  homeHeaderCell.h
//  SteelonCall
//
//  Created by INDOBYTES on 24/10/18.
//  Copyright © 2018 com.indobytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface homeHeaderCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;

@end
