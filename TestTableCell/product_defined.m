//
//  product_defined.m
//  SteelonCall
//
//  Created by Manoyadav on 01/12/16.
//  Copyright © 2016 com.way2online. All rights reserved.
//

#import "product_defined.h"
#import "ShippingHeaderCell.h"
#import "searchPopupVIew.h"
#import "NewLoginVC.h"
#import "CategoryVC.h"
#import "ProductCollectionCell.h"

@interface product_defined ()<moreSellers>
{
    NSString *slcdID_Compare;
    BOOL tonesClicked , piecesClicked , totalPriceChange;
    Product_defined_cell *cell;
    UIImage * currentImage;
    int tag;
    NSMutableArray *listArr,*pieceArr;
    NSString *dot;
    NSInteger searchSelected;
    
    NSArray * changeTonesArray;
    
    NSMutableArray *mainSearchArry;
    
    NSMutableArray *searchResultArray;
    NSDictionary *selectedDic;
    NSInteger count;
    
}
@end
@implementation product_defined
@synthesize activityView;
-(void)viewWillAppear:(BOOL)animated
{
    [activityView removeFromSuperview];
    dot = @"no";
    AppDelegate *del = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [del setShouldRotate:NO];
    
    self.navigationController.navigationBar.hidden = YES;
    
    user_data=[NSUserDefaults standardUserDefaults];
    user_id=[user_data valueForKey:@"user_id"];
    NSString *C_Count=[user_data valueForKey:@"cart_count"];
    if ([C_Count isEqual:[NSNull null]]||[C_Count isEqualToString:@""]||C_Count ==nil||[C_Count isEqualToString:@"<nil>"]||[C_Count isEqualToString:@"null"]||[C_Count isEqualToString:@"<null>"]||[C_Count isEqualToString:@"(null)"]) {
        C_Count = @"0";
    }
    _Cart_lbl.text=C_Count;
    if ([_seller_name isEqualToString:@""]||[_seller_name isEqual:[NSNull null]]||_seller_name==NULL)
    {
    }
    else
    {
        More_seller_tag=_tag_str;
        NSMutableDictionary *test=[[NSMutableDictionary alloc]init];
        test=[[_products_ary objectAtIndex:More_seller_tag] mutableCopy];
        
        [test setObject:_price forKey:@"price"];
        [test setObject:_seller_name forKey:@"seller"];
        [test setObject:_S_id forKey:@"child_id"];

        _Pin_code=[_pin mutableCopy];
        _Location=[_Location mutableCopy];
        
        _selected_qty=_s_qty;
        _selected_ids=_s_ids;
        
        _brand_lbl.text=_brand_str;
        _grade_lbl.text=_grade_str;
        
        [_products_ary replaceObjectAtIndex:More_seller_tag withObject:test];
        Products_dict=[_products_ary mutableCopy];
        
        T_P_Ary=[_TP_ary mutableCopy];
        [_list_view reloadData];
    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
    
}
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}


-(void)productIds:(NSDictionary *)prod
{
       _price = [prod valueForKey:@"price"];
     _ton_price = [prod valueForKey:@"tonprice"];
    _seller_name = [prod valueForKey:@"seller_name"];
      _S_id = [prod valueForKey:@"S_id"];
      _TP_ary = [prod valueForKey:@"TP_ary"];
      _Location = [prod valueForKey:@"Location"];
      _tag_str = [[prod valueForKey:@"tag_str"] intValue];
      _pin = [prod valueForKey:@"pin"];
      _s_ids = [prod valueForKey:@"s_ids"];
    _s_qty = [prod valueForKey:@"s_qty"];
    _brand_str = [prod valueForKey:@"brand_str"];
    _grade_str = [prod valueForKey:@"grade_str"];
    _products_ary = [prod valueForKey:@"products_ary"];
    
    if ([_seller_name isEqualToString:@""]||[_seller_name isEqual:[NSNull null]]||_seller_name==NULL)
    {
    }
    else
    {
        More_seller_tag=_tag_str;
        NSMutableDictionary *test=[[NSMutableDictionary alloc]init];
        test=[[_products_ary objectAtIndex:More_seller_tag] mutableCopy];
        
        [test setObject:_price forKey:@"price"];
         [test setObject:_ton_price forKey:@"tonprice"];
        [test setObject:_seller_name forKey:@"seller"];
        [test setObject:_S_id forKey:@"child_id"];
        
        _Pin_code=[_pin mutableCopy];
        _Location=[_Location mutableCopy];
        
        _selected_qty=_s_qty;
        _selected_ids=_s_ids;
        
        _brand_lbl.text=_brand_str;
        _grade_lbl.text=_grade_str;
        
        [_products_ary replaceObjectAtIndex:More_seller_tag withObject:test];
        Products_dict=[_products_ary mutableCopy];
        T_P_Ary=[_TP_ary mutableCopy];
        [self shipping_info_service];
        
        
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
     self.outOfStock_View.hidden = YES;
    
    _searchTextField.layer.cornerRadius = _searchTextField.frame.size.height/2;
    _searchTextField.clipsToBounds = YES;
    _searchTextField.layer.sublayerTransform = CATransform3DMakeTranslation(20, 0, 0);
    _categoryLabel.layer.cornerRadius = _categoryLabel.frame.size.height/2;
    _categoryLabel.clipsToBounds = YES;
    [_searchTextField addTarget:self action:@selector(searchItems:) forControlEvents:UIControlEventEditingChanged];
    
    _compareDealsBtn.layer.shadowRadius  = 0.5f;
    _compareDealsBtn.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    _compareDealsBtn.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    _compareDealsBtn.layer.shadowOpacity = 0.3f;
    _compareDealsBtn.layer.cornerRadius = 3;
    _compareDealsBtn.layer.masksToBounds = NO;
    
   // NSInteger k =   [UIScreen mainScreen].bounds.size.width ;
    
    
    // Do any additional setup after loading the view from its nib.
    _list_view.dataSource = self;
    _list_view.delegate = self;
    _list_view.allowsSelection=NO;
    _company_view.layer.cornerRadius = 20;
    _model_view.layer.cornerRadius = 20;
    pieces_per_ton_ary=[[NSMutableArray alloc]init];
    listArr=[[NSMutableArray alloc]init];
    pieceArr=[[NSMutableArray alloc]init];
    tonesClicked = NO;
    piecesClicked = NO;
    self.list_view.hidden = false;
    self.popUpView.hidden = true;
    _compare_view.layer.cornerRadius = 5;
    _compare_view.layer.masksToBounds = true;
    _pincodeLabel.text = [NSString stringWithFormat:@" YOUR SELECTED PINCODE : -%@",_Pin_code];
    [cell.tonesChangeBtn setImage:[UIImage imageNamed:@"edit"] forState:UIControlStateNormal];
    [cell.piecesChangeBTn setImage:[UIImage imageNamed:@"edit"] forState:UIControlStateNormal];
//    if ([_product_type isEqualToString:@""])
//    {
        tag_type=YES;
//    }
//    else
//        tag_type=NO;
    
    Products_dict=[[NSMutableArray alloc]init];

    keyboardIsShown=NO;
    //notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
//
    //tap gesture
    singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.list_view addGestureRecognizer:singleFingerTap];
    
    _list_view.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    searchView = [[searchPopupVIew alloc]initWithFrame:CGRectMake(_company_btn.frame.origin.x+10,_company_btn.frame.origin.x+55 ,[UIScreen mainScreen].bounds.size.width/4,200)];
    searchView.delegate =self;
    searchView.hidden =YES;
    Referesh_tag=@"NO";
    [self.view addSubview:searchView];
    
    noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, _list_view.bounds.size.width, _list_view.bounds.size.height)];
    noDataLabel.text             = @"Select Brand and Grade";
    noDataLabel.numberOfLines=2;
    noDataLabel.textColor        = [UIColor colorWithRed:7/255.0 green:21/255.0 blue:82/255.0 alpha:1];
    [noDataLabel setFont:[UIFont fontWithName:@"RaleWay-SemiBold" size:17]];
    noDataLabel.textAlignment    = NSTextAlignmentCenter;
    _list_view.backgroundView = noDataLabel;
    
    //The setup code (in viewDidLoad in your view controller)
    UITapGestureRecognizer *singleFingerTap1 =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap1:)];
    [_cart_view addGestureRecognizer:singleFingerTap1];
    
    
    _collectionBackView.hidden = YES;
    _Collection_PopUpView.hidden = YES;
    
    
    UINib *cellNib = [UINib nibWithNibName:@"ProductCollectionCell" bundle:nil];
    
    [self.self.collection_View registerNib:cellNib forCellWithReuseIdentifier:@"ProductCollectionCell"];
    
    //[self.collection_View registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"ProductCollectionCell"];
    
    _collection_View.allowsMultipleSelection = NO;
    [self call_tonnes_service];
    
    
     [self Getting_Brands:(id)0];
    
    
    
    
    
    
    
    
}
-(void)viewDidAppear:(BOOL)animated
{
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
    {
        searchView = [[searchPopupVIew alloc]initWithFrame:CGRectMake(10,_searchTextField.frame.origin.y+40 ,[UIScreen mainScreen].bounds.size.width-20,400)];
        
    }
    else
    {
        if ([UIScreen mainScreen].bounds.size.width == 320) {
            searchView = [[searchPopupVIew alloc]initWithFrame:CGRectMake(10,_searchTextField.frame.origin.y+40 ,[UIScreen mainScreen].bounds.size.width-20,200)];
        }else{
            searchView = [[searchPopupVIew alloc]initWithFrame:CGRectMake(10,_searchTextField.frame.origin.y+40 ,[UIScreen mainScreen].bounds.size.width-20,260)];
        }
    }
    searchView.delegate =self;
    searchView.hidden =YES;
    [self.view addSubview:searchView];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}
- (void)viewDidUnload
{
    _company_btn = nil;
    [searchItemsArray removeAllObjects];
    [super viewDidUnload];
}
-(void)searchItems:(UITextField *)textField{
    
    searchSelected = 2;
    if(textField.text.length>0)
    {
        searchView.hidden =NO;
        if (mainSearchArry.count==0) {
            [self searchWebserviceCall];
        }else{
            
            NSPredicate *pred  = [NSPredicate predicateWithFormat:@"(name CONTAINS[c] %@)",textField.text];
            searchResultArray =[NSMutableArray arrayWithArray:[mainSearchArry filteredArrayUsingPredicate:pred]];
            if (searchResultArray.count==0) {
                // searchResultArray = mainSearchArry;
            }
            searchItemsArray = searchResultArray;
            [searchView relaodSearchTable];
        }
    }else{
        //  [searchItemsArray removeAllObjects];
        selectedDic = [[NSDictionary alloc]init];
        searchView.hidden =YES;
    }
}
-(float)setHeighForSearchTable:(UITableView *)tableView
{
    return 40;
}

-(NSInteger )numberOffRowsInSearchTableView:(UITableView *)tableView
{
    if(searchSelected == 1){
        return searchItemsArray.count;
    }else{
        return searchItemsArray.count;
    }
}
- (JMOTableViewCell *)cellforRowAtSearchINdex:(UITableView*)tableVIew viewAtIndex:(NSIndexPath *)index
{
    
    
    if(searchSelected == 1){
        JMOTableViewCell *cell1 = [tableVIew dequeueReusableCellWithIdentifier:@"searchCell"];
        if (cell1==nil)
        {
            [tableVIew registerNib:[UINib nibWithNibName:@"JMOTableViewCell" bundle:nil] forCellReuseIdentifier:@"searchCell"];
            cell1 = [tableVIew dequeueReusableCellWithIdentifier:@"searchCell"];
        }
        //  [cell.textLabel setFont:[UIFont fontWithName:@"RaleWay-Bold" size:16]];
        // cell.textLabel.font = [UIFont fontWithName:@"RaleWay-Bold" size:16];
        cell1.contentView.backgroundColor = [UIColor colorWithRed:10/255.0f green:21/255.0f blue:80/255.0f alpha:1.0];
        cell1.textLabel.backgroundColor = [UIColor colorWithRed:10/255.0f green:21/255.0f blue:80/255.0f alpha:1.0];
        cell1.labelName.text =[searchItemsArray objectAtIndex:index.row];
        //  [cell.labelName setFont:[UIFont systemFontOfSize:13]];
        cell1.labelName.textAlignment = NSTextAlignmentCenter;
        return cell1;
    }else{
       JMOTableViewCell *cell = [tableVIew dequeueReusableCellWithIdentifier:@"searchCell"];
        if (cell==nil)
        {
            [tableVIew registerNib:[UINib nibWithNibName:@"JMOTableViewCell" bundle:nil] forCellReuseIdentifier:@"searchCell"];
            cell = [tableVIew dequeueReusableCellWithIdentifier:@"searchCell"];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.labelName.text =[[searchItemsArray objectAtIndex:index.row] valueForKey:@"name"];
        
        cell.contentView.backgroundColor = [UIColor colorWithRed:10/255.0f green:21/255.0f blue:80/255.0f alpha:1.0];
        cell.textLabel.backgroundColor = [UIColor colorWithRed:10/255.0f green:21/255.0f blue:80/255.0f alpha:1.0];    return cell;
    }
    
}

-(void)searchTableViewSelected:(UITableView *)table IndexPath:(NSIndexPath * )indexPath
{
    
    if(searchSelected == 1){
        selected_item =[searchItemsArray objectAtIndex:indexPath.row];
        if ([popup_tag isEqualToString:@"Brand"])
        {
            _brand_lbl.text=selected_item;
            _grade_lbl.text=@"Select Grade";
            
            [self Getting_Grades:(id)0];
        }
        else if ([popup_tag isEqualToString:@"Grade"])
        {
            _grade_lbl.text=selected_item;
            dot = @"no";
            [self Getting_Define_Products];
        }
        searchView.hidden =YES;
    }else{
        selectedDic = [[NSDictionary alloc]init];
        selectedDic =[searchItemsArray objectAtIndex:indexPath.row];
        _searchTextField.text =[selectedDic valueForKey:@"name"];
        searchView.hidden =YES;
    }
}

- (IBAction)searchProductActionClicked:(id)sender {
    
    _searchTextField.text= [_searchTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    [self.view endEditing:YES];
    if (_searchTextField.text.length == 0) {
        
        ALERT_DIALOG(@"alert",@"Please enter item name ");
    }
    else
    {
        if(selectedDic)
        {
            
            Products_List * newView = [[Products_List alloc] initWithNibName:@"Products_List" bundle:nil];
            newView.product_id=[selectedDic valueForKey:@"id"];
            newView.product_type=[selectedDic valueForKey:@"name"];
            newView.type=[selectedDic valueForKey:@"type"];
            
            newView.from=@"search";
            self.navigationController.navigationBar.hidden = NO;
            [self.navigationController pushViewController:newView animated:YES];
            _searchTextField.text =@"";
            selectedDic = nil;
            searchView.hidden =YES;
            
        }
        else
        {
            ALERT_DIALOG(@"alert",@"Please select an item ");
        }
    }
}

-(void)searchWebserviceCall
{
    [[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString:@"search" requestNumber:WS_Search showProgress:YES withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             if (data) {
                 
                 searchItemsArray = [[NSMutableArray alloc]init];
                 mainSearchArry = [[NSMutableArray alloc]init];
                 [searchItemsArray  addObjectsFromArray:data];
                 [ mainSearchArry addObjectsFromArray:data];
                 
                 [searchView relaodSearchTable];
                 //archive
                 
             }
             
         }
         
         
     }];
    
}
-(void)call_tonnes_service
{
    
     NSString *url_form=[NSString stringWithFormat:@"https://steeloncall.com/getcalculateqty?ids=%@",_selected_ids];
    
      // NSString *url_form=[NSString stringWithFormat:@"getcalculateqty?ids=%@",_selected_ids];
    NSString* urlEncoded = [url_form stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString: urlEncoded requestNumber:PRODUCT_TONNES showProgress:YES withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             NSLog(@" response %@",data);
             [pieces_per_ton_ary removeAllObjects];
             
             NSDictionary *res_data=data;
             for (int p=0; p<res_data.count; p++)
             {
                [pieces_per_ton_ary addObject:[[res_data valueForKey:@"pieces"]objectAtIndex:p]];
             }
        }
         else
         {
//             UIAlertController * alert=   [UIAlertController
//                                           alertControllerWithTitle:@"Alert"
//                                           message:[NSString stringWithFormat:@"%@",[data valueForKey:@"error_message"]]
//                                           preferredStyle:UIAlertControllerStyleAlert];
//             
//             UIAlertAction* ok = [UIAlertAction
//                                  actionWithTitle:@"OK"
//                                  style:UIAlertActionStyleDefault
//                                  handler:^(UIAlertAction * action)
//                                  {
//                                      [alert dismissViewControllerAnimated:YES completion:nil];
//                                  }];
//             [alert addAction:ok];
//             [self presentViewController:alert animated:YES completion:nil];
         }
     }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0) {
        
         return Products_dict.count;
    }
    else
    {
        if (Dict.count>0)
        {
            return Dict.count+1;
        }
        return 0;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        
    static NSString *simpleTableIdentifier = @"Product_defined_cell";
    cell = (Product_defined_cell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"Product_defined_cell" owner:self options:nil];
        cell = (Product_defined_cell *)[nib objectAtIndex:0];
    }
        
        cell.tonesChangeBtn.tag = indexPath.row;
        cell.piecesChangeBTn.tag = indexPath.row;
        
        [cell.tonesChangeBtn addTarget:self action:@selector(tonesChangeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [cell.piecesChangeBTn addTarget:self action:@selector(piecesChangeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    cell.defined_cell_view.layer.cornerRadius = 5;
    
    cell.remove.tag = indexPath.row;
    cell.save.tag = indexPath.row;
    cell.more_sellers.tag = indexPath.row;
    cell.detail_view.tag = indexPath.row;
    cell.tonnes_tf.delegate = self;
    cell.pieces_tf.delegate = self;
        cell.tonnes_tf.enabled = NO;
        cell.pieces_tf.enabled = NO;
        cell.pieces_tf.keyboardType=UIKeyboardTypePhonePad;

    UIToolbar *keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStyleBordered target:self
                                                                  action:@selector(doneClicked:)];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    cell.tonnes_tf.inputAccessoryView = keyboardDoneButtonView;
    cell.pieces_tf.inputAccessoryView = keyboardDoneButtonView;

    int size = 0;
    if ([UIScreen mainScreen].bounds.size.width <= 320) {
        
        size = 11;
        
    }else{
        
         size = 12;
    }
    UIColor *color = [UIColor grayColor];
    cell.tonnes_tf.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:@"Quantity in Tonnes"
                                    attributes:@{
                                                 NSForegroundColorAttributeName: color,
                                                 NSFontAttributeName : [UIFont fontWithName:@"Arial" size:size]
                                                 }
     ];
    cell.pieces_tf.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:@"Quantity in Pieces"
                                    attributes:@{
                                                 NSForegroundColorAttributeName: color,
                                                 NSFontAttributeName : [UIFont fontWithName:@"Arial" size:size]
                                                 }
     ];
    
    cell.tonnes_tf.minimumFontSize = 10;
    cell.pieces_tf.minimumFontSize = 10;
    NSString *Home_tag=[NSString stringWithFormat:@"1%ld",(long)indexPath.row];
    int Home_tag_int=[Home_tag intValue];
    NSString *Away_tag=[NSString stringWithFormat:@"2%ld",(long)indexPath.row];
    int Away_tag_int=[Away_tag intValue];
    
    cell.tonnes_tf.tag = Home_tag_int;
    cell.pieces_tf.tag = Away_tag_int;
    cell.Refresh_Btn.tag = (long)indexPath.row;

    [cell.Refresh_Btn addTarget:self action:@selector(Refresh_click:) forControlEvents:UIControlEventTouchUpInside];
    
    [[cell.Refresh_Btn layer] setBorderWidth:1.0f];
    [[cell.Refresh_Btn layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    [cell.remove addTarget:self action:@selector(remove_click:) forControlEvents:UIControlEventTouchUpInside];
    [cell.save addTarget:self action:@selector(save_click:) forControlEvents:UIControlEventTouchUpInside];
    [cell.more_sellers addTarget:self action:@selector(More_click:) forControlEvents:UIControlEventTouchUpInside];
    [cell.detail_view addTarget:self action:@selector(Details_Click:) forControlEvents:UIControlEventTouchUpInside];
    
        NSString *slrName=[NSString stringWithFormat:@"%@",[[Products_dict valueForKey:@"seller"]objectAtIndex:indexPath.row]];
        if ([slrName isEqualToString:@""]||[slrName isEqualToString:@"<null>"]||[slrName isEqualToString:@"(null)"])
        {
            cell.seller_name.text = @"";
            cell.Refresh_view.hidden=NO;
        }
        else{
            cell.seller_name.text = slrName;
            cell.Refresh_view.hidden=YES;
        }

    NSString *ton_price_str=[NSString stringWithFormat:@"Rs. %@",[[Products_dict valueForKey:@"tonprice"]objectAtIndex:indexPath.row]];
        
        
    NSString *Discount_str=[NSString stringWithFormat:@"%@",[[Products_dict valueForKey:@"discount"]objectAtIndex:indexPath.row]];
    NSString *tonnes;
     if (T_P_Ary.count>indexPath.row)
   tonnes=[[T_P_Ary valueForKey:@"tonnes"]objectAtIndex:indexPath.row];
    cell.price_ton.text=ton_price_str;   ////
    cell.tonnes_tf.text=tonnes;
    if (T_P_Ary.count>indexPath.row)
    cell.pieces_tf.text=[NSString stringWithFormat:@"%@",[[T_P_Ary valueForKey:@"pieces"]objectAtIndex:indexPath.row]];
        if ([Discount_str isEqualToString:@"0"]) {
            cell.dicountValueHeight.constant = 0;
            cell.discountLabelHeight.constant = 0;
        }
    cell.Discount.text=Discount_str;

    float ton_price_int=[ton_price_str floatValue];
    float tons_int=[tonnes floatValue];
    float total_ton_price=ton_price_int*tons_int;

        NSString * tg = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
        for (int i = 0; i<= listArr.count; i++)
        {
            if(listArr.count == 0)
            {
                [cell.tonesChangeBtn setImage:[UIImage imageNamed:@"edit"] forState:UIControlStateNormal];
              //  [cell.piecesChangeBTn setImage:[UIImage imageNamed:@"editB.png"] forState:UIControlStateNormal];
                NSString * price_str1 = [NSString stringWithFormat:@"Rs %@",[[Products_dict valueForKey:@"price"]objectAtIndex:indexPath.row]];
                cell.total_price.text=price_str1;////
                break;
            }
            else
            {
                if ([[listArr objectAtIndex:i] isEqualToString:tg]) {
                    [cell.tonesChangeBtn setImage:currentImage forState:UIControlStateNormal];
                //    [cell.piecesChangeBTn setImage:currentImage forState:UIControlStateNormal];
                    cell.total_price.text=[NSString stringWithFormat:@"Rs %@",price_str];
                    totalPriceChange = YES;
                    [listArr removeObjectAtIndex:i];
                    break;
                }
                else
                {
                    [cell.tonesChangeBtn setImage:[UIImage imageNamed:@"edit"] forState:UIControlStateNormal];
                  //  [cell.piecesChangeBTn setImage:[UIImage imageNamed:@"editB.png"] forState:UIControlStateNormal];
                   NSString * price_str1 = [NSString stringWithFormat:@"Rs %@",[[Products_dict valueForKey:@"price"]objectAtIndex:indexPath.row]];
                    cell.total_price.text=price_str1;
                    break;
                }
            }
        }
        for (int i = 0; i<= pieceArr.count; i++) {
            if(pieceArr.count == 0)
            {
               // [cell.tonesChangeBtn setImage:[UIImage imageNamed:@"editB.png"] forState:UIControlStateNormal];
                [cell.piecesChangeBTn setImage:[UIImage imageNamed:@"edit"] forState:UIControlStateNormal];
                
               NSString * price_str1 = [NSString stringWithFormat:@"Rs %@",[[Products_dict valueForKey:@"price"]objectAtIndex:indexPath.row]];
                cell.total_price.text=price_str1;
                break;
            }
            else
            {
                if ([[pieceArr objectAtIndex:i] isEqualToString:tg]) {
                  //  [cell.tonesChangeBtn setImage:currentImage forState:UIControlStateNormal];
                    [cell.piecesChangeBTn setImage:currentImage forState:UIControlStateNormal];
                    cell.total_price.text=[NSString stringWithFormat:@"Rs %@",price_str];
                    [pieceArr removeObjectAtIndex:i];
                    break;
                }
                else
                {
                   // [cell.tonesChangeBtn setImage:[UIImage imageNamed:@"editB.png"] forState:UIControlStateNormal];
                    [cell.piecesChangeBTn setImage:[UIImage imageNamed:@"edit"] forState:UIControlStateNormal];
                    NSString * price_str1 = [NSString stringWithFormat:@"Rs %@",[[Products_dict valueForKey:@"price"]objectAtIndex:indexPath.row]];
                    cell.total_price.text=price_str1;
                    break;
                }
            }
        }
     if (T_P_Ary.count>indexPath.row)
    cell.name.text=[[T_P_Ary valueForKey:@"name"]objectAtIndex:indexPath.row];
    
    NSURL *imageURL = [NSURL URLWithString:[[T_P_Ary valueForKey:@"img"]objectAtIndex:indexPath.row]];
    [cell.image sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    
     [cell.image.layer setBorderColor: [[UIColor colorWithRed:213.0/255.0 green:216.0/255.0 blue:224.0/255.0 alpha:1] CGColor]];
    [cell.image.layer setBorderWidth: 2.0];
        
    return cell;
    }
    
    else
    {
        if (indexPath.row==Dict.count)
        {
            static NSString *simpleTableIdentifier = @"Shipping_lastCell";
            Shipping_lastCell *sCell = (Shipping_lastCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            
            if (sCell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"Shipping_lastCell" owner:self options:nil];
                sCell = (Shipping_lastCell *)[nib objectAtIndex:0];
            }
            if ([S_H_Price isEqual:[NSNull null]]||[S_H_Price isEqualToString:@""]||S_H_Price==nil)
            {
                sCell.S_H_Price.text=@"";
            }
            else
                sCell.S_H_Price.text= S_H_Price;
            
            
            if ([shipping_tax_Str isEqual:[NSNull null]]||[shipping_tax_Str isEqualToString:@""]||shipping_tax_Str==nil)
            {
                sCell.shipping_lbl.text=@"";
            }
            else
                sCell.shipping_lbl.text=shipping_tax_Str;
            
            return sCell;
        }
        else
        {
            static NSString *simpleTableIdentifier = @"Shipping_info_cell";
            Shipping_info_cell *cell1 = (Shipping_info_cell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            
            if (cell1 == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"Shipping_info_cell" owner:self options:nil];
                cell1 = (Shipping_info_cell *)[nib objectAtIndex:0];
            }
            NSString *seller_name=[[Dict valueForKey:@"seller_name"]objectAtIndex:indexPath.row];
            NSString *total_tons=[NSString stringWithFormat:@"%@",[[Dict valueForKey:@"total_tons"]objectAtIndex:indexPath.row]];
            NSString *shipping_price=[NSString stringWithFormat:@"%@",[[Dict valueForKey:@"shipping_price"]objectAtIndex:indexPath.row]];
            NSString *shipping_and_handling_price=[NSString stringWithFormat:@"%@",[[Dict valueForKey:@"shipping_and_handling_price"]objectAtIndex:indexPath.row]];
            NSString *handling_price=[NSString stringWithFormat:@"%@",[[Dict valueForKey:@"handling_price"]objectAtIndex:indexPath.row]];
             NSString *handling_taxamount=[NSString stringWithFormat:@"%@",[[Dict valueForKey:@"handling_tax_price"]objectAtIndex:indexPath.row]];
            
            NSString *shippinf_Tax=[NSString stringWithFormat:@"%@",[[Dict valueForKey:@"shipping_tax_price"]objectAtIndex:indexPath.row]];
            
            
            NSString *distance=[NSString stringWithFormat:@"%@",[[Dict valueForKey:@"distance"]objectAtIndex:indexPath.row]];
            
//            cell1.total_lbl.layer.borderColor=[[UIColor blackColor]CGColor];
//            cell1.total_lbl.layer.borderWidth=0.6;
            
            if ([seller_name isEqual:[NSNull null]]||[seller_name isEqualToString:@""]||seller_name==nil || [seller_name isEqualToString:@"<null>"])
            {
                cell1.sellername.text=@"";
            }
            else
                cell1.sellername.text=seller_name;
            
            if ([total_tons isEqual:[NSNull null]]||[total_tons isEqualToString:@""]||total_tons==nil)
            {
                cell1.tons.text=@"";
            }
            else
                cell1.tons.text=total_tons;
            
            if ([shipping_price isEqual:[NSNull null]]||[shipping_price isEqualToString:@""]||shipping_price==nil)
            {
                cell1.shipping.text=@"";
            }
            else
                cell1.shipping.text=shipping_price;
            
            
            if ([shippinf_Tax isEqual:[NSNull null]]||[shipping_price isEqualToString:@""]||shippinf_Tax==nil)
            {
                cell1.shipping_taxLbl.text=@"";
            }
            else
                cell1.shipping_taxLbl.text=shipping_price;
            
            if ([handling_price isEqual:[NSNull null]]||[handling_price isEqualToString:@""]||handling_price==nil)
            {
                cell1.handling.text=@"";
            }
            else
                cell1.handling.text=handling_price;
            
            if ([handling_taxamount isEqual:[NSNull null]]||[handling_taxamount isEqualToString:@""]||handling_taxamount==nil)
            {
                cell1.handlingtaxamount.text=@"";
            }
            else
                cell1.handlingtaxamount.text= handling_taxamount                              ;
            
            if ([distance isEqual:[NSNull null]]||[distance isEqualToString:@""]||distance==nil || [distance isEqualToString:@"<null>"])
            {
                cell1.distance.text=@"";
            }
            else
                cell1.distance.text=distance;
            
            if ([shipping_and_handling_price isEqual:[NSNull null]]||[shipping_and_handling_price isEqualToString:@""]||shipping_and_handling_price==nil || [shipping_and_handling_price  isEqual: @"<null>"])
            {
                cell1.subtotal.text=@"";
            }
            else
                cell1.subtotal.text=shipping_and_handling_price;
            
                shipping_tax_Str = shipping_and_handling_price;
            
            
            return cell1;
        }
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    if (section==0) {
        
        return 40;
    }
    else
        return 0.0;
    
}


-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (section == 0){
        
        static NSString *simpleTableIdentifier = @"ShippingHeaderCell";
        ShippingHeaderCell *cell1 = (ShippingHeaderCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell1 == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ShippingHeaderCell" owner:self options:nil];
            cell1 = (ShippingHeaderCell *)[nib objectAtIndex:0];
        }
        return cell1;
    }
    
    
    return self.view;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
         return 260;
    }
    
    else
    {
            if (indexPath.row==Dict.count)
            {
                return 196;
            }
            else
                return 310;
    }
}

-(void)tonesChangeBtnClick:(id)sender
{
    UIButton *button=(UIButton *) sender;
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:button.tag inSection:0];
   // Product_defined_cell *cell = (Product_defined_cell *)sender.superview.superview.superview;
   // totalPriceChange =  YES;
    tag = [sender tag];
    
    
    NSString * tg = [NSString stringWithFormat:@"%d",tag];
    for (int i = 0; i<= listArr.count; i++)
    {
        if(listArr.count == 0)
        {
           [listArr addObject:tg];
           // [pieceArr addObject:tg];
            break;
        }
        else
        {
        if ([[listArr objectAtIndex:i] isEqualToString:tg]) {
           // [listArr removeObjectAtIndex:i];
            break;
        }
        else
        {
            [listArr addObject:tg];
          //  [pieceArr addObject:tg];
            break;
        }
        }
    }
   cell = (Product_defined_cell *)[_list_view cellForRowAtIndexPath:indexpath];
    if([[cell.tonesChangeBtn imageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"edit"]])
    {
        currentImage = [UIImage imageNamed:@"save-icon.png"];
        [cell.tonesChangeBtn setImage:[UIImage imageNamed:@"save-icon.png"] forState:UIControlStateNormal];
        cell.tonnes_tf.enabled = YES;
    }
    else if([[cell.tonesChangeBtn imageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"save-icon.png"]])
    {
        [[T_P_Ary objectAtIndex:tag] setValue:cell.pieces_tf.text forKey:@"pieces"];
        [[T_P_Ary objectAtIndex:tag] setValue:cell.tonnes_tf.text forKey:@"tonnes"];
       // _selected_qty = cell.tonnes_tf.text;
        dot = @"yes";
        [self change_tonnes_service:cell.tonnes_tf.text :tag];
        
        
        currentImage = [UIImage imageNamed:@"edit"];
        [cell.tonesChangeBtn setImage:[UIImage imageNamed:@"edit"] forState:UIControlStateNormal];
        cell.tonnes_tf.enabled = NO;
    }
}

-(void)piecesChangeBtnClick:(UIButton*)btn
{
    tag = btn.tag;
   // totalPriceChange =  YES;
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:btn.tag inSection:0];
    NSString * tg = [NSString stringWithFormat:@"%d",tag];
    
    
    for (int i = 0; i<= pieceArr.count; i++) {
        if(pieceArr.count == 0)
        {
            [pieceArr addObject:tg];
            //[listArr addObject:tg];
            break;
        }
        else
        {
            if ([[pieceArr objectAtIndex:i] isEqualToString:tg]) {
                // [listArr removeObjectAtIndex:i];
                break;
            }
            else
            {
                [pieceArr addObject:tg];
               // [listArr addObject:tg];
                break;
            }
        }
    }
    cell = (Product_defined_cell *)[_list_view cellForRowAtIndexPath:indexpath];
    
    if([[cell.piecesChangeBTn imageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"edit"]])
    {
        currentImage = [UIImage imageNamed:@"save-icon.png"];
        [cell.piecesChangeBTn setImage:[UIImage imageNamed:@"save-icon.png"] forState:UIControlStateNormal];
        cell.pieces_tf.enabled = YES;
    }
    else if([[cell.piecesChangeBTn imageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"save-icon.png"]])
    {
        [[T_P_Ary objectAtIndex:tag] setValue:cell.pieces_tf.text forKey:@"pieces"];
        [[T_P_Ary objectAtIndex:tag] setValue:cell.tonnes_tf.text forKey:@"tonnes"];
        [self change_tonnes_service:cell.tonnes_tf.text :tag];
        currentImage = [UIImage imageNamed:@"edit"];
        [cell.piecesChangeBTn setImage:[UIImage imageNamed:@"edit"] forState:UIControlStateNormal];
        cell.pieces_tf.enabled = NO;
    }
}

- (IBAction)Model_click:(id)sender
{
   // NSLog(@"model clicked");
    //[self Getting_Grades:(id)0];
    int tab_height=Grades_ary.count*60;
    if (tab_height>self.view.frame.size.height-200)
    {
        tab_height=self.view.frame.size.height-200;
    }
    
                 searchView.frame = CGRectMake(_model_view.frame.origin.x,_model_view.frame.origin.y+180 ,_model_view.frame.size.width,tab_height+10);
                   [searchView updateInstaceFrame:CGRectMake(0, 0,searchView.frame.size.width , searchView.frame.size.height)];
                 searchItemsArray = [[NSMutableArray alloc]init];
                 [searchItemsArray  addObjectsFromArray:Grades_ary];
                 searchView.hidden =NO;
                 popup_tag=@"Grade";
                  noDataLabel.text =@"";
                 [searchView relaodSearchTable];
                 NSLog(@" Grades response %@",Grades_ary);
}

- (IBAction)Refresh_click:(id)sender
{
    int tag1=[sender tag];
    NSLog(@"Refresh click");
    Referesh_tag=@"YES";
    Refreshing_id=[NSString stringWithFormat:@"%@",[[Products_dict valueForKey:@"id"]objectAtIndex:tag1]];
    _brand_lbl.text=@"Select Brand";
    _grade_lbl.text=@"Select Grade";
    Refreshing_Indexpath = [NSIndexPath indexPathForRow:tag1 inSection:0];
    [self Alert:@"Re-Select the Brand & Grade."];
}

- (IBAction)Company_click:(id)sender
{
    NSLog(@"Company clicked");
    
    searchSelected = 1;
    [self Getting_Brands:(id)0];
}

-(void)Getting_Brands:(id)sender
{
//    if (searchItemsArray.count==0)
//    {
    NSString *url_form;
    if ([Referesh_tag isEqualToString:@"YES"])
    {
        url_form=[NSString stringWithFormat:@"getBrandsBasedOnLocation?ids=%@&pincode=%@&location=%@",Refreshing_id,_Pin_code,_Location];
    }
    else
    {
        url_form=[NSString stringWithFormat:@"getBrandsBasedOnLocation?ids=%@&pincode=%@&location=%@",_selected_ids,_Pin_code,_Location];
        
        
    }
     NSString* urlEncoded = [url_form stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString:urlEncoded requestNumber:Getting_Brands showProgress:YES withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             NSArray *Brands_ary=data;
             searchItemsArray = [[NSMutableArray alloc]init];
             
             NSLog(@"%@",Brands_ary);
             
             searchItemsArray = Brands_ary;
           //  [searchItemsArray  addObjectsFromArray:Brands_ary];
             
             
             _collectionBackView.hidden = NO;
             _Collection_PopUpView.hidden = NO;
             
             _collection_View.delegate = self;
             _collection_View.dataSource = self;
             
             
             if (searchItemsArray.count==0){
                 
                 self.noBrands_Lbl.hidden = NO;
                 self.brands_AvailbleLBL.hidden = YES;
                 self.collection_View.hidden = YES;
                 self.popUp_ProceedBtn.hidden = YES;

                 
                 
             }
             else{
                 self.noBrands_Lbl.hidden = YES;
                 self.brands_AvailbleLBL.hidden = NO;
                  self.collection_View.hidden = NO;
                 self.popUp_ProceedBtn.hidden = NO;
                 
             }
             
             
             
             
             
             
             [_collection_View reloadData];
             
             
             
//
//             int tab_height=Brands_ary.count*60;
//             if (tab_height>self.view.frame.size.height-200)
//             {
//                 tab_height=self.view.frame.size.height-200;
//             }
//
//             searchView.frame = CGRectMake(_company_btn.frame.origin.x+25,_company_btn.frame.origin.y+180 ,_company_btn.frame.size.width+50,tab_height+30);
//             [searchView updateInstaceFrame:CGRectMake(0, 0,searchView.frame.size.width , searchView.frame.size.height)];
//
//             searchItemsArray = [[NSMutableArray alloc]init];
//             [searchItemsArray  addObjectsFromArray:Brands_ary];
//             searchView.hidden =NO;
//             popup_tag=@"Brand";
//             [searchView relaodSearchTable];

             NSLog(@" Brands response %@",Brands_ary);
         }
         else
         {

         }
     }];
}

-(void)Getting_Grades:(id)sender
{
    BOOL checkValue;
    checkValue = false;
   self.popUpView.hidden = true;
    if (searchItemsArray.count!=0)
    {
        NSString *url_form;
        if ([Referesh_tag isEqualToString:@"YES"])
        {
            url_form=[NSString stringWithFormat:@"getGradesBasedOnBrand?ids=%@&pincode=%@&location=%@&brand=%@",Refreshing_id,_Pin_code,_Location,_brand_lbl.text];
        }
        else
        {
            if ([_brand_lbl.text isEqualToString:@"Select Brand"]) {
                checkValue = true;
            }
            else
            {
            url_form=[NSString stringWithFormat:@"getGradesBasedOnBrand?ids=%@&pincode=%@&location=%@&brand=%@",_selected_ids,_Pin_code,_Location,_brand_lbl.text];
            }
        }
        if (checkValue) {
            self.popUpView.hidden = false;
        }
        else
        {
     NSString* urlEncoded = [url_form stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString:urlEncoded requestNumber:Getting_Grades showProgress:YES withHandler:^(BOOL success, id data)
     {
         if (success)
         {
            Grades_ary=data;
             if (tag_type==YES)//construction
             {
                 if ([Grades_ary containsObject:@"FE500"])
                 {
                     _grade_lbl.text=@"FE500";
                 }
                 else
                 {
                 if (Grades_ary.count>0)
                 {
                     
                     _grade_lbl.text=[Grades_ary objectAtIndex:1];
                     
                 }
                 else
                     _grade_lbl.text=[Grades_ary objectAtIndex:0];
                 }
                 dot = @"no";
                 [self Getting_Define_Products];
             }
             else
             {
                 
             }
            

         }
         else
         {
//             UIAlertController * alert=   [UIAlertController
//                                           alertControllerWithTitle:@"Alert"
//                                           message:[NSString stringWithFormat:@"%@",[data valueForKey:@"error_message"]]
//                                           preferredStyle:UIAlertControllerStyleAlert];
//             
//             UIAlertAction* ok = [UIAlertAction
//                                  actionWithTitle:@"OK"
//                                  style:UIAlertActionStyleDefault
//                                  handler:^(UIAlertAction * action)
//                                  {
//                                      [alert dismissViewControllerAnimated:YES completion:nil];
//                                  }];
//             [alert addAction:ok];
//             [self presentViewController:alert animated:YES completion:nil];
         }
     }];
    }
    }
}

-(void)Getting_Define_Products
{
    if (T_P_Ary.count==0)
    {
        
    }
    else
    {
    NSMutableArray *ary=[[NSMutableArray alloc]init];
    for (int i=0; i<T_P_Ary.count; i++)
    {
        NSString *d=[[T_P_Ary valueForKey:@"tonnes"]objectAtIndex:i];
        [ary addObject:d];
    }
    _selected_qty = [ary componentsJoinedByString:@"_"];
    }
    
    NSString *url_form;
    if ([Referesh_tag isEqualToString:@"YES"])
    {
        url_form=[NSString stringWithFormat:@"getDefineProductPrices?ids=%@&pincode=%@&qty=%@&location=%@&brand=%@&grade=%@",Refreshing_id,_Pin_code,_selected_qty,_Location,_brand_lbl.text,_grade_lbl.text];
    }
    else
    {
        url_form=[NSString stringWithFormat:@"getDefineProductPrices?ids=%@&pincode=%@&qty=%@&location=%@&brand=%@&grade=%@",_selected_ids,_Pin_code,_selected_qty,_Location,_brand_lbl.text,_grade_lbl.text];
    }
    
    NSString* urlEncoded = [url_form stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString:urlEncoded requestNumber:Getting_Def_Product showProgress:YES withHandler:^(BOOL success, id data)
     {
         if (success)
         {
           NSArray *Products_ary=data;
             
             NSLog(@" Grades response %@",Products_dict);
             
             if ([Referesh_tag isEqualToString:@"YES"])
             {
                 [Products_dict replaceObjectAtIndex:Refreshing_Indexpath.row withObject:[Products_ary objectAtIndex:0]];
                 
                 [_list_view beginUpdates];
                 [_list_view reloadRowsAtIndexPaths:[NSArray arrayWithObjects:Refreshing_Indexpath, nil] withRowAnimation:UITableViewRowAnimationNone];
                 [_list_view endUpdates];
                 
                  [self getChangeTonProductPrices_ServiceCall:_selected_ids :_selected_childs :_selected_qty ];
                  [self shipping_info_service];
                 
                 
                 
                 
             }
             else
             {
                 [Products_dict removeAllObjects];
                 for (int d=0; d<Products_ary.count; d++)
                 {
                     [Products_dict addObject:[Products_ary objectAtIndex:d]];
                 }
                 T_P_Ary=[_tonne_piece_ary mutableCopy];
             [self shipping_info_service];
                 [_list_view reloadData];
             }
             if ([dot isEqualToString:@"yes"]) {
                 
                  [self change_tonnes_service:cell.tonnes_tf.text :tag];
                 dot = @"no";
             }
             
             Referesh_tag=@"NO";
         }
         
         else
         {

         }
     }];
    
    
}

- (IBAction)Compare_click:(id)sender
{
    if ([_brand_lbl.text isEqualToString:@"Select Brand"]||[_grade_lbl.text isEqualToString:@"Select Grade"])
    {
        [self Alert:@"Select Brand and Grade"];
    }
    else
    {
        NSLog(@"Compare clicked");
        CompareViewController *newView = [[CompareViewController alloc]init];
        if ([slcdID_Compare isEqual:[NSNull null]]||[slcdID_Compare isEqualToString:@"<nil>"]||slcdID_Compare == nil||[slcdID_Compare isEqualToString:@""])
        {
            newView.selected_ids=_selected_ids;
        }
        else
        {
            newView.selected_ids=slcdID_Compare;//_selected_ids;
        }
        
        if (T_P_Ary.count==0)
        {
            
        }
        else
        {
            NSMutableArray *ary=[[NSMutableArray alloc]init];
            for (int i=0; i<T_P_Ary.count; i++)
            {
                NSString *d=[[T_P_Ary valueForKey:@"tonnes"]objectAtIndex:i];
                [ary addObject:d];
            }
            _selected_qty = [ary componentsJoinedByString:@"_"];
        }
        
        NSMutableArray *ary=[[NSMutableArray alloc]init];
        for (int i=0; i<Products_dict.count; i++)
        {
            NSString *d=[[Products_dict valueForKey:@"child_id"]objectAtIndex:i];
            [ary addObject:d];
        }
        _selected_childs = [ary componentsJoinedByString:@"_"];
        
        
        newView.product_data = Products_dict;
        newView.T_P_Ary=T_P_Ary;
        newView.Location=_Location;
        newView.selected_brand=_brand_lbl.text;
        newView.selected_grade=_grade_lbl.text;
        newView.sel_ton=_selected_qty;
        newView.sel_childs = _selected_childs;
        newView.selectedPincode = _pincodeLabel.text;
        [self.navigationController pushViewController:newView animated:YES];
    }
}

- (IBAction)Details_Click:(id)sender
{
    
    NSLog(@"details clicked %ld",(long)[sender tag]);
    int tag=[sender tag];
    if ([[[Products_dict valueForKey:@"seller"]objectAtIndex:tag] isEqualToString:@""]) {
        ALERT_DIALOG(@"Alert", @"Details Not Found");
    }
    else
    {
    More_Details *newView = [[More_Details alloc]init];
    newView.PINCODE=_Pin_code;
    newView.GRADE=_grade_lbl.text;
    newView.BRAND=_brand_lbl.text;
    newView.location=_Location;
    newView.ID=[[Products_dict valueForKey:@"id"]objectAtIndex:tag];
    newView.price_str=[[Products_dict valueForKey:@"price"]objectAtIndex:tag];
    newView.childId = [[Products_dict valueForKey:@"child_id"]objectAtIndex:tag];

    [self.navigationController pushViewController:newView animated:YES];
    }
}

- (IBAction)More_click:(id)sender
{
    NSLog(@"more clicked%ld",(long)[sender tag]);
    More_seller_tag=[sender tag];
    if ([[[Products_dict valueForKey:@"seller"]objectAtIndex:More_seller_tag] isEqualToString:@""]) {
        ALERT_DIALOG(@"Alert", @"No Sellars Found");
    }
    else
    {
    More_Sellers *newView = [[More_Sellers alloc]init];
    newView.delegate =self;
    newView.name=[[T_P_Ary valueForKey:@"name"]objectAtIndex:More_seller_tag];
    newView.grade=_grade_lbl.text;
    newView.brand=_brand_lbl.text;
    newView.Location=_Location;
    newView.Pin_code=_Pin_code;
    newView.tag_int=More_seller_tag;
    newView.selected_ids=_selected_ids;
    newView.selected_qty=[[T_P_Ary valueForKey:@"tonnes"]objectAtIndex:More_seller_tag];;
    newView.products_ary=Products_dict;
    newView.price=[[Products_dict valueForKey:@"tonprice"]objectAtIndex:More_seller_tag];
    newView.sel_name=[[Products_dict valueForKey:@"seller"]objectAtIndex:More_seller_tag];

    newView.tonne_piece_ary=T_P_Ary;
    newView.img_url=[[T_P_Ary valueForKey:@"img"]objectAtIndex:More_seller_tag];
    newView.ID=[[Products_dict valueForKey:@"id"]objectAtIndex:More_seller_tag];
    [self.navigationController pushViewController:newView animated:YES];
    }
}

- (IBAction)save_click:(id)sender
{
    /*
    NSLog(@"save clicked%ld",(long)[sender tag]);
    int tag=[sender tag];
    
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:tag inSection:0];
    Product_defined_cell *tappedCell = (Product_defined_cell *)[_list_view cellForRowAtIndexPath:indexpath];
    NSString *pieces=tappedCell.pieces_tf.text;
    NSString *tonnes=tappedCell.tonnes_tf.text;

    [[T_P_Ary objectAtIndex:tag] setValue:pieces forKey:@"pieces"];
    [[T_P_Ary objectAtIndex:tag] setValue:tonnes forKey:@"tonnes"];
     */
}

- (IBAction)remove_click:(id)sender
{
    NSLog(@"remove clicked%ld",(long)[sender tag]);
    int tag=[sender tag];
    
    NSString *idTodelete = [NSString stringWithFormat:@"%@",[[Products_dict objectAtIndex:tag] valueForKey:@"id"]];
    NSString *remove_ = [NSString stringWithFormat:@"_%@",idTodelete];
    
    [Products_dict removeObjectAtIndex:tag];
    [T_P_Ary removeObjectAtIndex:tag];
    [_tonne_piece_ary removeObjectAtIndex:tag];
    
    NSMutableArray *sel_ids=[[NSMutableArray alloc]init];
    sel_ids=[[_selected_ids componentsSeparatedByString:@"_"]mutableCopy];
    [sel_ids removeObjectAtIndex:tag];
    _selected_ids = [sel_ids componentsJoinedByString:@"_"];

    NSMutableArray *sel_qty=[[NSMutableArray alloc]init];
    sel_qty=[[_selected_qty componentsSeparatedByString:@"_"]mutableCopy];
    [sel_qty removeObjectAtIndex:tag];
    _selected_qty = [sel_qty componentsJoinedByString:@"_"];
    
    
    NSMutableArray *sel_childs=[[NSMutableArray alloc]init];
    sel_childs=[[_selected_childs componentsSeparatedByString:@"_"]mutableCopy];
    [sel_childs removeObjectAtIndex:tag];
    _selected_childs = [sel_qty componentsJoinedByString:@"_"];
    
    
   // [_list_view reloadData];
  //sai
    slcdID_Compare = _selected_ids;
    if ([slcdID_Compare containsString:remove_])
    {
        slcdID_Compare = [slcdID_Compare stringByReplacingOccurrencesOfString:remove_
                                                                   withString:@""];
    }
   else if ([slcdID_Compare containsString:idTodelete])
   {
        slcdID_Compare = [slcdID_Compare stringByReplacingOccurrencesOfString:idTodelete
                                                                   withString:@""];
    }
   dot = @"no";
    if (_tonne_piece_ary.count==0)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    [self Getting_Define_Products];
}

-(void)Add_To_Cart
{
    [Cart_Details_ary removeAllObjects];
    Cart_Details_ary=[[NSMutableArray alloc]init];
    BOOL isCheck =NO;
    for (int p=0; p<T_P_Ary.count; p++)
    {
        NSMutableDictionary *post_dict=[[NSMutableDictionary alloc]init];
        
        NSString  *Tonnes_id=[[T_P_Ary valueForKey:@"tonnes"]objectAtIndex:p];
        NSString  *Pieces_id=[[T_P_Ary valueForKey:@"pieces"]objectAtIndex:p];
        NSString  *Product_id=[[Products_dict valueForKey:@"id"]objectAtIndex:p];
        NSString  *Child_id=[[Products_dict valueForKey:@"child_id"]objectAtIndex:p];
        NSString  *tonePrice = [[Products_dict valueForKey:@"tonprice"]objectAtIndex:p];
        
        [post_dict setObject:Product_id forKey:@"product_id"];
        [post_dict setObject:Child_id forKey:@"child_id"];
        [post_dict setObject:Tonnes_id forKey:@"ton"];
        [post_dict setObject:Pieces_id forKey:@"piece"];
        [post_dict setObject:tonePrice forKey:@"price"];
        
        
        [Cart_Details_ary addObject:post_dict];
        
        if (Tonnes_id.length==0 || Pieces_id.length==0) {
            isCheck =YES;
            break;
        }
    }
    
    NSMutableDictionary *Add_To_Cart_Dict=[[NSMutableDictionary alloc]init];
    [Add_To_Cart_Dict setObject:Cart_Details_ary forKey:@"details"];
    if ([user_id isEqualToString:@""]||[user_id isEqual:[NSNull null]]||[user_id isEqualToString:@"<nil>"]||user_id == nil||[user_id isEqualToString:@"0"])
    {
        [Add_To_Cart_Dict setObject:@"0" forKey:@"customer_id"];
    }
    else
    [Add_To_Cart_Dict setObject:user_id forKey:@"customer_id"];
    [Add_To_Cart_Dict setObject:_Pin_code forKey:@"pincode"];
    [Add_To_Cart_Dict setObject:_Location forKey:@"location"];
    NSLog(@"Add to cart Dict %@",Add_To_Cart_Dict);
    if (!isCheck)
    {
        if ([user_id isEqualToString:@""]||[user_id isEqual:[NSNull null]]||[user_id isEqualToString:@"<nil>"]||user_id == nil||[user_id isEqualToString:@"0"])
        {
            [user_data setValue:self.pincode_default forKey:@"pincode"];
            [user_data setValue:self.city_default forKey:@"city"];
            [user_data synchronize];
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            NewLoginVC *myNewVC = (NewLoginVC *)[storyboard instantiateViewControllerWithIdentifier:@"NewLoginVC"];
            myNewVC.cart_dict=Add_To_Cart_Dict;
             myNewVC.From=@"define";
            
            [self.navigationController pushViewController:myNewVC animated:YES];
        }
        else
        {
            [user_data setValue:self.pincode_default forKey:@"pincode"];
            [user_data setValue:self.city_default forKey:@"city"];
            [user_data synchronize];
            
        [self service:Add_To_Cart_Dict];
            
        activityView.frame = [UIScreen mainScreen].bounds;
        [self.view addSubview:activityView];
            
            
        }
    }
    else
    {
        ALERT_DIALOG(@"Alert", @"Please enter tonns or pieces to continue");
       // ALERT_DIALOG(@"Alert", @"Quantity 0 not allowed");
    }
  
}

-(void)service:(NSDictionary *)parameters
{
    
     _HUD.hidden =NO;
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    manager.responseSerializer =responseSerializer;
    NSString  *final_price;

    for (int i = 0; i<Products_dict.count; i++) {
       
         final_price=[[Products_dict valueForKey:@"tonprice"]objectAtIndex:i];
            
        }
    
       // [parameters setValue:final_price forKey:@"price"];
    
   
    NSString *urlString;
    
    urlString= [NSString stringWithFormat:@"%@addProductsToCart",MAIN_Url];
    NSError *error;
   
    NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:&error];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
   // NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];

    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:url];
    
    [request addValue:@"text/html" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"text/html" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:jsonData2];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        if (!error)
        {
           // [activityView removeFromSuperview];
            //_HUD.hidden =YES;
            NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&error];
            NSLog(@"%@",json);
            NSString *response=[NSString stringWithFormat:@"%@",[json valueForKey:@"response"]];
            
            if ([response isEqualToString:@"1"])
            {
                //Background Thread
                     // _HUD.hidden =YES;
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        //Run UI Updates
                      //  [_HUD removeFromSuperview];

                   //_HUD.hidden =YES;
                        [activityView removeFromSuperview];

                My_Cart *define = [[My_Cart alloc]initWithNibName:@"My_Cart" bundle:nil];
                [self.navigationController pushViewController:define animated:YES];
                        
                    });
                
            }
            else
            {
                
                
                
              
                                   dispatch_async(dispatch_get_main_queue(), ^(void){
                                       
                                       
                                        [activityView removeFromSuperview];
                                   });
                
             //   [self Alert:@"Something went wrong"];
                NSString *str = [NSString stringWithFormat:@"%@",[json valueForKey:@"msg"]];
//                [self Alert:str];
                ALERT_DIALOG(@"Alert", str);
               // NSLog(@" Something went wrong");
            }
        }
        else
        {
           // _HUD.hidden =YES;
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
                           {
                               //Background Thread
                               // _HUD.hidden =YES;
                               dispatch_async(dispatch_get_main_queue(), ^(void){
                                   
                                   
                                   
                                      [activityView removeFromSuperview];
                               });
                           });
            NSString *str =[NSString stringWithFormat:@"%@",error];
            
            ALERT_DIALOG(@"Alert",str);
            
            
             //[self Alert:[NSString stringWithFormat:@"%@",error]];
//            NSLog(@"Error: %@", error);
        }
          
    }];
    
    [postDataTask resume];

}
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
didCompleteWithError:(NSError *)error
{
    if(error == nil)
    {
        NSLog(@"Download is Succesfull");
    }
    else
        NSLog(@"Error %@",[error userInfo]);
}

-(void)viewWillDisappear:(BOOL)animated
{
    _HUD.hidden =YES;
}

- (IBAction)continue_click:(id)sender
{
    BOOL checkValue = false;
    if ([_brand_lbl.text isEqualToString:@"Select Brand"]||[_grade_lbl.text isEqualToString:@"Select Grade"])
    {
        [self Alert:@"Select Brand and Grade"];
    }
    else
    {
    //[self shipping_info_service];
    
    if (Products_dict.count>0)
    {
        for (int i = 0; i<Products_dict.count; i++) {
            if ([[[Products_dict valueForKey:@"seller"]objectAtIndex:i] isEqualToString:@""]) {
                checkValue = true;
            }
        }
        if (checkValue) {
            ALERT_DIALOG(@"Alert", @"Please change Sellar");
        }
        else
        [self Add_To_Cart];
    }
else
    [self Alert:@"Cart should not be empty!"];
    }
}

-(void)shipping_info_service
{
    [Cart_Details_ary removeAllObjects];
    Cart_Details_ary=[[NSMutableArray alloc]init];
    
    for (int p=0; p<T_P_Ary.count; p++)
    {
        NSMutableDictionary *post_dict=[[NSMutableDictionary alloc]init];
        
        NSString  *Tonnes_id=[[T_P_Ary valueForKey:@"tonnes"]objectAtIndex:p];
        NSString  *Pieces_id=[[T_P_Ary valueForKey:@"pieces"]objectAtIndex:p];
        NSString  *Product_id=[[Products_dict valueForKey:@"id"]objectAtIndex:p];
        NSString  *Child_id=[[Products_dict valueForKey:@"child_id"]objectAtIndex:p];
        NSString  *final_price=[[Products_dict valueForKey:@"tonprice"]objectAtIndex:p];
        
        [post_dict setObject:Product_id forKey:@"product_id"];
        [post_dict setObject:Child_id forKey:@"child_id"];
        [post_dict setObject:Tonnes_id forKey:@"ton"];
        [post_dict setObject:Pieces_id forKey:@"piece"];
        [post_dict setObject:final_price forKey:@"price"];
        
        [Cart_Details_ary addObject:post_dict];
    }
    
    NSMutableDictionary *Add_To_Cart_Dict=[[NSMutableDictionary alloc]init];
    [Add_To_Cart_Dict setObject:Cart_Details_ary forKey:@"details"];
   // [Add_To_Cart_Dict setObject:_selected_childs forKey:@"childs"];
    [Add_To_Cart_Dict setObject:_Pin_code forKey:@"pincode"];
    [Add_To_Cart_Dict setObject:_Location forKey:@"location"];
   // NSLog(@"Add to cart Dict %@",Add_To_Cart_Dict);

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    manager.responseSerializer =responseSerializer;
    NSString *urlString;
    urlString= [NSString stringWithFormat:@"%@getshippig",MAIN_Url];
    NSError *error;
    
    NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:Add_To_Cart_Dict options:0 error:&error];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:url];
    
    [request addValue:@"text/html" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"text/html" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:jsonData2];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                              if (!error)
                                              {
                                                  _HUD.hidden =YES;
                                                 // NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                                  
                                                  NSDictionary* json_dict = [NSJSONSerialization JSONObjectWithData:data
                                                                                                       options:kNilOptions
                                                                                                         error:&error];
                                                  NSLog(@"%@",json_dict);
                                                  
                                                  
            if (json_dict.count>0)
            {
//                if ([user_id isEqualToString:@""]||[user_id isEqual:[NSNull null]]||[user_id isEqualToString:@"<nil>"]||user_id == nil||[user_id isEqualToString:@"0"])
//                {
//                    
//                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//                    LoginViewController *myNewVC = (LoginViewController *)[storyboard instantiateViewControllerWithIdentifier:@"LoginViewControllerID"];
//                    [self.navigationController pushViewController:myNewVC animated:YES];
//                    
//                  
//                   
////                }
//                else
//                {
                
                    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
                                   {
                                       //Background Thread
                                       _HUD.hidden =YES;
                                       dispatch_async(dispatch_get_main_queue(), ^(void){
                                           //Run UI Updates
                                           [_HUD removeFromSuperview];
                                           
                                           _HUD.hidden =YES;
                                           
//                                           Shipping_Info *define = [[Shipping_Info alloc]initWithNibName:@"Shipping_Info" bundle:nil];
//                                           define.json_dict=json_dict;
//                                           
//                                           [self.navigationController pushViewController:define animated:YES];
                                           
                                           Dict=[[NSMutableDictionary alloc]init];
                                           Dict=[[json_dict valueForKey:@"seller_info"]mutableCopy];
                                           NSLog(@"local dictionary %@",Dict);
                                           
                                           S_H_Price=[NSString stringWithFormat:@"%@",[[json_dict valueForKey:@"grand_total"]mutableCopy]];
                                           shipping_tax_Str=[NSString stringWithFormat:@"%@",[[json_dict valueForKey:@"shipping_and_handling_price"]mutableCopy]];
                                           
                                           
                                         [_list_view reloadData];
                                           
                                       });
                                   });
                    
             //   }
                   
                
            }
            else
            {
                    [self Alert:@"Something went wrong"];
            }
                                              }
                                              else
                                              {
                                                  _HUD.hidden =YES;
                                                  [self Alert:[NSString stringWithFormat:@"%@",error]];
                                              }
  }];
    
    [postDataTask resume];

}

#pragma mark PopUp Menu





- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    //CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    [self.view endEditing:YES];
    searchView.hidden =YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    
    
    if(textField.tag != 252){
        
    
    newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSLog(@"enterd text %@",newString);
    
    NSString *Home_tag_str=[NSString stringWithFormat:@"%ld",(long)textField.tag];
    
    NSString *TF_check = [NSString stringWithFormat:@"%c", [Home_tag_str characterAtIndex:0]];
    NSString *Tag = [Home_tag_str substringFromIndex:1];
    row=[Tag intValue];
    //   NSLog(@"tag %d",row);
    
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:row inSection:0];
    Product_defined_cell *tappedCell = (Product_defined_cell *)[_list_view cellForRowAtIndexPath:indexpath];
    
    NSString *pieces_per_tonne=[pieces_per_ton_ary objectAtIndex:row];
    int pieces_per_tonne_int=[pieces_per_tonne intValue];
    if(![string isEqualToString:@"0"] || textField.text.length>0  || [string isEqualToString:@""])
    {
        
        if ([TF_check isEqualToString:@"1"])//tonnes
        {
            NSArray *sep = [newString componentsSeparatedByString:@"."];
            //        NSString *lastStrng =[NSString stringWithFormat:@"%hu",[newString characterAtIndex:[newString length] - 1]];
            if (sep.count == 3) {
                return false;
            }
            if([sep count] >= 2)
            {
                NSString *sepStr=[NSString stringWithFormat:@"%@",[sep objectAtIndex:1]];
                if (sepStr.length>3)
                {
                    return !([sepStr length]>3);
                }
                
                float tonnes_int=[newString floatValue];
                int total_pieces_int=tonnes_int*pieces_per_tonne_int;
                NSString *total_pieces_str;
                if (total_pieces_int >0) {
                    
                    total_pieces_str=[NSString stringWithFormat:@"%d",total_pieces_int];
                    
                    if (total_pieces_str.length>6) {
                        NSRange range = NSMakeRange(0,6);
                        NSString *trimmedString=[total_pieces_str substringWithRange:range];
                        total_pieces_str =trimmedString;
                    }
                }
                else{
                    newString = @"";
                    total_pieces_str = @"";
                }
                tappedCell.pieces_tf.text=total_pieces_str;
                
            }
            else
            {
                float tonnes_int=[newString floatValue];
                int total_pieces_int=tonnes_int*pieces_per_tonne_int;
                NSString *total_pieces_str;
                if (total_pieces_int >0)
                {
                    total_pieces_str=[NSString stringWithFormat:@"%d",total_pieces_int];
                    
                    if (total_pieces_str.length>6) {
                        NSRange range = NSMakeRange(0,6);
                        NSString *trimmedString=[total_pieces_str substringWithRange:range];
                        total_pieces_str =trimmedString;
                    }
                }else{
                    newString = @"";
                    total_pieces_str = @"";
                }
                tappedCell.pieces_tf.text=total_pieces_str;;
                
                
            }
            
            
        }
        else if ([TF_check isEqualToString:@"2"])//pieces
        {
            float pieces_int=[newString floatValue];
            float total_tonnes_int=pieces_int/pieces_per_tonne_int;
            NSString *total_tonnes_str;
             NSArray *sep = [newString componentsSeparatedByString:@"."];
            if (sep.count == 2) {
                return false;
            }
            if (total_tonnes_int >0) {
                total_tonnes_str=[NSString stringWithFormat:@"%.3f",total_tonnes_int];
                if (total_tonnes_str.length>6) {
                    NSRange range = NSMakeRange(0,6);
                    NSString *trimmedString=[total_tonnes_str substringWithRange:range];
                    total_tonnes_str =trimmedString;
                    
                }
            }
            else
            {
                newString = @"";
                total_tonnes_str = @"";
            }
            tappedCell.tonnes_tf.text=total_tonnes_str;

            if (!string.length) return YES;  NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string]; NSString *expression = @"^([0-9]+)?(\\.([0-9]{1,2})?)?$"; NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:nil]; NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])]; if (numberOfMatches == 0) return NO;
            
        }
    }
    else
    {
        return NO;
    }
   
   }else{
        
    }
    
     return YES;
    
}

-(void)change_tonnes_service:(NSString *)tonnes :(int )tag_int
{
    NSString *child_id=[[Products_dict valueForKey:@"child_id"]objectAtIndex:tag_int];
    NSString *prod_id=[[Products_dict valueForKey:@"id"]objectAtIndex:tag_int];
    if ([user_id isEqualToString:@""]||[user_id isEqual:[NSNull null]]||[user_id isEqualToString:@"<nil>"]||user_id == nil||[user_id isEqualToString:@"0"])
    {
        user_id = @" ";
    }
    
    
    
    
    
    
    
    NSString *url_form=[NSString stringWithFormat:@"changetons?id=%@&child_id=%@&ton=%@&customer_id=%@&is_define=1&pincode=%@&location=%@&brand=%@&grade=%@",prod_id,child_id,tonnes,user_id,_Pin_code,_Location,_brand_lbl.text,_grade_lbl.text];
    
//    NSString *url_form=[NSString stringWithFormat:@"changetons?id=%@&child_id=%@&ton=%@&customer_id=%@&is_define=1",prod_id,child_id,tonnes,user_id];
    
    NSString* urlEncoded = [url_form stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString:urlEncoded requestNumber:Change_tons showProgress:YES withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             NSLog(@" response %@",data);
             if ([data count]>0)
             {
                NSDictionary *res_dict=[ data objectAtIndex:0];
                 NSString *status_str=[NSString stringWithFormat:@"%@",[res_dict valueForKey:@"status"]];
                 if ([status_str isEqualToString:@"0"])
                 {
                     [self Alert:[res_dict valueForKey:@"message"]];
                 }
                 else
                 {
             if (res_dict.count>0)
             {
                 NSString *status=[NSString stringWithFormat:@"%@",[res_dict valueForKey:@"status"]];
                 if ([status isEqualToString:@"0"]||[status isEqualToString:@"false"])
                 {
                     [self Alert:[res_dict valueForKey:@"message"]];
                 }
                 else
                 {
                 NSIndexPath *indexpath = [NSIndexPath indexPathForRow:tag_int inSection:0];
                 cell = (Product_defined_cell *)[_list_view cellForRowAtIndexPath:indexpath];
                 discount_str=[NSString stringWithFormat:@"%@",[res_dict valueForKey:@"discount"]];
                 price_str=[NSString stringWithFormat:@"%@",[res_dict valueForKey:@"price"]];
                     
                     for(int i=0 ; i<= Products_dict.count;i++)
                     {
                         if ([[[Products_dict valueForKey:@"id"]objectAtIndex:i] isEqualToString:[res_dict valueForKey:@"id"]]) {
                             [Products_dict replaceObjectAtIndex:i withObject:res_dict];
                             
                             
                             
                             
                             
                             break;
                         }
                     }
                 cell.Discount.text=discount_str;
                cell.total_price.text=price_str;
                     
                     [self getChangeTonProductPrices_ServiceCall:prod_id :_selected_childs :tonnes];
                     [self shipping_info_service];//shipping info
                }
             }
                 }
         }
         else
         {
         }
         }
     }];
}

- (void)keyboardWillHide:(NSNotification *)n
{
//    NSDictionary* userInfo = [n userInfo];
//
//    // get the size of the keyboard
//    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
//
//
//    // resize the scrollview
//    CGRect viewFrame = self.list_view.frame;
//    // I'm also subtracting a constant kTabBarHeight because my UIScrollView was offset by the UITabBar so really only the portion of the keyboard that is leftover pass the UITabBar is obscuring my UIScrollView.
//    viewFrame.size.height += (keyboardSize.height - 50);
//
//    [UIView beginAnimations:nil context:NULL];
//    [UIView setAnimationBeginsFromCurrentState:YES];
//    [self.list_view setFrame:viewFrame];
//    [UIView commitAnimations];
//
//    keyboardIsShown = NO;
    
   
   [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
}

- (void)keyboardWillShow:(NSNotification *)n
{
    // This is an ivar I'm using to ensure that we do not do the frame size adjustment on the `UIScrollView` if the keyboard is already shown.  This can happen if the user, after fixing editing a `UITextField`, scrolls the resized `UIScrollView` to another `UITextField` and attempts to edit the next `UITextField`.  If we were to resize the `UIScrollView` again, it would be disastrous.  NOTE: The keyboard notification will fire even when the keyboard is already shown.
//    if (keyboardIsShown)
//    {
//        return;
//    }
//
//    NSDictionary* userInfo = [n userInfo];
//
//    // get the size of the keyboard
//    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
//
//    // resize the noteView
//    CGRect viewFrame = self.list_view.frame;
//    // I'm also subtracting a constant kTabBarHeight because my UIScrollView was offset by the UITabBar so really only the portion of the keyboard that is leftover pass the UITabBar is obscuring my UIScrollView.
//    viewFrame.size.height -= (keyboardSize.height - 50);
//
//    [UIView beginAnimations:nil context:NULL];
//    [UIView setAnimationBeginsFromCurrentState:YES];
//    [self.list_view setFrame:viewFrame];
//    [UIView commitAnimations];
//    keyboardIsShown = YES;
    
    
    NSInteger height = [n.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    
    [self.view setFrame:CGRectMake(0,-110,self.view.frame.size.width,self.view.frame.size.height)];
    
}

-(void)getChangeTonProductPrices_ServiceCall:(NSString*)ids :(NSString*)childs :(NSString*)tones {
    

    NSMutableArray *ary=[[NSMutableArray alloc]init];
    for (int i=0; i<Products_dict.count; i++)
    {
        NSString *d=[[Products_dict valueForKey:@"child_id"]objectAtIndex:i];
        [ary addObject:d];
    }
    _selected_childs = [ary componentsJoinedByString:@"_"];
    
    
    NSMutableArray *ary1=[[NSMutableArray alloc]init];
    for (int i=0; i<T_P_Ary.count; i++)
    {
        NSString *d=[[T_P_Ary valueForKey:@"tonnes"]objectAtIndex:i];
        [ary1 addObject:d];
    }
    _selected_qty = [ary1 componentsJoinedByString:@"_"];
  
    
    
    NSDictionary *params = @{@"ids": _selected_ids,@"childs":_selected_childs,@"pincode":_Pin_code,@"location":_Location,@"brand":_brand_lbl.text,@"grade":_grade_lbl.text,@"qty":_selected_qty};
    
    [[STParsing sharedWebServiceHelper]requesting_POST_ServiceWithString1:@"getChangeTonProductPrices" parameters:params requestNumber:WS_Login showProgress:YES withHandler:^(BOOL success, id data)
     {
         
             if (success)
             {
                 NSLog(@"%@",data);
                 NSArray *res_dict=data;
                 NSString * price;
                 NSString * totalprice;
                 for (int i=0; i<res_dict.count; i++){
                     price  = res_dict[i][@"tonprice"];
                     totalprice = res_dict[i][@"price"];
                     
                     NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
                     NSDictionary *oldDict = (NSDictionary *)[Products_dict objectAtIndex:i];
                     [newDict addEntriesFromDictionary:oldDict];
                     [newDict setObject:price forKey:@"tonprice"];
                     [newDict setObject:totalprice forKey:@"price"];
                     [Products_dict replaceObjectAtIndex:i withObject:newDict];
                    
                   //[[Products_dict objectAtIndex:i] setValue:price forKey:@"tonprice"];
                     
                  
                 }
                 
                 
                 if (res_dict.count > 0 ){
                     price  = res_dict[0][@"child_id"];
                    
                 }
                 else{
                     price = @"";
                    
                 }
                 
                 if ([price  isEqual: @""] ||  [price isEqual:[NSNull null]] || [price isEqualToString:@"<null>"]){
                     self.outOfStock_View.hidden = NO;
                 }
                 else{
                     self.outOfStock_View.hidden = YES;
                 }
                // [self Getting_Define_Products];
               [self shipping_info_service];
                 
             }
             else
             {
                 [self Alert:@"Something went wrong"];
             }
         
     }];
    
}


-(void)Alert:(NSString *)Msg
{
    NSDictionary *options = @{kCRToastNotificationTypeKey:@(CRToastTypeNavigationBar),
                              
                              kCRToastTextKey : Msg,
                              
                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                              
                              kCRToastBackgroundColorKey : [UIColor colorWithRed:44.0/255.0 green:52.0/255.0 blue:75.0/255.0 alpha:1],
                              kCRToastTimeIntervalKey: @(2),
                              //                              kCRToastFontKey:[UIFont fontWithName:@"PT Sans Narrow" size:23],
                              kCRToastInteractionRespondersKey:@[[CRToastInteractionResponder interactionResponderWithInteractionType:CRToastInteractionTypeSwipeUp
                                                                  
                                                                                                                 automaticallyDismiss:YES
                                                                  
                                                                                                                                block:^(CRToastInteractionType interactionType){
                                                                                                                                    
                                                                                                                                    NSLog(@"Dismissed with %@ interaction", NSStringFromCRToastInteractionType(interactionType));
                                                                                                                                }]],
                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeGravity),
                              
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeGravity),
                              
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                              
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop)
                              
                              };
    
    [CRToastManager showNotificationWithOptions:options
     
                                completionBlock:^{
                                    
                                    NSLog(@"Completed");
                                    
                                }];
}

//-(void)Back_click
//{
//    
//}

//The event handling method
- (void)handleSingleTap1:(UITapGestureRecognizer *)recognizer
{
    CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    if ([user_id isEqualToString:@""]||[user_id isEqual:[NSNull null]]||[user_id isEqualToString:@"<nil>"]||user_id == nil||[user_id isEqualToString:@"0"])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        NewLoginVC *myNewVC = (NewLoginVC *)[storyboard instantiateViewControllerWithIdentifier:@"NewLoginVC"];
        [self.navigationController pushViewController:myNewVC animated:YES];
    }
    else//go to cart page
    {
        My_Cart *define = [[My_Cart alloc]init];
        [self.navigationController pushViewController:define animated:YES];
    }
    //Do stuff here...
}

- (IBAction)Cart_Click:(id)sender
{
    if ([user_id isEqualToString:@""]||[user_id isEqual:[NSNull null]]||[user_id isEqualToString:@"<nil>"]||user_id == nil||[user_id isEqualToString:@"0"]||[user_id isEqualToString:@"(null)"])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        NewLoginVC *myNewVC = (NewLoginVC *)[storyboard instantiateViewControllerWithIdentifier:@"NewLoginVC"];
        [self.navigationController pushViewController:myNewVC animated:YES];
    }
    else//go to cart page
    {
        My_Cart *define = [[My_Cart alloc]init];
        [self.navigationController pushViewController:define animated:YES];
    }
}
- (IBAction)back_Click:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)doneClicked:(id)sender
{
    NSLog(@"Done Clicked.");
    [self.view endEditing:YES];
}

- (IBAction)Shipping_info_click:(id)sender
{
    if ([_brand_lbl.text isEqualToString:@"Select Brand"]||[_grade_lbl.text isEqualToString:@"Select Grade"])
    {
        [self Alert:@"Select Brand and Grade"];
    }
    else
    {
//        [self shipping_info_service];
    }
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    
    return [searchItemsArray count];
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    ProductCollectionCell *cell = [_collection_View dequeueReusableCellWithReuseIdentifier:@"ProductCollectionCell" forIndexPath:indexPath];
   
    if (![[searchItemsArray [indexPath.row]valueForKey:@"image"] isKindOfClass:[NSNull class]]){
        NSString * url = [NSString stringWithFormat:@"%@media/%@",Banners_Url,[searchItemsArray [indexPath.row]valueForKey:@"image"]];
         NSURL *imageURL = [NSURL URLWithString:url];
         cell.brand_Image.hidden = NO;
        cell.brand_Lbl.hidden = YES;

        [cell.brand_Image sd_setImageWithURL:imageURL];
        
        
    }else{
        
        cell.brand_Image.hidden = YES;
        cell.brand_Lbl.hidden = NO;
        cell.brand_Lbl.text = [searchItemsArray [indexPath.row]valueForKey:@"name"];
        
    }
    cell.brand_NameLbl.text = [searchItemsArray [indexPath.row]valueForKey:@"name"];
//    
    if( count == indexPath.row){
       cell.image_check.hidden = NO;
       // cell.brand_Image.layer.borderColor = (__bridge CGColorRef _Nullable)([UIColor colorWithRed:41/255.0f green:183/255.0f blue:232/255.0f alpha:1.0f]);
        
    }
    else{
        cell.image_check.hidden = YES;
      //  cell.brand_Image.layer.borderColor = (__bridge CGColorRef _Nullable)([UIColor lightTextColor]);
    }
    
    
    
   
    
   
    return cell;
    
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
   
    count = indexPath.row;
    _brand_lbl.text=[searchItemsArray [indexPath.row]valueForKey:@"name"];
    _grade_lbl.text=@"Select Grade";
    [self Getting_Grades:(id)0];
    
    [_collection_View reloadData];
    
    
}



-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger width = collectionView.frame.size.width-10;
    return CGSizeMake((width/2), width/4);
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 5, 0, 0); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 5;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 20;
}


- (IBAction)goToHomeScreenAction:(id)sender
{
    HomeViewController *con = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
    [self.navigationController pushViewController:con animated:YES];
    
    
}
- (IBAction)categories_Action:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CategoryVC *myNewVC = (CategoryVC *)[storyboard instantiateViewControllerWithIdentifier:@"CategoryVC"];
    [self.navigationController pushViewController:myNewVC animated:YES];
    
    
}



- (IBAction)closeAction:(UIButton *)sender {
    
    _collectionBackView.hidden = YES;
    _Collection_PopUpView.hidden = YES;
    
}

- (IBAction)popUp_ProceedAction:(UIButton *)sender {
    
    
    _collectionBackView.hidden = YES;
    _Collection_PopUpView.hidden = YES;
    
    
}
@end
