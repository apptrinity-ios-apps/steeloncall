//
//  PD_cell0.h
//  Steeloncall_Seller
//
//  Created by INDOBYTES on 09/01/17.
//  Copyright © 2017 sai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PD_cell0 : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *sub_total;
@property (strong, nonatomic) IBOutlet UILabel *cst;

@property (strong, nonatomic) IBOutlet UILabel *net_amount;
@property (strong, nonatomic) IBOutlet UILabel *sh;
@property (strong, nonatomic) IBOutlet UILabel *grand_total;
@property (strong, nonatomic) IBOutlet UIView *view_2;
@property (strong, nonatomic) IBOutlet UIView *view_1;
@property (strong, nonatomic) IBOutlet UILabel *vat;
@property (strong, nonatomic) IBOutlet UILabel *paid;
@property (strong, nonatomic) IBOutlet UILabel *payable;
@property (strong, nonatomic) IBOutlet UILabel *cst_lbl;
@property (strong, nonatomic) IBOutlet UILabel *vat_lbl;
@property (strong, nonatomic) IBOutlet UILabel *vatHeadingLbl;
@property (strong, nonatomic) IBOutlet UILabel *cstHeadingLbl;
@property (strong, nonatomic) IBOutlet UILabel *cgstValue;
@property (strong, nonatomic) IBOutlet UILabel *cgstLabel;
@property (strong, nonatomic) IBOutlet UILabel *sgstValue;
@property (strong, nonatomic) IBOutlet UILabel *sgstLabel;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cgstheightlbl;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cgstvalueheightLbl;
@property (strong, nonatomic) IBOutlet UILabel *handlingAmount;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *sgdtlabelHieght;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *sgstValueHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *igstLabelHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *igstValueHeight;
@property (strong, nonatomic) IBOutlet UILabel *igstValue;
@property (strong, nonatomic) IBOutlet UIButton *cancel_Btn;


@end
