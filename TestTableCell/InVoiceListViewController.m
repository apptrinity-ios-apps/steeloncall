//
//  InVoiceListViewController.m
//  SteelonCall
//
//  Created by INDOBYTES on 03/04/17.
//  Copyright © 2017 com.indobytes. All rights reserved.
//

#import "InVoiceListViewController.h"
#import "STParsing.h"
#import "Header_InvoiceCell.h"
#import "Seller_Price_Cell.h"
#import "Seller_Invoice_Cell.h"
#import "pdfViewViewController.h"
#import "Invoice_Header_Cell.h"

@interface InVoiceListViewController ()
{
    NSMutableArray *customerAddress;
     NSMutableArray *invoiceResponse;
    NSMutableArray *invoiceDetails;
    
     NSMutableString *address,*address2;
    NSMutableString *baddress,*baddress2;
}

@end

@implementation InVoiceListViewController
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
//self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:10.0/255.0 green:21.0/255.0 blue:80.0/255.0 alpha:1];    self.navigationController.navigationBar.translucent = NO;
//    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
//    UIImageView *navigationImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 199, 40)];
//    navigationImage.image=[UIImage imageNamed:@"logo.png"];
//    self.navigationItem.titleView=navigationImage;
//    
//    UIButton *cameraButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 2, 40.0f, 40.0f)];
//    UIImage *cameraImage = [UIImage imageNamed:@"Back"];
//    [cameraButton setBackgroundImage:cameraImage forState:UIControlStateNormal];
//    [cameraButton addTarget:self action:@selector(BackClick:) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem* cameraButtonItem = [[UIBarButtonItem alloc] initWithCustomView:cameraButton];
//    self.navigationItem.leftBarButtonItem = cameraButtonItem;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _invoiceTableview.dataSource = self;
    _invoiceTableview.delegate = self;
    address = [[NSMutableString alloc]init];
    address2  = [[NSMutableString alloc]init];
    baddress = [[NSMutableString alloc]init];
    baddress2  = [[NSMutableString alloc]init];
    //self.navigationController.navigationBar.hidden = YES;
    [self invoiceService];
    
    //http://stg.steeloncall.com/calculate/userservice/getOrderInvoices/?order_id=384
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (invoiceResponse.count == 0) {
         return 0;
    }
    else
       
    return [invoiceDetails count]+1;
   
   
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    if (invoiceResponse.count == 0) {

    
        return 0;
    }
    else
    {
       
        if ( section == 0)
        {
            return 1;
        }
        else
        {
            NSArray *count1 = [[invoiceDetails  objectAtIndex:section-1]valueForKey:@"product"];
            return [count1 count]+1;
        }
    }
       
 }



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        return 390;
    }
//    else if (indexPath.section == 2)
//    {
//        return 260;
//    }
    else{
        NSArray *count1 = [[invoiceDetails  objectAtIndex:indexPath.section-1]valueForKey:@"product"];
        
        if (indexPath.row == [count1 count]) {
            return 260;//single
        }
        else
            
        {
            return 315;//product
        }
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.automaticallyAdjustsScrollViewInsets = NO;
    tableView.allowsSelection = NO;
    
    if (indexPath.section ==0 )
{
    static NSString *simpleTableIdentifier = @"Header_InvoiceCell";
    Header_InvoiceCell  *cell = (Header_InvoiceCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"Header_InvoiceCell" owner:self options:nil];
        cell = (Header_InvoiceCell *)[nib objectAtIndex:0];
    }
    
   
    
    cell.headerOrderId.text = _orderStatus;
    
    cell.headerOrderDate.text = _orderDate;
    
    if (customerAddress.count>0) {
        
        NSArray *arr = [[customerAddress valueForKey:@"payment_method"] objectAtIndex:0];
        
         cell.paymentMethod_Lbl.text = [[arr valueForKey:@"payment_method"] objectAtIndex:0] ;
        
         NSArray *arr1 = [[customerAddress valueForKey:@"shipping"] objectAtIndex:0];
        
        cell.shipingMethodLbl.text = [[arr1 valueForKey:@"shipping_method"] objectAtIndex:0] ;
        
       //shippingAddress
       
        
        NSArray *shippingAddres = [[customerAddress valueForKey:@"shipping_address"] objectAtIndex:0];
        
        cell.shippNameLbl.text = [[shippingAddres valueForKey:@"name"] objectAtIndex:0];
        NSString *company =[NSString stringWithFormat:@"%@",[[shippingAddres valueForKey:@"company"] objectAtIndex:0]];
        if ([company isEqualToString:@"<null>"]||([company isEqual:[NSNull null] ]||[company isEqualToString:@"nil"]||[company isEqualToString:@""])) {
            company = @"";
        }
        cell.sCompany.text = company;
        NSString *street = [NSString stringWithFormat:@"%@, ",[[shippingAddres valueForKey:@"street"] objectAtIndex:0]];
        
      address =  [[address stringByAppendingString:street] mutableCopy];
        
        NSString *city = [NSString stringWithFormat:@"%@, ",[[shippingAddres valueForKey:@"city"] objectAtIndex:0]];
        
        address = [[address stringByAppendingString:city]mutableCopy];
        NSString *region = [NSString stringWithFormat:@"%@, ",[[shippingAddres valueForKey:@"region"] objectAtIndex:0]];
        
       address =  [[address stringByAppendingString:region] mutableCopy];
        
        cell.ShipAddressLbl.text = address;
        
        NSString *zipcode = [NSString stringWithFormat:@"%@, ",[[shippingAddres valueForKey:@"zipcode"] objectAtIndex:0]];
        
        address2 = [[address2 stringByAppendingString:zipcode] mutableCopy];
        
        NSString *country = [NSString stringWithFormat:@"%@, ",[[shippingAddres valueForKey:@"country"] objectAtIndex:0]];
        
       address2 =  [[address2 stringByAppendingString:country] mutableCopy];
        
        cell.shipAddress2Lbl.text = address2;
        
        address = [@"" mutableCopy];
        address2 = [@"" mutableCopy];
        cell.shipTinLbl.text = [NSString stringWithFormat:@"TIN : %@",[[shippingAddres valueForKey:@"tin"] objectAtIndex:0]];
        cell.shipCstLbl.text = [NSString stringWithFormat:@"CST : %@",[[shippingAddres valueForKey:@"cst"] objectAtIndex:0]];
        
        cell.shipPanLbl.text = [NSString stringWithFormat:@"PAN : %@",[[shippingAddres valueForKey:@"pan"] objectAtIndex:0]];
        cell.shipPhoneLbl.text = [NSString stringWithFormat:@"Mobile : %@",[[shippingAddres valueForKey:@"mobile"] objectAtIndex:0]];
        NSString * phn1 = [[shippingAddres valueForKey:@"phone"] objectAtIndex:0];
        if ([phn1 isEqual:[NSNull null]]) {
            cell.ShipTeleLbl.text = [NSString stringWithFormat:@"Phone : -"];
        }
        else
        {
           cell.ShipTeleLbl.text = [NSString stringWithFormat:@"Phone : %@",[[shippingAddres valueForKey:@"phone"] objectAtIndex:0]];
        }

        
        
        
        // billing
        
        NSArray *billingAddres = [[customerAddress valueForKey:@"billing_address"] objectAtIndex:0];
        
        cell.billpNameLbl.text = [[billingAddres valueForKey:@"name"] objectAtIndex:0];
        NSString *bcompany =[NSString stringWithFormat:@"%@",[[billingAddres valueForKey:@"company"] objectAtIndex:0]];
        if ([bcompany isEqualToString:@"<null>"]||([bcompany isEqual:[NSNull null] ]||[bcompany isEqualToString:@"nil"]||[bcompany isEqualToString:@""])) {
            bcompany = @"";
        }
        cell.bCompany.text = bcompany;
     //   cell.bCompany.text = [[billingAddres valueForKey:@"company"] objectAtIndex:0];
        NSString *bstreet = [NSString stringWithFormat:@"%@, ",[[billingAddres valueForKey:@"street"] objectAtIndex:0]];
        
        baddress =  [[baddress stringByAppendingString:bstreet] mutableCopy];
        
        NSString *bcity = [NSString stringWithFormat:@"%@, ",[[billingAddres valueForKey:@"city"] objectAtIndex:0]];
        
        baddress = [[baddress stringByAppendingString:bcity]mutableCopy];
        
        NSString *bregion = [NSString stringWithFormat:@"%@, ",[[billingAddres valueForKey:@"region"] objectAtIndex:0]];
        
        baddress =  [[baddress stringByAppendingString:bregion] mutableCopy];
        
        cell.billAddressLbl.text = baddress;
        
        NSString *bzipcode = [NSString stringWithFormat:@"%@, ",[[billingAddres valueForKey:@"zipcode"] objectAtIndex:0]];
        NSString *bcountry = [NSString stringWithFormat:@"%@",[[billingAddres valueForKey:@"country"] objectAtIndex:0]];
        baddress2 = [[baddress2 stringByAppendingString:bzipcode] mutableCopy];
        baddress2 = [[baddress2 stringByAppendingString:bcountry] mutableCopy];            
       
        
        cell.billAddress2Lbl.text = baddress2;
        
        baddress = [@"" mutableCopy];
        baddress2 = [@"" mutableCopy];
        
        cell.billTinLbl.text = [NSString stringWithFormat:@"TIN : %@",[[billingAddres valueForKey:@"tin"] objectAtIndex:0]];
        cell.billCstLbl.text = [NSString stringWithFormat:@"CST : %@",[[billingAddres valueForKey:@"cst"] objectAtIndex:0]];
        
        cell.billPanLbl.text = [NSString stringWithFormat:@"PAN : %@",[[billingAddres valueForKey:@"pan"] objectAtIndex:0]];
        cell.billPhoneLbl.text = [NSString stringWithFormat:@"Mobile : %@",[[billingAddres valueForKey:@"mobile"] objectAtIndex:0]];
        NSString * phn = [[billingAddres valueForKey:@"phone"] objectAtIndex:0];
        if ([phn isEqual:[NSNull null]]) {
            cell.billTeleLbl.text = [NSString stringWithFormat:@"Phone : -"];
        }
        else
        {
        cell.billTeleLbl.text = [NSString stringWithFormat:@"Phone : %@",[[billingAddres valueForKey:@"phone"] objectAtIndex:0]];
        }
        
    }
   
    

    return cell;
}
    else{
        
        NSArray *count1 = [[invoiceDetails  objectAtIndex:indexPath.section-1]valueForKey:@"product"];
        
       
        
        if (indexPath.row == [count1 count]) {
            
            static NSString *simpleTableIdentifier = @"Seller_Price_Cell";
            Seller_Price_Cell  *cell = (Seller_Price_Cell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"Seller_Price_Cell" owner:self options:nil];
                cell = (Seller_Price_Cell *)[nib objectAtIndex:0];
                
              
//            
            }
            cell.printInvoiceBtn.tag=indexPath.section-1;
            [cell.printInvoiceBtn addTarget:self action:@selector(downLoadPDf:) forControlEvents:UIControlEventTouchUpInside];
            
            NSArray *values =[invoiceDetails objectAtIndex:indexPath.section-1];
            
             cell.subTotalLbl.text =[NSString stringWithFormat:@"%@",[values  valueForKey:@"subtotal"]];
            
              cell.shippingHandlingLbl.text =[NSString stringWithFormat:@"%@",[values  valueForKey:@"shipping_amount"]];
            
            NSString *igst = [NSString stringWithFormat:@"%@",[values  valueForKey:@"igst_amount"]];

            
            if ([igst isEqualToString:@"0"]) {
                cell.sgstlabelHeight.constant = 26;
                cell.sgstValueHeight.constant = 26;
               cell.cstLbl.text =[NSString stringWithFormat:@"%@",[values  valueForKey:@"cgst_amount"]];
                cell.vatLbl.text =[NSString stringWithFormat:@"%@",[values  valueForKey:@"sgst_amount"]];
            }
            else
            {
                cell.sgstlabelHeight.constant = 0;
                cell.sgstValueHeight.constant = 0;
                cell.cgstHeadingLbl.text = @" IGST Tax Amount";
                cell.cstLbl.text =[NSString stringWithFormat:@"%@",[values  valueForKey:@"igst_amount"]];
            }
//            else
//            {
//                cell.cstConstrainHeading.constant = 21;
//                cell.cstConstrainValue.constant = 21;
            
//                cell.cstHeadingLbl.text =[NSString stringWithFormat:@"CST(@%@%%)",[values  valueForKey:@"cst_percent"]];
//                
               
                
            
                
           // }
          //  NSString *vat = [NSString stringWithFormat:@"%@",[values  valueForKey:@"vat_amount"]];
            
            
//            if ([vat isEqualToString:@"0"]) {
//
//                cell.vatHeadingLbl.hidden = YES;
//                cell.vatLbl.hidden = YES;
//            }
//            else
//            {
//                
//                cell.vatHeadingLbl.hidden = NO;
//                cell.vatLbl.hidden = NO;
            
            // cell.vatHeadingLbl.text =[NSString stringWithFormat:@"VAT(@%@%%)",[values  valueForKey:@"vat_percent"]];
            //NSString *cgst=[NSString stringWithFormat:@"%@",[Orders_Dictionary valueForKey:@"cgst_tax_amount"]];
          //  NSString *sgst=[NSString stringWithFormat:@"%@",[Orders_Dictionary valueForKey:@"sgst_tax_amount"]];
           
           // }
            
             cell.netAmountLbl.text =[NSString stringWithFormat:@"%@",[values  valueForKey:@"netamount"]];
             cell.amountPaid.text =[NSString stringWithFormat:@"%@",[values  valueForKey:@"amount_paid"]];
            
             cell.amountPayable.text =[NSString stringWithFormat:@"%@",[values  valueForKey:@"amount_payable"]];
            
             cell.grandTotal.text =[NSString stringWithFormat:@"%@",[values  valueForKey:@"order_total"]];
            
            
            
            
           
            return cell;
        }
        else{
            
            static NSString *simpleTableIdentifier = @"Seller_Invoice_Cell";
            Seller_Invoice_Cell  *cell = (Seller_Invoice_Cell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"Seller_Invoice_Cell" owner:self options:nil];
                cell = (Seller_Invoice_Cell *)[nib objectAtIndex:0];
            }
            
            if (invoiceDetails.count >0) {
                
            
           
            NSArray *products = [[invoiceDetails objectAtIndex:indexPath.section-1] valueForKey:@"product"];
            
            cell.sellerName.text = [[products valueForKey:@"seller_name"] objectAtIndex:indexPath.row];
            
             cell.productName.text = [[products valueForKey:@"product_name"] objectAtIndex:indexPath.row];
            cell.brandLbl.text = [[products valueForKey:@"brand"] objectAtIndex:indexPath.row];
            cell.gradeLbl.text = [[products valueForKey:@"grade"] objectAtIndex:indexPath.row];
            cell.priceLbl.text = [[products valueForKey:@"price"] objectAtIndex:indexPath.row];
             cell.itemsLbl.text = [NSString stringWithFormat:@"%@ (Tonnes)",[[products valueForKey:@"qty"] objectAtIndex:indexPath.row]];
             cell.rowTotalLbl.text = [[products valueForKey:@"row_total"] objectAtIndex:indexPath.row];
           cell.discountLbl.text = [[products valueForKey:@"discount"] objectAtIndex:indexPath.row];
            
            }
            
         
            
            return cell;
        }
        
    }
    
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
   
    
    if (section==0)
    {
        return NULL;
    }
    else
    {
      
    
        UIView *head_view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 65)];
        [head_view setBackgroundColor:[UIColor whiteColor]];
        
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 30)];
        

        
        NSArray *values =[invoiceDetails objectAtIndex:section-1];
        title.text = [NSString stringWithFormat:@" Invoice #: %@",[values valueForKey:@"seller_invoice_id"] ];
        
        title.textColor = [UIColor whiteColor];
        title.backgroundColor=[UIColor colorWithRed:7/255.0 green:21/255.0 blue:82/255.0 alpha:1.0];
        title.textAlignment=NSTextAlignmentLeft;
        [title setFont:[UIFont fontWithName:@"PTSans-NarrowBold" size:18]];
        [head_view addSubview:title];
        
        UILabel *title2 = [[UILabel alloc] initWithFrame:CGRectMake(0, 30, self.view.frame.size.width, 30)];
        title2.text = [NSString stringWithFormat:@" Items Invoiced"];
        title2.textColor = [UIColor whiteColor];
        title2.backgroundColor=[UIColor colorWithRed:28/255.0 green:86/255.0 blue:160/255.0 alpha:1.0];
        title2.textAlignment=NSTextAlignmentLeft;
        [title2 setFont:[UIFont fontWithName:@"PTSans-NarrowBold" size:18]];
        [head_view addSubview:title2];
     
        return head_view;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section==0)
    {
        return 0;
    }
    return 65;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)invoiceService
{
    
    user_id =[[ NSUserDefaults standardUserDefaults]valueForKey:@"user_id"];
    
   
        NSString *urlString = [NSString stringWithFormat:@"getOrderInvoices/?order_id=%@",_orderId];
    
        [[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString:urlString requestNumber:WS_GET_ACCOUNT_INFO showProgress:YES withHandler:^(BOOL success, id data)
         {
             if (success)
             {
                 
                 if (data) {
                     
                     NSString * status = [NSString stringWithFormat:@"%@",[[data valueForKey:@"status"]objectAtIndex:0]];
                     if ([status isEqualToString:@"0"]) {
                         self.invoiceTableview.hidden = true;
                         ALERT_DIALOG(@"Alert", @"No Invoice Found");
                     }
                     else
                     {
                         invoiceResponse  = [[NSMutableArray alloc]init];
                         invoiceResponse = data;
                     
                         customerAddress = [[NSMutableArray alloc]init];
                         customerAddress = [invoiceResponse valueForKey:@"customer_address"];
                         invoiceDetails =[[NSMutableArray alloc]init];
                         invoiceDetails = [invoiceResponse valueForKey:@"invoice"];
                     
                         [_invoiceTableview reloadData];
                     }
                     
//                     quoteArr =[billingAddressService valueForKey:@"quote"];
//                     sellers_info =  [billingAddressService valueForKey:@"sellers_info"];
//                     productsArr = [[billingAddressService valueForKey:@"sellers_info"] valueForKey:@"products"];
//                     CST=[NSString stringWithFormat:@"%@",[[quoteArr objectAtIndex:0] valueForKey:@"cst"]];
//                     for(NSArray *subArray in productsArr)
//                     {
//                         NSLog(@"Array in myArray: %@",subArray);
//                     }
//                     
//                     shipping_address = [sellers_info valueForKey:@"shipping_address" ] ;
//                     seller_address = [sellers_info valueForKey:@"seller_address" ] ;
//

                     
                 }
                 
                 
             }else{
                 
                 
             }
             
                      }];
        
   }

- (IBAction)BackClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)downLoadPDf:(UIButton *)sender
{
    NSArray *values =[invoiceDetails objectAtIndex:sender.tag];
    
    NSString *s_id=[NSString stringWithFormat:@"%@",[values  valueForKey:@"invoice_id"]];
    
    pdfViewViewController *con  = [[pdfViewViewController alloc]initWithNibName:@"pdfViewViewController" bundle:nil];
    con.orderUrl =[NSString stringWithFormat:@"%@printInvoice/?invoice_id=%@",UserOrder_Url,s_id];
    [self.navigationController pushViewController:con animated:YES ];
}

- (IBAction)goToHomeScreenAction:(id)sender {
    HomeViewController *con = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
    [self.navigationController pushViewController:con animated:YES];  
}
@end
