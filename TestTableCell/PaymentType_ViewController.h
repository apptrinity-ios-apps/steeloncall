//
//  PaymentType_ViewController.h
//  steelonCallThree
//
//  Created by nagaraj  kumar on 03/12/16.
//  Copyright © 2016 nagaraj  kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STParsing.h"
#import "HomeViewController.h"

@interface PaymentType_ViewController : UIViewController<UITextViewDelegate,UITextFieldDelegate>
{
    NSUserDefaults *user_data;
    NSString *user_id;
    BOOL check_tag;
    NSDictionary *dict;
}
@property (strong, nonatomic) IBOutlet UIView *payment_subview;
@property (strong, nonatomic) IBOutlet UIButton *onlinePayBtn;
@property (strong, nonatomic) IBOutlet UIButton *cashOnDeliveryBtn;
@property (strong, nonatomic) IBOutlet UIButton *sub_OnlinePayBtn;
@property (strong, nonatomic) IBOutlet UIButton *sub_OflinePayBtn;
@property (strong, nonatomic)NSMutableArray *shippingAddress;
@property (strong, nonatomic)NSString *methodType;
@property (strong, nonatomic) IBOutlet UIButton *credit_btn;
- (IBAction)credit_click:(id)sender;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *credit_height_constraint;
@property (strong, nonatomic) IBOutlet UIView *view_1;
@property (strong, nonatomic) IBOutlet UIView *LC_view;
@property (strong, nonatomic) IBOutlet UIView *credit_view;
- (IBAction)Check_click_LC:(id)sender;
- (IBAction)onlinePayBtnAction:(id)sender;
- (IBAction)cashOnDelivryBtnAction:(id)sender;
- (IBAction)sub_OnlinePayBtnAction:(id)sender;
- (IBAction)sub_offlinePayBtnAction:(id)sender;
- (IBAction)makePaymentBtnActn:(id)sender;
- (IBAction)backBtnActn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *LC_check;
@property (strong, nonatomic) IBOutlet UITextView *documents_req;
@property (strong, nonatomic) IBOutlet UITextView *neg_bank;
@property (strong, nonatomic) IBOutlet UITextView *shipment_to;
@property (strong, nonatomic) IBOutlet UITextView *shipment_from;
@property (strong, nonatomic) IBOutlet UITextView *adv_bank;
@property (strong, nonatomic) IBOutlet UITextView *bank_charges;
@property (strong, nonatomic) IBOutlet UILabel *usance;

@property (strong, nonatomic) IBOutlet UILabel *partshipment;
@property (strong, nonatomic) IBOutlet UILabel *amount;
@property (strong, nonatomic) IBOutlet UITextView *issuing_bank;
@property (strong, nonatomic) IBOutlet UILabel *trans_shipment;
@property (strong, nonatomic) IBOutlet UITextView *credit_availbale;
@property (strong, nonatomic) IBOutlet UILabel *buyer_name;
@property (strong, nonatomic) IBOutlet UILabel *ben_name;
@property (strong, nonatomic) IBOutlet UITextField *exp_date;
@property (strong, nonatomic) IBOutlet UILabel *type_lc;
@property (strong, nonatomic) IBOutlet UIButton *lc_btn;
- (IBAction)lc_click:(id)sender;
- (IBAction)bg_click:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *BG_btn;

@property (strong, nonatomic) IBOutlet UIButton *payment_refenceBtn;

- (IBAction)payment_RefenceAction:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *payment_refenceTFT;



@end
