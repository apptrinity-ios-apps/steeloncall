//
//  List_cell.h
//  SteelonCall
//
//  Created by INDOBYTES on 27/07/17.
//  Copyright © 2017 com.indobytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface List_cell : UICollectionViewCell<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UIView *bgView;
@property (strong, nonatomic) IBOutlet UIButton *checkbox;
@property (strong, nonatomic) IBOutlet UITextField *tonnes;
@property (strong, nonatomic) IBOutlet UITextField *pieces;
@property (strong, nonatomic) IBOutlet UILabel *price;
@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UIImageView *product_img;

@end
