//
//  LeftPanelViewController.h
//  TestTableCell
//
//  Created by administrator on 29/11/16.
//  Copyright © 2016 com.SteelonCall. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "REFrostedViewController.h"
#import "Contact_Us.h"
@interface LeftPanelViewController : UIViewController<UITableViewDelegate ,UITableViewDataSource>

- (IBAction)yourOrderBtnClick:(id)sender;
- (IBAction)accountMgtBtnClick:(id)sender;
- (IBAction)trackOrderBtnAction:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *menu_name;
@property (strong, nonatomic) IBOutlet UILabel *menu_number;
@property (strong, nonatomic) IBOutlet UIButton *homeBtn;

@end
