//
//  RightPadding.m
//  SteelonCall
//
//  Created by INDOBYTES on 20/11/18.
//  Copyright © 2018 com.indobytes. All rights reserved.
//

#import "RightPadding.h"

@implementation RightPadding


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
- (CGRect) rightViewRectForBounds:(CGRect)bounds {
    
    CGRect textRect = [super rightViewRectForBounds:bounds];
    textRect.origin.x -= 20;
    return textRect;
}
- (id)initWithCoder:(NSCoder*)coder {
    self = [super initWithCoder:coder];
    
    if (self) {
        
        self.clipsToBounds = YES;
        [self setRightViewMode:UITextFieldViewModeUnlessEditing];
        
        self.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@""]];
    }
    
    return self;
}
@end
