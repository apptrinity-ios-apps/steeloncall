//
//  Contact_Us.m
//  SteelonCall
//
//  Created by INDOBYTES on 04/05/17.
//  Copyright © 2017 com.indobytes. All rights reserved.
//

#import "Contact_Us.h"

@interface Contact_Us ()

@end

@implementation Contact_Us

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(view_tap:)];
    [self.view addGestureRecognizer:singleFingerTap];
    [self adr];
}

-(void)adr
{
    
    [[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString:@"calculate/service/getContactUsContent" requestNumber:WS_SPONSORS showProgress:NO withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             dict=data;
             if (data)
             {
                 _title_name.text=[NSString stringWithFormat:@"%@",[[dict valueForKey:@"ro"]valueForKey:@"title"]];
                 _ro_address.text=[NSString stringWithFormat:@"%@",[[dict valueForKey:@"ro"]valueForKey:@"address"]];
                 _ro_ph.text=[NSString stringWithFormat:@"Ph: %@",[[dict valueForKey:@"ro"]valueForKey:@"phone"]];
                 _ao_address.text=[NSString stringWithFormat:@"%@",[[dict valueForKey:@"ao"]valueForKey:@"address"]];
                 _ao_ph.text=[NSString stringWithFormat:@"Ph: %@",[[dict valueForKey:@"ao"]valueForKey:@"phone"]];
                 
                 _ro_ph.userInteractionEnabled = YES;
                 UITapGestureRecognizer *tapGesture = \
                 [[UITapGestureRecognizer alloc]
                  initWithTarget:self action:@selector(ro_click)];
                 [_ro_ph addGestureRecognizer:tapGesture];
                 
                 _ao_ph.userInteractionEnabled = YES;
                 UITapGestureRecognizer *tapGesture2 = \
                 [[UITapGestureRecognizer alloc]
                  initWithTarget:self action:@selector(ao_click)];
                 [_ao_ph addGestureRecognizer:tapGesture2];
                 
             }
         }
         
     }];
}

-(void)ro_click
{
    NSString *phoneNumber = [@"tel://" stringByAppendingString:[NSString stringWithFormat:@"%@",[[dict valueForKey:@"ro"]valueForKey:@"phone"]]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

-(void)ao_click
{
    NSString *phoneNumber = [@"tel://" stringByAppendingString:[NSString stringWithFormat:@"%@",[[dict valueForKey:@"ao"]valueForKey:@"phone"]]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

//The event handling method
- (void)view_tap:(UITapGestureRecognizer *)recognizer
{
    CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    //Do stuff here...
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==_name_tf)
    {
        [_email_tf becomeFirstResponder];
    }
    else if (textField==_email_tf)
    {
        [_number_tf becomeFirstResponder];
    }
    else if (textField==_number_tf)
    {
        [_comment becomeFirstResponder];
    }
    
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)submit_click:(id)sender
{
    [self.view endEditing:YES];
    
    if ([self valide_Data] == YES)
    { // checking if the either of the string is empty and other validations
        [self Login_Dict];
    }
}

-(BOOL)valide_Data
{
    NSString *email_str = [_email_tf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *name_str=_name_tf.text;
    NSString *number_str=_number_tf.text;
    NSString *comment_str=_comment.text;

    if ([name_str length] == 0)
    {
        [self Alert :@"Name should not be empty."];
        return NO;
    }
    else if  ( [email_str length]==0)
    {
        [self Alert :@"Email should not be empty."];
        return NO;
    }
    else if ([self NSStringIsValidEmail:email_str]== NO)
    {
        [self Alert :@"Email should be valid."];
        return NO;
    }
    
    else if ([comment_str length] == 0)
    {
        [self Alert :@"Comment should not be empty."];
        return NO;
    }

   else if  ( [number_str length]==0)
    {
        [self Alert :@"Number should not be empty."];
        return NO;
    }
//else if ([number_str length]>10||[number_str length]<10)
//{
    
//}
    return YES;
}
-(void)Alert:(NSString *)Msg
{
    NSDictionary *options = @{kCRToastNotificationTypeKey:@(CRToastTypeNavigationBar),
                              
                              kCRToastTextKey : Msg,
                              
                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                              
                              kCRToastBackgroundColorKey : [UIColor colorWithRed:13.0/255.0 green:147.0/255.0 blue:68.0/255.0 alpha:1],
                              kCRToastTimeIntervalKey: @(2),
                              //                              kCRToastFontKey:[UIFont fontWithName:@"PT Sans Narrow" size:23],
                              kCRToastInteractionRespondersKey:@[[CRToastInteractionResponder interactionResponderWithInteractionType:CRToastInteractionTypeSwipeUp
                                                                  
                                                                                                                 automaticallyDismiss:YES
                                                                  
                                                                                                                                block:^(CRToastInteractionType interactionType){
                                                                                                                                    
                                                                                                                                    NSLog(@"Dismissed with %@ interaction", NSStringFromCRToastInteractionType(interactionType));
                                                                                                                                }]],
                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeGravity),
                              
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeGravity),
                              
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                              
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop)
                              };
    
    [CRToastManager showNotificationWithOptions:options
                                completionBlock:^{
                                    NSLog(@"Completed");
                                }];
}

-(void)Login_Dict
{
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:[_email_tf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"email"];
    [dict setObject:[_name_tf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"name"];
    [dict setObject:[_number_tf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"mobile_number"];
    [dict setObject:[_comment.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"comment"];

    NSLog(@"%@",dict);
    [self Login_Service:dict];
}

-(BOOL)NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

-(void)Login_Service:(NSDictionary *)Dict
{
    [[STParsing sharedWebServiceHelper]requesting_POST_ServiceWithString1:@"getContactusFormData" parameters:Dict requestNumber:WS_Login showProgress:YES withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             NSDictionary *res_dict=data;
             NSString *status=[NSString stringWithFormat:@"%@",[res_dict valueForKey:@"status"]];
             NSString *result=[res_dict valueForKey:@"result"];
             // 8 10 85 15
             if ([status isEqualToString:@"1"])
             {
                 //[self Alert:@"Thank You!"];
                 ALERT_DIALOG(@"Message",@"Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.");

                 [self.navigationController popViewControllerAnimated:YES];
             }
         }
         else
         {
             [self Alert:@"Something went wrong"];
         }
     }];
}

- (IBAction)back_click:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
