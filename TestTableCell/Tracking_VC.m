//
//  Tracking_VC.m
//  SteelonCall
//
//  Created by nagaraj  kumar on 14/11/18.
//  Copyright © 2018 com.indobytes. All rights reserved.
//

#import "Tracking_VC.h"
#import "TrackingCell.h"
#import "TrackingFooterCell.h"
#import "TrackingHeaderCell.h"
#import "TrackingFooter_HideCell.h"
#import "STParsing.h"
#import "QuartzCore/QuartzCore.h"
#import "Product_defined_cell.h"
#import "More_Details.h"
#import "More_Sellers.h"
#import "STParsing.h"
#import "My_Cart.h"
#import "searchPopupVIew.h"
#import "CRToastView.h"
#import "LoginViewController.h"
#import "CompareViewController.h"
#import "MBProgressHUD.h"
#import "Shipping_Info.h"
#import "More_Sellers.h"
#import "Shipping_info_cell.h"
#import "Shipping_lastCell.h"
#import "searchPopupVIew.h"


@interface Tracking_VC ()
{
    __weak IBOutlet UILabel *toalAmount_Lbl;
    long int hide_Status,hide_Tag;
}

@end

@implementation Tracking_VC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    hide_Status = 0;
    hide_Tag = -1;
    
    self.heading_Lbl.text = [NSString stringWithFormat:@"Order ID:%@",self.order_id];
    self.trackingTableView.delegate = self;
    self.trackingTableView.dataSource = self;
    
    // Do any additional setup after loading the view from its nib.
    [self Track_Order];
    
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
    
}


- (void)keyboardWillShow:(NSNotification *)n
{
    
    [self.view setFrame:CGRectMake(0,-110,self.view.frame.size.width,self.view.frame.size.height)];
    
}
- (void)keyboardWillHide:(NSNotification *)n
{
    
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return   Sellers_dict.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [[[Sellers_dict valueForKey:@"products"] objectAtIndex:section] count]+1;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    long int count = [[[Sellers_dict valueForKey:@"products"] objectAtIndex:indexPath.section] count]+1;
    
    if (indexPath.row == count-1){
        
        if (hide_Tag == indexPath.section)
        {
            return 362;
            
        }else{
            
             return 40;
        }
        
    }else{
        
      return 120;
    }
    
  
    
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    static NSString *simpleTableIdentifier = @"TrackingHeaderCell";
    TrackingHeaderCell  *cell = (TrackingHeaderCell *)[tableView dequeueReusableHeaderFooterViewWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TrackingHeaderCell" owner:self options:nil];
        cell = (TrackingHeaderCell *)[nib objectAtIndex:0];
    }
      NSMutableDictionary * product = [[Sellers_dict valueForKey:@"seller"] objectAtIndex:section];
    
    cell.header_Lbl.text = [product valueForKey:@"seller_name"];
    NSString *separatorString = @".";
    NSString *myString = [NSString stringWithFormat:@"%@",[product valueForKey:@"total_amount"]];
    NSString *myNewString = [myString componentsSeparatedByString:separatorString].firstObject;
    toalAmount_Lbl.text =[NSString stringWithFormat:@"Total Amount: %@.00", myNewString];
    
    return cell;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
     long int count = [[[Sellers_dict valueForKey:@"products"] objectAtIndex:indexPath.section] count]+1;
    
    if (indexPath.row == count-1) {
        
        if (hide_Tag == indexPath.section){
            
            
            static NSString *simpleTableIdentifier = @"TrackingFooterCell";
            TrackingFooterCell  *cell = (TrackingFooterCell *)[tableView dequeueReusableHeaderFooterViewWithIdentifier:simpleTableIdentifier];
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TrackingFooterCell" owner:self options:nil];
                cell = (TrackingFooterCell *)[nib objectAtIndex:0];
            }
            [cell.trackProgress_Btn setTag:indexPath.section];
            [cell.trackProgress_Btn addTarget:self action:@selector(ButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            
            NSMutableDictionary * product = [[Sellers_dict valueForKey:@"seller"] objectAtIndex:indexPath.section];
            
            
            
            cell.estimatedLbl.text = [NSString stringWithFormat:@"Estimated Delivery on :%@",[product valueForKey:@"estimate_date"]];
            cell.estimatedLbl.hidden = NO;
            
            NSString *status = [product valueForKey:@"order_status"];
            
            if ([status isEqualToString:@"pending"])
            {
                cell.Status_Image.image = [UIImage imageNamed:@"process1"];
                
            }else if ([status isEqualToString:@"processing"]) {
                
                cell.Status_Image.image = [UIImage imageNamed:@"process2"];
            }else if ([status isEqualToString:@"shipping"]){
               
                cell.Status_Image.image = [UIImage imageNamed:@"process3"];
            }else if ([status isEqualToString:@"complete"])
            {
               cell.Status_Image.image = [UIImage imageNamed:@"process4"];
                
                cell.estimatedLbl.hidden = YES;
                
            }
            else{
                
               cell.Status_Image.image = [UIImage imageNamed:@"process5"];
            }
            
            
            
            
            
            return cell;
           
            
        }else{
            
            static NSString *simpleTableIdentifier = @"TrackingFooter_HideCell";
            TrackingFooter_HideCell  *cell = (TrackingFooter_HideCell *)[tableView dequeueReusableHeaderFooterViewWithIdentifier:simpleTableIdentifier];
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TrackingFooter_HideCell" owner:self options:nil];
                cell = (TrackingFooter_HideCell *)[nib objectAtIndex:0];
            }
            [cell.track_ProgressHideBtn setTag:indexPath.section];
            [cell.track_ProgressHideBtn addTarget:self action:@selector(ButtonActionHide:) forControlEvents:UIControlEventTouchUpInside];
            return cell;
       
        
        }
        
        
        
    }
    else{
        
   
      static NSString *simpleTableIdentifier = @"TrackingCell";
      TrackingCell  *cell = (TrackingCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TrackingCell" owner:self options:nil];
        cell = (TrackingCell *)[nib objectAtIndex:0];
    }
   
        NSMutableDictionary * product = [[Sellers_dict valueForKey:@"products"] objectAtIndex:indexPath.section];
    cell.brand_Lbl.text = [[product valueForKey:@"brand"]objectAtIndex:indexPath.row];
    cell.Brand_TMTLbl.text = [[product valueForKey:@"name"]objectAtIndex:indexPath.row];
    cell.tons_Lbl.text = [NSString stringWithFormat:@"%@ Tons",[[product valueForKey:@"tons"] objectAtIndex:indexPath.row]];
    cell.pieces_Lbl.text = [NSString stringWithFormat:@"%@ pieces",[[product valueForKey:@"pieces"] objectAtIndex:indexPath.row]];
        
        NSString *separatorString = @".";
        NSString *myString = [NSString stringWithFormat:@"Rs: %@",[[product valueForKey:@"price"]objectAtIndex:indexPath.row]];
        NSString *myNewString = [myString componentsSeparatedByString:separatorString].firstObject;
        
      cell.price_Lbl.text = [NSString stringWithFormat:@"%@.00", myNewString];
        
        NSURL *imageURL = [NSURL URLWithString:[[product valueForKey:@"image"]objectAtIndex:indexPath.row]];
        [cell.brand_Image sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
        
    
    return cell;
    }
    
    
}


- (void)ButtonAction:(id)sender
{
    hide_Tag = -1;

    [_trackingTableView reloadData];
    
    
}
- (void)ButtonActionHide:(id)sender
{
     hide_Tag = [sender tag];
    
    [_trackingTableView reloadData];
    
    
}

//http://stg.steeloncall.com/calculate/service/getTrackingDetails/?tracking_id=100000577

-(void)Track_Order
{
    NSString *url_form;
    url_form=[NSString stringWithFormat:@"getTrackingDetails/?tracking_id=%@",_order_id];
    
    NSString* urlEncoded = [url_form stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString:urlEncoded requestNumber:PRODUCT_LIST showProgress:YES withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             NSLog(@" response %@",data);
             // Product_List_Dict=data;
             Sellers_dict=data;
             NSString * staus =[NSString stringWithFormat:@"%@",[data valueForKey:@"status"]];
             staus = [staus substringFromIndex:6];
             staus = [staus substringToIndex:1];
             if ([staus isEqualToString:@"0"]) {
                 [self Alert:@"No Products Found!"];
             }
             else
             {
                 if (Sellers_dict.count==0)
                 {
                     [self Alert:@"No Products Found!"];
                 }
                 else
                     [_trackingTableView reloadData];
             }
         }
         else
         {
             [self Alert:@"Something Went Wrong!"];
         }
     }];
}
-(void)Alert:(NSString *)Msg
{
    NSDictionary *options = @{kCRToastNotificationTypeKey:@(CRToastTypeNavigationBar),
                              
                              kCRToastTextKey : Msg,
                              
                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                              
                              kCRToastBackgroundColorKey : [UIColor colorWithRed:44.0/255.0 green:52.0/255.0 blue:75.0/255.0 alpha:1],
                              kCRToastTimeIntervalKey: @(2),
                              //                              kCRToastFontKey:[UIFont fontWithName:@"PT Sans Narrow" size:23],
                              kCRToastInteractionRespondersKey:@[[CRToastInteractionResponder interactionResponderWithInteractionType:CRToastInteractionTypeSwipeUp
                                                                  
                                                                                                                 automaticallyDismiss:YES
                                                                  
                                                                                                                                block:^(CRToastInteractionType interactionType){
                                                                                                                                    
                                                                                                                                    NSLog(@"Dismissed with %@ interaction", NSStringFromCRToastInteractionType(interactionType));
                                                                                                                                }]],
                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeGravity),
                              
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeGravity),
                              
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                              
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop)
                              
                              };
    
    [CRToastManager showNotificationWithOptions:options
     
                                completionBlock:^{
                                    
                                    NSLog(@"Completed");
                                    
                                }];
}





- (IBAction)back_Action:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (IBAction)logoBtn_Action:(id)sender {
    
    HomeViewController *con = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
    [self.navigationController pushViewController:con animated:YES];
    
}
@end
