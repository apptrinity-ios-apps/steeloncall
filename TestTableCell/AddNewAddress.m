//
//  AddNewAddress.m
//  SteelonCall
//
//  Created by S s Vali on 9/12/17.
//  Copyright © 2017 com.indobytes. All rights reserved.
//

#import "AddNewAddress.h"

@interface AddNewAddress ()
{
    NSArray *contactArr,*adrsArr;
    AddAddressCell *contactCell;
    AddAdrs2 *aCell;
    AddAdress3 *a3Cell;
    BOOL shippingCheckValue,billingCheckValue;
    NSUserDefaults * userData;
}
@end

@implementation AddNewAddress

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    contactArr = [[NSArray alloc]initWithObjects:@"First Name *",@"Last Name *",@"Company",@"Telephone *", nil];
    adrsArr = [[NSArray alloc]initWithObjects:@"Street Address*",@"Street Address 2",@"City*",@"State/Province*",@"Zip/Postal Code*",@"Country",@"Mobile Number*",@"PAN Number*",@"GST IN Number", nil];
    [_addrstable registerNib:[UINib nibWithNibName:@"AddAddressCell" bundle:nil] forCellReuseIdentifier:@"AddAddressCell"];
    userData = [NSUserDefaults standardUserDefaults];
    //The setup code (in viewDidLoad in your view controller)
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [_addrstable addGestureRecognizer:singleFingerTap];
}

//The event handling method
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    [self.view endEditing:YES];
    //Do stuff here...
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 295;
    }
    else if(indexPath.section == 1)
    {
        return 635;
    }
    else
    {
        return 150;
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, self.view.frame.size.width, 30)];
    if (section == 0) {
        title.text = @"        Contact Information";
    }
    else if(section == 1)
    {
        title.text = @"        Address";
    }
    else
    {
        return nil;
    }
    title.textColor = [UIColor colorWithRed:0/255.0 green:197/255.0f blue:235/255.0f alpha:1.0f];
   // title.backgroundColor=[UIColor colorWithRed:10/255.0 green:21/255.0 blue:80/255.0 alpha:1.0];
    title.textAlignment=NSTextAlignmentLeft;
    [title setFont:[UIFont fontWithName:@"RaleWay-SemiBold" size:18]];
    return title;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0 || section == 1) {
        return 40;
    }
    else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        contactCell = (AddAddressCell *)[self.addrstable dequeueReusableCellWithIdentifier:@"AddAddressCell"];
        if (contactCell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AddAddressCell" owner:self options:nil];
            contactCell = (AddAddressCell *)[nib objectAtIndex:0];
        }
        
        contactCell.selectionStyle = UITableViewCellSelectionStyleNone;
        contactCell.dataLabel.delegate = self;
         contactCell.lastName.delegate = self;
         contactCell.company.delegate = self;
         contactCell.telephoneLabel.delegate = self;
        
        if ([_from isEqualToString:@"edit"])
        {
            NSString *dataLabel=[NSString stringWithFormat:@"%@",[_address_dict valueForKey:@"firstname"]];
            NSString *lastName=[NSString stringWithFormat:@"%@",[_address_dict valueForKey:@"lastname"]];
            NSString *company=[NSString stringWithFormat:@"%@",[_address_dict valueForKey:@"company"]];
            NSString *telephoneLabel=[NSString stringWithFormat:@"%@",[_address_dict valueForKey:@"telephone"]];

            if ([dataLabel isEqualToString:@"(null)"]||[dataLabel isEqualToString:@"<null>"]||[dataLabel isEqualToString:@""]||dataLabel.length == nil)
            {
                contactCell.dataLabel.text=@"";
            }
            else
            contactCell.dataLabel.text=dataLabel;
            
            if ([lastName isEqualToString:@"(null)"]||[lastName isEqualToString:@"<null>"]||[lastName isEqualToString:@""]||lastName.length == nil)
            {
                contactCell.lastName.text=@"";
            }
            else
            contactCell.lastName.text=lastName;
            
            if ([company isEqualToString:@"(null)"]||[company isEqualToString:@"<null>"]||[company isEqualToString:@""]||company.length == nil)
            {
                contactCell.company.text=@"";
            }
            else
            contactCell.company.text=company;
            
            if ([telephoneLabel isEqualToString:@"(null)"]||[telephoneLabel isEqualToString:@"<null>"]||[telephoneLabel isEqualToString:@""]||telephoneLabel.length == nil)
            {
                contactCell.telephoneLabel.text=@"";
            }
            else
            contactCell.telephoneLabel.text=telephoneLabel;
        }
        return contactCell;
    }
   else if(indexPath.section == 1)
   {
       aCell = (AddAdrs2 *)[self.addrstable dequeueReusableCellWithIdentifier:@"AddAdrs2"];
       if (aCell == nil)
       {
           NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AddAdrs2" owner:self options:nil];
           aCell = (AddAdrs2 *)[nib objectAtIndex:0];
       }
       aCell.selectionStyle = UITableViewCellSelectionStyleNone;

       aCell.strettAdrs.delegate = self;
       aCell.strtAdrs2.delegate = self;
       aCell.city.delegate = self;
       aCell.state.delegate = self;
       aCell.zipcode.delegate = self;
       aCell.mobile.delegate = self;
       aCell.pan.delegate = self;
       aCell.gstinNumber.delegate = self;
       
      NSString *streeeeeet= [NSString stringWithFormat:@"%@",[_address_dict valueForKey:@"street"]];
       NSArray *strt=[streeeeeet componentsSeparatedByString:@"\n"];
       
       if (strt.count>0)
       {
           NSString *str=[NSString stringWithFormat:@"%@",[strt objectAtIndex:0]];
           if ([str isEqualToString:@"(null)"]||[str isEqualToString:@"<null>"]||[str isEqualToString:@""]||str.length == nil)
           {
               aCell.strettAdrs.text=@"";
           }
           else
               aCell.strettAdrs.text=str;
       }
       if (strt.count>1)
       {
       aCell.strtAdrs2.text=[NSString stringWithFormat:@"%@",[strt objectAtIndex:1]];
       }
       
       NSString *city=[NSString stringWithFormat:@"%@",[_address_dict valueForKey:@"city"]];
       NSString *state=[NSString stringWithFormat:@"%@",[_address_dict valueForKey:@"region"]];
       NSString *zipcode=[NSString stringWithFormat:@"%@",[_address_dict valueForKey:@"postcode"]];
       NSString *mobile=[NSString stringWithFormat:@"%@",[_address_dict valueForKey:@"mobile"]];
       NSString *pan=[NSString stringWithFormat:@"%@",[_address_dict valueForKey:@"pan"]];
       NSString *gstinNumber=[NSString stringWithFormat:@"%@",[_address_dict valueForKey:@"telephone"]];

       if ([city isEqualToString:@"(null)"]||[city isEqualToString:@"<null>"]||[city isEqualToString:@""]||city.length == nil)
       {
           aCell.city.text=@"";
       }
       else
       aCell.city.text=city;
       if ([state isEqualToString:@"(null)"]||[state isEqualToString:@"<null>"]||[state isEqualToString:@""]||state.length == nil)
       {
           aCell.state.text=@"";
       }else
       aCell.state.text=state;
       if ([zipcode isEqualToString:@"(null)"]||[zipcode isEqualToString:@"<null>"]||[zipcode isEqualToString:@""]||zipcode.length == nil)
       {
           aCell.zipcode.text=@"";
       }else
       aCell.zipcode.text=zipcode;
       if ([mobile isEqualToString:@"(null)"]||[mobile isEqualToString:@"<null>"]||[mobile isEqualToString:@""]||mobile.length == nil)
       {
           aCell.mobile.text=@"";
       }else
       aCell.mobile.text=mobile;
       if ([pan isEqualToString:@"(null)"]||[pan isEqualToString:@"<null>"]||[pan isEqualToString:@""]||pan.length == nil)
       {
           aCell.pan.text=@"";
       }else
       aCell.pan.text=pan;
       if ([gstinNumber isEqualToString:@"(null)"]||[gstinNumber isEqualToString:@"<null>"]||[gstinNumber isEqualToString:@""]||gstinNumber.length == nil)
       {
           aCell.gstinNumber.text=@"";
       }else
       aCell.gstinNumber.text=gstinNumber;

       
       return aCell;
   }
    else
    {
        a3Cell = (AddAdress3 *)[self.addrstable dequeueReusableCellWithIdentifier:@"AddAdress3"];
        if (a3Cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AddAdress3" owner:self options:nil];
            a3Cell = (AddAdress3 *)[nib objectAtIndex:0];
        }
        a3Cell.selectionStyle = UITableViewCellSelectionStyleNone;

        [a3Cell.backBtn addTarget:self action:@selector(backBtnAction) forControlEvents:UIControlEventTouchUpInside];
        [a3Cell.saveAddrsBtn addTarget:self action:@selector(saveAddrsBtnAction) forControlEvents:UIControlEventTouchUpInside];
        [a3Cell.defalutBillingadrsBtn addTarget:self action:@selector(defalutBillingadrsBtnAction) forControlEvents:UIControlEventTouchUpInside];
        [a3Cell.defaultShippingAdrsBtn addTarget:self action:@selector(defaultShippingAdrsBtnAction) forControlEvents:UIControlEventTouchUpInside];
        if ([_from_default isEqualToString:@"yes"])
        {
            a3Cell.defaultShippingAdrsBtn.hidden=YES;
            a3Cell.defalutBillingadrsBtn.hidden=YES;
        }
        else
        {
            a3Cell.defaultShippingAdrsBtn.hidden=NO;
            a3Cell.defalutBillingadrsBtn.hidden=NO;
        }
        return a3Cell;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField;
{
    if ([contactCell.dataLabel isFirstResponder]) {
        [contactCell.dataLabel resignFirstResponder];
        [contactCell.lastName becomeFirstResponder];
    }
    else  if ([contactCell.lastName isFirstResponder]) {
        [contactCell.lastName resignFirstResponder];
        [contactCell.company becomeFirstResponder];
    }
    else  if ([contactCell.lastName isFirstResponder]) {
        [contactCell.lastName resignFirstResponder];
        [contactCell.company becomeFirstResponder];
    }
    else  if ([contactCell.company isFirstResponder]) {
        [contactCell.company resignFirstResponder];
        [contactCell.telephoneLabel becomeFirstResponder];
    }
    else  if ([contactCell.telephoneLabel isFirstResponder]) {
        [contactCell.telephoneLabel resignFirstResponder];
        [aCell.strettAdrs becomeFirstResponder];
    }
    else  if ([aCell.strettAdrs isFirstResponder]) {
        [aCell.strettAdrs resignFirstResponder];
        [aCell.strtAdrs2 becomeFirstResponder];
    }
    else  if ([aCell.strtAdrs2 isFirstResponder]) {
        [aCell.strtAdrs2 resignFirstResponder];
        [aCell.city becomeFirstResponder];
    }
    else  if ([aCell.city isFirstResponder]) {
        [aCell.city resignFirstResponder];
        [aCell.state becomeFirstResponder];
    }
    else  if ([aCell.state isFirstResponder]) {
        [aCell.state resignFirstResponder];
        [aCell.zipcode becomeFirstResponder];
    }
    else  if ([aCell.zipcode isFirstResponder]) {
        [aCell.zipcode resignFirstResponder];
        [aCell.mobile becomeFirstResponder];
    }
    else  if ([aCell.mobile isFirstResponder]) {
        [aCell.mobile resignFirstResponder];
        [aCell.pan becomeFirstResponder];
    }
    else  if ([aCell.pan isFirstResponder]) {
        [aCell.pan resignFirstResponder];
         [aCell.gstinNumber resignFirstResponder];
    }
    else  if ([aCell.gstinNumber isFirstResponder]) {
        [aCell.gstinNumber resignFirstResponder];
    }
    return true;
}
-(void)backBtnAction
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)defaultShippingAdrsBtnAction
{
    if (shippingCheckValue) {
        shippingCheckValue = false;
         [a3Cell.defaultShippingAdrsBtn setImage:[UIImage imageNamed:@"Radio_Deselect1"] forState:UIControlStateNormal];
    }
    else
    {
        shippingCheckValue = true;
        [a3Cell.defaultShippingAdrsBtn setImage:[UIImage imageNamed:@"Radio_select1"] forState:UIControlStateNormal];
    }
}
-(void)defalutBillingadrsBtnAction
{
    if (billingCheckValue) {
        billingCheckValue = false;
        [a3Cell.defalutBillingadrsBtn setImage:[UIImage imageNamed:@"Radio_Deselect1"] forState:UIControlStateNormal];
    }
    else
    {
        billingCheckValue = true;
        [a3Cell.defalutBillingadrsBtn setImage:[UIImage imageNamed:@"Radio_select1"] forState:UIControlStateNormal];
    }
}

//- (IBAction)backBtnAction:(id)sender {
//    ;
//}

- (void)saveAddrsBtnAction
{
    if ([contactCell.dataLabel.text isEqualToString:@""]||[contactCell.lastName.text isEqualToString:@""]||[aCell.strettAdrs.text isEqualToString:@""]||[aCell.city.text isEqualToString:@""]||[aCell.state.text isEqualToString:@""]||[aCell.zipcode.text isEqualToString:@""]||[aCell.mobile.text isEqualToString:@""]||[aCell.pan.text isEqualToString:@""])//||[aCell.gstinNumber.text isEqualToString:@""]
    {
        ALERT_DIALOG(@"Alert",@"Please eneter all mandatory fields");
    }
    else
    {
        NSString *dadrs,*sadrs;
        if (billingCheckValue) {
            dadrs = @"1";
        }
        else
        {
            dadrs = @"0";
        }
        if (shippingCheckValue) {
            sadrs = @"1";
        }
        else
        {
            sadrs = @"0";
        }
        userData = [NSUserDefaults standardUserDefaults];
        NSString *email =[NSString stringWithFormat:@"%@",[userData valueForKey:@"emailID"]];
        NSString *user_id =[NSString stringWithFormat:@"%@",[userData valueForKey:@"user_id"]];
        NSDictionary *params;
        NSString *e_id=[NSString stringWithFormat:@"%@",[_address_dict valueForKey:@"entity_id"]];
        
        if ([_from_default isEqualToString:@"yes"])
        {
            dadrs = @"1";
            sadrs = @"1";
        }
        if ([_from isEqualToString:@"edit"])
        {
            params = @{@"firstname": contactCell.dataLabel.text
                                     ,@"lastname":contactCell.lastName.text
                                     ,@"company": contactCell.company.text
                                     ,@"telephone": contactCell.telephoneLabel.text
                                     ,@"street1": aCell.strettAdrs.text
                                     ,@"street2": aCell.strtAdrs2.text
                                     ,@"city": aCell.city.text
                                     ,@"region": aCell.state.text
                                     ,@"postcode": aCell.zipcode.text
                                     ,@"country_id": @"IN"
                                     ,@"mobile": aCell.mobile.text
                                     ,@"pan": aCell.pan.text
                                     ,@"gstin":aCell.gstinNumber.text
                                     ,@"default_billing":dadrs
                                     ,@"default_shipping":sadrs
                                     ,@"email":email
                                     ,@"id":user_id
                       ,@"address_id":e_id
                                     };
        }
        else
        {
        params = @{@"firstname": contactCell.dataLabel.text
                                 ,@"lastname":contactCell.lastName.text
                                 ,@"company": contactCell.company.text
                                 ,@"telephone": contactCell.telephoneLabel.text
                                 ,@"street1": aCell.strettAdrs.text
                                 ,@"street2": aCell.strtAdrs2.text
                                 ,@"city": aCell.city.text
                                 ,@"region": aCell.state.text
                                 ,@"postcode": aCell.zipcode.text
                                 ,@"country_id": @"IN"
                                 ,@"mobile": aCell.mobile.text
                                 ,@"pan": aCell.pan.text
                                 ,@"gstin":aCell.gstinNumber.text
                                 ,@"default_billing":dadrs
                                 ,@"default_shipping":sadrs
                                 ,@"email":email
                                 ,@"id":user_id
//                                 ,@"entity_id"
                                 };
        }
        if ([_from isEqualToString:@"edit"])
        {
            [[STParsing sharedWebServiceHelper]requesting_POST_ServiceWithString1:@"updateCustomerAddress" parameters:params requestNumber:WS_UPDATE_ADDRESS showProgress:YES withHandler:^(BOOL success, id data)
             {
                 if (success)
                 {
                     // ALERT_DIALOG(@"Alert", [data valueForKey:@"message"]);
                     AccountManageViewController *ac = [[AccountManageViewController alloc]initWithNibName:@"AccountManageViewController" bundle:nil];
                     [self.navigationController pushViewController:ac animated:YES];
                 }
                 else
                 {
                     ALERT_DIALOG(@"Alert", @"Something went wrong Please try again");
                 }
             }];
        }
        else
        {
        [[STParsing sharedWebServiceHelper]requesting_POST_ServiceWithString1:@"saveCustomerAddress" parameters:params requestNumber:WS_UPDATE_ADDRESS showProgress:YES withHandler:^(BOOL success, id data)
         {
             if (success)
             {
                // ALERT_DIALOG(@"Alert", [data valueForKey:@"message"]);
                 AccountManageViewController *ac = [[AccountManageViewController alloc]initWithNibName:@"AccountManageViewController" bundle:nil];
                 [self.navigationController pushViewController:ac animated:YES];
              }
             else
             {
                 ALERT_DIALOG(@"Alert", @"Something went wrong Please try again");
             }
         }];
        
        }
    }
}

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)goToHomeScreenAction:(id)sender {
    HomeViewController *con = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
    [self.navigationController pushViewController:con animated:YES];
    
    
}

@end
