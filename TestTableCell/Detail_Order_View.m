//
//  Detail_Order_View.m
//  SteelonCall
//
//  Created by Manoyadav on 06/12/16.
//  Copyright © 2016 com.way2online. All rights reserved.
//

#import "Detail_Order_View.h"
#import "InVoiceListViewController.h"

@interface Detail_Order_View ()

@end

@implementation Detail_Order_View

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
//self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:10.0/255.0 green:21.0/255.0 blue:80.0/255.0 alpha:1];    self.navigationController.navigationBar.translucent = NO;
//    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
//    UIImageView *navigationImage=[[UIImageView alloc]initWithFrame:CGRectMake(40, 20, 150, 30)];
//    navigationImage.image=[UIImage imageNamed:@"logo"];
//    self.navigationItem.titleView=navigationImage;
//    
//    UIButton *cameraButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 15, 40.0f, 40.0f)];
//    UIImage *cameraImage = [UIImage imageNamed:@"Back"];
//    [cameraButton setBackgroundImage:cameraImage forState:UIControlStateNormal];
//    [cameraButton addTarget:self action:@selector(Back_click) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem* cameraButtonItem = [[UIBarButtonItem alloc] initWithCustomView:cameraButton];
//    self.navigationItem.leftBarButtonItem = cameraButtonItem;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   // _invoiceBtnOutlet .hidden = YES;
//    self.navigationController.navigationBar.hidden = NO;
    _Orders_table.dataSource = self;
    _Orders_table.delegate = self;
    _Orders_table.separatorStyle = UITableViewCellSeparatorStyleNone;
    _Orders_table.allowsSelection=NO;
    user_data=[NSUserDefaults standardUserDefaults];
    user_id=[user_data valueForKey:@"user_id"];
//    user_id=@"73";
    Orders_Dict=[[NSArray alloc]init];
    _Orders_table.hidden =YES;
    [self Order_Details];
    
    
}

-(void)Order_Details
{
    NSString *url_form=[NSString stringWithFormat:@"orderdetails/?id=%@",_mainProductId];
    NSString* urlEncoded = [url_form stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString:urlEncoded requestNumber:Order_Detail showProgress:YES withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             NSLog(@" response %@",data);
             Orders_Dict=data;
            Orders_Dictionary=[[NSArray alloc]init];
             Payment_Dict=[[NSArray alloc]init];
             Shipment_Dict=[[NSArray alloc]init];
             
             Orders_Dictionary=[Orders_Dict objectAtIndex:0];
             po_tag=[NSString stringWithFormat:@"%@",[Orders_Dictionary valueForKey:@"po_id"]];
             Ordered_Products=[Orders_Dictionary valueForKey:@"product"];
             Payment_Dict=[[Orders_Dictionary valueForKey:@"billing_address"]objectAtIndex:0];
             Shipment_Dict=[[Orders_Dictionary valueForKey:@"shipping_address"]objectAtIndex:0];
             
             Payment_method=[[Orders_Dictionary valueForKey:@"payment_method"]objectAtIndex:0];
             Shipment__method=[[Orders_Dictionary valueForKey:@"shipping_method"]objectAtIndex:0];
             if ([[Orders_Dictionary valueForKey:@"order_status"]isEqualToString:@"Pending"]||[[Orders_Dictionary valueForKey:@"order_status"]isEqualToString:@"Canceled"]) {
                 self.invoiceBtnOutlet.hidden = true;
                 self.shippingInvoiceBtn.hidden = true;
                
             }
             else
             {
                 self.invoiceBtnOutlet.hidden = false;
                 self.shippingInvoiceBtn.hidden = false;
             }
             if ([po_tag isEqualToString:@""]||po_tag == nil|| [po_tag isEqual:[NSNull null]]||[po_tag isEqualToString:@"<null>"]||[po_tag isEqualToString:@"0"])
             {
                   self.poBtn.hidden= true;
             }
             else
                    self.poBtn.hidden= false;

             [_Orders_table reloadData];
              _Orders_table.hidden =NO;
             
//             for (int l=0; l<Product_List_Dict.count; l++)
//             {
//                 if (l==0)
//                 {
//                     product_ids_str=[[Product_List_Dict valueForKey:@"id"]objectAtIndex:l];
//                 }
//                 else
//                     product_ids_str=[NSString stringWithFormat:@"%@_%@",product_ids_str,[[Product_List_Dict valueForKey:@"id"]objectAtIndex:l]];
//             }
//             NSLog(@"ids string %@",product_ids_str);
//             [self call_tonnes_service];
         }
         else
         {
//             UIAlertController * alert=   [UIAlertController
//                                           alertControllerWithTitle:@"Alert"
//                                           message:[NSString stringWithFormat:@"%@",[data valueForKey:@"error_message"]]
//                                           preferredStyle:UIAlertControllerStyleAlert];
//             
//             UIAlertAction* ok = [UIAlertAction
//                                  actionWithTitle:@"OK"
//                                  style:UIAlertActionStyleDefault
//                                  handler:^(UIAlertAction * action)
//                                  {
//                                      [alert dismissViewControllerAnimated:YES completion:nil];
//                                  }];
//             [alert addAction:ok];
//             [self presentViewController:alert animated:YES completion:nil];
         }
     }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section ==0||section ==2)
    {
        return Orders_Dict.count;
    }
    else if (section ==1)
    {
        return Ordered_Products.count;
    }
    else
        return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section ==0 )
    {
        static NSString *simpleTableIdentifier = @"OP_Cell1";
        OP_Cell1  *cell = (OP_Cell1 *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"OP_Cell1" owner:self options:nil];
            cell = (OP_Cell1 *)[nib objectAtIndex:0];
        }
        cell.shippingAddressView.layer.shadowRadius  = 1.5f;
        cell.shippingAddressView.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
        cell.shippingAddressView.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
        cell.shippingAddressView.layer.shadowOpacity = 0.3f;
        cell.shippingAddressView.layer.cornerRadius = 3;
       cell.shippingAddressView.layer.masksToBounds = NO;
        
        UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
        UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(cell.shippingAddressView.bounds, shadowInsets)];
        cell.shippingAddressView.layer.shadowPath    = shadowPath.CGPath;
        cell.purchase_order.text=[NSString stringWithFormat:@"Purchase Order#%@",[Ordered_Products valueForKey:@"purchase_order_id"]];
        cell.order_date.text=[NSString stringWithFormat:@"Purchase Order %@",[Ordered_Products valueForKey:@"purchase_order_date"]];
        
        NSString *s_name=[NSString stringWithFormat:@"%@",[Shipment_Dict valueForKey:@"name"]];
        
        
        cell.shippingNameLabel.text = s_name;
        
        NSString *order_id_str=[NSString stringWithFormat:@"%@-%@",[Orders_Dictionary valueForKey:@"order_id"],[Orders_Dictionary valueForKey:@"order_status"]];
        cell.order_id.text=order_id_str;
        
//        if ([[Orders_Dictionary valueForKey:@"order_status"]isEqualToString:@"Complete"]) {
//            _invoiceBtnOutlet .hidden = NO;
//        }
//        else
//        {
//            
//        }
        
        NSString *order_date_str=[NSString stringWithFormat:@"%@",[Orders_Dictionary valueForKey:@"Date"]];
        cell.order_d.text=order_date_str;
        
        NSString *s_phone=[NSString stringWithFormat:@"Mobile  :%@",[Shipment_Dict valueForKey:@"phone"]];
        if ([s_phone isEqualToString:@"<null>"]) {
            cell.s_ph.text=@"";
        }
        else
            cell.shippingNumerLabel.text=s_phone;
        
        NSString *s_mob=[NSString stringWithFormat:@"%@",[Shipment_Dict valueForKey:@"mobile"]];
        if ([s_mob isEqualToString:@"<null>"]) {
            cell.s_mb.text=@"";
        }
        else
            cell.s_mb.text=s_mob;

       NSString *s_Comapny =[NSString stringWithFormat:@"%@",[Shipment_Dict valueForKey:@"company"]];
        
        if ([s_Comapny isEqualToString:@"<null>"]) {
            s_Comapny=@"";
        }
       NSString *s_City=[NSString stringWithFormat:@"%@",[Shipment_Dict valueForKey:@"city"]];
        NSString *s_Street = [NSString stringWithFormat:@"%@",[Shipment_Dict valueForKey:@"street"]];
       
        
        NSString *s_region_pincode=[NSString stringWithFormat:@"%@ %@",[Shipment_Dict valueForKey:@"region"],[Shipment_Dict valueForKey:@"zipcode"]];
        
         NSString *s_Country=[NSString stringWithFormat:@"%@",[Shipment_Dict valueForKey:@"country"]];
                NSArray *myStrings = [[NSArray alloc] initWithObjects:s_Comapny, s_Street, s_City, s_region_pincode, s_Country, nil];
        
                NSString *joinedString = [myStrings componentsJoinedByString:@","];
        
        
        cell.shippingAddessLabel.text = joinedString;
        
        
        
        NSString *b_Name=[NSString stringWithFormat:@"%@",[Payment_Dict valueForKey:@"name"]];
        cell.billingNameLabel.text = b_Name;
        
       
        
        NSString *b_phone=[NSString stringWithFormat:@"Mobile  :%@",[Payment_Dict valueForKey:@"phone"]];
        if ([b_phone isEqualToString:@"<null>"]) {
            cell.billingMobileNumber.text=@"";
        }
        else
            cell.billingMobileNumber.text=b_phone;
        
        NSString *b_mob=[NSString stringWithFormat:@"%@",[Payment_Dict valueForKey:@"mobile"]];
        if ([b_mob isEqualToString:@"<null>"]) {
            cell.b_mb.text=@"";
        }
        else
            cell.b_mb.text=b_mob;
         NSString *b_Street =[NSString stringWithFormat:@"%@",[Payment_Dict valueForKey:@"street"]];
        NSString *b_company=[NSString stringWithFormat:@"%@",[Payment_Dict valueForKey:@"company"]];
        if ([b_company isEqualToString:@"<null>"]) {
            b_company=@"";
        }
        NSString *b_City=[NSString stringWithFormat:@"%@",[Payment_Dict valueForKey:@"city"]];
         NSString *b_region_zipcode=[NSString stringWithFormat:@"%@ %@",[Payment_Dict valueForKey:@"region"],[Payment_Dict valueForKey:@"zipcode"]];
       
        NSString *b_country=[NSString stringWithFormat:@"%@",[Payment_Dict valueForKey:@"country"]];
        
        
        NSArray *myStrings1 = [[NSArray alloc] initWithObjects:b_company, b_Street, b_City, b_region_zipcode, b_country, nil];
        
        NSString *joinedString1 = [myStrings1 componentsJoinedByString:@","];
        
        
        cell.billingAddressLabel.text = joinedString1;
        
        NSString *pan_str=[NSString stringWithFormat:@"PAN No: %@",[Shipment_Dict valueForKey:@"pan"]];
        if ([pan_str isEqual:[NSNull null]]||[pan_str isEqualToString:@"PAN No: <null>"])
        {
            cell.shippingPanLabel.text=@"PAN No: ";
        }
        else
            cell.shippingPanLabel.text=pan_str;
        
//        NSString *cst_str=[NSString stringWithFormat:@"CST No: %@",[Shipment_Dict valueForKey:@"cst"]];
//        if ([cst_str isEqual:[NSNull null]]||[cst_str isEqualToString:@"CST No: <null>"])
//        {
//            cell.s_cst.text=@"CST No: ";
//        }
//        else
//            cell.s_cst.text=cst_str;
//
//        NSString *tin_str=[NSString stringWithFormat:@"TIN No: %@",[Shipment_Dict valueForKey:@"tin"]];
//        if ([tin_str isEqual:[NSNull null]]||[tin_str isEqualToString:@"TIN No: <null>"])
//        {
//            cell.s_tin.text=@"TIN No: ";
//        }
//        else
//            cell.s_tin.text=tin_str;
//
        NSString *pan_str1=[NSString stringWithFormat:@"PAN No: %@",[Payment_Dict valueForKey:@"pan"]];
        if ([pan_str1 isEqual:[NSNull null]]||[pan_str1 isEqualToString:@"PAN No: <null>"])
        {
            cell.billingpanLabel.text=@"PAN No:";
        }
        else
            cell.billingpanLabel.text=pan_str1;
        
        
        cell.s_method.text=[NSString stringWithFormat:@"%@",[Shipment__method valueForKey:@"shipping_method"]];
        cell.p_method.text=[NSString stringWithFormat:@"%@",[Payment_method valueForKey:@"payment_method"]];
        return cell;
    }
    else if (indexPath.section ==2 )
    {
        static NSString *simpleTableIdentifier = @"PD_cell0";
        PD_cell0 *cell = (PD_cell0 *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PD_cell0" owner:self options:nil];
            cell = (PD_cell0 *)[nib objectAtIndex:0];
        }
        
        cell.sub_total.text=[NSString stringWithFormat:@" %@",[Orders_Dictionary valueForKey:@"subtotal"]];
        
       NSString *cgst=[NSString stringWithFormat:@"%@",[Orders_Dictionary valueForKey:@"cgst_tax_amount"]];
        NSString *sgst=[NSString stringWithFormat:@"%@",[Orders_Dictionary valueForKey:@"sgst_tax_amount"]];
        NSString *igst = [NSString stringWithFormat:@"%@",[Orders_Dictionary valueForKey:@"igst_tax_amount"]];
       // NSString *cgst = @"0";
        
        cell.sgstValue.text = [NSString stringWithFormat:@" %@",[Orders_Dictionary valueForKey:@"sgst_tax_amount"]];
        if ([cgst isEqualToString:@"(null)"]||[cgst isEqualToString:@"<nil>"]||[cgst isEqual:[NSNull null]]||cgst == nil||[cgst isEqualToString:@""]||[cgst isEqualToString:@"0"])
        {
            cell.cgstheightlbl.constant = 0;
            cell.cgstvalueheightLbl.constant = 0;
            
        }else
        {
           cell.cgstValue.text = [NSString stringWithFormat:@" %@",[Orders_Dictionary valueForKey:@"cgst_tax_amount"]];
        }
        
        if ([sgst isEqualToString:@"(null)"]||[sgst isEqualToString:@"<nil>"]||[sgst isEqual:[NSNull null]]||sgst == nil||[sgst isEqualToString:@""]||[sgst isEqualToString:@"0"])
        {
            cell.sgstValueHeight.constant = 0;
            cell.sgdtlabelHieght.constant = 0;
        }else
        {
            cell.sgstValue.text = [NSString stringWithFormat:@" %@",[Orders_Dictionary valueForKey:@"sgst_tax_amount"]];
        }
        if ([igst isEqualToString:@"(null)"]||[igst isEqualToString:@"<nil>"]||[igst isEqual:[NSNull null]]||igst == nil||[igst isEqualToString:@""]||[igst isEqualToString:@"0"])
        {
            cell.igstLabelHeight.constant = 0;
            cell.igstValueHeight.constant = 0;
        }else
        {
            cell.igstValue.text = [NSString stringWithFormat:@" %@",[Orders_Dictionary valueForKey:@"igst_tax_amount"]];
        }
        cell.net_amount.text=[Orders_Dictionary valueForKey:@"netamount"];
        cell.handlingAmount.text = [Orders_Dictionary valueForKey:@"handling_amount"];
        cell.sh.text=[Orders_Dictionary valueForKey:@"shipping_amount"];
        cell.grand_total.text=[Orders_Dictionary valueForKey:@"order_total"];
        cell.paid.text=[Orders_Dictionary valueForKey:@"amount_paid"];
        cell.payable.text=[Orders_Dictionary valueForKey:@"amount_payable"];

        cell.view_1.layer.borderColor=[UIColor grayColor].CGColor;
        cell.view_1.layer.borderWidth = 0.5f;
        cell.view_2.layer.borderColor=[UIColor grayColor].CGColor;
        cell.view_2.layer.borderWidth = 0.5f;
        
        
        if ([[Orders_Dictionary valueForKey:@"order_status"]isEqualToString:@"Pending"]) {
            self.invoiceBtnOutlet.hidden = true;
            self.shippingInvoiceBtn.hidden = true;
            
            cell.cancel_Btn.hidden = NO;
            
            [cell.cancel_Btn addTarget:self action:@selector(orderCancelAction:) forControlEvents:UIControlEventTouchUpInside];

            
        }
        else
        {
            cell.cancel_Btn.hidden = YES;
            self.invoiceBtnOutlet.hidden = false;
            self.shippingInvoiceBtn.hidden = false;
        }
        
        
        
        /*
        cell.zipcode.text=[Shipment_Dict valueForKey:@"zipcode"];

        cell.shipping_address.layer.borderColor=[UIColor grayColor].CGColor;
        cell.shipping_address.layer.borderWidth = 1.0f;
        cell.payment_address.layer.borderColor=[UIColor grayColor].CGColor;
        cell.payment_address.layer.borderWidth = 1.0f;
         */
        return cell;
    }
    else//middle cells
    {
        static NSString *simpleTableIdentifier = @"product_cell";
        product_cell *cell = (Detail_Order *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"product_cell" owner:self options:nil];
            cell = (product_cell *)[nib objectAtIndex:0];
        }
        NSString *Name=[NSString stringWithFormat:@"%@",[[Ordered_Products objectAtIndex:indexPath.row]valueForKey:@"product_name"]];
        cell.name.text=Name;
        
        
        
        cell.gradeLbl.text = [NSString stringWithFormat:@"%@",[[Ordered_Products objectAtIndex:indexPath.row]valueForKey:@"grade"]];
        
        cell.brandLbl.text = [NSString stringWithFormat:@"%@",[[Ordered_Products objectAtIndex:indexPath.row]valueForKey:@"brand"]];
        
        NSString *s_Name=[NSString stringWithFormat:@"%@",[[Ordered_Products objectAtIndex:indexPath.row]valueForKey:@"seller_name"]];
        cell.seller_name.text=s_Name;
        NSString *price_ton=[NSString stringWithFormat:@"%@",[[Ordered_Products objectAtIndex:indexPath.row]valueForKey:@"price"]];
        cell.price_ton.text=price_ton;
        NSString *total=[NSString stringWithFormat:@"%@",[[Ordered_Products objectAtIndex:indexPath.row]valueForKey:@"row_total"]];
        cell.total.text=total;
        
        NSString *dis=[NSString stringWithFormat:@"%@",[[Ordered_Products objectAtIndex:indexPath.row]valueForKey:@"discount"]];
        if ([dis isEqualToString:@"Rs.0.00"]) {
            cell.discount.text =  @"Rs.0.00";
           
        }
        else{
           
             cell.discount.text=dis;
        }
       
        
        NSString *ton=[NSString stringWithFormat:@"%@ (Tonnes)",[[Ordered_Products objectAtIndex:indexPath.row]valueForKey:@"qty"]];
        NSString *piece=[NSString stringWithFormat:@"%@ (pieces)",[[Ordered_Products objectAtIndex:indexPath.row]valueForKey:@"pieces"]];
       
        cell.q_ton.text=ton;
        cell.q_pie.text =piece;

        NSURL *imageURL = [NSURL URLWithString:[[Ordered_Products valueForKey:@"image"]objectAtIndex:indexPath.row]];
        [cell.img sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
        
        [cell.img.layer setBorderColor: [[UIColor colorWithRed:213.0/255.0 green:216.0/255.0 blue:224.0/255.0 alpha:1] CGColor]];
        [cell.img.layer setBorderWidth: 2.0];
        
       
//    cell.border_view.layer.borderColor=[UIColor grayColor].CGColor;
//    cell.border_view.layer.borderWidth = 1.0f;
   
        return cell;
    }
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 1)
    {
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, self.view.frame.size.width, 30)];
        title.text = @"ITEMS ORDERED";
        title.textColor = [UIColor whiteColor];
        title.backgroundColor=[UIColor colorWithRed:44/255.0 green:184/255.0 blue:232/255.0 alpha:1.0];
        title.textAlignment=NSTextAlignmentCenter;
        [title setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
        return title;
    }
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 1)
    {
        return 40;
    }
    else{
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        return 656;
    }
    else if (indexPath.section == 2)
    {
        return 366;
    }
    else
        return 246;
}

-(void)Back_click
{
    self.navigationController.navigationBar.hidden = YES;
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)backAction:(id)sender {
    self.navigationController.navigationBar.hidden = YES;
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)orderCancelAction:(UIButton *)sender
{
    
    
    
    [self orderCancelServiceCall ];
    
    
    
}



-(void)orderCancelServiceCall {
    
    
   // cancelOrder/?customer_id=" + user_id+"&order_id=" + main_order_id
    
    
    NSString *url_form=[NSString stringWithFormat:@"cancelOrder/?customer_id=%@&order_id=%@",user_id,_mainProductId];
    NSString* urlEncoded = [url_form stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString:urlEncoded requestNumber:Order_Detail showProgress:YES withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             NSString * str = data[@"message"];
              ALERT_DIALOG(@"Alert",str);
             [self Order_Details];
             
            
         }
         else
         {
           
             NSString * str = data[@"message"];
             ALERT_DIALOG(@"Alert",str);
             
         }
     }];
    
    
    
}






/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)invoiceAction:(id)sender {
    
   NSString *orderId = [NSString stringWithFormat:@"%@",[Orders_Dictionary valueForKey:@"main_order_id"]] ;
    
    InVoiceListViewController *inVoiceListViewController = [[InVoiceListViewController alloc] initWithNibName:@"InVoiceListViewController" bundle:nil];
    
    inVoiceListViewController.orderId = orderId;
    
    inVoiceListViewController.orderDate = [NSString stringWithFormat:@"%@",[Orders_Dictionary valueForKey:@"Date"]];
    
    inVoiceListViewController.orderStatus = [NSString stringWithFormat:@"%@-%@",[Orders_Dictionary valueForKey:@"order_id"],[Orders_Dictionary valueForKey:@"order_status"]];
    
    
    [self.navigationController pushViewController:inVoiceListViewController animated:YES];
    //
    
}

- (IBAction)shipingHandlinInvoiceAction:(id)sender {
    
    pdfViewViewController *con  = [[pdfViewViewController alloc]initWithNibName:@"pdfViewViewController" bundle:nil];
    NSString *orderId = [NSString stringWithFormat:@"%@",[Orders_Dictionary valueForKey:@"main_order_id"]] ;
  //  con.orderUrl =[NSString stringWithFormat:@"https://steeloncall.com/calculate/service/getShippingAndHandlingInvoice/?order_id=%@",orderId];
    
      con.orderUrl =[NSString stringWithFormat:@"http://stg.steeloncall.com/calculate/service/getShippingAndHandlingInvoice/?order_id=%@",orderId];
    
    [self.navigationController pushViewController:con animated:YES ];

}

- (IBAction)shippingPoAction:(id)sender
{
    pdfViewViewController *con  = [[pdfViewViewController alloc]initWithNibName:@"pdfViewViewController" bundle:nil];
    NSString *orderId = [NSString stringWithFormat:@"%@",[Orders_Dictionary valueForKey:@"main_order_id"]] ;
    //con.orderUrl =[NSString stringWithFormat:@"https://steeloncall.com/calculate/service/getShippingAndHandlingPO/?order_id=%@",orderId];
    
     con.orderUrl =[NSString stringWithFormat:@"http://stg.steeloncall.com/calculate/service/getShippingAndHandlingPO/?order_id=%@",orderId];
    
    [self.navigationController pushViewController:con animated:YES ];
}

- (IBAction)goToHomeScreenAction:(id)sender
{
    HomeViewController *con = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
    [self.navigationController pushViewController:con animated:YES];
}
@end
