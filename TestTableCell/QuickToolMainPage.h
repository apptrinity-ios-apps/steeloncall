//
//  QuickToolMainPage.h
//  Smatrimony
//
//  Created by S s Vali on 7/17/17.
//  Copyright © 2017 Indobytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChildController.h"
#import "HomeViewController.h"
@interface QuickToolMainPage : UIViewController<UIPageViewControllerDelegate,UIPageViewControllerDataSource>

@property (strong, nonatomic) UIPageViewController *pageController;
@property (weak, nonatomic) IBOutlet UIView *pageViewOutlet;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;
@property (weak, nonatomic) IBOutlet UIButton *skipBtn;

- (IBAction)nextAction:(id)sender;
- (IBAction)skipAction:(id)sender;

@end
