//
//  Contact_Us.h
//  SteelonCall
//
//  Created by INDOBYTES on 04/05/17.
//  Copyright © 2017 com.indobytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STParsing.h"
#import "CRToastView.h"

@interface Contact_Us : UIViewController<UITextFieldDelegate,UITextViewDelegate>
{
    NSDictionary *dict;
}
@property (strong, nonatomic) IBOutlet UITextField *name_tf;
@property (strong, nonatomic) IBOutlet UITextField *email_tf;
@property (strong, nonatomic) IBOutlet UITextField *number_tf;
@property (strong, nonatomic) IBOutlet UITextView *comment;
- (IBAction)submit_click:(id)sender;
- (IBAction)back_click:(id)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *scr;
@property (strong, nonatomic) IBOutlet UILabel *ao_adr;
@property (strong, nonatomic) IBOutlet UILabel *ao_ph;
@property (strong, nonatomic) IBOutlet UILabel *ro_ph;
@property (strong, nonatomic) IBOutlet UILabel *ro_adr;
@property (strong, nonatomic) IBOutlet UILabel *title_name;
@property (strong, nonatomic) IBOutlet UITextView *ao_address;
@property (strong, nonatomic) IBOutlet UITextView *ro_address;

@end
