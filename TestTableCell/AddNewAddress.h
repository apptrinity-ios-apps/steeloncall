//
//  AddNewAddress.h
//  SteelonCall
//
//  Created by S s Vali on 9/12/17.
//  Copyright © 2017 com.indobytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddAddressCell.h"
#import "AddAdrs2.h"
#import "STParsing.h"
#import "AddAdress3.h"
#import "AccountManageViewController.h"
#import "HomeViewController.h"

@interface AddNewAddress : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITableView *addrstable;
@property (strong, nonatomic) NSDictionary *address_dict;
@property (strong, nonatomic) NSString *from;
@property (strong, nonatomic) NSString *from_default;

- (IBAction)backBtnAction:(id)sender;
- (IBAction)saveAddressAction:(id)sender;
- (IBAction)backAction:(id)sender;
@end
