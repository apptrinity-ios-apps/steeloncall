//
//  brandsCell.h
//  SteelonCall
//
//  Created by S s Vali on 8/18/17.
//  Copyright © 2017 com.indobytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface brandsCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *brandImage;

@end
