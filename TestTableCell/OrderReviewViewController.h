//
//  OrderReviewViewController.h
//  steelonCallThree
//
//  Created by nagaraj  kumar on 02/12/16.
//  Copyright © 2016 nagaraj  kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderReviewViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>
{
    NSUserDefaults *user_data;
    NSString *user_id,*Cart_Count;
    NSString *CST,*check_tag;
    int cst_cell_tag;
    NSString *igst;
    
}
@property(strong, nonatomic)NSArray *productArr;
@property (weak, nonatomic) IBOutlet UIButton *CustShippingBtn;
@property (weak, nonatomic) IBOutlet UIButton *steelOnShippingBtn;
@property (weak, nonatomic) IBOutlet UIView *customerShpngView;


@property (strong, nonatomic) IBOutlet UITableView *orderRecordTableview;
- (IBAction)backBtnActn:(id)sender;
- (IBAction)cart_Click_Actn:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *cart_CountLbl;
@property (weak, nonatomic) IBOutlet UIView *cart_BackgrndView;
- (IBAction)customerShipping_BtnActn:(id)sender;
- (IBAction)steelonCallShipping_BtnActn:(id)sender;
- (IBAction)customer_Shpng_BtnActn:(id)sender;


@end
