//
//  testimonialCell.h
//  SteelonCall
//
//  Created by S s Vali on 8/10/17.
//  Copyright © 2017 com.indobytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface testimonialCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UITextView *descriptionLabel;
@property (strong, nonatomic) IBOutlet UILabel *emailLabel;
@property (strong, nonatomic) IBOutlet UILabel *websiteLabel;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *companyLabel;
@property (strong, nonatomic) IBOutlet UILabel *addressLabel;
@end
