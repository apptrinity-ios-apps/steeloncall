//
//  blogCell.h
//  SteelonCall
//
//  Created by S s Vali on 8/10/17.
//  Copyright © 2017 com.indobytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface blogCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UITextView *desc;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UIButton *linkbtn;
@property (strong, nonatomic) IBOutlet UIImageView *imageOutlet;
@property (strong, nonatomic) IBOutlet UIButton *linkAction;

@end
