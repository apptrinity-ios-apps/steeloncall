//
//  ChildController.h
//  Smatrimony
//
//  Created by S s Vali on 7/17/17.
//  Copyright © 2017 Indobytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChildController : UIViewController

@property (assign, nonatomic) NSInteger index;
@property (weak, nonatomic) IBOutlet UIImageView *imageOutlet;
@property (weak, nonatomic) IBOutlet UILabel *titleOutlet;
@property (weak, nonatomic) IBOutlet UILabel *mainTitle_Lbl;

@end
