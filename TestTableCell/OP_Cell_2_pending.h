//
//  OP_Cell_2.h
//  Steeloncall_Seller
//
//  Created by INDOBYTES on 21/12/16.
//  Copyright © 2016 sai. All rights reserved.

#import <UIKit/UIKit.h>

@interface OP_Cell_2_pending : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *product_name;

@property (strong, nonatomic) IBOutlet UILabel *brand;
@property (strong, nonatomic) IBOutlet UILabel *grade;
@property (strong, nonatomic) IBOutlet UILabel *quantity;

@end
