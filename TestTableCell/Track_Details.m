 //
//  Track_Details.m
//  SteelonCall
//
//  Created by INDOBYTES on 03/04/17.
//  Copyright © 2017 com.indobytes. All rights reserved.
//

#import "Track_Details.h"

@interface Track_Details ()

@end

@implementation Track_Details



-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = NO;
self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:10.0/255.0 green:21.0/255.0 blue:80.0/255.0 alpha:1];    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    UIImageView *navigationImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 199, 40)];
    navigationImage.image=[UIImage imageNamed:@"logo.png"];
    self.navigationItem.titleView=navigationImage;
    
    UIButton *cameraButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 2, 40.0f, 40.0f)];
    UIImage *cameraImage = [UIImage imageNamed:@"Back"];
    [cameraButton setBackgroundImage:cameraImage forState:UIControlStateNormal];
    [cameraButton addTarget:self action:@selector(Back_click) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* cameraButtonItem = [[UIBarButtonItem alloc] initWithCustomView:cameraButton];
    self.navigationItem.leftBarButtonItem = cameraButtonItem;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self Track_Order];
}

//http://stg.steeloncall.com/calculate/service/getTrackingDetails/?tracking_id=100000577

-(void)Track_Order
{
    NSString *url_form;
            url_form=[NSString stringWithFormat:@"getTrackingDetails/?tracking_id=%@",_order_id];
    
    NSString* urlEncoded = [url_form stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString:urlEncoded requestNumber:PRODUCT_LIST showProgress:YES withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             NSLog(@" response %@",data);
            // Product_List_Dict=data;
             Sellers_dict=data;
             NSString * staus =[NSString stringWithFormat:@"%@",[data valueForKey:@"status"]];
             staus = [staus substringFromIndex:6];
            staus = [staus substringToIndex:1];
             if ([staus isEqualToString:@"0"]) {
                 [self Alert:@"No Products Found!"];
             }
             else
             {
             if (Sellers_dict.count==0)
             {
                 [self Alert:@"No Products Found!"];
             }
             else
                 [_table reloadData];
             }
         }
         else
         {
             [self Alert:@"Something Went Wrong!"];
         }
     }];
}
-(void)Alert:(NSString *)Msg
{
    NSDictionary *options = @{kCRToastNotificationTypeKey:@(CRToastTypeNavigationBar),
                              
                              kCRToastTextKey : Msg,
                              
                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                              
                              kCRToastBackgroundColorKey : [UIColor colorWithRed:44.0/255.0 green:52.0/255.0 blue:75.0/255.0 alpha:1],
                              kCRToastTimeIntervalKey: @(2),
                              //                              kCRToastFontKey:[UIFont fontWithName:@"PT Sans Narrow" size:23],
                              kCRToastInteractionRespondersKey:@[[CRToastInteractionResponder interactionResponderWithInteractionType:CRToastInteractionTypeSwipeUp
                                                                  
                                                                                                                 automaticallyDismiss:YES
                                                                  
                                                                                                                                block:^(CRToastInteractionType interactionType){
                                                                                                                                    
                                                                                                                                    NSLog(@"Dismissed with %@ interaction", NSStringFromCRToastInteractionType(interactionType));
                                                                                                                                }]],
                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeGravity),
                              
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeGravity),
                              
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                              
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop)
                              
                              };
    
    [CRToastManager showNotificationWithOptions:options
     
                                completionBlock:^{
                                    
                                    NSLog(@"Completed");
                                    
                                }];
}
-(void)Back_click
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0)
    {
        return 1;
    }
    else
    {
        NSDictionary *products=[[Sellers_dict valueForKey:@"products"]objectAtIndex:section-1];
        NSDictionary *comments=[[Sellers_dict valueForKey:@"comments"]objectAtIndex:section-1];
        return products.count+comments.count;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return Sellers_dict.count+1;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section==0)
    {
        return NULL;
    }
    else
    {
       UIView *head_view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 100)];
       [head_view setBackgroundColor:[UIColor whiteColor]];
        
     UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 30)];
        NSDictionary *s_name_dict=[[Sellers_dict valueForKey:@"seller"]objectAtIndex:section-1];
     title.text = [NSString stringWithFormat:@"Seller Name: %@",[s_name_dict valueForKey:@"seller_name"] ];
        
      [title setFont:[UIFont systemFontOfSize:13]];
        
     title.textColor = [UIColor whiteColor];
     title.backgroundColor=[UIColor colorWithRed:44.0/255.0 green:52.0/255.0 blue:75.0/255.0 alpha:1.0];
     title.textAlignment=NSTextAlignmentLeft;
     //[title setFont:[UIFont fontWithName:@"PTSans-NarrowBold" size:18]];
        [head_view addSubview:title];
        
        UILabel *title2 = [[UILabel alloc] initWithFrame:CGRectMake(10, 60, 120, 30)];
        title2.text = [NSString stringWithFormat:@"Product Details"];
        title2.textColor = [UIColor whiteColor];
        title2.backgroundColor=[UIColor colorWithRed:191.0/255.0 green:36.0/255.0 blue:42.0/255.0 alpha:1.0];
        title2.textAlignment=NSTextAlignmentLeft;
        [title2 setFont:[UIFont fontWithName:@"PTSans-NarrowBold" size:18]];
        [head_view addSubview:title2];
     return head_view;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section==0)
    {
        return 0;
    }
    return 90;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0)
    {
        static NSString *simpleTableIdentifier = @"OP_Cell0";
        OP_Cell0  *cell = (OP_Cell0 *)[_table dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"OP_Cell0" owner:self options:nil];
            cell = (OP_Cell0 *)[nib objectAtIndex:0];
        }
        cell.order__id.text=[NSString stringWithFormat:@"Order Id:#%@",_order_id ];
        NSDictionary *s_name_dict=[Sellers_dict valueForKey:@"seller"];
        NSString *order_status=[NSString stringWithFormat:@"Order Status: %@",[[s_name_dict valueForKey:@"order_status"]objectAtIndex:0] ];
        if ([order_status isEqualToString:@"Order Status: (null)"]) {
            cell.order_status.text = @"Order Status: No orders Found";
        }
        else
        cell.order_status.text=order_status;
        
        return cell;
    }
    else
    {
        NSDictionary *products=[[Sellers_dict valueForKey:@"products"]objectAtIndex:indexPath.section-1];
        
        if (indexPath.row<products.count)
        {
            static NSString *simpleTableIdentifier = @"OP_Cell_2_pending";
            OP_Cell_2_pending  *cell = (OP_Cell_2_pending *)[_table dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"OP_Cell_2_pending" owner:self options:nil];
                cell = (OP_Cell_2_pending *)[nib objectAtIndex:0];
            }
            NSString *product_name=[[products valueForKey:@"name"]objectAtIndex:indexPath.row];
            NSString *product_grade=[[products valueForKey:@"grade"]objectAtIndex:indexPath.row];
            NSString *product_brand=[[products valueForKey:@"brand"]objectAtIndex:indexPath.row];
            NSString *product_qty=[[products valueForKey:@"qty"]objectAtIndex:indexPath.row];

            cell.product_name.text=[NSString stringWithFormat:@"%@", product_name];
            cell.grade.text=[NSString stringWithFormat:@"%@", product_grade];
            cell.brand.text=[NSString stringWithFormat:@"%@", product_brand];
            cell.quantity.text=[NSString stringWithFormat:@"%@", product_qty];
            
            if ([product_name isEqualToString:@"  MS Binding Wire"])
            {
                cell.brand.hidden=YES;
                cell.grade.hidden=YES;
            }
            
            return cell;
        }
        else
        {
        static NSString *simpleTableIdentifier = @"OP_Cell_2_invoice";
        OP_Cell_2_invoice  *cell = (OP_Cell_2_invoice *)[_table dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"OP_Cell_2_invoice" owner:self options:nil];
            cell = (OP_Cell_2_invoice *)[nib objectAtIndex:0];
        }
            NSDictionary *comments=[[Sellers_dict valueForKey:@"comments"]objectAtIndex:indexPath.section-1];
            int c_int=indexPath.row-products.count;
            NSString *place=[[comments valueForKey:@"place"]objectAtIndex:c_int];
            NSString *comment=[[comments valueForKey:@"comment"]objectAtIndex:c_int];
            NSString *status=[[comments valueForKey:@"status"]objectAtIndex:c_int];
            NSString *date=[[comments valueForKey:@"date"]objectAtIndex:c_int];
            NSString *time=[[comments valueForKey:@"time"]objectAtIndex:c_int];

            cell.location.text=[NSString stringWithFormat:@"%@", place];
            cell.details.text=[NSString stringWithFormat:@"%@", comment];
            cell.status.text=[NSString stringWithFormat:@"%@", status];
            cell.date.text=[NSString stringWithFormat:@"%@", date];
            cell.time.text=[NSString stringWithFormat:@"%@", time];

        return cell;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0)
    {
        return 60;
    }
    else
    {
        NSDictionary *products=[[Sellers_dict valueForKey:@"products"]objectAtIndex:indexPath.section-1];
        
        if (indexPath.row<products.count)
        {
            return 170;
        }
        else
            return 210;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
