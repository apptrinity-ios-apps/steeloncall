//
//  Compare_Landscape_cell.h
//  SteelonCall
//
//  Created by INDOBYTES on 31/07/17.
//  Copyright © 2017 com.indobytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Compare_Landscape_cell : UITableViewCell
//tata
@property (strong, nonatomic) IBOutlet UILabel *tataPriceLbl;
@property (strong, nonatomic) IBOutlet UILabel *tataFinalPriceLbl;
@property (strong, nonatomic) IBOutlet UILabel *tataSellerNameLbl;
//vizag
@property (strong, nonatomic) IBOutlet UILabel *vizagPriceLbl;
@property (strong, nonatomic) IBOutlet UILabel *vizagFinalPriceLbl;
@property (strong, nonatomic) IBOutlet UILabel *VizagSellerNameLbl;

//jsw
@property (strong, nonatomic) IBOutlet UILabel *jswPriceLbl;
@property (strong, nonatomic) IBOutlet UILabel *jswFinalPriceLbl;
@property (strong, nonatomic) IBOutlet UILabel *jswSellerNameLbl;

@property (strong, nonatomic) IBOutlet UILabel *per_ton3;
@property (strong, nonatomic) IBOutlet UILabel *estimated3;
@property (strong, nonatomic) IBOutlet UILabel *sellername3;


@property (strong, nonatomic) IBOutlet UILabel *per_ton;
@property (strong, nonatomic) IBOutlet UILabel *estimated;
@property (strong, nonatomic) IBOutlet UILabel *sellername;

@property (strong, nonatomic) IBOutlet UILabel *per_ton1;
@property (strong, nonatomic) IBOutlet UILabel *estimated1;
@property (strong, nonatomic) IBOutlet UILabel *sellername1;

@property (strong, nonatomic) IBOutlet UILabel *per_ton2;
@property (strong, nonatomic) IBOutlet UILabel *estimated2;
@property (strong, nonatomic) IBOutlet UILabel *sellername2;
//compare
@property (strong, nonatomic) IBOutlet UILabel *comparePriceLbl;
@property (strong, nonatomic) IBOutlet UILabel *compareFinalPriceLbl;
@property (strong, nonatomic) IBOutlet UILabel *compareCellarNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *tonnes;
@property (strong, nonatomic) IBOutlet UILabel *pieces;

@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UIImageView *img;



@end
