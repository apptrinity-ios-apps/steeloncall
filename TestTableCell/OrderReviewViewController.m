//
//  OrderReviewViewController.m
//  steelonCallThree
//
//  Created by nagaraj  kumar on 02/12/16.
//  Copyright © 2016 nagaraj  kumar. All rights reserved.
//

#import "OrderReviewViewController.h"
#import "OrderReview_HeadingTableViewCell.h"
#import "OrderReview_DetailsTableViewCell.h"
#import "OrderReview_AddressTableViewCell.h"
#import "OrderReview_AmountTableViewCell.h"
#import "PaymentType_ViewController.h"
#import "LoginViewController.h"
#import "My_Cart.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NewLoginVC.h"

@interface OrderReviewViewController ()
{
    NSMutableArray *quoteArr;
    NSMutableArray *sellers_info;
    NSMutableArray *shipping_address;
    NSMutableArray *seller_address;
    NSMutableArray *productsArr;
    NSMutableString *shipping_address1;
    NSMutableString * seller_address1;
    NSString *method;
    
    NSString *total_distance;
    NSString *total_price;
    NSString *total_tons;
}


@end

@implementation OrderReviewViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    igst=@"1";
    // Do any additional setup after loading the view from its nib.
     self.automaticallyAdjustsScrollViewInsets = NO;
    _orderRecordTableview.dataSource = self;
    _orderRecordTableview.delegate = self;
    check_tag=@"0";

    _orderRecordTableview.hidden = YES;
    _customerShpngView.hidden =YES;
    
     quoteArr = [[NSMutableArray alloc]init];
     sellers_info = [[NSMutableArray alloc]init];
     shipping_address = [[NSMutableArray alloc]init];
     seller_address = [[NSMutableArray alloc]init];
    productsArr = [[NSMutableArray alloc]init];
    
    user_data=[NSUserDefaults standardUserDefaults];
    user_id=[user_data valueForKey:@"user_id"];
    
    method = @"";// selection type for shipping

    // servic
    NSString *url_form=[NSString stringWithFormat:@"getShippingAndHandling?customer_id=%@",user_id];
   // NSString *respo = [self getDataFrom:url_form];
   // NSLog(@"%@",respo);
    
    [[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString:url_form requestNumber:Shipping_Handling showProgress:YES withHandler:^(BOOL success, id data)
     {
         NSDictionary *billingAddressService=data;
         
         if (success)
         {
             quoteArr =[billingAddressService valueForKey:@"quote"];
             sellers_info =  [billingAddressService valueForKey:@"sellers_info"];
            productsArr = [[billingAddressService valueForKey:@"sellers_info"] valueForKey:@"products"];
             CST=[NSString stringWithFormat:@"%@",[[quoteArr objectAtIndex:0] valueForKey:@"cst"]];
             for(NSArray *subArray in productsArr)
             {
                 NSLog(@"Array in myArray: %@",subArray);
             }
            
             shipping_address = [sellers_info valueForKey:@"shipping_address" ] ;
             seller_address = [sellers_info valueForKey:@"seller_address" ] ;
             [_orderRecordTableview reloadData];
             
             [self steeloncall];

         }
         else
         {
         }
     }];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
    {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    user_data=[NSUserDefaults standardUserDefaults];
    NSString *C_Count=[user_data valueForKey:@"cart_count"];
    _cart_CountLbl.text=C_Count;
}

//- (NSString *) getDataFrom:(NSString *)url{
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
//    [request setHTTPMethod:@"GET"];
//    [request setURL:[NSURL URLWithString:url]];
//    
//    NSError *error = [[NSError alloc] init];
//    NSHTTPURLResponse *responseCode = nil;
//    
//    NSData *oResponseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&responseCode error:&error];
//    
//    
//    if([responseCode statusCode] != 200){
//        NSLog(@"Error getting %@, HTTP status code %li", url, (long)[responseCode statusCode]);
//        return nil;
//    }
//    
//    
//    
//    return [[NSString alloc] initWithData:oResponseData encoding:NSUTF8StringEncoding];
//}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden =YES;
   // self.automaticallyAdjustsScrollViewInsets = NO;
    user_data=[NSUserDefaults standardUserDefaults];
    NSString *C_Count=[user_data valueForKey:@"cart_count"];
    if ([C_Count isEqual:[NSNull null]]||[C_Count isEqualToString:@""]||C_Count ==nil||[C_Count isEqualToString:@"<nil>"])
    {
        _cart_CountLbl.text=@"0";
        user_data=[NSUserDefaults standardUserDefaults];
        [user_data setValue:@"0" forKey:@"cart_count"];
    }
    else
        _cart_CountLbl.text=C_Count;
    
    _cart_BackgrndView.layer.cornerRadius = 10;
    
    //tap gesture
    UITapGestureRecognizer *cart_tap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(cart_Click_Actn:)];
    [_cart_BackgrndView addGestureRecognizer:cart_tap];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (sellers_info.count==0)
    {
        return 0;
    }
    else
        return [sellers_info count]+1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    //base View
    if (section == sellers_info.count) {
        return nil;
    }
    else
    {
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, self.view.frame.size.width, 30)];
        title.text =[NSString stringWithFormat:@"      Delivery%ld",(long)section+1];
        title.textColor = [UIColor colorWithRed:10/255.0 green:21/255.0 blue:80/255.0 alpha:1.0];
       // title.backgroundColor=[UIColor colorWithRed:10/255.0 green:21/255.0 blue:80/255.0 alpha:1.0];
        title.textAlignment=NSTextAlignmentLeft;
        [title setFont:[UIFont fontWithName:@"Roboto-Bold" size:15]];
        return title;
    }
 //   return nil;
//    UIView *vi=[[UIView alloc]initWithFrame:CGRectMake(0,0,[UIScreen mainScreen].bounds.size.width, 44)];
//    
//    UILabel *prdctLbl = [[UILabel alloc]initWithFrame:CGRectMake(8, 0, ([UIScreen mainScreen].bounds.size.width-16)/2-1, vi.frame.size.height)];
//    prdctLbl.text = @"Product";
//    prdctLbl.textColor =   [UIColor whiteColor];
//    prdctLbl.backgroundColor = [UIColor colorWithRed:44/255.0f green:52/255.0f blue:75/255.0f alpha:1.0f];
//    prdctLbl.textAlignment = NSTextAlignmentCenter;
//    
//    vi.backgroundColor = [UIColor whiteColor];
//    [vi addSubview:prdctLbl];
//    
//    UILabel *QntyLbl = [[UILabel alloc]initWithFrame:CGRectMake(prdctLbl.frame.origin.x+prdctLbl.frame.size.width+1, 0, prdctLbl.frame.size.width, vi.frame.size.height)];
//    QntyLbl.text = @"Quantity";
//    QntyLbl.textColor = [UIColor whiteColor];
//    QntyLbl.backgroundColor = [UIColor colorWithRed:44/255.0f green:52/255.0f blue:75/255.0f alpha:1.0f];
//    QntyLbl.textAlignment = NSTextAlignmentCenter;
//    
//    vi.backgroundColor = [UIColor whiteColor];
//    [vi addSubview:QntyLbl];
//    return vi;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (sellers_info.count==0)
    {
        return 0;
    }
    else
    {
        if ([sellers_info count] == section )
        {
            return 1;
        }
        else
        {
        return [[[sellers_info objectAtIndex:section] valueForKey:@"products"] count]+1;
        }
    }
}

- (IBAction)cst_click:(id)sender
{
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:cst_cell_tag inSection:[sellers_info count]];
    OrderReview_AmountTableViewCell *tappedCell = (OrderReview_AmountTableViewCell *)[_orderRecordTableview cellForRowAtIndexPath:indexpath];

    if ([check_tag isEqualToString:@"0"])
    {
        check_tag=@"1";
        [tappedCell.check_box setImage:[UIImage imageNamed:@"Radio_select1"] forState:UIControlStateNormal];
    }
    else
    {
        check_tag=@"0";
        [tappedCell.check_box setImage:[UIImage imageNamed:@"Radio_Deselect1"] forState:UIControlStateNormal];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([sellers_info count]>0)
    {
       if ([sellers_info count] == indexPath.section )
       {
           cst_cell_tag=indexPath.row;
           static NSString *simpleTableIdentifier = @"cellamount";
           OrderReview_AmountTableViewCell *cell = (OrderReview_AmountTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
           
           if (cell == nil)
           {
               NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"OrderReview_AmountTableViewCell" owner:self options:nil];
               cell = (OrderReview_AmountTableViewCell *)[nib objectAtIndex:0];
               
               [cell.continueBtnOutlet addTarget:self action:@selector(continuePayment) forControlEvents:UIControlEventTouchUpInside];
           }
           
           if (![CST isEqualToString:@"1"])
           {
               cell.cst_view.hidden=YES;
           }
           else
               cell.cst_view.hidden=NO;

           [cell.check_box addTarget:self action:@selector(cst_click:) forControlEvents:UIControlEventTouchUpInside];
           
           cell.shipping_Amount_Lbl.text =[NSString stringWithFormat:@"Rs. %@",[[quoteArr objectAtIndex:0] valueForKey:@"total_amount"]];
           cell.total_Hndling_Price_Lbl.text =[NSString stringWithFormat:@"Rs. %@",[[quoteArr objectAtIndex:0] valueForKey:@"handling_price"]];
           cell.shippnig_Handling_Charges.text =[NSString stringWithFormat:@"Rs. %@",[[quoteArr objectAtIndex:0] valueForKey:@"shipping_handling_price"]];
           cell.selectionStyle=UITableViewCellSelectionStyleNone;
           
           return cell;

       }
       else
       {
        NSLog(@"%ld : %ld  : %ld",(long)indexPath.section,(long)indexPath.row,[[[sellers_info objectAtIndex:indexPath.section] valueForKey:@"products"] count]);

    if ([[[sellers_info objectAtIndex:indexPath.section] valueForKey:@"products"] count] == indexPath.row) {
        
        static NSString *simpleTableIdentifier = @"celladdressdetails";
        OrderReview_AddressTableViewCell *cell = (OrderReview_AddressTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"OrderReview_AddressTableViewCell" owner:self options:nil];
            cell = (OrderReview_AddressTableViewCell *)[nib objectAtIndex:0];
        }
        //seller_address
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        cell.seller_NameLbl.text = [[[[sellers_info objectAtIndex:indexPath.section] valueForKey:@"seller_address"] objectAtIndex:0] valueForKey:@"name"];
        
        NSString *street = [[[[sellers_info objectAtIndex:indexPath.section] valueForKey:@"seller_address"] objectAtIndex:0] valueForKey:@"street"];
        NSString *city = [[[[sellers_info objectAtIndex:indexPath.section] valueForKey:@"seller_address"] objectAtIndex:0] valueForKey:@"city"];
        NSString *pincode = [[[[sellers_info objectAtIndex:indexPath.section] valueForKey:@"seller_address"] objectAtIndex:0] valueForKey:@"pincode"];
        NSString *flat = [[[[sellers_info objectAtIndex:indexPath.section] valueForKey:@"seller_address"] objectAtIndex:0] valueForKey:@"flat"];

        seller_address1 = [@"" mutableCopy];
        
        NSMutableString *sellerAddress;
         sellerAddress = [@"" mutableCopy];
        if ([flat isEqual:[NSNull null] ]||[flat isEqualToString:@"<null>"]||[flat isEqualToString:@""])
        {
            flat = @"";
            NSLog(@"null");
        }
        else
        {
            
            sellerAddress =  [[sellerAddress stringByAppendingString:flat] mutableCopy];
           
//            flat=[NSString stringWithFormat:@"%@",flat];
//            seller_address1 = [[seller_address1  stringByAppendingString:flat] mutableCopy];
        }
        if ([street isEqual:[NSNull null] ]||[street isEqualToString:@"<null>"]||[street isEqualToString:@""])
        {
            street = @"";
        }
        else
        {
         
            
            sellerAddress = [[sellerAddress stringByAppendingString:street] mutableCopy];
//            street=[NSString stringWithFormat:@",%@",street];
//            seller_address1 = [[seller_address1  stringByAppendingString:street] mutableCopy];
        }
        
        if ([city isEqual:[NSNull null] ]||[city isEqualToString:@"<null>"]||[city isEqualToString:@""])
        {
           // cell.sellerCity.text = @"";
            city =  @"";
        }
        else
        {
          //  cell.sellerStreet.text = city;
            city=[NSString stringWithFormat:@"%@",city];

//            seller_address1 = [[sellerAddress stringByAppendingString:city] mutableCopy];
        }
         NSString *seller_state=[NSString stringWithFormat:@"%@",[[[[sellers_info objectAtIndex:indexPath.section] valueForKey:@"seller_address"] objectAtIndex:0] valueForKey:@"state"]];
        if ([seller_state isEqual:[NSNull null] ]||[seller_state isEqualToString:@"<null>"]||[seller_state isEqualToString:@""])
        {
          //  cell.sellerState.text = city;
            seller_state = @"";
             sellerAddress = [[sellerAddress stringByAppendingString:city] mutableCopy];
        }
        else
        {
            
          //  cell.sellerState.text = seller_state;
            seller_state=[NSString stringWithFormat:@",%@",seller_state];
            
            

//           seller_address1=[[seller_address1 stringByAppendingString:seller_state]mutableCopy] ;
//            cell.sellerState.text = seller_address1;
        }
        if ([pincode isEqual:[NSNull null] ]||[pincode isEqualToString:@"<null>"]||[pincode isEqualToString:@""])
        {
            pincode = @"";
            NSLog(@"null");
        }
        else
        {
            //cell.sellerZipcode.text = pincode;
            
          
            
            [[sellerAddress stringByAppendingString:pincode] mutableCopy];
            
//            pincode=[NSString stringWithFormat:@",%@",pincode];
//
//            seller_address1 = [[seller_address1 stringByAppendingString:pincode] mutableCopy];
        }
        NSArray *myStrings = [[NSArray alloc] initWithObjects:flat, street, city, seller_state, pincode, nil];
        NSString *joinedString = [myStrings componentsJoinedByString:@","];
      //  cell.sellar_AddressTXtView.text = seller_address1;
        cell.sellerAddressLabel.text = joinedString;
        
        
        NSMutableString *shippingAddress;
        
        shippingAddress = [@"" mutableCopy];
        shipping_address1 = [@"" mutableCopy];
        NSString *f_name=[[[[sellers_info objectAtIndex:indexPath.section] valueForKey:@"shipping_address"] objectAtIndex:0] valueForKey:@"firstname"];
        
        
        if ([f_name isEqual:[NSNull null] ]||[f_name isEqualToString:@"<null>"]||[f_name isEqualToString:@""])
        {
            NSLog(@"null");
             f_name = @"";
        }
        else
        {
            cell.shipping_NameLbl.text = [NSString stringWithFormat:@"%@ %@",[[[[sellers_info objectAtIndex:indexPath.section] valueForKey:@"shipping_address"] objectAtIndex:0] valueForKey:@"firstname"],[[[[sellers_info objectAtIndex:indexPath.section] valueForKey:@"shipping_address"] objectAtIndex:0] valueForKey:@"lastname"]];
            
        }
        
        
        NSLog(@"%@",[NSString stringWithFormat:@"%@",[[[[sellers_info objectAtIndex:indexPath.section] valueForKey:@"shipping_address"] objectAtIndex:0] valueForKey:@"street1"]]);
        NSString *street1=[NSString stringWithFormat:@"%@",[[[[sellers_info objectAtIndex:indexPath.section] valueForKey:@"shipping_address"] objectAtIndex:0] valueForKey:@"street1"]];
        NSString *street2=[NSString stringWithFormat:@"%@",[[[[sellers_info objectAtIndex:indexPath.section] valueForKey:@"shipping_address"] objectAtIndex:0] valueForKey:@"street2"]];
        
        if ([street1 isEqual:[NSNull null] ]||[street1 isEqualToString:@"<null>"]||[street1 isEqualToString:@""])
        {
          //  cell.shippingStreet.text = @"";
            street1 = @"";
            NSLog(@"null");
        }
        else
        {
             shippingAddress = [[shippingAddress stringByAppendingString:street1] mutableCopy];
            
//            street1=[NSString stringWithFormat:@",%@",street1];
//
//            shipping_address1=[[shipping_address1 stringByAppendingString:street1]mutableCopy] ;
        }
        
//        if ([street2 isEqual:[NSNull null] ]||[street2 isEqualToString:@"<null>"]||[street2 isEqualToString:@""])
//        {
//            
//            NSLog(@"null");
//        }
//        else
//        {
//            street2=[NSString stringWithFormat:@",%@",street2];
//
//            shipping_address1=[[shipping_address1 stringByAppendingString:street2] mutableCopy];
//        }
        
        NSString *city_str=[[[[sellers_info objectAtIndex:indexPath.section] valueForKey:@"shipping_address"] objectAtIndex:0] valueForKey:@"city"];
        NSString *district_str=[[[[sellers_info objectAtIndex:indexPath.section] valueForKey:@"shipping_address"] objectAtIndex:0] valueForKey:@"district"];
        NSString *region_str=[[[[sellers_info objectAtIndex:indexPath.section] valueForKey:@"shipping_address"] objectAtIndex:0] valueForKey:@"region"];
        NSString *postcode_str=[[[[sellers_info objectAtIndex:indexPath.section] valueForKey:@"shipping_address"] objectAtIndex:0] valueForKey:@"postcode"];
        
//        if (indexPath.row==0)
//        {
//            if ([region_str isEqualToString:seller_state])
//            {
//                igst=@"0";
//            }
//        }
        
        if ([city_str isEqual:[NSNull null] ]||[city_str isEqualToString:@"<null>"]||[city_str isEqualToString:@""])
        {
           // cell.shippingCity.text = @"";
            NSLog(@"null");
            city_str = @"";
        }
        else
        {
            
             shippingAddress =  [[shippingAddress stringByAppendingString:city_str] mutableCopy];
         //   cell.shippingCity.text = city_str;
//            city_str=[NSString stringWithFormat:@",%@",city_str];
//
//            shipping_address1= [[shipping_address1 stringByAppendingString:city_str] mutableCopy];
        }
        
        if ([district_str isEqual:[NSNull null] ]||[district_str isEqualToString:@"<null>"]||[district_str isEqualToString:@""])
        {
            district_str = @"";
            NSLog(@"null");
        }
        else
        {
            district_str=[NSString stringWithFormat:@"%@",district_str];

            shipping_address1=[[shipping_address1 stringByAppendingString:district_str]mutableCopy];
        }
        if ([region_str isEqual:[NSNull null] ]||[region_str isEqualToString:@"<null>"]||[region_str isEqualToString:@""])
        {
            // cell.shippingState.text = shipping_address1;;
            region_str = @"";
            NSLog(@"null");
        }
        else
        {
            region_str=[NSString stringWithFormat:@",%@",region_str];

            shipping_address1=[[shipping_address1 stringByAppendingString:region_str]mutableCopy];
           // cell.shippingState.text = shipping_address1;
        }
        
        if ([postcode_str isEqual:[NSNull null] ]||[postcode_str isEqualToString:@"<null>"]||[postcode_str isEqualToString:@""])
        {
           // cell.shippingZipcode.text = @"";
            postcode_str = @"";
            NSLog(@"null");
        }
        else
        {
            
            shippingAddress =  [[shippingAddress stringByAppendingString:postcode_str] mutableCopy];
          // cell.shippingZipcode.text = postcode_str;
//            postcode_str=[NSString stringWithFormat:@",%@",postcode_str];
//
//            shipping_address1=[[shipping_address1 stringByAppendingString:postcode_str]mutableCopy] ;
        }
        
        NSArray *myStrings1 = [[NSArray alloc] initWithObjects:f_name, street1,city_str ,district_str, region_str, postcode_str, nil];
        NSString *joinedString1 = [myStrings1 componentsJoinedByString:@","];
        
        cell.shippingAddressLabel.text = joinedString1;
        
        cell.distanceLbl.text = [NSString stringWithFormat:@"%@",[[sellers_info objectAtIndex:indexPath.section] valueForKey:@"total_distance" ]];
        cell.shipping_AmontLbl.text = [NSString stringWithFormat:@"Rs.%@",[[sellers_info objectAtIndex:indexPath.section] valueForKey:@"total_price" ]];
        cell.Handling_charges.text = [NSString stringWithFormat:@"Rs.%@",[[sellers_info objectAtIndex:indexPath.section] valueForKey:@"seller_handling_price" ]];
        //cell.distanceLbl.text = [NSString stringWithFormat:@"%@",[[sellers_info objectAtIndex:indexPath.section] valueForKey:@"total_tons"]];
        
     //   cell.Shipping_AddressTxtView.text = shipping_address1;
        
        return cell;
    }
    else
    {
        static NSString *simpleTableIdentifier = @"cellorderdetails";
        OrderReview_DetailsTableViewCell *cell = (OrderReview_DetailsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"OrderReview_DetailsTableViewCell" owner:self options:nil];
            cell = (OrderReview_DetailsTableViewCell *)[nib objectAtIndex:0];
        }
        
           // if (indexPath.row!=0) {
                
              //  for (int i = 1; i<[[[sellers_info objectAtIndex:indexPath.section] valueForKey:@"products"] count]; i++) {
        NSLog(@" >>> %ld : %ld",(long)indexPath.section,(long)indexPath.row);

         if (indexPath.row < [[[sellers_info objectAtIndex:indexPath.section] valueForKey:@"products"] count] )
         {
             
//             [cell.view_border.layer setBorderColor:[UIColor blackColor].CGColor];
//             [cell.view_border.layer setBorderWidth:1.0f];
             
                    cell.order_title_lbl.text = [[[[sellers_info objectAtIndex:indexPath.section] valueForKey:@"products"] objectAtIndex:indexPath.row] valueForKey:@"name"];
             
              cell.orderTitleLabel1.text = [[[[sellers_info objectAtIndex:indexPath.section] valueForKey:@"products"] objectAtIndex:indexPath.row] valueForKey:@"name"];
                    
                    cell.Tons_Lbl.text =[NSString stringWithFormat:@"%@",[[[[sellers_info objectAtIndex:indexPath.section] valueForKey:@"products"] objectAtIndex:indexPath.row] valueForKey:@"tons"]] ;
                    
                    
                    cell.pices_Lbl.text =[NSString stringWithFormat:@"%d",[[[[[sellers_info objectAtIndex:indexPath.section] valueForKey:@"products"] objectAtIndex:indexPath.row] valueForKey:@"piece"] intValue]];
             cell.gradeLbl.text = [NSString stringWithFormat:@"%@",[[[[sellers_info objectAtIndex:indexPath.section] valueForKey:@"products"] objectAtIndex:indexPath.row] valueForKey:@"grade"] ];
             
             cell.brandLbl.text = [NSString stringWithFormat:@"%@",[[[[sellers_info objectAtIndex:indexPath.section] valueForKey:@"products"] objectAtIndex:indexPath.row] valueForKey:@"brand"] ];
             
             SDWebImageManager *manager = [SDWebImageManager sharedManager];
             
             NSURL *ImageUrl = [NSURL URLWithString:[[[[sellers_info objectAtIndex:indexPath.section] valueForKey:@"products"] objectAtIndex:indexPath.row] valueForKey:@"img"]];
             
             [manager downloadImageWithURL:ImageUrl options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize)
              {
              } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                  
                  if(image)
                  {
                      cell.order_Image.image = image;
                      NSLog(@"image=====%@",image);
                  }
              }];
           
         }
             //   }
        
          //  }
       
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        return cell;
    
       }
       }
    }else{
        
                    static NSString *simpleTableIdentifier = @"cellheading";
                    OrderReview_HeadingTableViewCell *cell = (OrderReview_HeadingTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
                    if (cell == nil)
                    {
                        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"OrderReview_HeadingTableViewCell" owner:self options:nil];
                        cell = (OrderReview_HeadingTableViewCell *)[nib objectAtIndex:0];
                    }
                        cell.selectionStyle=UITableViewCellSelectionStyleNone;
                    
                    
                    
                    return cell;

    }
  

   
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    //    UIViewController *billController = [[BillingInfoViewController alloc] initWithNibName:@"BillingInfoViewController" bundle:nil];
    //
    //    [self.navigationController pushViewController:billController animated:YES];
    
    //nothing to do now
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
   if (section == sellers_info.count )
    {
        return 0;
    }
   else
    {
          return 40;// 44
    }
//    
//    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
if ([sellers_info count]>0)
    {
         if ([sellers_info count] == indexPath.section )
         {
              return 191;
         }
         else
         {
        if ([[[sellers_info objectAtIndex:indexPath.section] valueForKey:@"products"] count] == indexPath.row)
        {
            return 400;//273
        }
        else
        {
            return 205;
        }
         }
    }else{
      return  167;
    }
    
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if ([scrollView isKindOfClass:[_orderRecordTableview class]]) {
        
        
        CGFloat sectionHeaderHeight = 44;
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
    }
}

-(void)shipping_method
{
    NSString *shipping_handling_price;
    
    /*
     {
     "handling_price" = "342.8";
     igst = 0;
     "quote_total_price" = 925;
     "quote_total_tons" = "1.714";
     "shipping_handling_price" = "1267.8";
     "total_amount" = 925;
     }
     */
    
    NSString *shipping_price,*handling_price;
    if ([method isEqualToString:@"2"])
    {
        shipping_price = [NSString stringWithFormat:@"%@", [ [quoteArr objectAtIndex:0]   valueForKey:@"quote_total_price"]];
        handling_price = [NSString stringWithFormat:@"%@", [ [quoteArr objectAtIndex:0]   valueForKey:@"handling_price"]];
    }
    else
    {
    shipping_handling_price = [NSString stringWithFormat:@"%@", [ [quoteArr objectAtIndex:0]   valueForKey:@"handling_price"]];
    }
    
     NSString *url_form=[NSString stringWithFormat:@"saveShippingMethod?customer_id=%@&amount=%@&handling_amount=%@&method=%@&igst=%@",user_id,shipping_price,handling_price,method, [ [quoteArr objectAtIndex:0]   valueForKey:@"igst"]];
    
    [[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString: url_form requestNumber:Shipping_Method showProgress:YES withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             NSDictionary *res_dict=data;
             NSString *response=[NSString stringWithFormat:@"%@",[res_dict valueForKey:@"response"]];
             if ([response isEqualToString:@"1"])
             {
                 PaymentType_ViewController *paymentType_ViewController = [[PaymentType_ViewController alloc] initWithNibName:@"PaymentType_ViewController" bundle:nil];
                 
                 paymentType_ViewController.shippingAddress = shipping_address;
                 paymentType_ViewController.methodType = method;
                 [self.navigationController pushViewController:paymentType_ViewController animated:YES];
             }
             else
             {
                 ALERT_DIALOG(@"Alert",@"Error");
             }
         }
         else
         {
              ALERT_DIALOG(@"Alert",@"Something went wrong please try again..!");
         }
     }];
}

-(void)continuePayment
{
    [self shipping_method];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtnActn:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)cart_Click_Actn:(id)sender {
    user_data=[NSUserDefaults standardUserDefaults];
    user_id=[user_data valueForKey:@"user_id"];
    
    if ([user_id isEqualToString:@""]||[user_id isEqual:[NSNull null]]||[user_id isEqualToString:@"<nil>"]||user_id == nil||[user_id isEqualToString:@"0"])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        NewLoginVC *myNewVC = (NewLoginVC *)[storyboard instantiateViewControllerWithIdentifier:@"NewLoginVC"];
        [self.navigationController pushViewController:myNewVC animated:YES];
    }
    else//go to cart page
    {
        My_Cart *define = [[My_Cart alloc]init];
        [self.navigationController pushViewController:define animated:YES];
    }
}
- (IBAction)customerShipping_BtnActn:(id)sender {
    
    _orderRecordTableview.hidden = YES;
    _customerShpngView.hidden =NO;
    
    method= @"1";
    
    [_CustShippingBtn setImage:[UIImage imageNamed:@"Radio_select"] forState:UIControlStateNormal];
    [_steelOnShippingBtn setImage:[UIImage imageNamed:@"Radio_Deselect"] forState:UIControlStateNormal];
}

- (IBAction)steelonCallShipping_BtnActn:(id)sender {
    [self steeloncall];
   
}
-(void)steeloncall
{
    _orderRecordTableview.hidden = NO;
    _customerShpngView.hidden =YES;
    
    method= @"2";
    [_CustShippingBtn setImage:[UIImage imageNamed:@"Radio_Deselect"] forState:UIControlStateNormal];
    [_steelOnShippingBtn setImage:[UIImage imageNamed:@"Radio_select"] forState:UIControlStateNormal];
}
- (IBAction)customer_Shpng_BtnActn:(id)sender {
    
    [self shipping_method];
}

- (IBAction)goToHomeScreenAction:(id)sender {
    HomeViewController *con = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
    [self.navigationController pushViewController:con animated:YES];
    
    
}

@end
