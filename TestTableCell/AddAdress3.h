//
//  AddAdress3.h
//  SteelonCall
//
//  Created by S s Vali on 9/12/17.
//  Copyright © 2017 com.indobytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddAdress3 : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *backBtn;
@property (strong, nonatomic) IBOutlet UIButton *saveAddrsBtn;
@property (strong, nonatomic) IBOutlet UIButton *defalutBillingadrsBtn;
@property (strong, nonatomic) IBOutlet UIButton *defaultShippingAdrsBtn;

@end
