//
//  AccountManageViewController.m
//  SteelonCall
//
//  Created by Manoyadav on 05/12/16.
//  Copyright © 2016 com.way2online. All rights reserved.
//

#import "AccountManageViewController.h"
#import "URLS.h"
#import "LoginViewController.h"
#import "STParsing.h"
@interface AccountManageViewController ()

@end

@implementation AccountManageViewController

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

-(void)call_tonnes_service
{
    NSUserDefaults *user_data=[NSUserDefaults standardUserDefaults];
    NSString *user_id=[user_data valueForKey:@"user_id"];
    
  //  NSString *url_form=[NSString stringWithFormat:@"https://steeloncall.com/calculate/userservice/getCustomerAdressList/?user_id=%@",user_id];
     NSString *url_form=[NSString stringWithFormat:@"http://stg.steeloncall.com/calculate/userservice/getCustomerAdressList/?user_id=%@",user_id];
    
    // making a GET request to /init
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:url_form]];
    
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
      ^(NSData * _Nullable data,
        NSURLResponse * _Nullable response,
        NSError * _Nullable error)
    {
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                             options:kNilOptions
                                                               error:&error];
        
        default_ary = [json objectForKey:@"default"];
        additional_ary = [json objectForKey:@"additional"];
        
        
        

     
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [_add_table reloadData];
            _addressCount_Lbl.text = [NSString stringWithFormat:@"Added %lu Default Addresses",(unsigned long)default_ary.count];
            
        });
        
        
        
    }] resume];
}
     
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //notifications
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    _accountMAnaheConstraint.constant = 0;
    _manageAddressConstraint.constant =0;
    _accountSettings_view.hidden =YES;
    _addressView.hidden =YES;
    _mailBtn.hidden =YES;
    _passBtn.hidden =YES;
    keyboardIsShown =NO;
    
    [self.view layoutIfNeeded];
    
    
    for (UITextField *field in _changeAccountFields) {
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 5, 20)];
        field.leftView = paddingView;
        field.leftViewMode = UITextFieldViewModeAlways;

//        field.layer.cornerRadius = 5;
//        
//        field.layer.masksToBounds =YES;
        field.layer.borderColor =  [UIColor colorWithRed:164/255.0 green:164/255.0f blue:164/255.0f alpha:1.0f].CGColor;
        field.layer.backgroundColor = [UIColor whiteColor].CGColor;
        field.layer.borderWidth =1;
    }
    
    for (UITextField *field in _manageAddressFields) {
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 5, 20)];
        field.leftView = paddingView;
        field.leftViewMode = UITextFieldViewModeAlways;

//        field.layer.cornerRadius = 5;
//        
//        field.layer.masksToBounds =YES;
        field.layer.borderColor = [UIColor colorWithRed:164/255.0 green:164/255.0f blue:164/255.0f alpha:1.0f].CGColor;
         field.layer.backgroundColor = [UIColor whiteColor].CGColor;
        field.layer.borderWidth =0.5;
    }
    
   // _cart_view.layer.cornerRadius = 10;
    
    //tap gesture
    UITapGestureRecognizer *cart_tap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(Cart_click:)];
    [_cart_view addGestureRecognizer:cart_tap];

   NSUserDefaults *user_data=[NSUserDefaults standardUserDefaults];
    NSString *C_Count=[user_data valueForKey:@"cart_count"];
    _Cart_count.text=C_Count;
    
    _add_table.delegate=self;
    _add_table.dataSource=self;
    
    
    _firstName.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0);
    _lastName.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0);
    _streetField.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0);
    _cityField.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0);
    _regionField.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0);
    _pinCodeField.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0);
    
    _mobileNumberField.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0);
    _confirmPasswordField.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0);
    _latestPasswordField.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0);
    _oldPasswordField.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0);
    _emailField.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0);
    
    
    
    [self getUserData];
}

-(void)viewDidAppear:(BOOL)animated
{
    scrollHeight = _scrollView.frame.size.height;
}

-(void)getUserData{

NSString *user_id =[[ NSUserDefaults standardUserDefaults]valueForKey:@"user_id"];
//73
if (user_id.length>0)
{
    NSString *urlString = [NSString stringWithFormat:@"getcustomer?id=%@",user_id];
    
    
    [[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString:urlString requestNumber:WS_GET_ACCOUNT_INFO showProgress:YES withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             
             if (data) {
                 NSArray *ary = data;
                 if (ary.count>1) {
                     
                 for (UITextField *field in _changeAccountFields) {
                     
                     switch (field.tag) {
                         case 0:{
                             if (![[[data objectAtIndex:0]valueForKey:@"email"] isKindOfClass:[NSNull class]]) {
                                 field.text = [[data objectAtIndex:0]valueForKey:@"email"];
                                 self.accountLbl.text = [[data objectAtIndex:0]valueForKey:@"email"];
                             }
                         }
                             break;
                         case 1:{
                             if (![[[data objectAtIndex:0]valueForKey:@"last_name"] isKindOfClass:[NSNull class]]) {
                                 field.text = [[data objectAtIndex:0]valueForKey:@"last_name"];
                             }
                            }
                             break;
                         case 2:{
                             if (![[[data objectAtIndex:0]valueForKey:@"first_name"] isKindOfClass:[NSNull class]]) {
                                 field.text = [[data objectAtIndex:0]valueForKey:@"first_name"];
                             }
                         }
                             break;
                                                      
                         default:
                             break;
                     }
                 }
                 
                 for (UITextField *field in _manageAddressFields) {
                     
                     
                     switch (field.tag) {
                         case 0:{
                             if (![[[data objectAtIndex:1]valueForKey:@"street"] isKindOfClass:[NSNull class]]) {
                                 field.text = [[data objectAtIndex:1]valueForKey:@"street"];
                             }
                             
                         }
                             
                             break;
                         case 1:{
                             if (![[[data objectAtIndex:1]valueForKey:@"city"] isKindOfClass:[NSNull class]]) {
                                 field.text = [[data objectAtIndex:1]valueForKey:@"city"];
                             }
                         }
                             
                             break;
                         case 2:{
                             if (![[[data objectAtIndex:1]valueForKey:@"region"] isKindOfClass:[NSNull class]]) {
                                 field.text = [[data objectAtIndex:1]valueForKey:@"region"];
                             }

                             
                         }
                             
                             
                             break;
                         case 3:{
                             if (![[[data objectAtIndex:1]valueForKey:@"postcode"] isKindOfClass:[NSNull class]]) {
                                 field.text = [[data objectAtIndex:1]valueForKey:@"postcode"];
                             }
                             
                             
                         }
                             
                             
                             break;
                         case 4:{
                             if (![[[data objectAtIndex:1]valueForKey:@"telephone"] isKindOfClass:[NSNull class]]) {
                                 field.text = [[data objectAtIndex:1]valueForKey:@"telephone"];
                             }
                             
                             
                         }
                             
                             
                             break;
                             
                         default:
                             break;
                     }
                 }

                 
                 
                     
                     
                 }
         }
         
                 
             }else{
                 
             }
     }];
    
    [self call_tonnes_service];

}
    
else
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LoginViewController *myNewVC = (LoginViewController *)[storyboard instantiateViewControllerWithIdentifier:@"LoginViewControllerID"];
    [self.navigationController pushViewController:myNewVC animated:YES];
}

}
//    userorders?id=" + user_id
//    userorders
//    calculate/userservice/orderdetails/?id="+mainOrderId


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)returnExit:(id)sender
{
}

- (IBAction)changeAccountSettingsActionClicked:(UIButton *)sender
{
    [_addressBtn setSelected:NO];

    if (sender.selected) {
        [sender setSelected:NO];
        _accountMAnaheConstraint.constant = 0;
        _manageAddressConstraint.constant =0;
        _accountSettings_view.hidden =NO;
        [self.acountSetingsImg setImage:[UIImage imageNamed:@"right-arrow"]];
        _mailBtn.hidden =YES;
        _passBtn.hidden =YES;
        sender.enabled =NO;
        [UIView animateWithDuration:0.5 animations:^{
            [self.view layoutIfNeeded];
            
        } completion:^(BOOL finished) {
            sender.enabled =YES;
            _addressView.hidden =YES;
            _accountSettings_view.hidden =YES;

        }];

    }else{
        [sender setSelected:YES];
        _accountMAnaheConstraint.constant = 430;
        _manageAddressConstraint.constant =0;
        _accountSettings_view.hidden =NO;
        [self.acountSetingsImg setImage:[UIImage imageNamed:@"down-arrow"]];
        sender.enabled =NO;
        [UIView animateWithDuration:0.5 animations:^{
            [self.view layoutIfNeeded];
            
        } completion:^(BOOL finished) {
            _mailBtn.hidden =NO;
            _passBtn.hidden =NO;
            _addressView.hidden =YES;
            sender.enabled =YES;
        }];
    }
}

- (IBAction)manageAddressClicked:(UIButton *)sender
{
    [_add_table reloadData];
    int add_x=self.address_btn.frame.origin.x;
    int btn_x=self.view.frame.size.height+60-(add_x+30+0);
    _address_height.constant=btn_x;

    [_settingsBtn setSelected:NO];
    
    if (sender.selected) {
        [sender setSelected:NO];

        _accountMAnaheConstraint.constant = 0;
        _manageAddressConstraint.constant =0;
        [self.manageAdrsImage setImage:[UIImage imageNamed:@"right-arrow"]];
        _addressView.hidden =NO;
        _mailBtn.hidden =YES;
        _passBtn.hidden =YES;
        sender.enabled =YES;
        [UIView animateWithDuration:0.5 animations:^{
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            
            _accountSettings_view.hidden =YES;
             _addressView.hidden =YES;
            _mailBtn.hidden =NO;
            _passBtn.hidden =NO;
            sender.enabled =YES;
            
        }];
    }else{
        [sender setSelected:YES];
        _mailBtn.hidden =YES;
        _passBtn.hidden =YES;
        _accountMAnaheConstraint.constant = 0;
        _manageAddressConstraint.constant =303;
        [self.manageAdrsImage setImage:[UIImage imageNamed:@"down-arrow"]];
        _addressView.hidden =NO;
        sender.enabled =NO;
        [UIView animateWithDuration:0.5 animations:^{
            [self.view layoutIfNeeded];
            
        } completion:^(BOOL finished) {
            sender.enabled =YES;
            _accountSettings_view.hidden =YES;
            
        }];
    }
  
}

- (IBAction)backBtnClicked:(id)sender
{
    if ([_From isEqualToString:@"Login"])
    {
        NSArray *currentControllers = self.navigationController.viewControllers;
        
        
        BOOL ischeck = false;
        
        for (UIViewController *view in currentControllers ) {
            if ([view isKindOfClass:[HomeViewController class]]) {
                ischeck =YES;
                [self.navigationController popToViewController:view animated:YES];
                break;
                
            }
        }
        
        if (!ischeck) {
            HomeViewController *con = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
            /* Create a mutable array out of this array */
            NSMutableArray *newControllers = [NSMutableArray
                                              arrayWithArray:@[con]];
            
            /* Remove the last object from the array */
            
            /* Assign this array to the Navigation Controller */
            self.navigationController.viewControllers = newControllers;
            [self.navigationController pushViewController:con animated:YES];
        }
    }
    else
    {
        [self.navigationController setNavigationBarHidden:NO animated:NO];
    //[self.navigationController popViewControllerAnimated:YES];
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate back_click];
    }
    
}

- (void)keyboardWillHide:(NSNotification *)n
{
    NSDictionary* userInfo = [n userInfo];
    
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    
    // resize the scrollview
    CGRect viewFrame = self.scrollView.frame;
    // I'm also subtracting a constant kTabBarHeight because my UIScrollView was offset by the UITabBar so really only the portion of the keyboard that is leftover pass the UITabBar is obscuring my UIScrollView.
    viewFrame.size.height += (keyboardSize.height - 0);
    if (viewFrame.size.height <scrollHeight) {
        viewFrame.size.height = scrollHeight;
    }
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [self.scrollView setFrame:viewFrame];
    [UIView commitAnimations];
    
    keyboardIsShown = NO;
}

- (void)keyboardWillShow:(NSNotification *)n
{
    // This is an ivar I'm using to ensure that we do not do the frame size adjustment on the `UIScrollView` if the keyboard is already shown.  This can happen if the user, after fixing editing a `UITextField`, scrolls the resized `UIScrollView` to another `UITextField` and attempts to edit the next `UITextField`.  If we were to resize the `UIScrollView` again, it would be disastrous.  NOTE: The keyboard notification will fire even when the keyboard is already shown.
    if (keyboardIsShown) {
        return;
    }
    
    NSDictionary* userInfo = [n userInfo];
    
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    // resize the noteView
    CGRect viewFrame = self.scrollView.frame;
    // I'm also subtracting a constant kTabBarHeight because my UIScrollView was offset by the UITabBar so really only the portion of the keyboard that is leftover pass the UITabBar is obscuring my UIScrollView.
    viewFrame.size.height -= (keyboardSize.height - 0);
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [self.scrollView setFrame:viewFrame];
    [UIView commitAnimations];
    keyboardIsShown = YES;
}

- (IBAction)updateEmailFieldAction:(id)sender {
    
    if ([self valide_email]) {
        [self updateEmail];
    }
}

- (IBAction)updatePasswordField:(id)sender
{
    if ([self valide_password])
    {
        [self updatePassword];
    }
}

- (IBAction)updateShippingAddress:(id)sender
{
    if ([self validateAddress])
    {
        [self updateAddress];
    }
}

-(BOOL)valide_email
{
    NSString *email_str = [_emailField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if  ( [email_str length]==0)
    {
        ALERT_DIALOG(@"Alert",@"Email should not be empty");
     
        return NO;
    }
    else{
        if ([self NSStringIsValidEmail: email_str]){
            
            
            return YES;
        }else{
              ALERT_DIALOG(@"Alert",@"Please provide valid email id");
        }
        
            }
        return YES;
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
/*
AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
[appDelegate back_click];
 */
-(BOOL)valide_password
{
    NSString *oldPSWD = [_oldPasswordField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    _oldPasswordField.text = oldPSWD;
       NSString *newPSWD = [_latestPasswordField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    _latestPasswordField.text = newPSWD;
     NSString *confirmPSWD = [_confirmPasswordField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    _confirmPasswordField.text = confirmPSWD;
    
    if  ( [oldPSWD length]==0)
    {
        ALERT_DIALOG(@"Alert",@"Old password should not be empty");
        
        return NO;
    }else if  ( [newPSWD length]==0)
    {
        ALERT_DIALOG(@"Alert",@"New password should not be empty");
        
        return NO;
    }else if  ( [confirmPSWD length]==0)
    {
        ALERT_DIALOG(@"Alert",@"Latest password should not be empty");
        
        return NO;
    } else if ([newPSWD length] < 6)
    {
        
        ALERT_DIALOG(@"Alert",@"Password must be atleast 6 characters long");

        return NO;
    }
    
    else if  ( ![newPSWD isEqualToString:confirmPSWD])
    {
        ALERT_DIALOG(@"Alert",@"New password should match confirm password");
        
        return NO;
    }
   
    return YES;
}

-(BOOL)validateAddress{
    NSString *st = [_streetField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    _streetField.text = st;
    NSString *city = [_cityField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    _cityField.text = city;
    NSString *reg = [_regionField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    _regionField.text = reg;
    NSString *pincode = [_pinCodeField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    _pinCodeField.text = pincode;
    NSString *phone = [_mobileNumberField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    _mobileNumberField.text = phone;
    if  ( [st length]==0  ||  [city length]==0 || [reg length]==0 ||  [pincode length]==0||  [phone length]==0)
    {
        ALERT_DIALOG(@"Alert",@"Fields should not be empty");
        return NO;

    }else if ([pincode length] < 6){
        ALERT_DIALOG(@"Alert",@"Please provide valid Zipcode ");
           return NO;

    }else if ([phone length] < 10){
        ALERT_DIALOG(@"Alert",@"Please provide valid mobile number ");
           return NO;
        
    }

    return YES;
}

-(void)updateEmail{
    NSDictionary *params = @{@"email": _emailField.text,@"id":[[ NSUserDefaults standardUserDefaults]valueForKey:@"user_id"],@"firstname":_firstName.text,@"lastname":_lastName.text};
    
    [[STParsing sharedWebServiceHelper]requesting_POST_ServiceWithString1:@"updateCustomerAccountDetails" parameters:params requestNumber:WS_UPDATE_EMAIL showProgress:YES withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             ALERT_DIALOG(@"ALert", [data valueForKey:@"message"]);
             
           
         }
         else
         {
             ALERT_DIALOG(@"ALert", @"Something went wrong Please try again");
         }
     }];
    
    

    
}

-(void)updatePassword{
    NSDictionary *params = @{@"oldpassword": _oldPasswordField.text
                             ,@"id":[[ NSUserDefaults standardUserDefaults]valueForKey:@"user_id"]
                             ,@"newpassword": _latestPasswordField.text
                             };
    
    [[STParsing sharedWebServiceHelper]requesting_POST_ServiceWithString1:@"updateUserPassword" parameters:params requestNumber:WS_UPDATE_PASSWORD showProgress:YES withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             ALERT_DIALOG(@"Alert", [data valueForKey:@"message"]);
             _oldPasswordField.text =@"";
             _latestPasswordField.text =@"";
             _confirmPasswordField.text =@"";
             
             
         }
         else
         {
             ALERT_DIALOG(@"Alert", @"Something went wrong Please try again");
         }
     }];
    
    
    
    
}
-(void)updateAddress{
    NSDictionary *params = @{@"street": _streetField.text
                             ,@"id":[[ NSUserDefaults standardUserDefaults]valueForKey:@"user_id"]
                             ,@"city": _cityField.text
                             ,@"region": _regionField.text
                             ,@"postcode": _pinCodeField.text
                             ,@"phone": _mobileNumberField.text
                             };
    
    [[STParsing sharedWebServiceHelper]requesting_POST_ServiceWithString1:@"updateCustomerAddress" parameters:params requestNumber:WS_UPDATE_ADDRESS showProgress:YES withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             
             ALERT_DIALOG(@"Alert", [data valueForKey:@"message"]);
             
             
         }
         else
         {
             ALERT_DIALOG(@"Alert", @"Something went wrong Please try again");
         }
     }];
    
    
    
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (!string.length)
        return YES;
    
    if (textField == self.pinCodeField || textField == self.mobileNumberField)
    {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSString *expression = @"^([0-9]+)?(\\.([0-9]{1,2})?)?$";
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                               options:NSRegularExpressionCaseInsensitive
                                                                                 error:nil];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString
                                                            options:0
                                                              range:NSMakeRange(0, [newString length])];
        if (numberOfMatches == 0)
            return NO;
        

        if (textField == self.pinCodeField){
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 6;
        }else{
            if(range.length + range.location > textField.text.length)
            {
                return NO;
            }
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return newLength <= 10;
        }
    }
    return YES;
}

- (IBAction)cart_click:(id)sender
{
    My_Cart *define = [[My_Cart alloc]init];
    [self.navigationController pushViewController:define animated:YES];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0)
    {
        return default_ary.count;
    }
    return additional_ary.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    //base View
    UIView *vi=[[UIView alloc]initWithFrame:CGRectMake(0, 0,tableView.frame.size.width, 25)];
    
    if (section==0)
    {
        // vi.backgroundColor=[UIColor colorWithRed:233/255.0f green:243/255.0f blue:255/255.0f alpha:1];
    }
    else
    vi.backgroundColor = [UIColor whiteColor];
    
    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(8,5,[UIScreen mainScreen].bounds.size.width-16, 25)];
    // lbl.backgroundColor = [UIColor blueColor];
   // NSString *dates =[[yourOrdersArray objectAtIndex:section]valueForKey:@"Date"];
    if (section==0)
    {
        lbl.text = [NSString stringWithFormat:@"Default Address"] ;
    }
    else
        lbl.text = [NSString stringWithFormat:@"Additional Address Entries"] ;
    lbl.backgroundColor = [UIColor whiteColor];
    lbl.textColor=[UIColor colorWithRed:22/255.0f green:84/255.0f blue:161/255.0f alpha:1];
    [vi addSubview:lbl];
    
    return vi;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==0)
    {
    static NSString *simpleTableIdentifier = @"Address_cell_id";
    Address_cell *cell = (Address_cell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"Address_cell" owner:self options:nil];
        cell = (Address_cell *)[nib objectAtIndex:0];
    }
       // cell.contentView.backgroundColor=[UIColor colorWithRed:233/255.0f green:243/255.0f blue:255/255.0f alpha:1];

        NSString *f_name=[NSString stringWithFormat:@"%@",[[default_ary valueForKey:@"firstname"]objectAtIndex:indexPath.row]];
        NSString *l_name=[NSString stringWithFormat:@"%@",[[default_ary valueForKey:@"lastname"]objectAtIndex:indexPath.row]];
        NSString *name=[NSString stringWithFormat:@"%@ %@",f_name,l_name];
        NSString *company=[NSString stringWithFormat:@"%@",[[default_ary valueForKey:@"company"]objectAtIndex:indexPath.row]];

        NSString *street=[NSString stringWithFormat:@"%@",[[default_ary valueForKey:@"street"]objectAtIndex:indexPath.row]];
        NSString *city=[NSString stringWithFormat:@"%@",[[default_ary valueForKey:@"city"]objectAtIndex:indexPath.row]];
        NSString *region=[NSString stringWithFormat:@"%@",[[default_ary valueForKey:@"region"]objectAtIndex:indexPath.row]];
        NSString *postcode=[NSString stringWithFormat:@"%@",[[default_ary valueForKey:@"postcode"]objectAtIndex:indexPath.row]];
        NSString *full_add=[NSString stringWithFormat:@"%@,%@,%@",city,region,postcode];
        
        NSString *telephone=[NSString stringWithFormat:@"T: %@",[[default_ary valueForKey:@"telephone"]objectAtIndex:indexPath.row]];
        NSString *mobile=[NSString stringWithFormat:@"Mobile: %@",[[default_ary valueForKey:@"mobile"]objectAtIndex:indexPath.row]];
        NSString *pan=[NSString stringWithFormat:@"PAN Number : %@",[[default_ary valueForKey:@"pan"]objectAtIndex:indexPath.row]];
        NSString *cst=[NSString stringWithFormat:@"CST Number : %@",[[default_ary valueForKey:@"cst"]objectAtIndex:indexPath.row]];
        
        if(name == nil||[name isEqualToString:@""]||[name isEqualToString:@"<null> <null>"])
        {
            cell.name.text=@"";
        }
        else
            cell.name.text=name;
        
        if(company == nil||[company isEqualToString:@""]||[company isEqualToString:@"<null>"])
        {
            cell.company.text=@"";
        }
        else
            cell.company.text=company;
        
        if(street == nil||[street isEqualToString:@""]||[street isEqualToString:@"<null>"])
        {
            cell.colony.text=@"";
        }
        else
        {
            NSArray *strt=[street componentsSeparatedByString:@"\n"];
            
            cell.colony.text=[NSString stringWithFormat:@"%@",[strt objectAtIndex:0]];
//            aCell.strtAdrs2.text=[NSString stringWithFormat:@"%@",[strt objectAtIndex:1]];
//            cell.colony.text=street;
        }
        
        if(full_add == nil||[full_add isEqualToString:@""]||[full_add isEqualToString:@"<null>,<null>,<null>"])
        {
            cell.address.text=@"";
        }
        else
            cell.address.text=full_add;
        
        if(telephone == nil||[telephone isEqualToString:@""]||[telephone isEqualToString:@"T: <null>"])
        {
            cell.tele.text=@"T:";
        }
        else
            cell.tele.text=telephone;
        
        if(mobile == nil||[mobile isEqualToString:@""]||[mobile isEqualToString:@"Mobile: <null>"])
        {
            cell.mobile.text=@"Mobile:";
        }
        else
            cell.mobile.text=mobile;
        
        if(pan == nil||[pan isEqualToString:@""]||[pan isEqualToString:@"PAN Number : <null>"])
        {
            cell.pan.text=@"PAN Number :";
        }
        else
            cell.pan.text=pan;
        
        if(cst == nil||[cst isEqualToString:@""]||[cst isEqualToString:@"CST Number : <null>"])
        {
            cell.cst.text=@"CST Number :";
        }
        else
            cell.cst.text=cst;
        
        if (indexPath.row==0)
        {
            cell.head.text=@"Default Billing Address";
            [cell.change_address setTitle:@"Change Billing Address" forState:UIControlStateNormal];
        }
        else
        {
            cell.head.text=@"Default Shipping Address";
            [cell.change_address setTitle:@"Change Shipping Address" forState:UIControlStateNormal];
        }
        cell.change_address.tag=indexPath.row;
        [cell.change_address addTarget:self action:@selector(Change_addr_Click:) forControlEvents:UIControlEventTouchUpInside];

    return cell;
    }
    else if(indexPath.section==1)
    {
        static NSString *simpleTableIdentifier = @"Address_cell_id2";
        Address_cell2 *cell = (Address_cell2 *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"Address_cell2" owner:self options:nil];
            cell = (Address_cell2 *)[nib objectAtIndex:0];
        }
        NSString *f_name=[NSString stringWithFormat:@"%@",[[additional_ary valueForKey:@"firstname"]objectAtIndex:indexPath.row]];
        NSString *l_name=[NSString stringWithFormat:@"%@",[[additional_ary valueForKey:@"lastname"]objectAtIndex:indexPath.row]];
        NSString *name=[NSString stringWithFormat:@"%@ %@",f_name,l_name];
        NSString *street=[NSString stringWithFormat:@"%@",[[additional_ary valueForKey:@"street"]objectAtIndex:indexPath.row]];

        NSString *company=[NSString stringWithFormat:@"%@",[[additional_ary valueForKey:@"company"]objectAtIndex:indexPath.row]];
        NSString *city=[NSString stringWithFormat:@"%@",[[additional_ary valueForKey:@"city"]objectAtIndex:indexPath.row]];
        NSString *region=[NSString stringWithFormat:@"%@",[[additional_ary valueForKey:@"region"]objectAtIndex:indexPath.row]];
        NSString *postcode=[NSString stringWithFormat:@"%@",[[additional_ary valueForKey:@"postcode"]objectAtIndex:indexPath.row]];
        NSString *full_add=[NSString stringWithFormat:@"%@,%@,%@",city,region,postcode];
        
        NSString *telephone=[NSString stringWithFormat:@"T: %@",[[additional_ary valueForKey:@"telephone"]objectAtIndex:indexPath.row]];
        NSString *mobile=[NSString stringWithFormat:@"Mobile: %@",[[additional_ary valueForKey:@"mobile"]objectAtIndex:indexPath.row]];
        NSString *pan=[NSString stringWithFormat:@"PAN Number : %@",[[additional_ary valueForKey:@"pan"]objectAtIndex:indexPath.row]];
        NSString *cst=[NSString stringWithFormat:@"CST Number : %@",[[additional_ary valueForKey:@"cst"]objectAtIndex:indexPath.row]];
        
        if(name == nil||[name isEqualToString:@""]||[name isEqualToString:@"<null> <null>"])
        {
            cell.name.text=@"";
        }
        else
            cell.name.text=name;
        
        if(company == nil||[company isEqualToString:@""]||[company isEqualToString:@"<null>"])
        {
        cell.company.text=@"";
        }
        else
            cell.company.text=company;

        if(street == nil||[street isEqualToString:@""]||[street isEqualToString:@"<null>"])
        {
            cell.colony.text=@"";
        }
        else
        cell.colony.text=street;

        if(full_add == nil||[full_add isEqualToString:@""]||[full_add isEqualToString:@"<null>,<null>,<null>"])
        {
            cell.address.text=@"";
        }
        else
        cell.address.text=full_add;
        
        if(telephone == nil||[telephone isEqualToString:@""]||[telephone isEqualToString:@"T: <null>"])
        {
            cell.tele.text=@"T:";
        }
        else
        cell.tele.text=telephone;
        
        if(mobile == nil||[mobile isEqualToString:@""]||[mobile isEqualToString:@"Mobile: <null>"])
        {
            cell.mobile.text=@"Mobile:";
        }
        else
        cell.mobile.text=mobile;
        
        if(pan == nil||[pan isEqualToString:@""]||[pan isEqualToString:@"PAN Number : <null>"])
        {
            cell.pan.text=@"PAN Number :";
        }
        else
        cell.pan.text=pan;
        
        if(cst == nil||[cst isEqualToString:@""]||[cst isEqualToString:@"CST Number : <null>"])
        {
            cell.cst.text=@"CST Number :";
        }
        else
        cell.cst.text=cst;
        
        
        cell.edit_addr.tag=indexPath.row;
        cell.delete_addr.tag=indexPath.row;

        [cell.edit_addr addTarget:self action:@selector(edit_addr_Click:) forControlEvents:UIControlEventTouchUpInside];
        [cell.delete_addr addTarget:self action:@selector(del_addr_Click:) forControlEvents:UIControlEventTouchUpInside];

        
        return cell;
    }
    return NULL;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 263;
}

- (IBAction)add_address:(id)sender
{
    AddNewAddress *define = [[AddNewAddress alloc]initWithNibName:@"AddNewAddress" bundle:nil];
    [self.navigationController pushViewController:define animated:YES];
}

- (IBAction)del_addr_Click:(id)sender
{
    NSLog(@"details clicked %ld",(long)[sender tag]);
    
    UIButton *button=(UIButton *) sender;
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:button.tag inSection:1];
    
    NSDictionary *local=[additional_ary objectAtIndex:[sender tag]];

//    NSString *url_form=[NSString stringWithFormat:@"https://steeloncall.com/calculate/userservice/deleteCustomerAddress/?address_id=%@",[local valueForKey:@"entity_id"]];
    
     NSString *url_form=[NSString stringWithFormat:@"http://stg.steeloncall.com/calculate/userservice/deleteCustomerAddress/?address_id=%@",[local valueForKey:@"entity_id"]];
    
    // making a GET request to /init
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:url_form]];
    
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
      ^(NSData * _Nullable data,
        NSURLResponse * _Nullable response,
        NSError * _Nullable error)
      {
          NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                               options:kNilOptions
                                                                 error:&error];
          
//          [_add_table deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexpath]
//                           withRowAnimation:UITableViewRowAnimationFade];
//          [_add_table endUpdates];
          [self call_tonnes_service];
          
          NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:0];
          
          [_add_table scrollToRowAtIndexPath:indexpath atScrollPosition:UITableViewScrollPositionTop animated:YES];
          
      }] resume];
    [self Alert:@"Deleted Successfully!"];

}

- (IBAction)edit_addr_Click:(id)sender
{
    NSLog(@"details clicked %ld",(long)[sender tag]);
    
    UIButton *button=(UIButton *) sender;
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:button.tag inSection:0];
    NSDictionary *local=[additional_ary objectAtIndex:[sender tag]];
    AddNewAddress *define = [[AddNewAddress alloc]initWithNibName:@"AddNewAddress" bundle:nil];
    define.address_dict=local;
    define.from=@"edit";
    [self.navigationController pushViewController:define animated:YES];
}

- (IBAction)Change_addr_Click:(id)sender
{
    NSLog(@"details clicked %ld",(long)[sender tag]);
    
    UIButton *button=(UIButton *) sender;
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:button.tag inSection:0];
    NSDictionary *local=[default_ary objectAtIndex:[sender tag]];
    
    AddNewAddress *define = [[AddNewAddress alloc]initWithNibName:@"AddNewAddress" bundle:nil];
    define.address_dict=local;
    define.from=@"edit";
    define.from_default=@"yes";
    [self.navigationController pushViewController:define animated:YES];
}
-(void)Alert:(NSString *)Msg
{
    NSDictionary *options = @{kCRToastNotificationTypeKey:@(CRToastTypeNavigationBar),
                              
                              kCRToastTextKey : Msg,
                              
                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                              
                              kCRToastBackgroundColorKey : [UIColor colorWithRed:44.0/255.0 green:52.0/255.0 blue:75.0/255.0 alpha:1],
                              kCRToastTimeIntervalKey: @(2),
                              //                              kCRToastFontKey:[UIFont fontWithName:@"PT Sans Narrow" size:23],
                              kCRToastInteractionRespondersKey:@[[CRToastInteractionResponder interactionResponderWithInteractionType:CRToastInteractionTypeSwipeUp
                                                                  
                                                                                                                 automaticallyDismiss:YES
                                                                  
                                                                                                                                block:^(CRToastInteractionType interactionType){
                                                                                                                                    
                                                                                                                                    NSLog(@"Dismissed with %@ interaction", NSStringFromCRToastInteractionType(interactionType));
                                                                                                                                }]],
                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeGravity),
                              
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeGravity),
                              
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                              
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop)
                              
                              };
    
    [CRToastManager showNotificationWithOptions:options
     
                                completionBlock:^{
                                    
                                    NSLog(@"Completed");
                                    
                                }];
}


- (IBAction)goToHomeScreenAction:(id)sender {
    HomeViewController *con = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
    [self.navigationController pushViewController:con animated:YES];
    
    
}
@end
