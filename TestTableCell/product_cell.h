//
//  Product_defined_cell.h
//  SteelonCall
//
//  Created by Manoyadav on 29/11/16.
//  Copyright © 2016 Manoyadav. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface product_cell : UITableViewCell<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *img;
@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UILabel *seller_name;
@property (strong, nonatomic) IBOutlet UILabel *price_ton;
@property (strong, nonatomic) IBOutlet UILabel *q_ton;
@property (strong, nonatomic) IBOutlet UILabel *q_pie;
@property (strong, nonatomic) IBOutlet UILabel *discount;
@property (strong, nonatomic) IBOutlet UILabel *total;
@property (strong, nonatomic) IBOutlet UIView *border_view;
@property (strong, nonatomic) IBOutlet UILabel *brandLbl;
@property (strong, nonatomic) IBOutlet UILabel *gradeLbl;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *discountValueHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *discountLabelHeight;


@end
