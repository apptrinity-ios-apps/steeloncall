//
//  Seller_Price_Cell.h
//  SteelonCall
//
//  Created by INDOBYTES on 03/04/17.
//  Copyright © 2017 com.indobytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Seller_Price_Cell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *subTotalLbl;
@property (strong, nonatomic) IBOutlet UILabel *cstLbl;
@property (strong, nonatomic) IBOutlet UILabel *vatLbl;
@property (strong, nonatomic) IBOutlet UILabel *netAmountLbl;
@property (strong, nonatomic) IBOutlet UILabel *shippingHandlingLbl;
@property (strong, nonatomic) IBOutlet UILabel *handlingLabel;

@property (strong, nonatomic) IBOutlet UILabel *grandTotal;
@property (strong, nonatomic) IBOutlet UILabel *amountPaid;
@property (strong, nonatomic) IBOutlet UILabel *amountPayable;
@property (strong, nonatomic) IBOutlet UIButton *printInvoiceBtn;

@property (strong, nonatomic) IBOutlet UILabel *cgstHeadingLbl;
@property (strong, nonatomic) IBOutlet UILabel *vatHeadingLbl;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cstConstrainHeading;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *sgstlabelHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *sgstValueHeight;



@end
