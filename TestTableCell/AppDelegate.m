//
//  AppDelegate.m
//  TestTableCell
//
//  Created by administrator on 19/11/16.
//  Copyright © 2016 com.SteelonCall. All rights reserved.
//

#import "AppDelegate.h"
#import "REFrostedViewController.h"
#import "DEMONavigationController.h"
#import "HomeViewController.h"
#import "LeftPanelViewController.h"
#import "ATAppUpdater.h"
@interface AppDelegate ()

@end

@implementation AppDelegate
BOOL shouldRotate;
@synthesize deviceName;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    deviceName = [[UIDevice currentDevice]model];
    NSUserDefaults * user = [NSUserDefaults standardUserDefaults];
    [user setObject:deviceName forKey:@"device"];
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"quickToolValue"] isEqualToString:@"NO"]) {
        [[NSUserDefaults standardUserDefaults]setObject:@"NO" forKey:@"quickToolValue"];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"YES" forKey:@"quickToolValue"];
    }
    
    [[ATAppUpdater sharedUpdater] showUpdateWithForce];
    
    // Override point for customization after application launch.
    _itemsArray = [[NSArray alloc]init];
   
    DEMONavigationController *navigationController = [[DEMONavigationController alloc] initWithRootViewController:[[HomeViewController alloc] init]];
    LeftPanelViewController *menuController = [[LeftPanelViewController alloc] initWithNibName:@"LeftPanelViewController" bundle:nil];
    
    // Create frosted view controller
    //
    REFrostedViewController *frostedViewController = [[REFrostedViewController alloc] initWithContentViewController:navigationController menuViewController:menuController];
    frostedViewController.direction = REFrostedViewControllerDirectionLeft;
    
    // Make it a root controller
    [Fabric with:@[[Crashlytics class]]];

    //
    
//#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
//    
//    
//        UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width, 20)];
//        view.backgroundColor=[UIColor colorWithRed:87/225 green:22/225 blue:100/225 alpha:1];
//        [self.window.rootViewController.view addSubview:view];
    
    [self registerForRemoteNotifications];
    self.window.rootViewController = frostedViewController;
     [self.window makeKeyAndVisible];
    return YES;
}

-(void)back_click
{
    DEMONavigationController *navigationController = [[DEMONavigationController alloc] initWithRootViewController:[[HomeViewController alloc] init]];
    LeftPanelViewController *menuController = [[LeftPanelViewController alloc] initWithNibName:@"LeftPanelViewController" bundle:nil];
    //
    REFrostedViewController *frostedViewController = [[REFrostedViewController alloc] initWithContentViewController:navigationController menuViewController:menuController];
    frostedViewController.direction = REFrostedViewControllerDirectionLeft;
    
    // Make it a root controller
    [Fabric with:@[[Crashlytics class]]];
    self.window.rootViewController = frostedViewController;
    [self.window makeKeyAndVisible];
}

- (void)registerForRemoteNotifications {
    
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
        
        
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if(!error){
                [[UIApplication sharedApplication] registerForRemoteNotifications];
            }
        }];
    }
    else {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        {
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
            [[UIApplication sharedApplication] registerForRemoteNotifications];
            
            NSLog(@"current notifications : %@", [[UIApplication sharedApplication] currentUserNotificationSettings]);
        }
        else
        {
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
             (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
        }
    }
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    // custom stuff we do to register the device with our AWS middleman
    NSLog(@"%@",[deviceToken description]);
    
    NSString *str = [NSString stringWithFormat:@"%@",deviceToken];

    NSString *deviceTokenStr = [[[[deviceToken description]
                                  stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                 stringByReplacingOccurrencesOfString: @">" withString: @""]
                                stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    NSLog(@"Device Token: %@", deviceTokenStr);
    
}

//notif test
-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    UIAlertView *notificationAlert = [[UIAlertView alloc] initWithTitle:@"Notification"    message:@"Go To Cart!"
                                                               delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    
   // [notificationAlert show];
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler
{
    NSLog(@"User Info : %@",response.notification.request.content.userInfo);
    completionHandler();
    
    //    UIAlertView *notificationAlert = [[UIAlertView alloc] initWithTitle:@"Notification"    message:@"This local notification"
    //                                                               delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    //
    //    [notificationAlert show];
    
    My_Cart *define = [[My_Cart alloc]init];
    //[self.navigationController pushViewController:define animated:YES];
    define.From_check_cart=@"N";
    self.window.rootViewController = define;
}

-(void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void(^)(UIBackgroundFetchResult))completionHandler
{
    // iOS 10 will handle notifications through other methods
    
    if( SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0") )
    {
        NSLog( @"iOS version >= 10. Let NotificationCenter handle this one." );
        
        // set a member variable to tell the new delegate that this is background
        return;
    }
    
    NSLog( @"HANDLE PUSH, didReceiveRemoteNotification: %@", userInfo );
    
     // custom code to handle notification content
    
    if( [UIApplication sharedApplication].applicationState == UIApplicationStateInactive )
    {
        NSLog( @"INACTIVE" );
        
        completionHandler( UIBackgroundFetchResultNewData );
        
    }
    else if( [UIApplication sharedApplication].applicationState == UIApplicationStateBackground )
    {
        NSLog( @"BACKGROUND" );
        completionHandler( UIBackgroundFetchResultNewData );
    }
    else  
    {
        NSLog( @"FOREGROUND" );
        completionHandler( UIBackgroundFetchResultNewData );
    }
}

//Called when a notification is delivered to a foreground app.
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
    NSLog(@"User Info : %@",notification.request.content.userInfo);
    completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
}

//Called to let your app know which action was selected by the user for a given notification.
//-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler{
//    NSLog(@"User Info : %@",response.notification.request.content.userInfo);
//    completionHandler();
//    
//    
//}


 //Handle actions

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void (^)())completionHandler {
    //Handle action as well
}

-(UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window{
    if (self.shouldRotate) {
        return UIInterfaceOrientationMaskAll;
    }else{
        return UIInterfaceOrientationMaskPortrait;
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
