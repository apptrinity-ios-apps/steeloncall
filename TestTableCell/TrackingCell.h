//
//  TrackingCell.h
//  SteelonCall
//
//  Created by nagaraj  kumar on 14/11/18.
//  Copyright © 2018 com.indobytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrackingCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *brand_Image;

@property (weak, nonatomic) IBOutlet UILabel *brand_Lbl;

@property (weak, nonatomic) IBOutlet UILabel *Brand_TMTLbl;
@property (weak, nonatomic) IBOutlet UILabel *tons_Lbl;

@property (weak, nonatomic) IBOutlet UILabel *pieces_Lbl;
@property (weak, nonatomic) IBOutlet UILabel *price_Lbl;

@end
