//
//  AddAddressCell.m
//  SteelonCall
//
//  Created by S s Vali on 9/12/17.
//  Copyright © 2017 com.indobytes. All rights reserved.
//

#import "AddAddressCell.h"

@implementation AddAddressCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
      _dataLabel.layer.sublayerTransform = CATransform3DMakeTranslation(20, 0, 0);
    
    _lastName.layer.sublayerTransform = CATransform3DMakeTranslation(20, 0, 0);
    _company.layer.sublayerTransform = CATransform3DMakeTranslation(20, 0, 0);
    _telephoneLabel.layer.sublayerTransform = CATransform3DMakeTranslation(20, 0, 0);

    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
