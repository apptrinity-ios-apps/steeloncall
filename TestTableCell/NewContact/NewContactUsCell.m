//
//  NewContactUsCell.m
//  SteelonCall
//
//  Created by nagaraj  kumar on 10/11/18.
//  Copyright © 2018 com.indobytes. All rights reserved.
//

#import "NewContactUsCell.h"

@implementation NewContactUsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _nameTF.layer.sublayerTransform = CATransform3DMakeTranslation(20, 0, 0);
    _mailTF.layer.sublayerTransform = CATransform3DMakeTranslation(20, 0, 0);
    _numberTF.layer.sublayerTransform = CATransform3DMakeTranslation(20, 0, 0);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
@end
