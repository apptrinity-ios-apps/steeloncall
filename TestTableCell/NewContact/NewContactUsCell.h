//
//  NewContactUsCell.h
//  SteelonCall
//
//  Created by nagaraj  kumar on 10/11/18.
//  Copyright © 2018 com.indobytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewContactUsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *nameTF;
@property (weak, nonatomic) IBOutlet UITextField *mailTF;
@property (weak, nonatomic) IBOutlet UITextField *numberTF;
@property (weak, nonatomic) IBOutlet UITextView *messageTV;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

@end
