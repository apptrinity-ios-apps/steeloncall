//
//  NewContactUsVC.h
//  SteelonCall
//
//  Created by nagaraj  kumar on 10/11/18.
//  Copyright © 2018 com.indobytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewContactUsVC : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITableView *contactTableview;

@end
