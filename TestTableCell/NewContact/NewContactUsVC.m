//
//  NewContactUsVC.m
//  SteelonCall
//
//  Created by nagaraj  kumar on 10/11/18.
//  Copyright © 2018 com.indobytes. All rights reserved.
//

#import "NewContactUsVC.h"
#import "NewContactUsCell.h"
#import "STParsing.h"
#import "CRToastView.h"
@interface NewContactUsVC ()

@end

@implementation NewContactUsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(view_tap:)];
    [self.view addGestureRecognizer:singleFingerTap];
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 753;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        static NSString *simpleTableIdentifier = @"NewContactUsCell";
        NewContactUsCell *cell = (NewContactUsCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"NewContactUsCell" owner:self options:nil];
            cell = (NewContactUsCell *)[nib objectAtIndex:0];
        }
    
    cell.nameTF.delegate = self;
    cell.numberTF.delegate = self;
    cell.mailTF.delegate = self;
    
    [cell.submitBtn addTarget:self action:@selector(submitBtnAction) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
    
        
        
    
}
- (void)view_tap:(UITapGestureRecognizer *)recognizer
{
    CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    //Do stuff here...
    [self.view endEditing:YES];
}
-(void)submitBtnAction
{
    [self.view endEditing:YES];
    
    if ([self valide_Data] == YES)
    { // checking if the either of the string is empty and other validations
        [self Login_Dict];
    }
    
}
-(void)Login_Dict
{
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:0];
    NewContactUsCell *tappedCell = (NewContactUsCell *)[_contactTableview cellForRowAtIndexPath:indexpath];
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:[tappedCell.mailTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"email"];
    [dict setObject:[tappedCell.nameTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"name"];
    [dict setObject:[tappedCell.numberTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"mobile_number"];
    [dict setObject:[tappedCell.messageTV.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"comment"];
    
    NSLog(@"%@",dict);
    [self Login_Service:dict];
}

-(BOOL)NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

-(BOOL)valide_Data
{
    
    
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:0];
    NewContactUsCell *tappedCell = (NewContactUsCell *)[_contactTableview cellForRowAtIndexPath:indexpath];
    
    NSString *email_str = [tappedCell.mailTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *name_str= tappedCell.nameTF.text;
    NSString *number_str= tappedCell.numberTF.text;
    NSString *comment_str= tappedCell.messageTV.text;
    
    if ([name_str length] == 0)
    {
        [self Alert :@"Name should not be empty."];
        return NO;
    }
    else if  ( [email_str length]==0)
    {
        [self Alert :@"Email should not be empty."];
        return NO;
    }
    else if ([self NSStringIsValidEmail:email_str]== NO)
    {
        [self Alert :@"Email should be valid."];
        return NO;
    }
    
    else if ([comment_str length] == 0)
    {
        [self Alert :@"Comment should not be empty."];
        return NO;
    }
    
    else if  ( [number_str length]==0)
    {
        [self Alert :@"Number should not be empty."];
        return NO;
    }
    //else if ([number_str length]>10||[number_str length]<10)
    //{
    
    //}
    return YES;
}

-(void)Alert:(NSString *)Msg
{
    NSDictionary *options = @{kCRToastNotificationTypeKey:@(CRToastTypeNavigationBar),
                              
                              kCRToastTextKey : Msg,
                              
                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                              
                              kCRToastBackgroundColorKey : [UIColor colorWithRed:13.0/255.0 green:147.0/255.0 blue:68.0/255.0 alpha:1],
                              kCRToastTimeIntervalKey: @(2),
                              //                              kCRToastFontKey:[UIFont fontWithName:@"PT Sans Narrow" size:23],
                              kCRToastInteractionRespondersKey:@[[CRToastInteractionResponder interactionResponderWithInteractionType:CRToastInteractionTypeSwipeUp
                                                                  
                                                                                                                 automaticallyDismiss:YES
                                                                  
                                                                                                                                block:^(CRToastInteractionType interactionType){
                                                                                                                                    
                                                                                                                                    NSLog(@"Dismissed with %@ interaction", NSStringFromCRToastInteractionType(interactionType));
                                                                                                                                }]],
                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeGravity),
                              
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeGravity),
                              
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                              
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop)
                              };
    
    [CRToastManager showNotificationWithOptions:options
                                completionBlock:^{
                                    NSLog(@"Completed");
                                }];
}

-(void)Login_Service:(NSDictionary *)Dict
{
    [[STParsing sharedWebServiceHelper]requesting_POST_ServiceWithString1:@"getContactusFormData" parameters:Dict requestNumber:WS_Login showProgress:YES withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             NSDictionary *res_dict=data;
             NSString *status=[NSString stringWithFormat:@"%@",[res_dict valueForKey:@"status"]];
             NSString *result=[res_dict valueForKey:@"result"];
             // 8 10 85 15
             if ([status isEqualToString:@"1"])
             {
                 //[self Alert:@"Thank You!"];
                 ALERT_DIALOG(@"Message",@"Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.");
                 
                 [self.navigationController popViewControllerAnimated:YES];
             }
         }
         else
         {
             [self Alert:@"Something went wrong"];
         }
     }];
}


- (IBAction)back_click:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
