//
//  OrderReview_AddressTableViewCell.h
//  steelonCallThree
//
//  Created by nagaraj  kumar on 02/12/16.
//  Copyright © 2016 nagaraj  kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderReview_AddressTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *seller_NameLbl;
@property (strong, nonatomic) IBOutlet UILabel *shipping_NameLbl;


@property (strong, nonatomic) IBOutlet UILabel *distanceLbl;
@property (strong, nonatomic) IBOutlet UILabel *shipping_AmontLbl;
@property (strong, nonatomic) IBOutlet UILabel *Handling_charges;
@property (strong, nonatomic) IBOutlet UILabel *sellerAddressLabel;
@property (strong, nonatomic) IBOutlet UILabel *shippingAddressLabel;






@end
