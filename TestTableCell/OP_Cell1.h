//
//  OP_Cell1.h
//  Steeloncall_Seller
//
//  Created by INDOBYTES on 21/12/16.
//  Copyright © 2016 sai. All rights reserved.

#import <UIKit/UIKit.h>

@interface OP_Cell1 : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *purchase_order;
@property (strong, nonatomic) IBOutlet UILabel *order_date;
@property (strong, nonatomic) IBOutlet UILabel *s_addr1;
@property (strong, nonatomic) IBOutlet UILabel *s_addr3;
@property (strong, nonatomic) IBOutlet UILabel *s_addr4;
@property (strong, nonatomic) IBOutlet UILabel *s_addr2;
@property (strong, nonatomic) IBOutlet UILabel *b_addr1;
@property (strong, nonatomic) IBOutlet UILabel *s_ph;
@property (strong, nonatomic) IBOutlet UILabel *s_mb;
@property (strong, nonatomic) IBOutlet UILabel *b_addr3;
@property (strong, nonatomic) IBOutlet UILabel *b_addr4;
@property (strong, nonatomic) IBOutlet UILabel *b_addr2;
@property (strong, nonatomic) IBOutlet UILabel *b_ph;
@property (strong, nonatomic) IBOutlet UILabel *b_mb;
@property (strong, nonatomic) IBOutlet UILabel *p_method;
@property (strong, nonatomic) IBOutlet UILabel *s_method;
@property (strong, nonatomic) IBOutlet UILabel *b_country;
@property (strong, nonatomic) IBOutlet UILabel *s_country;
@property (strong, nonatomic) IBOutlet UILabel *s_pan;
@property (strong, nonatomic) IBOutlet UILabel *s_cst;
@property (strong, nonatomic) IBOutlet UILabel *s_tin;
@property (strong, nonatomic) IBOutlet UILabel *b_pan;
@property (strong, nonatomic) IBOutlet UILabel *b_cst;
@property (strong, nonatomic) IBOutlet UILabel *b_tin;
@property (strong, nonatomic) IBOutlet UILabel *order_id;
@property (strong, nonatomic) IBOutlet UILabel *order_d;
@property (strong, nonatomic) IBOutlet UILabel *sStreet;
@property (strong, nonatomic) IBOutlet UILabel *bStreet;
@property (strong, nonatomic) IBOutlet UIView *shippingAddressView;
@property (strong, nonatomic) IBOutlet UIView *billingAddressView;
@property (strong, nonatomic) IBOutlet UILabel *billingNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *billingTollFreeNumber;
@property (strong, nonatomic) IBOutlet UILabel *shippingTollFree;

@property (strong, nonatomic) IBOutlet UILabel *billingAddressLabel;
@property (strong, nonatomic) IBOutlet UILabel *billingpanLabel;

@property (strong, nonatomic) IBOutlet UILabel *billingMobileNumber;

@property (strong, nonatomic) IBOutlet UILabel *shippingNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *shippingAddessLabel;
@property (strong, nonatomic) IBOutlet UILabel *shippingNumerLabel;
@property (strong, nonatomic) IBOutlet UILabel *shippingPanLabel;

@end
