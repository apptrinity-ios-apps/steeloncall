//
//  Product_defined_cell.m
//  SteelonCall
//
//  Created by Manoyadav on 29/11/16.
//  Copyright © 2016 Manoyadav. All rights reserved.
//

#import "Product_defined_cell.h"

@implementation Product_defined_cell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _imageView_bg.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _imageView_bg.layer.borderWidth = 0.5;
    _imageView_bg.layer.cornerRadius = 5;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



@end
