//
//  Custom1Cell.h
//  SteelonCall
//
//  Created by S s Vali on 8/3/17.
//  Copyright © 2017 com.indobytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Custom1Cell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *item_name;
@property (weak, nonatomic) IBOutlet UIImageView *item_imageView;

@end
