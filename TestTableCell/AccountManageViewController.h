//
//  AccountManageViewController.h
//  SteelonCall
//
//  Created by Manoyadav on 05/12/16.
//  Copyright © 2016 com.way2online. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Address_cell.h"
#import "Address_cell2.h"
#import "AddNewAddress.h"

@interface AccountManageViewController : UIViewController<UIScrollViewDelegate,UITextFieldDelegate>
{
    BOOL keyboardIsShown;
    int scrollHeight;
    NSArray* default_ary;
    NSArray* additional_ary;
}

@property (weak, nonatomic) IBOutlet UILabel *accountLbl;


@property (strong, nonatomic) IBOutlet UILabel *Cart_count;
@property (strong, nonatomic) IBOutlet UITableView *add_table;
@property (strong, nonatomic) IBOutlet UIView *cart_view;
- (IBAction)add_address:(id)sender;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *address_height;
@property (strong, nonatomic) IBOutlet UIButton *address_btn;
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *changeAccountFields;

- (IBAction)cart_click:(id)sender;
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *manageAddressFields;
- (IBAction)returnExit:(id)sender;
//298
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *accountMAnaheConstraint;
//303
@property (strong, nonatomic)  NSString *From;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *manageAddressConstraint;
@property (strong, nonatomic) IBOutlet UIView *accountSettings_view;
@property (strong, nonatomic) IBOutlet UIView *addressView;
@property (strong, nonatomic) IBOutlet UIButton *settingsBtn;
@property (strong, nonatomic) IBOutlet UIButton *addressBtn;
@property (strong, nonatomic) IBOutlet UIButton *mailBtn;
@property (strong, nonatomic) IBOutlet UIButton *passBtn;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UITextField *emailField;

@property (strong, nonatomic) IBOutlet UITextField *oldPasswordField;
@property (strong, nonatomic) IBOutlet UITextField *latestPasswordField;

@property (strong, nonatomic) IBOutlet UITextField *confirmPasswordField;

@property (strong, nonatomic) IBOutlet UITextField *streetField;
@property (strong, nonatomic) IBOutlet UITextField *cityField;
@property (strong, nonatomic) IBOutlet UITextField *regionField;
@property (strong, nonatomic) IBOutlet UITextField *pinCodeField;
@property (strong, nonatomic) IBOutlet UITextField *mobileNumberField;
@property (strong, nonatomic) IBOutlet UIImageView *acountSetingsImg;
@property (strong, nonatomic) IBOutlet UIImageView *manageAdrsImage;
- (IBAction)goToHomeScreenAction:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *firstName;

@property (strong, nonatomic) IBOutlet UITextField *lastName;
@property (strong, nonatomic) IBOutlet UILabel *addressCount_Lbl;


@end
