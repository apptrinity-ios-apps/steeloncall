//
//  Tracking_VC.h
//  SteelonCall
//
//  Created by nagaraj  kumar on 14/11/18.
//  Copyright © 2018 com.indobytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STParsing.h"
#import "CRToastView.h"

@interface Tracking_VC : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    NSDictionary *Sellers_dict;
}

@property (weak, nonatomic) IBOutlet UILabel *heading_Lbl;

@property (weak, nonatomic) IBOutlet UITableView *trackingTableView;
@property (strong, nonatomic) IBOutlet NSString *order_id;
- (IBAction)back_Action:(id)sender;
- (IBAction)logoBtn_Action:(id)sender;

@end
