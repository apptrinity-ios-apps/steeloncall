//
//  CompareViewController.m
//  steelonCallThree
//
//  Created by nagaraj  kumar on 29/11/16.
//  Copyright © 2016 nagaraj  kumar. All rights reserved.
//

#import "CompareViewController.h"
#import "CompareTableViewCell.h"
#import "BillingInfoViewController.h"
#import "STParsing.h"
#import "AppDelegate.h"
#import "LoginViewController.h"
#import "NewLoginVC.h"

@interface CompareViewController ()<NIDropDownDelegate>
{
     NSMutableArray *tataVizagArr;//both tata and vizag
     NSMutableArray * compareArr;//compare button list
     NSMutableArray * brandArr;//to compare with tata and vizag list
}

@end

@implementation CompareViewController
@synthesize btnSelect;

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    CGRect screenBounds = [UIScreen mainScreen].bounds ;
    CGFloat width = CGRectGetWidth(screenBounds)  ;
    CGFloat height = CGRectGetHeight(screenBounds) ;
    // do something before rotation
    NSLog(@"view width before%f",width);
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    //UIInterfaceOrientationLandscapeRight
    [self custom_sizes];
    //[self viewDidLoad];
}
-(void)custom_sizes
{
    CGRect screenBounds = [UIScreen mainScreen].bounds ;
    CGFloat width = CGRectGetWidth(screenBounds)  ;
    CGFloat height = CGRectGetHeight(screenBounds) ;
    // do something after rotation
    NSLog(@"view width after %f",width);
    float view_width;
//    if ([sponser_count_tag isEqualToString:@"2"])
//    {
        view_width=width/5;
        _product_width.constant=view_width;
        _width_2.constant=view_width;
        _width_3.constant=view_width;
        _width_4.constant=view_width;
        _width_5.constant=view_width;
        _jsw_lbl.hidden=NO;
  //  }
//    else
//    {
//        view_width=width/4;
//        _product_width.constant=view_width;
//        _width_2.constant=view_width;
//        _width_3.constant=view_width;
//        _width_4.constant=0;
//        _width_5.constant=view_width;
//        _jsw_lbl.hidden=YES;
//    }

}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
    
    // Use this to allow upside down as well
    //return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _compareListview.dataSource = self;
    _compareListview.delegate = self;
    tataVizagArr = [[NSMutableArray alloc]init];
    compareArr = [[NSMutableArray alloc]init];
    brandArr = [[NSMutableArray alloc]init];
    tonnes_names_ary=[_T_P_Ary mutableCopy];
    self.automaticallyAdjustsScrollViewInsets = NO;
    _lbl_1.text=_selected_brand;
    btnSelect.layer.borderWidth = 1;
    btnSelect.layer.borderColor = [[UIColor blackColor] CGColor];
    btnSelect.layer.cornerRadius = 5;

    CGRect screenBounds = [UIScreen mainScreen].bounds ;
    CGFloat width = CGRectGetWidth(screenBounds)  ;
    CGFloat height = CGRectGetHeight(screenBounds) ;
    // do something after rotation
    NSLog(@"view width after %f",width);
    float view_width=width/4;
    _product_width.constant=view_width;
    _width_2.constant=view_width;
    _width_3.constant=view_width;
    _width_4.constant=0;
    _width_5.constant=view_width;
    _jsw_lbl.hidden=YES;
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
    {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    user_data=[NSUserDefaults standardUserDefaults];
    
    NSString *C_Count=[user_data valueForKey:@"cart_count"];
    if ([C_Count isEqual:[NSNull null]]||[C_Count isEqualToString:@""]||C_Count ==nil||[C_Count isEqualToString:@"<nil>"])
    {
        _Cart_count.text=@"0";
        user_data=[NSUserDefaults standardUserDefaults];
        [user_data setValue:@"0" forKey:@"cart_count"];
    }
    else
        _Cart_count.text=C_Count;
    
    
//    _Cart_BackgrndView.layer.borderWidth = 1;
//    _Cart_BackgrndView.layer.borderColor = [[UIColor blackColor] CGColor];
//    _Cart_BackgrndView.layer.cornerRadius = 10;
//    
   // tap gesture
    UITapGestureRecognizer *cart_tap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(Cart_click:)];
    [_Cart_count addGestureRecognizer:cart_tap];
    
    UITapGestureRecognizer *tap =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapped)];
    [self.view addGestureRecognizer:tap];
    [tap setNumberOfTapsRequired:1];
    [tap setCancelsTouchesInView:NO];
    
    [self preferredStatusBarStyle];
    [self setNeedsStatusBarAppearanceUpdate];
    self.navigationController.navigationBar.hidden =YES;
    
           [self getcomapare];
}

-(void)tapped
{
    //    _searchTextField.text =@"";
    //    selectedDic = [[NSDictionary alloc]init];
    //    searchView.hidden =YES;
    [self.view endEditing:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    AppDelegate *del = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [del setShouldRotate:YES];
    
    [UIViewController attemptRotationToDeviceOrientation];

    self.navigationController.navigationBar.hidden =YES;
    self.automaticallyAdjustsScrollViewInsets = NO;

    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
    {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    user_data=[NSUserDefaults standardUserDefaults];
    NSString *C_Count=[user_data valueForKey:@"cart_count"];
    _Cart_count.text=C_Count;
}

-(void)getcomapare
{
   // _sel_ton = [_sel_ton stringByReplacingOccurrencesOfString:@"_"
                                        // withString:@","];
    NSString *url_form=[NSString stringWithFormat:@"getcompare?ids=%@&location=%@&pincode=%@&brand=%@&grade=%@&ton=%@&childs=%@",_selected_ids,_Location,_selectedPincode,_selected_brand,_selected_grade,_sel_ton,_sel_childs];
    
    NSString* urlEncoded = [url_form stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString:urlEncoded requestNumber:WS_GET_GetComapare showProgress:YES withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             if (data)
             {
                 brandsInfoList_dic=[data valueForKey:@"brandsInfoList"] ;
                tataVizagArr=[brandsInfoList_dic valueForKey:@"brandinfo"] ;
//                 for (int i=0 ; i<ary.count ; i++){
//
//
//                    NSMutableArray *arr1 = [[ary objectAtIndex:i] mutableCopy];
//                     tataVizagArr = [tataVizagArr addObject:[[ary objectAtIndex:i] mutableCopy]];
//
//                 }
                 
                 
                 
                 
                 

                 //tataVizagArr = [[ary objectAtIndex:0] mutableCopy];
                 compareArr = [data valueForKey:@"otherbrands"];
             }
             else
             {
             }
             if ([_selected_brand isEqualToString:@"Vizag Steel"]||[_selected_brand isEqualToString:@"JSW Steel"])
             {
                 //[_compareListview reloadData];
             }
             else
             {
               // [btnSelect setTitle:_selected_brand forState:UIControlStateNormal];
                //[self getBrandInfo:_selected_brand];
             }
             [self getBrandInfo:_selected_brand];
             if (tataVizagArr.count==2)
             {
             sponser_count_tag=@"2";
             }
             else
                 sponser_count_tag=@"1";

             [self custom_sizes];

                 //[[UIDevice currentDevice] setValue:@(UIInterfaceOrientationLandscapeLeft) forKey:@"orientation"];
             //[_compareListview reloadData];

         }
         else
         {
             //            UIAlertController * alert=   [UIAlertController
             //                                          alertControllerWithTitle:@"Alert"
             //                                          message:[NSString stringWithFormat:@"%@",[data valueForKey:@"error_message"]]
             //                                          preferredStyle:UIAlertControllerStyleAlert];
             //
             //            UIAlertAction* ok = [UIAlertAction
             //                                 actionWithTitle:@"OK"
             //                                 style:UIAlertActionStyleDefault
             //                                 handler:^(UIAlertAction * action)
             //                                 {
             //                                     [alert dismissViewControllerAnimated:YES completion:nil];
             //                                 }];
             //            [alert addAction:ok];
             //            [self presentViewController:alert animated:YES completion:nil];
         }
     }];
}

- (void)viewDidUnload
{
    //    [btnSelect release];
    btnSelect = nil;
    [self setBtnSelect:nil];
    [super viewDidUnload];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tataVizagArr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if ([sponser_count_tag isEqualToString:@"2"])
//    {
        static NSString *simpleTableIdentifier = @"CellIdentifier1";
        Compare_Landscape_cell *cell = (Compare_Landscape_cell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"Compare_Landscape_cell" owner:self options:nil];
            cell = (Compare_Landscape_cell *)[nib objectAtIndex:0];
        }
        
     
        
        
            NSArray *ar = tataVizagArr[indexPath.row];
        
          for (int i=0 ; i<ar.count ; i++)
         {
        
             NSDictionary *obj = ar[i];
             
             NSString *brand = [obj valueForKey:@"brand"];
             NSString *seller = [obj valueForKey:@"seller"];
            

             if ([brand  isEqual: @"Vizag Steel"])
                        {
                            if ([seller  isEqual: @""])
                            {
                                cell.vizagPriceLbl.text = @"Out Of Stock";
                                cell.VizagSellerNameLbl.hidden=YES;
                                cell.vizagFinalPriceLbl.hidden=YES;
                                cell.per_ton2.hidden=YES;
                                cell.estimated2.hidden=YES;
                                cell.sellername2.hidden=YES;
                            }
                            else
                            {
                                cell.vizagPriceLbl.text = [NSString stringWithFormat:@"Rs. %@",[obj valueForKey:@"tonprice"]];
                                cell.VizagSellerNameLbl.text = [NSString stringWithFormat:@" %@",[obj valueForKey:@"seller"]];
                                 float tonsFromScreen = [[NSString stringWithFormat:@"%@",[[tonnes_names_ary valueForKey:@"tonnes"]objectAtIndex:indexPath.row]] floatValue];
            
                                cell.vizagFinalPriceLbl.text = [NSString stringWithFormat:@"Rs. %@",[obj valueForKey:@"price"] /**tonsFromScreen*/];//pass on of tons
            
                                cell.VizagSellerNameLbl.hidden=NO;
                                cell.vizagFinalPriceLbl.hidden=NO;
                                cell.per_ton2.hidden=NO;
                                cell.estimated2.hidden=NO;
                                cell.sellername2.hidden=NO;
                            }
                        }
              if ([brand  isEqual: @"JSW Steel"])
            {
                 if ([seller  isEqual: @""])
                {
                    cell.jswPriceLbl.text = @"Out Of Stock";
                    cell.jswSellerNameLbl.hidden=YES;
                    cell.jswFinalPriceLbl.hidden=YES;
                    cell.per_ton3.hidden=YES;
                    cell.estimated3.hidden=YES;
                    cell.sellername3.hidden=YES;
                }
                else
                {
                    cell.jswPriceLbl.text = [NSString stringWithFormat:@"Rs. %@",[obj valueForKey:@"tonprice"]];
                    cell.jswSellerNameLbl.text = [NSString stringWithFormat:@" %@",[obj valueForKey:@"seller"]];
                    float tonsFromScreen = [[NSString stringWithFormat:@"%@",[[tonnes_names_ary valueForKey:@"tonnes"]objectAtIndex:indexPath.row]] floatValue];
                    
                    cell.jswFinalPriceLbl.text = [NSString stringWithFormat:@"Rs. %@",[obj valueForKey:@"price"] /**tonsFromScreen*/];//pass on of tons
                    
                    cell.jswSellerNameLbl.hidden=NO;
                    cell.jswFinalPriceLbl.hidden=NO;
                    cell.per_ton3.hidden=NO;
                    cell.estimated3.hidden=NO;
                    cell.sellername3.hidden=NO;
                }
            }
        
            cell.name.text=[[tonnes_names_ary valueForKey:@"name"]objectAtIndex:indexPath.row];
            cell.tonnes.text=[NSString stringWithFormat:@"%@ tonnes",[[tonnes_names_ary valueForKey:@"tonnes"]objectAtIndex:indexPath.row]];
            cell.pieces.text=[NSString stringWithFormat:@"%@ Pieces",[[tonnes_names_ary valueForKey:@"pieces"]objectAtIndex:indexPath.row]];
            NSURL *imageURL = [NSURL URLWithString:[[tonnes_names_ary valueForKey:@"img"]objectAtIndex:indexPath.row]];
            [cell.img sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
           
//            [cell.img.layer setBorderColor: [[UIColor colorWithRed:213.0/255.0 green:216.0/255.0 blue:224.0/255.0 alpha:1] CGColor]];
//            [cell.img.layer setBorderWidth: 2.0];
        }
        
        if (!([brandArr count] == 0))
        {
            
            //  NSArray *arr = brandArr[indexPath.row];
            
            cell.compareCellarNameLbl.hidden=NO;
            cell.per_ton2.hidden=NO;
            cell.estimated2.hidden=NO;
            cell.sellername2.hidden=NO;
            
            if ([[[brandArr objectAtIndex:indexPath.row] valueForKey:@"seller"]isEqualToString:@""])
            {
                //cell.comparePriceLbl.text = @"Not Avilable";
                //cell.compareCellarNameLbl.text = @"Not Avilable";
                if ([[[brandArr objectAtIndex:indexPath.row] valueForKey:@"brand"]isEqualToString:_selected_brand])
                {
                    cell.tataPriceLbl.text = @"Out Of Stock";
                    cell.tataSellerNameLbl.hidden=YES;
                    cell.tataFinalPriceLbl.hidden=YES;
                    cell.per_ton.hidden=YES;
                    cell.estimated.hidden=YES;
                    cell.sellername.hidden=YES;
                    
                }
                else
                {
                    cell.comparePriceLbl.text = @"Out Of Stock";
                    cell.compareCellarNameLbl.hidden=YES;
                    cell.compareFinalPriceLbl.hidden=YES;
                    cell.per_ton2.hidden=YES;
                    cell.estimated2.hidden=YES;
                    cell.sellername2.hidden=YES;
                }
            }
            else
            {
                if ([[[brandArr objectAtIndex:indexPath.row] valueForKey:@"brand"]isEqualToString:_selected_brand])
                {
//                    cell.tataPriceLbl.text = [NSString stringWithFormat:@"Rs. %@",[[brandArr objectAtIndex:indexPath.row]valueForKey:@"tonprice"]];
//                    cell.tataSellerNameLbl.text = [NSString stringWithFormat:@" %@",[[brandArr objectAtIndex:indexPath.row]valueForKey:@"seller"]];
//                   // float tonsFromScreen = [[NSString stringWithFormat:@"%@",[[tonnes_names_ary valueForKey:@"tonnes"]objectAtIndex:indexPath.row]] floatValue];
//                    cell.tataFinalPriceLbl.text = [NSString stringWithFormat:@"Rs. %@",[[brandArr objectAtIndex:indexPath.row]valueForKey:@"price"] /**tonsFromScreen*/];//pass on of tons
                    
                    cell.tataPriceLbl.text = [NSString stringWithFormat:@"Rs. %@",[[_product_data objectAtIndex:indexPath.row]valueForKey:@"tonprice"]];
                    cell.tataSellerNameLbl.text = [NSString stringWithFormat:@" %@",[[_product_data objectAtIndex:indexPath.row]valueForKey:@"seller"]];
                    // float tonsFromScreen = [[NSString stringWithFormat:@"%@",[[tonnes_names_ary valueForKey:@"tonnes"]objectAtIndex:indexPath.row]] floatValue];
                    cell.tataFinalPriceLbl.text = [NSString stringWithFormat:@"Rs. %@",[[_product_data objectAtIndex:indexPath.row]valueForKey:@"price"] /**tonsFromScreen*/];//pass on of tons
                    
                    
                    
                    cell.tataSellerNameLbl.hidden=NO;
                    cell.tataFinalPriceLbl.hidden=NO;
                    cell.per_ton.hidden=NO;
                    cell.estimated.hidden=NO;
                    cell.sellername.hidden=NO;
                    
                    cell.comparePriceLbl.text = @"Out Of Stock";
                    cell.compareCellarNameLbl.hidden=YES;
                    cell.compareFinalPriceLbl.hidden=YES;
                    cell.per_ton2.hidden=YES;
                    cell.estimated2.hidden=YES;
                    cell.sellername2.hidden=YES;
                }
                else
                {
                    cell.comparePriceLbl.text = [NSString stringWithFormat:@"Rs. %@",[[brandArr objectAtIndex:indexPath.row] valueForKey:@"tonprice"]];
                    cell.compareCellarNameLbl.text = [NSString stringWithFormat:@" %@",[[brandArr objectAtIndex:indexPath.row] valueForKey:@"seller"]];
                    float tonsFromScreen = [[NSString stringWithFormat:@"%@",[[tonnes_names_ary valueForKey:@"tonnes"]objectAtIndex:indexPath.row]] floatValue];
                    
                    cell.compareFinalPriceLbl.text = [NSString stringWithFormat:@"Rs. %@",[[brandArr objectAtIndex:indexPath.row] valueForKey:@"price"] /**tonsFromScreen*/];//pass no of tonns
                    
                    cell.compareCellarNameLbl.hidden=NO;
                    cell.compareFinalPriceLbl.hidden=NO;
                    cell.per_ton2.hidden=NO;
                    cell.estimated2.hidden=NO;
                    cell.sellername2.hidden=NO;
                }
            }
        }
        else
        {
            // cell.comparePriceLbl.text = @"Not Avilable";
            // cell.compareCellarNameLbl.text = @"Not Avilable";
            
            cell.comparePriceLbl.text = @"Out Of Stock";
            cell.compareCellarNameLbl.hidden=YES;
            //   cell.tataFinalPriceLbl.hidden=YES;
            cell.per_ton2.hidden=YES;
            cell.estimated2.hidden=YES;
            cell.sellername2.hidden=YES;
        }
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        return cell;
    //}
    
    //else//portrait
//    {
//        static NSString *simpleTableIdentifier = @"CellIdentifier";
//        CompareTableViewCell *cell = (CompareTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
//
//        if (cell == nil)
//        {
//            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CompareTableViewCell" owner:self options:nil];
//            cell = (CompareTableViewCell *)[nib objectAtIndex:0];
//        }
//
//    NSArray *ar = tataVizagArr[indexPath.row];
//    for (NSDictionary *dictt in ar)
//    {
//        // accessing the custom objects inside the nested arrays
//            if ([[dictt valueForKey:@"brand"]isEqualToString:@"Vizag Steel"])
//            {
//                if ([[dictt valueForKey:@"seller"]isEqualToString:@""])
//                {
//                    cell.vizagPriceLbl.text = @"Out Of Stock";
//                    cell.VizagSellerNameLbl.hidden=YES;
//                    cell.vizagFinalPriceLbl.hidden=YES;
//                    cell.per_ton1.hidden=YES;
//                    cell.estimated1.hidden=YES;
//                    cell.sellername1.hidden=YES;
//                }
//                else
//                {
//                cell.vizagPriceLbl.text = [NSString stringWithFormat:@"Rs. %@",[dictt valueForKey:@"tonprice"]];
//                cell.VizagSellerNameLbl.text = [NSString stringWithFormat:@" %@",[dictt valueForKey:@"seller"]];
//
//                     float tonsFromScreen = [[NSString stringWithFormat:@"%@",[[tonnes_names_ary valueForKey:@"tonnes"]objectAtIndex:indexPath.row]] floatValue];
//
//                cell.vizagFinalPriceLbl.text = [NSString stringWithFormat:@"Rs. %@",[dictt valueForKey:@"price"] /**tonsFromScreen*/];
//
//                    cell.VizagSellerNameLbl.hidden=NO;
//                    cell.vizagFinalPriceLbl.hidden=NO;
//                    cell.per_ton1.hidden=NO;
//                    cell.estimated1.hidden=NO;
//                    cell.sellername1.hidden=NO;
//                }
//             }
//
//          else if ([[dictt valueForKey:@"brand"]isEqualToString:@"JSW Steel"])
//            {
//                if ([[dictt valueForKey:@"seller"]isEqualToString:@""])
//                {
//                    cell.tataPriceLbl.text = @"Out Of Stock";
//                    cell.tataSellerNameLbl.hidden=YES;
//                    cell.tataFinalPriceLbl.hidden=YES;
//                    cell.per_ton.hidden=YES;
//                    cell.estimated.hidden=YES;
//                    cell.sellername.hidden=YES;
//                }
//                else
//                {
//                    cell.tataPriceLbl.text = [NSString stringWithFormat:@"₹ %@",[dictt valueForKey:@"tonprice"]];
//                    cell.tataSellerNameLbl.text = [NSString stringWithFormat:@" %@",[dictt valueForKey:@"seller"]];
//                     float tonsFromScreen = [[NSString stringWithFormat:@"%@",[[tonnes_names_ary valueForKey:@"tonnes"]objectAtIndex:indexPath.row]] floatValue];
//
//                    cell.tataFinalPriceLbl.text = [NSString stringWithFormat:@"₹ %@",[dictt valueForKey:@"price"] ];//pass on of tons
//
//                    cell.tataSellerNameLbl.hidden=NO;
//                    cell.tataFinalPriceLbl.hidden=NO;
//                    cell.per_ton.hidden=NO;
//                    cell.estimated.hidden=NO;
//                    cell.sellername.hidden=NO;
//                }
//            }
//          else{
//
//
//        cell.name.text=[[tonnes_names_ary valueForKey:@"name"]objectAtIndex:indexPath.row];
//        cell.tonnes.text=[NSString stringWithFormat:@"%@ tonnes",[[tonnes_names_ary valueForKey:@"tonnes"]objectAtIndex:indexPath.row]];
//        cell.pieces.text=[NSString stringWithFormat:@"%@ Pieces",[[tonnes_names_ary valueForKey:@"pieces"]objectAtIndex:indexPath.row]];
//        NSURL *imageURL = [NSURL URLWithString:[[tonnes_names_ary valueForKey:@"img"]objectAtIndex:indexPath.row]];
//        [cell.img sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
//          }
////        [cell.img.layer setBorderColor: [[UIColor colorWithRed:213.0/255.0 green:216.0/255.0 blue:224.0/255.0 alpha:1] CGColor]];
////        [cell.img.layer setBorderWidth: 2.0];
//    }
//
//    if (!([brandArr count] == 0))
//    {
//
//      //  NSArray *arr = brandArr[indexPath.row];
//
//        cell.compareCellarNameLbl.hidden=NO;
//        cell.per_ton2.hidden=NO;
//        cell.estimated2.hidden=NO;
//        cell.sellername2.hidden=NO;
//
//                if ([[[brandArr objectAtIndex:indexPath.row] valueForKey:@"seller"]isEqualToString:@""])
//                {
//                    //cell.comparePriceLbl.text = @"Not Avilable";
//                    //cell.compareCellarNameLbl.text = @"Not Avilable";
//                    if ([[[brandArr objectAtIndex:indexPath.row] valueForKey:@"brand"]isEqualToString:_selected_brand])
//                    {
//                        cell.tataPriceLbl.text = @"Out Of Stock";
//                        cell.tataSellerNameLbl.hidden=YES;
//                        cell.tataFinalPriceLbl.hidden=YES;
//                        cell.per_ton.hidden=YES;
//                        cell.estimated.hidden=YES;
//                        cell.sellername.hidden=YES;
//
//                    }
//                    else
//                    {
//                    cell.comparePriceLbl.text = @"Out Of Stock";
//                    cell.compareCellarNameLbl.hidden=YES;
//                       cell.compareFinalPriceLbl.hidden=YES;
//                    cell.per_ton2.hidden=YES;
//                    cell.estimated2.hidden=YES;
//                    cell.sellername2.hidden=YES;
//                    }
//                }
//                else
//                {
//                    if ([[[brandArr objectAtIndex:indexPath.row] valueForKey:@"brand"]isEqualToString:_selected_brand])
//                    {
//                            cell.tataPriceLbl.text = [NSString stringWithFormat:@"Rs. %@",[[brandArr objectAtIndex:indexPath.row]valueForKey:@"tonprice"]];
//                            cell.tataSellerNameLbl.text = [NSString stringWithFormat:@" %@",[[brandArr objectAtIndex:indexPath.row]valueForKey:@"seller"]];
//                            float tonsFromScreen = [[NSString stringWithFormat:@"%@",[[tonnes_names_ary valueForKey:@"tonnes"]objectAtIndex:indexPath.row]] floatValue];
//
//                            cell.tataFinalPriceLbl.text = [NSString stringWithFormat:@"Rs. %@",[[brandArr objectAtIndex:indexPath.row]valueForKey:@"price"] /**tonsFromScreen*/];//pass on of tons
//
//                            cell.tataSellerNameLbl.hidden=NO;
//                            cell.tataFinalPriceLbl.hidden=NO;
//                            cell.per_ton.hidden=NO;
//                            cell.estimated.hidden=NO;
//                            cell.sellername.hidden=NO;
//
//                        cell.comparePriceLbl.text = @"Out Of Stock";
//                        cell.compareCellarNameLbl.hidden=YES;
//                        cell.compareFinalPriceLbl.hidden=YES;
//                        cell.per_ton2.hidden=YES;
//                        cell.estimated2.hidden=YES;
//                        cell.sellername2.hidden=YES;
//                    }
//                    else
//                    {
//                    cell.comparePriceLbl.text = [NSString stringWithFormat:@"Rs. %@",[[brandArr objectAtIndex:indexPath.row] valueForKey:@"tonprice"]];
//                    cell.compareCellarNameLbl.text = [NSString stringWithFormat:@" %@",[[brandArr objectAtIndex:indexPath.row] valueForKey:@"seller"]];
//                    float tonsFromScreen = [[NSString stringWithFormat:@"%@",[[tonnes_names_ary valueForKey:@"tonnes"]objectAtIndex:indexPath.row]] floatValue];
//
//                    cell.compareFinalPriceLbl.text = [NSString stringWithFormat:@"Rs. %@",[[brandArr objectAtIndex:indexPath.row] valueForKey:@"price"] /**tonsFromScreen*/];//pass no of tonns
//
//                    cell.compareCellarNameLbl.hidden=NO;
//                    cell.compareFinalPriceLbl.hidden=NO;
//                    cell.per_ton2.hidden=NO;
//                    cell.estimated2.hidden=NO;
//                    cell.sellername2.hidden=NO;
//                    }
//                }
//    }
//    else
//    {
//       // cell.comparePriceLbl.text = @"Not Avilable";
//       // cell.compareCellarNameLbl.text = @"Not Avilable";
//
//        cell.comparePriceLbl.text = @"Out Of Stock";
//        cell.compareCellarNameLbl.hidden=YES;
//     //   cell.tataFinalPriceLbl.hidden=YES;
//        cell.per_ton2.hidden=YES;
//        cell.estimated2.hidden=YES;
//        cell.sellername2.hidden=YES;
//    }
//
//    cell.selectionStyle=UITableViewCellSelectionStyleNone;
//
//    return cell;
//    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{    //nothing to do now
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 260;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)selectClicked:(id)sender
{
    NSArray * arr = [[NSArray alloc] init];
    arr = [compareArr copy];
   

   
    if(dropDown == nil) {
        CGFloat f = 200;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :@"down"];
        dropDown.delegate = self;
        dropDown.userInteractionEnabled =YES;
        _compareListview.userInteractionEnabled = false;
        [self.view bringSubviewToFront:dropDown];
    }
    else {
        _compareListview.userInteractionEnabled = true;
        [dropDown hideDropDown:sender];
        [self rel];
    }
}

- (IBAction)backButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)Cart_click:(id)sender {
    
    user_data=[NSUserDefaults standardUserDefaults];
    user_id=[user_data valueForKey:@"user_id"];
    
    if ([user_id isEqualToString:@""]||[user_id isEqual:[NSNull null]]||[user_id isEqualToString:@"<nil>"]||user_id == nil||[user_id isEqualToString:@"0"])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        NewLoginVC *myNewVC = (NewLoginVC *)[storyboard instantiateViewControllerWithIdentifier:@"NewLoginVC"];
        [self.navigationController pushViewController:myNewVC animated:YES];
    }
    else//go to cart page
    {
        My_Cart *define = [[My_Cart alloc]init];
        [self.navigationController pushViewController:define animated:YES];
    }
}

- (void) niDropDownDelegateMethod: (NIDropDown *) sender selectedTXT:(NSString *)selctedTitle {
    
    [self rel];
    
    NSLog(@"%@", selctedTitle);
    NSString *brand = selctedTitle;
    if (![selctedTitle isEqualToString:@"--Select Brand--"]){
        
            [self getBrandInfo:brand];
    }else{
        
            [btnSelect setTitle:@"Select Brand" forState:UIControlStateNormal];
    }
}

-(void)rel
{
    _compareListview.userInteractionEnabled = true;
    dropDown = nil;
}

-(void)getBrandInfo:(NSString *)brand

{
    
  //  _sel_ton = [_sel_ton stringByReplacingOccurrencesOfString:@"_"
                                            //       withString:@","];
    
    NSString *url_form=[NSString stringWithFormat:@"getMinProductInfo?ids=%@&location=%@&brand=%@&grade=%@&ton=%@&childs=%@",_selected_ids,_Location,brand,_selected_grade,_sel_ton,_sel_childs];
    
    NSString* urlEncoded = [url_form stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString:urlEncoded requestNumber:WS_GET_GetBrandInfo showProgress:YES withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             if (data)
             {
                    brandArr = data;
                     [_compareListview reloadData];
             }
             
         }
         else
         {
             //            UIAlertController * alert=   [UIAlertController
             //                                          alertControllerWithTitle:@"Alert"
             //                                          message:[NSString stringWithFormat:@"%@",[data valueForKey:@"error_message"]]
             //                                          preferredStyle:UIAlertControllerStyleAlert];
             //
             //            UIAlertAction* ok = [UIAlertAction
             //                                 actionWithTitle:@"OK"
             //                                 style:UIAlertActionStyleDefault
             //                                 handler:^(UIAlertAction * action)
             //                                 {
             //                                     [alert dismissViewControllerAnimated:YES completion:nil];
             //                                 }];
             //            [alert addAction:ok];
             //            [self presentViewController:alert animated:YES completion:nil];
         }
     }];
    
    
    
    
    
    
    
    
    
}
- (IBAction)goToHomeScreenAction:(id)sender {
    HomeViewController *con = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
    [self.navigationController pushViewController:con animated:YES];
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
