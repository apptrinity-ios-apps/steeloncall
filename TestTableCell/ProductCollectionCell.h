//
//  ProductCollectionCell.h
//  SteelonCall
//
//  Created by INDOBYTES on 20/11/18.
//  Copyright © 2018 com.indobytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductCollectionCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *image_check;

@property (strong, nonatomic) IBOutlet UIImageView *brand_Image;
@property (strong, nonatomic) IBOutlet UILabel *brand_Lbl;

@property (strong, nonatomic) IBOutlet UILabel *brand_NameLbl;



@end
