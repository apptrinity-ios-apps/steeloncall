//
//  ChieldTableViewCell.m
//  SteelonCall
//
//  Created by Manoyadav on 01/12/16.
//  Copyright © 2016 com.way2online. All rights reserved.
//

#import "ChieldTableViewCell.h"

@implementation ChieldTableViewCell
{
    NSUserDefaults * user_data;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    user_data=[NSUserDefaults standardUserDefaults];
    NSString * deviceName = [user_data valueForKey:@"device"];
    
    if ([deviceName isEqualToString:@"iPhone"]) {
        _backImage.frame = CGRectMake(25,5, 210.0, 40);
    }
    else
    {
        _backImage.frame = CGRectMake(25,5, 460, 40);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
