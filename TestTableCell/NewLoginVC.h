//
//  NewLoginVC.h
//  SteelonCall
//
//  Created by nagaraj  kumar on 24/10/18.
//  Copyright © 2018 com.indobytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RegisterViewController.h"
#import "CRToastView.h"
#import "STParsing.h"
#import "My_Cart.h"
#import "MBProgressHUD.h"
#import "YourOrdersViewController.h"
#import "Tracking.h"
#import "AccountManageViewController.h"




@interface NewLoginVC : UIViewController<UITextFieldDelegate>
{
    BOOL Networkstatus;
    UIView *network_view;
    NSUserDefaults *user_data;
    
    
}



@property (weak, nonatomic) IBOutlet UITextField *forgot_Email_TFT;


@property (weak, nonatomic) IBOutlet UIView *forgotPassword_view;


@property (weak, nonatomic) IBOutlet UIButton *proceed_Btn;


@property (strong, nonatomic) IBOutlet UIView *activityView;
@property (nonatomic, strong) MBProgressHUD  *HUD;



@property (weak, nonatomic) IBOutlet UIButton *BG_Btn;


@property (weak, nonatomic) IBOutlet UIButton *login_Btn;
@property (weak, nonatomic) IBOutlet UIButton *register_Btn;

@property (weak, nonatomic) IBOutlet UIView *loginPopUpView;

@property (weak, nonatomic) IBOutlet UIButton *popUp_LoginBtn;
@property (weak, nonatomic) IBOutlet UITextField *emailTFT;
@property (weak, nonatomic) IBOutlet UITextField *passwordTFT;
@property (weak, nonatomic) IBOutlet UIView *email_image;
@property (weak, nonatomic) IBOutlet UIImageView *password_image;


@property (weak, nonatomic) IBOutlet UIButton *forgotPassword_Btn;

@property (weak, nonatomic) IBOutlet UIView *signUp_popView;
@property (weak, nonatomic) IBOutlet UITextField *signUp_email;
@property (weak, nonatomic) IBOutlet UITextField *signUp_password;
@property (weak, nonatomic) IBOutlet UITextField *signUp_ConfirmPassword;

@property (weak, nonatomic) IBOutlet UIButton *signUp_registerBtn;

@property (weak, nonatomic) IBOutlet UIImageView *signUp_password_Image;
@property (weak, nonatomic) IBOutlet UIImageView *signUp_confirmPass_Image;


//@property (nonatomic, strong) MBProgressHUD  *HUD;
@property (strong, nonatomic)  NSString *From;
@property (strong, nonatomic)  NSDictionary *cart_dict;


@end
