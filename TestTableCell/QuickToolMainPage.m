//
//  QuickToolMainPage.m
//  Smatrimony
//
//  Created by S s Vali on 7/17/17.
//  Copyright © 2017 Indobytes. All rights reserved.
//

#import "QuickToolMainPage.h"

@interface QuickToolMainPage ()
{
NSIndexPath *path;
NSUInteger presentIndex;
}
@end

@implementation QuickToolMainPage

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.pageControl.numberOfPages = 6;
    [self pageViewHandler];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)pageViewHandler
{
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    self.pageController.view.autoresizesSubviews =YES;
    
    self.pageController.dataSource = self;
    self.pageController.delegate = self;
    
    
    ChildController *childViewController = [[ChildController alloc] initWithNibName:@"ChildController" bundle:nil];
    childViewController.index = 0;
    NSArray *viewControllers = [NSArray arrayWithObject:childViewController];
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    
    [self.pageViewOutlet addSubview:self.pageController.view];
    [self.pageController.view setFrame:CGRectMake(0,0,self.pageViewOutlet.bounds.size.width, self.pageViewOutlet.bounds.size.height)];
    
    [self.pageController didMoveToParentViewController:self];
    
    
}
#pragma mark - pageViewDelegates

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = [(ChildController *)viewController index];
    ChildController *childViewController = [[ChildController alloc] initWithNibName:@"ChildController" bundle:nil];
    presentIndex = index;
    if (index == 0) {
        return nil;
    }
    
    index--;
    //    childViewController.selectedValue = [NSString stringWithFormat:@"%@",[menuArr objectAtIndex:index]];
    //    childViewController.checkValue = _displayValue;
    
    if (index == 6) {
        [self.nextBtn setTitle:@"START" forState:UIControlStateNormal];
        self.skipBtn.hidden = true;
        return nil;
    }
    else
    {
        [self.nextBtn setTitle:@"NEXT" forState:UIControlStateNormal];
        self.skipBtn.hidden = false;
    }
    childViewController.index = index;
    return childViewController;
}
- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = [(ChildController *)viewController index];
    ChildController *childViewController = [[ChildController alloc] initWithNibName:@"ChildController" bundle:nil];
    presentIndex = index;
    index++;
    childViewController.index = index;
    
    if (index == 6) {
        [self.nextBtn setTitle:@"START" forState:UIControlStateNormal];
        self.skipBtn.hidden = true;
        return nil;
    }
    else
    {
        [self.nextBtn setTitle:@"NEXT" forState:UIControlStateNormal];
        self.skipBtn.hidden = false;
    }
    
    return childViewController;
}
- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
    
    if (!completed)
    {
        return;
    }
    
    
    NSUInteger indexOfCurrentPage = [[self.pageController.viewControllers lastObject] index];
    self.pageControl.currentPage = indexOfCurrentPage;
    self.pageControl.pageIndicatorTintColor = [UIColor clearColor];
    self.pageControl.currentPageIndicatorTintColor = [UIColor lightGrayColor];
   // self.pageControl.backgroundColor = [UIColor blueColor];
    
}
- (IBAction)nextAction:(id)sender {
    presentIndex += 1;
    self.pageControl.currentPage = presentIndex;
    self.pageControl.pageIndicatorTintColor = [UIColor whiteColor];
    self.pageControl.currentPageIndicatorTintColor = [UIColor lightGrayColor];
    if (presentIndex == 5) {
        [self.nextBtn setTitle:@"START" forState:UIControlStateNormal];
        self.skipBtn.hidden = true;
    }
    else
    {
        [self.nextBtn setTitle:@"NEXT" forState:UIControlStateNormal];
        self.skipBtn.hidden = false;
    }
    if (presentIndex == 6) {
        
        HomeViewController * obj = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
        [self.navigationController pushViewController:obj animated:YES];
    }
    else
    {
        
        ChildController *childViewController = [[ChildController alloc] initWithNibName:@"ChildController" bundle:nil];
        childViewController.index = presentIndex;
        [self.pageController setViewControllers:@[childViewController] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    }
    
}
- (IBAction)skipAction:(id)sender {
    HomeViewController * obj = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
    [self.navigationController pushViewController:obj animated:YES];
}
@end
