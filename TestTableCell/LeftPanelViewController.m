//
//  LeftPanelViewController.m
//  TestTableCell
//
//  Created by administrator on 29/11/16.
//  Copyright © 2016 com.SteelonCall. All rights reserved.
//

#import "LeftPanelViewController.h"
#import "TreeViewNode.h"
#import "MainTableViewCell.h"
#import "ChieldTableViewCell.h"
#import "AppDelegate.h"
#import "CateGoryTableViewCell.h"
#import "Products_List.h"
#import "DEMONavigationController.h"
@interface LeftPanelViewController ()
{
    NSUInteger indentation;
    NSArray *nodes;
    AppDelegate *appDle;
}

@property (strong, nonatomic) IBOutlet UITableView *leftMenuTable;
@property (nonatomic, retain) NSMutableArray *displayArray;
@property (strong, nonatomic) IBOutlet UIImageView *logoImg;

- (void)expandCollapseNode:(NSNotification *)notification;

- (void)fillDisplayArray;
- (void)fillNodeWithChildrenArray:(NSArray *)childrenArray;

- (void)fillNodesArray;
- (NSArray *)fillChildrenForNode;

- (IBAction)expandAll:(id)sender;
- (IBAction)collapseAll:(id)sender;

@end

@implementation LeftPanelViewController

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
    
}
-(void )viewWillAppear:(BOOL)animated{
   // [self.navigationController setNavigationBarHidden:NO];
    [self preferredStatusBarStyle];
    [self setNeedsStatusBarAppearanceUpdate];
    AppDelegate *del = (AppDelegate *)[UIApplication sharedApplication].delegate;
    if (appDle.itemsArray.count>0) {
        
        [self fillNodesArray];
        [self fillDisplayArray];
    }
    [self getUserData];
    [self.leftMenuTable reloadData];
    [del setShouldRotate:NO];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
  //  _logoImg.hidden = true;
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(expandCollapseNode:) name:@"ProjectTreeNodeButtonClicked" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(CloseCells) name:@"close" object:nil];
    
//self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"img_1.jpg"]];
   // [self.navigationController setNavigationBarHidden:NO];
    appDle = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDle.itemsArray.count>0) {
        
        [self fillNodesArray];
        [self fillDisplayArray];
    }
   
    [self.leftMenuTable reloadData];
    self.leftMenuTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self getUserData];
}

-(void)getUserData
{
    NSString *user_id =[[ NSUserDefaults standardUserDefaults]valueForKey:@"user_id"];
    //73
   NSUserDefaults * user_data=[NSUserDefaults standardUserDefaults];
   // NSString * deviceName = [user_data valueForKey:@"device"];
    NSString * deviceName = [user_data valueForKey:@"device"];
    
    if ([deviceName isEqualToString:@"iPhone"]) {
       
        _homeBtn.frame = CGRectMake(15,5, 220.0, 40);
    }
    else
    {
        _homeBtn.frame = CGRectMake(15,5, 470.0, 40);
    }

    
    
    if (user_id.length>0)
    {
        NSString *urlString = [NSString stringWithFormat:@"getcustomer?id=%@",user_id];
        
        
        [[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString:urlString requestNumber:WS_GET_ACCOUNT_INFO showProgress:YES withHandler:^(BOOL success, id data)
         {
             if (success)
             {
                 if (data)
                 {
                     NSArray *ary = data;
                     if (ary.count>0)
                     {
                         NSString *f_name=[NSString stringWithFormat:@"%@",[[data objectAtIndex:0]valueForKey:@"first_name"]];
                         NSString *l_name=[NSString stringWithFormat:@"%@",[[data objectAtIndex:0]valueForKey:@"last_name"]];
                         if (f_name == nil || [f_name isEqualToString:@"<null>"]|| [f_name isEqualToString:@"(null)"])
                         {
                             f_name=@"";
                         }
                         if (l_name == nil || [l_name isEqualToString:@"<null>"]|| [l_name isEqualToString:@"(null)"])
                         {
                             l_name=@"";
                         }
                         
                             _menu_name.text = [NSString stringWithFormat:@"%@ %@",f_name,l_name];
                         _menu_number.text=@"";

                     }
                     if (ary.count>1)
                     {
                         NSString *telephone=[NSString stringWithFormat:@"%@",[[data objectAtIndex:1]valueForKey:@"telephone"]];
                         NSString *mobile=[NSString stringWithFormat:@"%@",[[data objectAtIndex:1]valueForKey:@"mobile"]];
                         NSString *phone;
                         if (mobile == nil || [mobile isEqualToString:@"<null>"]|| [mobile isEqualToString:@"(null)"])
                         {
                            // phone=@"";
                             if (telephone == nil || [telephone isEqualToString:@"<null>"]|| [telephone isEqualToString:@"(null)"])
                             {
                                 phone=@"";
                             }
                             else
                             {
                                 phone=telephone;
                             }
                         }
//                         else if (telephone == nil || [telephone isEqualToString:@"<null>"]|| [telephone isEqualToString:@"(null)"])
//                         {
//                             phone=@"";
//                         }
                         else
                             phone=mobile;
 
                             _menu_number.text = phone;
                     }
                 }
             }
         }];
    }
}
- (void)viewDidUnload
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    [super viewDidUnload];
}


-(void)CloseCells
{
    for (TreeViewNode *treeNode in nodes)
    {
        treeNode.isExpanded = NO;
    }
    [self fillDisplayArray];
    [self.leftMenuTable reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)ClosePannel:(id)sender {
    // Dismiss keyboard (optional)
    //
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    // Present the view controller
    //
    [self.frostedViewController hideMenuViewController];

}

#pragma mark - Messages to fill the tree nodes and the display array

//This function is used to expand and collapse the node as a response to the ProjectTreeNodeButtonClicked notification
- (void)expandCollapseNode:(NSNotification *)notification
{
    [self fillDisplayArray];
    [self.leftMenuTable reloadData];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
//These two functions are used to fill the nodes array with the tree nodes
- (void)fillNodesArray
{
    TreeViewNode *firstLevelNode1 = [[TreeViewNode alloc]init];
    firstLevelNode1.nodeLevel = 0;
   // firstLevelNode1.item_img = @"Construction";
    
     firstLevelNode1.item_img = @"old-handphone.png";
    firstLevelNode1.nodeObject = [NSString stringWithFormat:@"CONTACT US"];
    firstLevelNode1.isExpanded = NO;
    firstLevelNode1.nodeChildren = [[self fillChildrenForNode] mutableCopy];
    firstLevelNode1.isHidden = YES;
    firstLevelNode1.isExpandable = NO;
    
    TreeViewNode *firstLevelNode2 = [[TreeViewNode alloc]init];
    firstLevelNode2.nodeLevel = 0;
     // firstLevelNode2.item_img = @"Fabrication";
    firstLevelNode2.item_img = @"Star";
    firstLevelNode2.nodeObject = [NSString stringWithFormat:@"RATE OUR APP"];
    firstLevelNode2.isExpanded = NO;
    firstLevelNode2.nodeChildren = [[self subNodes] mutableCopy];
     firstLevelNode2.isHidden = NO;
    firstLevelNode2.isExpandable = NO;
    
    TreeViewNode *firstLevelNode3 = [[TreeViewNode alloc]init];
    firstLevelNode3.nodeLevel = 0;
    firstLevelNode3.item_img = @"leftBlogIcon";
    firstLevelNode3.nodeObject = [NSString stringWithFormat:@"REQUEST CALLBACK"];
    firstLevelNode3.isExpanded = NO;
    firstLevelNode3.isHidden = NO;
    firstLevelNode3.nodeChildren = [[self fillChildrenForNode] mutableCopy];
    firstLevelNode3.isExpandable = NO;
    
//    TreeViewNode *firstLevelNode4 = [[TreeViewNode alloc]init];
//    firstLevelNode4.nodeLevel = 0;
//    firstLevelNode4.item_img = @"password";
//    firstLevelNode4.nodeObject = [NSString stringWithFormat:@"SIGN OUT"];
//    firstLevelNode4.isExpanded = NO;
//    firstLevelNode4.isHidden = NO;
//    firstLevelNode4.isExpandable = NO;
    
//    TreeViewNode *firstLevelNode5 = [[TreeViewNode alloc]init];
//    firstLevelNode5.nodeLevel = 0;
//    firstLevelNode5.item_img = @"rupee";
//    firstLevelNode5.nodeObject = [NSString stringWithFormat:@"Refer & Earn"];
//    firstLevelNode5.isExpanded = NO;
//    firstLevelNode5.isHidden = NO;
//    firstLevelNode5.isExpandable = NO;
    
//    TreeViewNode *firstLevelNode6 = [[TreeViewNode alloc]init];
//    firstLevelNode6.nodeLevel = 0;
//    firstLevelNode6.item_img = @"old-handphone.png";
//    firstLevelNode6.nodeObject = [NSString stringWithFormat:@"Contact Us"];
//    firstLevelNode6.isExpanded = NO;
//    firstLevelNode6.isHidden = NO;
//    firstLevelNode6.isExpandable = NO;
    
        NSUserDefaults *user_data=[NSUserDefaults standardUserDefaults];
        NSString *user_id=[user_data valueForKey:@"user_id"];
        TreeViewNode *firstLevelNode7;
        if ([user_id isEqualToString:@""]||[user_id isEqual:[NSNull null]]||[user_id isEqualToString:@"<nil>"]||user_id == nil||[user_id isEqualToString:@"0"])
        {
            firstLevelNode7 = [[TreeViewNode alloc]init];
            firstLevelNode7.nodeLevel = 0;
            firstLevelNode7.item_img = @"leftMenuSignIn";
            firstLevelNode7.nodeObject = [NSString stringWithFormat:@"SIGN IN"];
            firstLevelNode7.isExpanded = NO;
            firstLevelNode7.isHidden = NO;
            firstLevelNode7.isExpandable = NO;
        }
        else
        {
            firstLevelNode7 = [[TreeViewNode alloc]init];
            firstLevelNode7.nodeLevel = 0;
            firstLevelNode7.item_img = @"password";
            firstLevelNode7.nodeObject = [NSString stringWithFormat:@"SIGN OUT"];
            firstLevelNode7.isExpanded = NO;
            firstLevelNode7.isHidden = NO;
            firstLevelNode7.isExpandable = NO;
        }
    nodes = [NSMutableArray arrayWithObjects:firstLevelNode1, firstLevelNode2, firstLevelNode3,firstLevelNode7, nil];
}

- (NSArray *)fillChildrenForNode
{
    NSMutableArray *childrenArray = [[NSMutableArray alloc]init];
  //  NSLog(@"%@",appDle.itemsArray);

    NSArray *ar =[[appDle.itemsArray objectAtIndex:1] valueForKey:@"Objects"];
    
    for (NSDictionary *dic in ar)
    {
        TreeViewNode *secondLevelNode1 = [[TreeViewNode alloc]init];
        secondLevelNode1.nodeLevel = 1;
        secondLevelNode1.isExpanded = NO;
        secondLevelNode1.nodeObject = [dic valueForKey:@"name"];
        secondLevelNode1.item_id =[dic valueForKey:@"id"];
        secondLevelNode1.isHidden = YES;
        [childrenArray addObject:secondLevelNode1];
    }
   
    
    return childrenArray;
}
- (NSArray *)subNodes
{
    TreeViewNode *secondLevelNode1 = [[TreeViewNode alloc]init];
    secondLevelNode1.nodeLevel = 1;
    secondLevelNode1.isExpanded = NO;
    secondLevelNode1.nodeObject = @"LIGHT STRUCTURALS";
    secondLevelNode1.nodeChildren = [[self lightchieldNodes] mutableCopy];
    secondLevelNode1.isHidden = NO;
    
    TreeViewNode *secondLevelNode2 = [[TreeViewNode alloc]init];
    secondLevelNode2.nodeLevel = 1;
    secondLevelNode2.nodeObject = @"HEAVY STRUCTURALS";
    secondLevelNode2.nodeChildren = [[self HeavychieldNodes] mutableCopy];
    secondLevelNode1.isHidden = NO;
    NSArray *childrenArray = [NSArray arrayWithObjects:secondLevelNode1, secondLevelNode2, nil];
    
    return childrenArray;
}
- (NSArray *)HeavychieldNodes
{
    
    NSMutableArray *childrenArray = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dic in [[appDle.itemsArray objectAtIndex:2] valueForKey:@"sections"]) {
        if (![[dic valueForKey:@"name"] isEqualToString:@"LIGHT STRUCTURALS"]) {
            
            for (NSDictionary *dic1 in [dic valueForKey:@"secObjects"]) {
                TreeViewNode *secondLevelNode1 = [[TreeViewNode alloc]init];
                secondLevelNode1.nodeLevel = 2;
                secondLevelNode1.isExpanded = NO;
                secondLevelNode1.nodeObject = [dic1 valueForKey:@"name"];
                secondLevelNode1.item_id =[dic1 valueForKey:@"id"];
                [childrenArray addObject:secondLevelNode1];
                
            }
        }
      
        
    }

    
    return childrenArray;
}
- (NSArray *)lightchieldNodes
{
    
    NSMutableArray *childrenArray = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dic in [[appDle.itemsArray objectAtIndex:2] valueForKey:@"sections"]) {
        if ([[dic valueForKey:@"name"] isEqualToString:@"LIGHT STRUCTURALS"]) {
            
            for (NSDictionary *dic1 in [dic valueForKey:@"secObjects"]) {
                TreeViewNode *secondLevelNode1 = [[TreeViewNode alloc]init];
                secondLevelNode1.nodeLevel = 2;
                secondLevelNode1.isExpanded = NO;
                secondLevelNode1.nodeObject = [dic1 valueForKey:@"name"];
                secondLevelNode1.item_id =[dic1 valueForKey:@"id"];
                [childrenArray addObject:secondLevelNode1];
                
            }
        }
        
        
    }
    
    
    return childrenArray;
}
//This function is used to fill the array that is actually displayed on the table view
- (void)fillDisplayArray
{
    self.displayArray = [[NSMutableArray alloc]init];
    for (TreeViewNode *node in nodes) {
        [self.displayArray addObject:node];
        if (node.isExpanded) {
            if (node.isExpandable) {
                [self fillNodeWithChildrenArray:node.nodeChildren];
                
            }
            else
            {
              //  ALERT_DIALOG(@"Alert", @"It is not expandable");
                [self Products_list:node.nodeObject];
            }
        }
    }
}

//This function is used to add the children of the expanded node to the display array
- (void)fillNodeWithChildrenArray:(NSArray *)childrenArray
{
    for (TreeViewNode *node in childrenArray) {
        [self.displayArray addObject:node];
        if (node.isExpanded) {
            [self fillNodeWithChildrenArray:node.nodeChildren];
        }
    }
}

//These functions are used to expand and collapse all the nodes just connect them to two buttons and they will work
- (IBAction)expandAll:(id)sender
{
    [self fillNodesArray];
    [self fillDisplayArray];
    [self.leftMenuTable reloadData];
}

- (IBAction)collapseAll:(id)sender
{
    for (TreeViewNode *treeNode in nodes) {
        treeNode.isExpanded = NO;
    }
    [self fillDisplayArray];
    [self.leftMenuTable reloadData];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
//   NSUserDefaults *user_data=[NSUserDefaults standardUserDefaults];
//    NSString *user_id=[user_data valueForKey:@"user_id"];
    
//    if ([user_id isEqualToString:@""]||[user_id isEqual:[NSNull null]]||[user_id isEqualToString:@"<nil>"]||user_id == nil||[user_id isEqualToString:@"0"])
//    {
//        return self.displayArray.count-1;
//    }
//    else
    
    
    return self.displayArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //It's cruical here that this identifier is treeNodeCell and that the cell identifier in the story board is anything else but not treeNodeCell
    
    TreeViewNode *node = [self.displayArray objectAtIndex:indexPath.row];
    
    if (node.nodeLevel ==0) {
        static NSString *CellIdentifier = @"mainCell";
        UINib *nib = [UINib nibWithNibName:@"MainTableViewCell" bundle:nil];
        [tableView registerNib:nib forCellReuseIdentifier:CellIdentifier];
        
        MainTableViewCell *cell = (MainTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        
        cell.treeNode = node;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.catNAme_lbl.text = node.nodeObject;
        cell.imgVIew.image = [UIImage imageNamed:node.item_img];
        if (node.isExpandable) {
            
        }
        else
        {
            
           // cell.textLabel.text = node.nodeObject;
        }
        if (node.isExpanded) {

           // [cell.mainCell_Btn setSelected:YES];
        }
        else {
            [cell.mainCell_Btn setSelected:NO];
//            [cell setTheButtonBackgroundImage:[UIImage imageNamed:@"Close"]];
        }
        [cell setNeedsDisplay];
        
        
        // Configure the cell...
        
        return cell;
    }else if (node.nodeLevel ==1){
        static NSString *CellIdentifier = @"treeNodeCell";
        UINib *nib = [UINib nibWithNibName:@"CateGoryTableViewCell" bundle:nil];
        [tableView registerNib:nib forCellReuseIdentifier:CellIdentifier];
        
        CateGoryTableViewCell *cell = (CateGoryTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        cell.treeNode = node;
        
        cell.cellLabel.text = node.nodeObject;
        
         cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (node.isHidden == YES) {
            cell.cellButton.hidden =NO;
            cell.expandBtn.hidden =YES;
            cell.expandBtn.enabled =NO;
            
        }else{
            cell.cellButton.hidden =NO;
            cell.expandBtn.hidden =NO;
            cell.expandBtn.enabled =YES;


        }
        if (node.isExpanded) {
            [cell setTheButtonBackgroundImage:[UIImage imageNamed:@"down-arrow"]];
            
        }
        else {
             [cell.cellButton setSelected:NO];
            [cell setTheButtonBackgroundImage:[UIImage imageNamed:@"right-arrow"]];
        }
        [cell setNeedsDisplay];
        
        
        // Configure the cell...
        
        return cell;
    }else{
        static NSString *CellIdentifier = @"cheildCell";
        UINib *nib = [UINib nibWithNibName:@"ChieldTableViewCell" bundle:nil];
        [tableView registerNib:nib forCellReuseIdentifier:CellIdentifier];
        
        ChieldTableViewCell *cell = (ChieldTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
         cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.treeNode = node;
        
        cell.chield_lbl.text = node.nodeObject;
//        
//        if (node.isExpanded) {
//            [cell setTheButtonBackgroundImage:[UIImage imageNamed:@"Open"]];
//        }
//        else {
//            [cell setTheButtonBackgroundImage:[UIImage imageNamed:@"Close"]];
//        }
        [cell setNeedsDisplay];
        
        
        // Configure the cell...
        
        return cell;
    }
  
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%ld",(long)indexPath.row);
    
 
     TreeViewNode *node = [self.displayArray objectAtIndex:indexPath.row];
 
        // use node.itemId for ID
    [self Products_list:node.item_id];
        
}
- (IBAction)contact_us:(id)sender
{
    [self.frostedViewController hideMenuViewController];

    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"contact_click" object:self userInfo:nil];
    
   
}

-(void)Products_list:(NSString *)productId
{
    NSDictionary* userInfo = @{@"id": productId};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"leftMenuClicked" object:self userInfo:userInfo];
    
    [self.view endEditing:YES];
    
    [self.frostedViewController.view endEditing:YES];
    
    // Present the view controller
    //
    [self.frostedViewController hideMenuViewController];

 }

- (IBAction)yourOrderBtnClick:(id)sender {
    [self Products_list:@"yourOrder"];
    //yourorder
//    NSDictionary* userInfo = @{@"id": @"yourOrder"};
//    
//    
//    
//    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
//    [nc postNotificationName:@"yourorder" object:self userInfo:userInfo];
//    [self.view endEditing:YES];
//    
//    [self.frostedViewController.view endEditing:YES];
//    
//    // Present the view controller
//    //
//    [self.frostedViewController hideMenuViewController];
}

- (IBAction)accountMgtBtnClick:(id)sender {
    [self Products_list:@"accountMgt"];
}

- (IBAction)trackOrderBtnAction:(id)sender {
    [self Products_list:@"trackOrder"];
}
@end
