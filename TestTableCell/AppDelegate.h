//
//  AppDelegate.h
//  TestTableCell
//
//  Created by administrator on 19/11/16.
//  Copyright © 2016 com.SteelonCall. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <UserNotifications/UserNotifications.h>
#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate>

-(void)back_click;
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain)NSArray *itemsArray;
@property(assign,nonatomic)BOOL shouldRotate;
@property(strong,nonatomic)NSString * deviceName;

@end

