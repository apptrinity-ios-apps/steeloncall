//
//  NewLoginVC.m
//  SteelonCall
//
//  Created by nagaraj  kumar on 24/10/18.
//  Copyright © 2018 com.indobytes. All rights reserved.
//
#import "AFNetworking.h"
#import "URLS.h"
#import "NewLoginVC.h"

@interface NewLoginVC ()
{
    NSString *errMsg;
}
@end

@implementation NewLoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    user_data=[NSUserDefaults standardUserDefaults];
    errMsg = @"Please Enter Email Id";
    
     _emailTFT.layer.sublayerTransform = CATransform3DMakeTranslation(20, 0, 0);
     _passwordTFT.layer.sublayerTransform = CATransform3DMakeTranslation(20, 0, 0);
     _signUp_email.layer.sublayerTransform = CATransform3DMakeTranslation(20, 0, 0);
     _signUp_password.layer.sublayerTransform = CATransform3DMakeTranslation(20, 0, 0);
     _signUp_ConfirmPassword.layer.sublayerTransform = CATransform3DMakeTranslation(20, 0, 0);
     _forgot_Email_TFT.layer.sublayerTransform = CATransform3DMakeTranslation(20, 0, 0);
  
    
    self.login_Btn.clipsToBounds = YES;
    self.login_Btn.layer.cornerRadius = 23;
    
    
    self.register_Btn.clipsToBounds = YES;
    self.register_Btn.layer.cornerRadius = 23;
    
    
    self.loginPopUpView.clipsToBounds = YES;
    self.loginPopUpView.layer.cornerRadius = 8;
    
    self.forgotPassword_view.clipsToBounds = YES;
    self.forgotPassword_view.layer.cornerRadius = 8;
    
    self.signUp_popView.clipsToBounds = YES;
    self.signUp_popView.layer.cornerRadius = 8;
    
    self.popUp_LoginBtn.clipsToBounds = YES;
    self.popUp_LoginBtn.layer.cornerRadius = 21;
    
    self.register_Btn.clipsToBounds = YES;
    self.register_Btn.layer.cornerRadius = 23;
    
    self.signUp_registerBtn.clipsToBounds = YES;
    self.signUp_registerBtn.layer.cornerRadius = 21;
    
    
    self.proceed_Btn.clipsToBounds = YES;
    self.proceed_Btn.layer.cornerRadius = 21;
    
    self.email_image.hidden = YES;
    self.password_image.hidden = YES;
    
    self.signUp_password_Image.hidden =YES;
    self.signUp_confirmPass_Image.hidden = YES;
    
    
    self.loginPopUpView.hidden = YES;
    self.signUp_popView.hidden = YES;
    self.BG_Btn.hidden = YES;
    self.forgotPassword_view.hidden = YES;
    
    self.emailTFT.delegate = self;
    self.passwordTFT.delegate = self;
    self.signUp_email.delegate = self;
    self.signUp_password.delegate = self;
    self.signUp_ConfirmPassword.delegate = self;
    self.forgot_Email_TFT.delegate = self;


    _emailTFT.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    _passwordTFT.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    _signUp_email.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    _signUp_password.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    _signUp_ConfirmPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Confirm Password" attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    _forgot_Email_TFT.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter Your Email" attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];

    
    
     [_emailTFT setRightViewMode:UITextFieldViewModeAlways];
     [_forgot_Email_TFT setRightViewMode:UITextFieldViewModeAlways];
     [_signUp_email setRightViewMode:UITextFieldViewModeAlways];
     [_signUp_ConfirmPassword setRightViewMode:UITextFieldViewModeAlways];
     [_signUp_password setRightViewMode:UITextFieldViewModeAlways];

    
    // Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}


-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    
   
    
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason
{
    
    
    if (textField == _emailTFT){
        
        if (_emailTFT.text.length>=8){
            
            _email_image.hidden = NO;
        }else{
            _email_image.hidden = YES;
        }
       
    }
    else if (textField == _passwordTFT){
        
        if (_passwordTFT.text.length>=5){
            
            _password_image.hidden = NO;
        }else{
            _password_image.hidden = YES;
        }
    }
    else if (textField == _signUp_password){
        
        if (_signUp_password.text.length>=6){
            
           _signUp_password_Image .hidden = NO;
        }else{
            _signUp_password_Image.hidden = YES;
        }
        
    }
    else if (textField == _signUp_ConfirmPassword){
        
        if (_signUp_ConfirmPassword.text.length>=5){
            
            _signUp_confirmPass_Image.hidden = NO;
        }else{
            _signUp_confirmPass_Image.hidden = YES;
        }
    }
   
    
   
}

- (BOOL) textField: (UITextField *)theTextField shouldChangeCharactersInRange: (NSRange)range replacementString: (NSString *)string {
    
    
    if (theTextField == _emailTFT){
        if (_emailTFT.text.length>=8){
            
            if ([self NSStringIsValidEmail:_emailTFT.text]== NO){
                
                  _email_image.hidden = YES;
            }else{
                _email_image.hidden = NO;
            }
         
        }else{
        _email_image.hidden = YES;
        }
    }
    
    
    else if (theTextField == _passwordTFT){
        
        if (_passwordTFT.text.length>=5){
            
            _password_image.hidden = NO;
        }else{
            _password_image.hidden = YES;
        }
        
        
    }
    
    
    else if (theTextField == _signUp_password){
        
        if (_signUp_password.text.length>=5){
            
            _signUp_password_Image .hidden = NO;
        }else{
            _signUp_password_Image.hidden = YES;
        }
        
    }
    else if (theTextField == _signUp_ConfirmPassword){
        
        if (_signUp_ConfirmPassword.text.length>=5){
            
            _signUp_confirmPass_Image.hidden = NO;
        }else{
            _signUp_confirmPass_Image.hidden = YES;
        }
    }
    else if (theTextField == _forgot_Email_TFT){
        
        if (_forgot_Email_TFT.text.length>=8){
            
            if ([self NSStringIsValidEmail:_forgot_Email_TFT.text]== NO){
                
               
            }else{
                
               
            }
            
        }
    
    }
    
    
    
    return YES;
    
}

- (void)keyboardWillShow:(NSNotification *)n
{
    
    [self.view setFrame:CGRectMake(0,-110,self.view.frame.size.width,self.view.frame.size.height)];
    
}
- (void)keyboardWillHide:(NSNotification *)n
{
    
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    
    [self.view endEditing:YES];
    
    
    return YES;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    errMsg = @"Please Enter Email Id";
    self.navigationController.navigationBar.hidden = YES;
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}


-(void)Login_Dict
{
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:[self.emailTFT.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"email"];
    [dict setObject:[self.passwordTFT.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"password"];
    
    NSLog(@"%@",dict);
    [self Login_Service:dict];
}

-(void)Login_Service:(NSDictionary *)Dict
{
    NSDictionary *params = @{@"email": self.emailTFT.text,@"password": self.passwordTFT.text};
    
    
    _activityView.frame = [UIScreen mainScreen].bounds;
    [self.view addSubview:_activityView];
    
    [[STParsing sharedWebServiceHelper]requesting_POST_ServiceWithString1:@"userLogin" parameters:params requestNumber:WS_Login showProgress:YES withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             
             [_activityView removeFromSuperview];
             NSDictionary *res_dict=data;
             NSString *response=[NSString stringWithFormat:@"%@",[res_dict valueForKey:@"response"]];
             if ([response isEqualToString:@"1"])
             {
                 NSString *User_Id=[res_dict valueForKey:@"userId"];
                 if ([User_Id isEqualToString:@""]||[User_Id isEqualToString:@"0"])
                 {
                     [self Alert:@"Something went wrong!"];
                 }
                 else
                 {
                     user_data=[NSUserDefaults standardUserDefaults];
                     [user_data setValue:User_Id forKey:@"user_id"];
                     [user_data setValue:self.emailTFT.text forKey:@"emailID"];
                     
                     //cart service
                     [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"ckeckLOGIN"];
                     [self kartCntService:User_Id];
                 }
             }
             else
             {
                  [_activityView removeFromSuperview];
                 [self Alert:@"Login failed!"];
             }
         }
         else
         { [_activityView removeFromSuperview];
             [self Alert:@"Something went wrong"];
         }
     }];
}

-(void)kartCntService:(NSString *)userId
{
    NSDictionary *params = @{@"customer_id":userId};
    
    [[STParsing sharedWebServiceHelper]requesting_POST_ServiceWithString1:@"getCartCountAfterLogin" parameters:params requestNumber:WS_Login showProgress:YES withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             NSDictionary *res_dict=data;
             NSString *cartCount=[NSString stringWithFormat:@"%@",[[[res_dict valueForKey:@"cart"] objectAtIndex:0] valueForKey:@"cart_count"]];
             
             if (cartCount == nil||[cartCount isEqualToString:@"<null>"]) {
                 cartCount =@"0";
             }
             
             [user_data setValue:cartCount forKey:@"cart_count"];
             
             
             [self success:userId];
             
         }
         else
         {
             [self success:userId];
             [self Alert:@"Something went wrong"];
         }
     }];
}

-(void)success:(NSString *)User_Id
{
    if ([_From isEqualToString:@"orders"])
    {
        YourOrdersViewController *define = [[YourOrdersViewController alloc]initWithNibName:@"YourOrdersViewController" bundle:nil];
        define.From=@"Login";
        [self.navigationController pushViewController:define animated:YES];
    }
    else if ([_From isEqualToString:@"signIn"])
    {
        HomeViewController *con = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
        [self.navigationController pushViewController:con animated:YES];
    }
    else if ([_From isEqualToString:@"tracking"])
    {
        Tracking *define = [[Tracking alloc]initWithNibName:@"Tracking" bundle:nil];
        define.From=@"Login";
        [self.navigationController pushViewController:define animated:YES];
    }
    else if ([_From isEqualToString:@"management"])
    {
        AccountManageViewController *ac = [[AccountManageViewController alloc]initWithNibName:@"AccountManageViewController" bundle:nil];
        ac.From=@"Login";
        [self.navigationController pushViewController:ac animated:YES];
    }
    //    else if ([_From isEqualToString:@"listingPage"])
    //    {
    //        Products_List *ac = [[Products_List alloc]initWithNibName:@"Products_List" bundle:nil];
    //        ac.product_id=_product_id;
    //        ac.From = @"Login";
    //
    //        [self.navigationController pushViewController:ac animated:YES];
    //    }
    
    else
    {
        if ([_From isEqualToString:@"define"])
        {
            // NSMutableDictionary *abc;
            //_cart_dict=[[NSMutableDictionary alloc]init];
            NSLog(@"dict %@",_cart_dict);
            [_cart_dict setValue:User_Id forKey:@"customer_id"];
            [self service:_cart_dict];
        }
        else
        {
            My_Cart *define = [[My_Cart alloc]init];
            define.From_check=@"login";
            [self.navigationController pushViewController:define animated:YES];
        }
        //                         activityView.frame = [UIScreen mainScreen].bounds;
        //                         [self.view addSubview:activityView];
    }
}


-(BOOL)valide_Data
{
    NSString *email_str = [_emailTFT.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *pwd_str=_passwordTFT.text;
    
    if  ( [email_str length]==0)
    {
        [self Alert :@"Email should not be empty."];
        return NO;
    }
    else if ([self NSStringIsValidEmail:email_str]== NO)
    {
        [self Alert :@"Email should be valid."];
        return NO;
    }
    else if ([pwd_str length] == 0)
    {
        [self Alert :@"Password should not be empty."];
        return NO;
    }
    else if ([pwd_str length] < 6)
    {
        [self Alert :@"Password must be atleast 6 characters long."];
        return NO;
    }
    if ([email_str length] >6){
        
        self.email_image.hidden = NO;
        return YES;
    }
    if ([pwd_str length]>=6){
        
        self.password_image.hidden = NO;
        return YES;
        
    }
    
    return YES;
}

-(BOOL)NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
    
}


-(void)service:(NSDictionary *)parameters
{
    _HUD.hidden =NO;
    if (parameters.count>0)
    {
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
        responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
        manager.responseSerializer =responseSerializer;
        NSString *urlString;
        urlString= [NSString stringWithFormat:@"%@addProductsToCart",MAIN_Url];
        NSError *error;
        
        NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:&error];
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        // NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        
        NSURL *url = [NSURL URLWithString:urlString];
        
        NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:url];
        
        [request addValue:@"text/html" forHTTPHeaderField:@"Content-Type"];
        [request addValue:@"text/html" forHTTPHeaderField:@"Accept"];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:jsonData2];
        
        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                              {
                                                  if (!error)
                                                  {
                                                      // [activityView .removeFromSuperview];
                                                      //_HUD.hidden =YES;
                                                      NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                                      
                                                      NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                                                           options:kNilOptions
                                                                                                             error:&error];
                                                      NSLog(@"%@",json);
                                                      NSString *response=[NSString stringWithFormat:@"%@",[json valueForKey:@"response"]];
                                                      
                                                      if ([response isEqualToString:@"1"])
                                                      {
                                                          //Background Thread
                                                          // _HUD.hidden =YES;
                                                          dispatch_async(dispatch_get_main_queue(), ^(void){
                                                              //Run UI Updates
                                                              //  [_HUD removeFromSuperview];
                                                              
                                                              //_HUD.hidden =YES;
                                                              //[activityView removeFromSuperview];
                                                              
                                                              My_Cart *define = [[My_Cart alloc]init];
                                                              define.From_check=@"login";
                                                              [self.navigationController pushViewController:define animated:YES];
                                                          });
                                                          
                                                      }
                                                      else
                                                      {
                                                          
                                                          
                                                          
                                                          
                                                          dispatch_async(dispatch_get_main_queue(), ^(void){
                                                              
                                                              
                                                              //[activityView removeFromSuperview];
                                                          });
                                                          
                                                          //   [self Alert:@"Something went wrong"];
                                                          
                                                          ALERT_DIALOG(@"Alert", @"Something went wrong");
                                                          NSLog(@" Something went wrong");
                                                      }
                                                  }
                                                  else
                                                  {
                                                      // _HUD.hidden =YES;
                                                      dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
                                                                     {
                                                                         //Background Thread
                                                                         // _HUD.hidden =YES;
                                                                         dispatch_async(dispatch_get_main_queue(), ^(void){
                                                                             
                                                                             
                                                                             
                                                                             // [activityView removeFromSuperview];
                                                                         });
                                                                     });
                                                      NSString *str =[NSString stringWithFormat:@"%@",error];
                                                      
                                                      ALERT_DIALOG(@"Alert",str);
                                                      
                                                      
                                                      //[self Alert:[NSString stringWithFormat:@"%@",error]];
                                                      //            NSLog(@"Error: %@", error);
                                                  }
                                                  
                                              }];
        
        [postDataTask resume];
    }
}
-(void)Fwd_Dict:(NSString *)msg
{
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:msg forKey:@"email"];
    
    NSLog(@"%@",dict);
    [self Fwd_Service:dict];
}

-(void)Fwd_Service:(NSDictionary *)Dict
{
    NSString *url_form=[NSString stringWithFormat:@"sendForgotPasswordLink?/"];
    NSString* urlEncoded = [url_form stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    _activityView.frame = [UIScreen mainScreen].bounds;
    [self.view addSubview:_activityView];
    
    [[STParsing sharedWebServiceHelper]requesting_POST_ServiceWithString1:urlEncoded parameters:Dict requestNumber:fwd showProgress:YES withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             
             [self.activityView removeFromSuperview];
             NSDictionary *res_dict=data;
             NSString *response=[NSString stringWithFormat:@"%@",[res_dict valueForKey:@"response"]];
             if ([response isEqualToString:@"Reset Password Link send to your email"])
             {
                 [self Alert:response];
                 
                 
                 self.forgotPassword_view.hidden = YES;
                 self.loginPopUpView.hidden= NO;
                 
             }
             else
             {
                 [self Alert:@"Entered Email does not exist"];
             }
         }
         else
         {  [self.activityView removeFromSuperview];
             [self Alert:@"Something went wrong"];
         }
     }];
}

-(void)forgot_pwd_service:(NSString *)msg
{
    if  ( msg.length==0)
    {
        errMsg = @"Please enter Email Id";
        
        [self forgotPasswordAction:(id)0];
    }
    
    else if  ([self NSStringIsValidEmail:msg]== NO)
    {
        errMsg = @"entered email is not valid";
        
       [self forgotPasswordAction:(id)0];
        
    }
    else
        
        [self Fwd_Dict:msg];
}


-(void)Alert:(NSString *)Msg
{
    NSDictionary *options = @{kCRToastNotificationTypeKey:@(CRToastTypeNavigationBar),
                              
                              kCRToastTextKey : Msg,
                              
                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                              
                              kCRToastBackgroundColorKey : [UIColor colorWithRed:13.0/255.0 green:147.0/255.0 blue:68.0/255.0 alpha:1],
                              kCRToastTimeIntervalKey: @(2),
                              //                              kCRToastFontKey:[UIFont fontWithName:@"PT Sans Narrow" size:23],
                              kCRToastInteractionRespondersKey:@[[CRToastInteractionResponder interactionResponderWithInteractionType:CRToastInteractionTypeSwipeUp
                                                                  
                                                                                                                 automaticallyDismiss:YES
                                                                                                                                block:^(CRToastInteractionType interactionType){
                                                                                                                                    
                                                                                                                                    NSLog(@"Dismissed with %@ interaction", NSStringFromCRToastInteractionType(interactionType));
                                                                                                                                }]],
                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeGravity),
                              
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeGravity),
                              
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                              
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop)
                              
                              };
    
    [CRToastManager showNotificationWithOptions:options
     
                                completionBlock:^{
                                    
                                    NSLog(@"Completed");
                                    
                                }];
}


// Registration Logic

-(void)Register_Dict
{
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:[_signUp_email.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"email"];
    [dict setObject:[_signUp_password.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"password"];
    
    NSLog(@"%@",dict);
    [self Register_Service:dict];
}

-(void)Register_Service:(NSDictionary *)Dict
{
    
    _activityView.frame = [UIScreen mainScreen].bounds;
    [self.view addSubview:_activityView];
    
    [[STParsing sharedWebServiceHelper]requesting_POST_ServiceWithString1:@"register" parameters:Dict requestNumber:Registration showProgress:YES withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             
             [_activityView removeFromSuperview];
             NSDictionary *res_dict=data;
             NSString *response=[NSString stringWithFormat:@"%@",[res_dict valueForKey:@"response"]];
             
             if ([response isEqualToString:@"1"])
             {
                 NSString *User_Id=[res_dict valueForKey:@"userId"];
                 if ([User_Id isEqual:[NSNull null]]||[User_Id isEqualToString:@"<null>"]||[User_Id isEqualToString:@""]||[User_Id isEqualToString:@"0"])
                 {
                     [self Alert:@"Something went wrong!"];
                 }
                 else
                 {
                     UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Success" message:@"User regiestered successfully" preferredStyle:UIAlertControllerStyleAlert];
                     
                     [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                         
                         [user_data setValue:User_Id forKey:@"user_id"];
                         [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"ckeckLOGIN"];
                         UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                         
//                         LoginViewController *myNewVC = (LoginViewController *)[storyboard instantiateViewControllerWithIdentifier:@"LoginViewControllerID"];
//                         //  ALERT_DIALOG(@"Success", @"User regiestered successfully");
//                         myNewVC.cart_dict=_cart_dict;
//                         myNewVC.From=_From;
                         
                         self.signUp_popView.hidden = YES;
                         self.loginPopUpView.hidden = NO;
                         
                       //[self.navigationController pushViewController:myNewVC animated:YES];
                         
                     }]];
                     
                     
                     dispatch_async(dispatch_get_main_queue(),
                                    ^ {
                                        [self presentViewController:alertController animated:YES completion:nil];
                                    });
                     
                     
                     
                     
                     
                     user_data=[NSUserDefaults standardUserDefaults];
                     
                 }
             }
             else
             {
                  [_activityView removeFromSuperview];
                 
                 [self Alert:@"Email you have entered is already registered"];
             }
         }
         else
         {
              [_activityView removeFromSuperview];
             [self Alert:@"Something went wrong"];
         }
     }];
    
}


-(BOOL)valide_SignUp_Data
{
    NSString *email_str = [_signUp_email.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *pwd_str=_signUp_password.text;
    NSString *Cpwd_str=_signUp_ConfirmPassword.text;
    
    if  ( [email_str length]==0)
    {
        [self Alert :@"Email should not be empty."];
        return NO;
    }
    else if ([self NSStringIsValidEmail:email_str]== NO)
    {
        [self Alert :@"Email should be valid."];
        return NO;
    }
    else if ([pwd_str length] == 0)
    {
        [self Alert :@"Password should not be empty."];
        return NO;
    }
    else if ([pwd_str length] < 6)
    {
        [self Alert :@"Password must be atleast 6 characters long."];
        return NO;
    }
    else if (! [Cpwd_str isEqualToString:pwd_str] )
    {
        [self Alert :@"Password mismatch."];
        return NO;
    }
    return YES;
}






- (IBAction)loginAction:(id)sender {
    
   
    self.loginPopUpView.hidden = NO;
    self.signUp_popView.hidden = YES;
    self.BG_Btn.hidden = NO;

    
}

- (IBAction)registerAction:(id)sender {
    
   
    self.loginPopUpView.hidden = YES;
    self.signUp_popView.hidden = NO;
    self.BG_Btn.hidden = NO;

    
}
- (IBAction)popup_LoginAction:(id)sender {
    
    [self.view endEditing:YES];
    if ([self valide_Data] == YES)
    { // checking if the either of the string is empty and other validations
        [self Login_Dict];
    }
    
    
}
- (IBAction)forgotPasswordAction:(id)sender {
    
    
    self.loginPopUpView.hidden = YES;
    self.forgotPassword_view.hidden = NO;
    
    
    
//    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Message" message:errMsg preferredStyle:UIAlertControllerStyleAlert];
//
//    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
//        textField.placeholder = @"Please Enter Email Id";
//        [alertController addAction:[UIAlertAction actionWithTitle:@"Submit" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
//                                    {
//                                        [self forgot_pwd_service:textField.text];
//                                    }]];
//    }];
//
//    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
//                                {
//                                    errMsg = @"Please Enter Email Id";
//                                    [self dismissViewControllerAnimated:YES completion:nil];
//                                }]];
//
//    dispatch_async(dispatch_get_main_queue(), ^
//                   {
//                       [self presentViewController:alertController animated:YES completion:nil];
//                   });
    
    
    
}
- (IBAction)popUpLogin_CloseAction:(id)sender {
    
    
    self.loginPopUpView.hidden = YES;
    self.signUp_popView.hidden = YES;
    self.BG_Btn.hidden = YES;

    
}
- (IBAction)popUpSignUp_RegisterAction:(id)sender {
    
    [self.view endEditing:YES];
    
    if ([self valide_SignUp_Data] == YES)
    { // checking if the either of the string is empty and other validations
        [self Register_Dict];
    }
    
    
    
    NSArray *currentControllers = self.navigationController.viewControllers;
    BOOL ischeck = false;
    
    for (UIViewController *view in currentControllers ) {
        if ([view isKindOfClass:[NewLoginVC class]]) {
            ischeck =YES;
            [self.navigationController popToViewController:view animated:YES];
            break;
            
        }
    }
    
    if (!ischeck) {
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        NewLoginVC *myNewVC = (NewLoginVC *)[storyboard instantiateViewControllerWithIdentifier:@"NewLoginVC"];
        myNewVC.From=_From;
        myNewVC.cart_dict=_cart_dict;
        NSMutableArray *newControllers = [NSMutableArray
                                          arrayWithArray:@[myNewVC]];
        
       
        
        
        /* Assign this array to the Navigation Controller */
        self.navigationController.viewControllers = newControllers;
        [self.navigationController pushViewController:myNewVC animated:YES];
        
        
    }
    
    
    
}
- (IBAction)proceed_Action:(id)sender {
    
    NSString *email_str = [_forgot_Email_TFT.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if  ( [email_str length]==0)
    {
        [self Alert :@"Email should not be empty."];
    }
    else if ([self NSStringIsValidEmail:email_str]== NO)
    {
        [self Alert :@"Email should be valid."];
    }
    else {
    
    [self forgot_pwd_service:self.forgot_Email_TFT.text];
        
    }
    
    
    
    
}


- (IBAction)forgotView_CloseAction:(id)sender {
    
    self.loginPopUpView.hidden = NO;
    self.forgotPassword_view.hidden = YES;
    
}

- (IBAction)popUpRegister_CloseAction:(id)sender {
    
    self.loginPopUpView.hidden = YES;
    self.signUp_popView.hidden = YES;
    self.BG_Btn.hidden = YES;

}
- (IBAction)close_Action:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


@end
