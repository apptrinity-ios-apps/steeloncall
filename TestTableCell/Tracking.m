//
//  Tracking.m
//  SteelonCall
//
//  Created by INDOBYTES on 03/04/17.
//  Copyright © 2017 com.indobytes. All rights reserved.
//

#import "Tracking.h"
#import "Tracking_VC.h"

@interface Tracking ()

@end

@implementation Tracking
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
//self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:10.0/255.0 green:21.0/255.0 blue:80.0/255.0 alpha:1];    self.navigationController.navigationBar.translucent = NO;
//    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
//    UIImageView *navigationImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 20, 199, 40)];
//    navigationImage.image=[UIImage imageNamed:@"logo.png"];
//    self.navigationItem.titleView=navigationImage;
//    
//    UIButton *cameraButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 2, 40.0f, 40.0f)];
//    UIImage *cameraImage = [UIImage imageNamed:@"Back"];
//    [cameraButton setBackgroundImage:cameraImage forState:UIControlStateNormal];
//    [cameraButton addTarget:self action:@selector(Back_click) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem* cameraButtonItem = [[UIBarButtonItem alloc] initWithCustomView:cameraButton];
//    self.navigationItem.leftBarButtonItem = cameraButtonItem;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    // Do any additional setup after loading the view from its nib.
//    _border_view.layer.borderColor = [UIColor grayColor].CGColor;
//    _border_view.layer.borderWidth =1;
    _order_tf.delegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
    
    
    
}


- (void)keyboardWillShow:(NSNotification *)n
{
    
    [self.view setFrame:CGRectMake(0,-110,self.view.frame.size.width,self.view.frame.size.height)];
    
}
- (void)keyboardWillHide:(NSNotification *)n
{
    
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backAction:(id)sender {
    if ([_From isEqualToString:@"Login"])
    {
        NSArray *currentControllers = self.navigationController.viewControllers;
        
        BOOL ischeck = false;
        
        for (UIViewController *view in currentControllers ) {
            if ([view isKindOfClass:[HomeViewController class]]) {
                ischeck =YES;
                [self.navigationController popToViewController:view animated:YES];
                break;
                
            }
        }
        
        if (!ischeck) {
            HomeViewController *con = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
            /* Create a mutable array out of this array */
            NSMutableArray *newControllers = [NSMutableArray
                                              arrayWithArray:@[con]];
            
            /* Remove the last object from the array */
            
            
            /* Assign this array to the Navigation Controller */
            self.navigationController.viewControllers = newControllers;
            [self.navigationController pushViewController:con animated:YES];
            
            
        }    }
    else
        [self.navigationController popViewControllerAnimated:YES];
}

//-(void)Back_click
//{
//    if ([_From isEqualToString:@"Login"])
//    {
//        NSArray *currentControllers = self.navigationController.viewControllers;
//        
//        BOOL ischeck = false;
//        
//        for (UIViewController *view in currentControllers ) {
//            if ([view isKindOfClass:[HomeViewController class]]) {
//                ischeck =YES;
//                [self.navigationController popToViewController:view animated:YES];
//                break;
//                
//            }
//        }
//        
//        if (!ischeck) {
//            HomeViewController *con = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
//            /* Create a mutable array out of this array */
//            NSMutableArray *newControllers = [NSMutableArray
//                                              arrayWithArray:@[con]];
//            
//            /* Remove the last object from the array */
//            
//            
//            /* Assign this array to the Navigation Controller */
//            self.navigationController.viewControllers = newControllers;
//            [self.navigationController pushViewController:con animated:YES];
//            
//            
//        }    }
//    else
//        [self.navigationController popViewControllerAnimated:YES];
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)Alert:(NSString *)Msg
{
    NSDictionary *options = @{kCRToastNotificationTypeKey:@(CRToastTypeNavigationBar),
                              
                              kCRToastTextKey : Msg,
                              
                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                              
                              kCRToastBackgroundColorKey : [UIColor colorWithRed:44.0/255.0 green:52.0/255.0 blue:75.0/255.0 alpha:1],
                              kCRToastTimeIntervalKey: @(2),
                              //                              kCRToastFontKey:[UIFont fontWithName:@"PT Sans Narrow" size:23],
                              kCRToastInteractionRespondersKey:@[[CRToastInteractionResponder interactionResponderWithInteractionType:CRToastInteractionTypeSwipeUp
                                                                  
                                                                                                                 automaticallyDismiss:YES
                                                                  
                                                                                                                                block:^(CRToastInteractionType interactionType){
                                                                                                                                    
                                                                                                                                    NSLog(@"Dismissed with %@ interaction", NSStringFromCRToastInteractionType(interactionType));
                                                                                                                                }]],
                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeGravity),
                              
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeGravity),
                              
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                              
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop)
                              
                              };
    
    [CRToastManager showNotificationWithOptions:options
     
                                completionBlock:^{
                                    
                                    NSLog(@"Completed");
                                    
                                }];
}
- (IBAction)submit_click:(id)sender
{
    if([_order_tf.text isEqualToString:@""])
    {
        [self Alert:@"Order ID Should not be empty!"];
    }
    else
    {
        NSString *url_form;
        url_form=[NSString stringWithFormat:@"getTrackingDetails/?tracking_id=%@",_order_tf.text];
        
        NSString* urlEncoded = [url_form stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString:urlEncoded requestNumber:PRODUCT_LIST showProgress:YES withHandler:^(BOOL success, id data)
         {
             if (success)
             {
                 NSDictionary *Sellers_dict=data;
                 NSString * staus =[NSString stringWithFormat:@"%@",[data valueForKey:@"status"]];
                 staus = [staus substringFromIndex:6];
                 staus = [staus substringToIndex:1];
                 if ([staus isEqualToString:@"0"]) {
                     [self Alert:@"No Products Found!"];
                 }
                 else
                 {
                     if (Sellers_dict.count==0)
                     {
                         [self Alert:@"No Products Found!"];
                     }
                     else
                     {
                         
                         UIStoryboard *view3 = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                         Tracking_VC *define =[view3 instantiateViewControllerWithIdentifier:@"Tracking_VC"];
                         define.order_id=_order_tf.text;
                        [self.navigationController pushViewController:define animated:YES];
                         
                         
//                         Tracking_VC * define = [[Tracking_VC alloc]init];
////                         Tracking_VC *define = [[Tracking_VC alloc]initWithNibName:@"Tracking_VC" bundle:nil];
//                         define.order_id=_order_tf.text;
//                         [self.navigationController pushViewController:define animated:YES];
                         
                         
                     }
                 }
             }
         }];
    }
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)dismissKeyboard
{
    [_order_tf resignFirstResponder];
}

- (IBAction)goToHomeScreenAction:(id)sender {
    HomeViewController *con = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
    [self.navigationController pushViewController:con animated:YES];
    
    
}
@end
