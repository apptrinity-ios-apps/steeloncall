//
//  TrackingHeaderCell.h
//  SteelonCall
//
//  Created by nagaraj  kumar on 14/11/18.
//  Copyright © 2018 com.indobytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrackingHeaderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *header_Lbl;

@end
