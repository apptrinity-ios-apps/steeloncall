//
//  CompareViewController.h
//  steelonCallThree
//
//  Created by nagaraj  kumar on 29/11/16.
//  Copyright © 2016 nagaraj  kumar. All rights reserved.

#import <UIKit/UIKit.h>
#import "NIDropDown.h"
#import "UIImageView+WebCache.h"
#import "Compare_Landscape_cell.h"
@interface CompareViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
        IBOutlet UIButton *btnSelect;
        NIDropDown *dropDown;
    NSMutableArray *tonnes_names_ary;
    NSUserDefaults *user_data;
    NSString *user_id;
    NSString *sponser_count_tag;
    NSDictionary *brandsInfoList_dic;
}

@property (strong, nonatomic) IBOutlet UITableView *compareListview;

@property (retain, nonatomic) IBOutlet UIButton *btnSelect;
@property (retain, nonatomic) IBOutlet NSString *sel_ton;

- (IBAction)selectClicked:(id)sender;
- (IBAction)backButtonClicked:(id)sender;
- (IBAction)Cart_click:(id)sender;

//@property (strong, nonatomic) IBOutlet UIButton *selectBtnOutlet;
@property (weak, nonatomic) IBOutlet UILabel *Cart_count;
@property (strong, nonatomic) NSString *Location;
@property (strong, nonatomic) NSString *selected_ids;
@property (strong, nonatomic) NSString *selected_brand;
@property (strong, nonatomic) NSString *selected_grade;
@property (strong, nonatomic) NSString *selectedPincode;
@property (strong, nonatomic) NSString *sel_childs;
@property (strong, nonatomic) NSMutableArray *T_P_Ary,*product_data;
@property (weak, nonatomic) IBOutlet UIButton *cart_view;
@property (weak, nonatomic) IBOutlet UIView *Cart_BackgrndView;
@property (strong, nonatomic) IBOutlet UILabel *lbl_1;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *landscape_leading;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *portrait_leading;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *product_width;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *width_2;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *width_4;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *width_3;
@property (strong, nonatomic) IBOutlet UILabel *jsw_lbl;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *width_5;
-(void)rel;

@end
