//
//  Seller_Invoice_Cell.h
//  SteelonCall
//
//  Created by INDOBYTES on 03/04/17.
//  Copyright © 2017 com.indobytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Seller_Invoice_Cell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *sellerName;
@property (strong, nonatomic) IBOutlet UILabel *productName;
@property (strong, nonatomic) IBOutlet UILabel *brandLbl;
@property (strong, nonatomic) IBOutlet UILabel *gradeLbl;
@property (strong, nonatomic) IBOutlet UILabel *priceLbl;
@property (strong, nonatomic) IBOutlet UILabel *itemsLbl;
@property (strong, nonatomic) IBOutlet UILabel *discountLbl;
@property (strong, nonatomic) IBOutlet UILabel *rowTotalLbl;


@end
