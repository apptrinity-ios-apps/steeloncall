//
//  ChildController.m
//  Smatrimony
//
//  Created by S s Vali on 7/17/17.
//  Copyright © 2017 Indobytes. All rights reserved.
//

#import "ChildController.h"

@interface ChildController ()
{
    NSArray *imgsArr,*titleArr, *mainTitleArr;
}
@end

@implementation ChildController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    imgsArr = [[NSArray alloc]initWithObjects:[UIImage imageNamed:@"Walkthrough-01.png"],[UIImage imageNamed:@"Walkthrough-02.png"],[UIImage imageNamed:@"Walkthrough-03.png"],[UIImage imageNamed:@"Walkthrough-04.png"],[UIImage imageNamed:@"Walkthrough-05.png"],[UIImage imageNamed:@"Walkthrough-06.png"], nil];
    
    titleArr = [[NSArray alloc]initWithObjects:@"Choose the category of steel you need",@"Enter no.of Tons or no.of Pieces you need",@"Enter your Delivery pincode and city",@"Pick the best deal of steel",@"Choose a Comfortable Payment Method",@"You can Track your order in real time", nil];
    
    
    mainTitleArr = [[NSArray alloc]initWithObjects:@"Select Category",@"Select Product",@"Check Availability",@"Compare Deals",@"Checkout",@"Track Order", nil];
    
    
    
    self.imageOutlet.image = [imgsArr objectAtIndex:_index];
    self.titleOutlet.text = [titleArr objectAtIndex:_index];
    self.mainTitle_Lbl.text = [mainTitleArr objectAtIndex:_index];
    
  //  self.imageOutlet.image = [UIImage imageNamed:@"apple.png"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
