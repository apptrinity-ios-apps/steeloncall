//
//  YourOrderHeaderCell.h
//  SteelonCall
//
//  Created by nagaraj  kumar on 16/11/18.
//  Copyright © 2018 com.indobytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YourOrderHeaderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *order_idLBL;
@property (weak, nonatomic) IBOutlet UILabel *date_LBL;

@end
