//
//  My_Cart.m
//  SteelonCall
//
//  Created by Manoyadav on 02/12/16.
//  Copyright © 2016 com.way2online. All rights reserved.

#import "My_Cart.h"
#define ACCEPTABLE_CHARACTERS @"0123456789."

@interface My_Cart ()
{
    NSString *checkAvilability;
    int row;
}

@end

@implementation My_Cart

-(void)viewWillAppear:(BOOL)animated
{
    AppDelegate *del = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [del setShouldRotate:NO];
    [self.navigationController setNavigationBarHidden:NO animated:NO];

    self.navigationController.navigationBar.hidden = YES;
    /*
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:10.0/255.0 green:21.0/255.0 blue:80.0/255.0 alpha:1];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
     UIImageView *navigationImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 188, 40)];
    navigationImage.image=[UIImage imageNamed:@"logo.png"];
    self.navigationItem.titleView=navigationImage;
    
    UIButton *cameraButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 2, 40.0f, 40.0f)];
    UIImage *cameraImage = [UIImage imageNamed:@"Back"];
    [cameraButton setBackgroundImage:cameraImage forState:UIControlStateNormal];
    [cameraButton addTarget:self action:@selector(Back_click) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* cameraButtonItem = [[UIBarButtonItem alloc] initWithCustomView:cameraButton];
    self.navigationItem.leftBarButtonItem = cameraButtonItem;
     */
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    user_data=[NSUserDefaults standardUserDefaults];
    user_id=[user_data valueForKey:@"user_id"];
    _cart_count.text=[NSString stringWithFormat:@"My Cart (%@)",[user_data valueForKey:@"cart_count"]];
    
    checkAvilability = @"true";
    
    if ([_cart_count.text isEqualToString:@"My Cart (0)"])
    {
//        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, _cart_table.bounds.size.width, _cart_table.bounds.size.height)];
//        noDataLabel.text             = @"You have no items in your shopping cart.";
//        noDataLabel.textColor        = [UIColor blackColor];
//        noDataLabel.textAlignment    = NSTextAlignmentCenter;
       // _cart_table.backgroundView = noDataLabel;
        _cart_table.hidden = YES;
        
        _cart_table.separatorStyle = UITableViewCellSeparatorStyleNone;
        _grand_view.hidden=YES;
        _btn_view.hidden=YES;
    }
    else
    {
        _cart_table.hidden = NO;
        _grand_view.hidden=NO;
        _btn_view.hidden=NO;
    }
    Grand_total=0;
    _cart_table.dataSource = self;
    _cart_table.delegate = self;
    _cart_table.separatorStyle = UITableViewCellSeparatorStyleNone;
    _cart_table.allowsSelection=NO;
    Cart_Products_ary=[[NSMutableArray alloc]init];
    _cart_table.separatorStyle = UITableViewCellSeparatorStyleNone;
    pieces_per_ton_ary=[[NSMutableArray alloc]init];
    Grand_Total_Dict=[[NSMutableDictionary alloc]init];
    keyboardIsShown=NO;
    //notifications
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWillShow:)
//                                                 name:UIKeyboardWillShowNotification
//                                               object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWillHide:)
//                                                 name:UIKeyboardWillHideNotification
//                                               object:nil];
    
    //tap gesture
    singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.cart_table addGestureRecognizer:singleFingerTap];
    
    
    _cart_view.layer.cornerRadius = 10;
    
    //tap gesture
    UITapGestureRecognizer *cart_tap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(Cart_click:)];
    [_cart_view addGestureRecognizer:cart_tap];
    
    NSUserDefaults *user_data=[NSUserDefaults standardUserDefaults];
    NSString *C_Count=[user_data valueForKey:@"cart_count"];
    _Cart_lbl.text=C_Count;
    
    [self card_products];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    //CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    [self.view endEditing:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    tableHeight = self.cart_table.frame.size.height;
}

-(void)card_products
{
    if (user_id == nil|| [user_id isEqual:[NSNull null]]||[user_id isEqualToString:@"(null)"]||[user_id isEqualToString:@"<null>"])
    {
        ALERT_DIALOG(@"Alert", @"User Id Empty!");
    }
    else
    {
    NSString *url_form=[NSString stringWithFormat:@"getCustomerQuote?customer_id=%@",user_id];
    NSString* urlEncoded = [url_form stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString:urlEncoded requestNumber:Getting_Carts showProgress:YES withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             NSDictionary *Cart_Products_dict=data;
             NSArray *Products_ary=[Cart_Products_dict valueForKey:@"response"];
             NSString *check=[NSString stringWithFormat:@"%@",Products_ary];
            
             
             if ([check isEqualToString:@"0"])
             {
                // _grand_view.hidden=YES;
                // _btn_view.hidden=YES;

             }
             else
             {
               //  _grand_view.hidden=NO;
               //  _btn_view.hidden=NO;

                 _shopNow.hidden = YES;
                 Grand_total=0;
                 for (int d=0; d<Products_ary.count; d++)
                 {
                    if (d==0)
                    {
                        product_ids_str=[[Products_ary valueForKey:@"id"]objectAtIndex:d];
                    }
                    else
                        product_ids_str=[NSString stringWithFormat:@"%@_%@",product_ids_str,[[Products_ary valueForKey:@"id"]objectAtIndex:d]];
                     
                     [Cart_Products_ary addObject:[Products_ary objectAtIndex:d]];
                     NSString *pri_str=[[Cart_Products_ary valueForKey:@"price"]objectAtIndex:d];
                     float prict_int=[pri_str floatValue];
                     Grand_total=Grand_total+prict_int;
                     [Grand_Total_Dict setObject:pri_str forKey:[NSString stringWithFormat:@"%d",d]];
                 }
                 _Grand_lbl.text=[NSString stringWithFormat:@"Rs. %.2f",Grand_total];
                
                 user_data=[NSUserDefaults standardUserDefaults];
                 [user_data setValue:[NSString stringWithFormat:@"%lu",(unsigned long)Cart_Products_ary.count] forKey:@"cart_count"];

                 _cart_count.text=[NSString stringWithFormat:@"My Cart (%@)",[user_data valueForKey:@"cart_count"]];
                 
                 if ([_cart_count.text isEqualToString:@"My Cart (0)"])
                 {
//                     UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, _cart_table.bounds.size.width, _cart_table.bounds.size.height)];
//                     noDataLabel.text             = @"You have no items in your shopping cart.";
//                     noDataLabel.textColor        = [UIColor blackColor];
//                     noDataLabel.textAlignment    = NSTextAlignmentCenter;
//                     _cart_table.backgroundView = noDataLabel;
                     _cart_table.hidden = YES;
                     
                     _cart_table.separatorStyle = UITableViewCellSeparatorStyleNone;
                     _grand_view.hidden=YES;
                     _btn_view.hidden=YES;
                     _shopNow.hidden = YES;
                 }
                 else
                 {
                     _cart_table.hidden = NO;
                     _grand_view.hidden=NO;
                     _btn_view.hidden=NO;
                     _shopNow.hidden = YES;
                 }
                 
                 [self call_tonnes_service];
                 
                 [_cart_table reloadData];
             }
         }
         else
         {
            
         }
     }];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (Cart_Products_ary.count==0)
    {
        return 0;
    }
    else
    return Cart_Products_ary.count+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row ==Cart_Products_ary.count )
    {
        static NSString *simpleTableIdentifier = @"Coupan_cell";
        Coupan_cell *cell = (Coupan_cell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
//        cell.border_view.layer.borderColor=[UIColor blackColor].CGColor;
//        cell.border_view.layer.borderWidth = 2.0f;
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"Coupan_cell" owner:self options:nil];
            cell = (Coupan_cell *)[nib objectAtIndex:0];
        }
        [cell.send addTarget:self action:@selector(send_click) forControlEvents:UIControlEventTouchUpInside];
        cell.coupon.text=@"";
        return cell;
    }
    else
    {
    static NSString *simpleTableIdentifier = @"MyCart_Cell";
    MyCart_Cell *cell = (MyCart_Cell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MyCart_Cell" owner:self options:nil];
        cell = (MyCart_Cell *)[nib objectAtIndex:0];
    }
        cell.remove.tag=indexPath.row;
        cell.save.tag=indexPath.row;

        [cell.remove addTarget:self action:@selector(remove_click:) forControlEvents:UIControlEventTouchUpInside];
        [cell.save addTarget:self action:@selector(save_click:) forControlEvents:UIControlEventTouchUpInside];
        cell.name.text=[[Cart_Products_ary valueForKey:@"name"]objectAtIndex:indexPath.row];
         cell.seller_name.text=[NSString stringWithFormat:@" %@",[[Cart_Products_ary valueForKey:@"sellerName"]objectAtIndex:indexPath.row]];
         cell.brand.text=[NSString stringWithFormat:@" %@",[[Cart_Products_ary valueForKey:@"brand"]objectAtIndex:indexPath.row]];
         cell.price.text=[NSString stringWithFormat:@" Rs.%@",[[Cart_Products_ary valueForKey:@"tonprice"]objectAtIndex:indexPath.row]];//changed to price
         cell.tonnnes.text=[NSString stringWithFormat:@" %@",[[Cart_Products_ary valueForKey:@"quantityInTonnes"]objectAtIndex:indexPath.row]];
         cell.pieces.text=[NSString stringWithFormat:@" %@",[[Cart_Products_ary valueForKey:@"quantityInPieces"]objectAtIndex:indexPath.row]];
         cell.rolling.text=[NSString stringWithFormat:@" %@",[[Cart_Products_ary valueForKey:@"grade"]objectAtIndex:indexPath.row]];
        
        NSString * discount_Str = [NSString stringWithFormat:@" %@",[[Cart_Products_ary valueForKey:@"discount"]objectAtIndex:indexPath.row]];
        if ([discount_Str isEqualToString:@" 0"]||[discount_Str isEqualToString:@"0"]) {
            cell.discountLabelheight.constant = 0;
            cell.dicountValueHeight.constant = 0;
        }
        else
        {
            cell.Discount_lbl.text =[NSString stringWithFormat:@" %@",discount_Str];
        }
        NSURL *imageURL = [NSURL URLWithString:[[Cart_Products_ary valueForKey:@"img"]objectAtIndex:indexPath.row]];
        [cell.img sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
        
        UIToolbar *keyboardDoneButtonView = [[UIToolbar alloc] init];
        [keyboardDoneButtonView sizeToFit];
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                       style:UIBarButtonItemStyleBordered target:self
                                                                      action:@selector(doneClicked:)];
        [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
        cell.tonnnes.inputAccessoryView = keyboardDoneButtonView;
        cell.pieces.inputAccessoryView = keyboardDoneButtonView;
        cell.tonnnes.delegate = self;
        cell.pieces.delegate = self;
        cell.pieces.keyboardType=UIKeyboardTypePhonePad;

        NSString *Home_tag=[NSString stringWithFormat:@"1%ld",(long)indexPath.row];
        int Home_tag_int=[Home_tag intValue];
        NSString *Away_tag=[NSString stringWithFormat:@"2%ld",(long)indexPath.row];
        int Away_tag_int=[Away_tag intValue];
        
        cell.tonnnes.tag = Home_tag_int;
        cell.pieces.tag = Away_tag_int;
        
        UIColor *color = [UIColor grayColor];
        cell.tonnnes.attributedPlaceholder =
        [[NSAttributedString alloc] initWithString:@"Quantity in Tonnes"
                                        attributes:@{
                                                     NSForegroundColorAttributeName: color,
                                                     NSFontAttributeName : [UIFont fontWithName:@"Arial" size:12.0]
                                                     }
         ];
        cell.pieces.attributedPlaceholder =
        [[NSAttributedString alloc] initWithString:@"Quantity in Pieces"
                                        attributes:@{
                                                     NSForegroundColorAttributeName: color,
                                                     NSFontAttributeName : [UIFont fontWithName:@"Arial" size:12.0]
                                                     }
         ];

        float ton_price_int=[cell.price.text floatValue];
        float tons_int=[cell.tonnnes.text floatValue];
        float total_ton_price=ton_price_int*tons_int;
        cell.total_price.text=[NSString stringWithFormat:@" Rs.%@",[[Cart_Products_ary valueForKey:@"price"]objectAtIndex:indexPath.row]];
        return cell;
    }
}

- (IBAction)save_click:(id)sender
{
    /*
        int tag=[sender tag];
    NSString *parent_id=[[Cart_Products_ary valueForKey:@"id"]objectAtIndex:tag];
    NSString *child_id=[[Cart_Products_ary valueForKey:@"childId"]objectAtIndex:tag];
    NSString *quantity=[[Cart_Products_ary valueForKey:@"quantityInTonnes"]objectAtIndex:tag];

        NSString *url_form=[NSString stringWithFormat:@"editProductQty?parent_id=%@&child_id=%@&qty=%@&customer_id=%@",parent_id,child_id,quantity,user_id];
        NSString* urlEncoded = [url_form stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString:urlEncoded requestNumber:Save_cart showProgress:YES withHandler:^(BOOL success, id data)
         {
             if (success)
             {
                 NSDictionary *Cart_Products_dict=data;
                 NSLog(@"Status %@",Cart_Products_dict);
            }
             else
             {
             }
         }];
     */
}

-(void)remove_click:(id)sender
{
    int tag=[sender tag];
    NSString *url_form=[NSString stringWithFormat:@"deleteCartItem?customer_id=%@&product_id=%@",user_id,[[Cart_Products_ary valueForKey:@"id"]objectAtIndex:tag]];
    NSString* urlEncoded = [url_form stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString:urlEncoded requestNumber:Remove_cart showProgress:YES withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             NSDictionary *Cart_Products_dict=data;
             NSLog(@"Status %@",Cart_Products_dict);
             [Cart_Products_ary removeObjectAtIndex:tag];
             [pieces_per_ton_ary removeObjectAtIndex:tag];
             Grand_total =0;
             
             for (int d=0; d<Cart_Products_ary.count; d++)
             {
                 NSString *pri_str=[[Cart_Products_ary valueForKey:@"price"]objectAtIndex:d];
                 float prict_int=[pri_str floatValue];
                 Grand_total=Grand_total+prict_int;
             }
             
             _Grand_lbl.text=[NSString stringWithFormat:@"Rs. %.2f",Grand_total];

             user_data=[NSUserDefaults standardUserDefaults];
             [user_data setValue:[NSString stringWithFormat:@"%lu",(unsigned long)Cart_Products_ary.count] forKey:@"cart_count"];
             
              _cart_count.text=[NSString stringWithFormat:@"My Cart (%@)",[user_data valueForKey:@"cart_count"]];
             
             if ([_cart_count.text isEqualToString:@"My Cart (0)"])
             {
                 UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, _cart_table.bounds.size.width, _cart_table.bounds.size.height)];
                 noDataLabel.text             = @"You have no items in your shopping cart.";
                 noDataLabel.textColor        = [UIColor blackColor];
                 noDataLabel.textAlignment    = NSTextAlignmentCenter;
                 _cart_table.backgroundView = noDataLabel;
                 _cart_table.separatorStyle = UITableViewCellSeparatorStyleNone;
                 _grand_view.hidden=YES;
                 _btn_view.hidden=YES;
                 
                 _cart_table.hidden = YES;
                 _shopNow.hidden = NO;
                 
             }
             else
             {
                 _grand_view.hidden=NO;
                 _btn_view.hidden=NO;
             }
             
             [_cart_table reloadData];
         }
         else
         {
         }
     }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == Cart_Products_ary.count)
    {
        return 260;// coupon height
    }
    else
      //  return 330;
        return 273;
}

-(void)send_click
{
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:Cart_Products_ary.count inSection:0];
        Coupan_cell *coupan_cell = (Coupan_cell *)
        [_cart_table cellForRowAtIndexPath:indexpath];
    NSString *coupon_text=coupan_cell.coupon.text;
    
    if ([coupon_text isEqualToString:@""]||[coupon_text isEqual:[NSNull null]]||coupon_text == nil)
    {
        // [self Alert:@"Please enter valid coupon code!"];
        ALERT_DIALOG(@"Alert", @"Please enter valid coupon code!");
    }
    else
    {
        NSString *url_form=[NSString stringWithFormat:@"couponPost?customer_id=%@&&coupon_code=%@&&remove=0",user_id,coupon_text];
        NSString* urlEncoded = [url_form stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString:urlEncoded requestNumber:Coupon_send showProgress:YES withHandler:^(BOOL success, id data)
         {
             if (success)
             {
                 NSArray *Coupon_dict=data;
                 NSDictionary *coupon_dict=[[Coupon_dict valueForKey:@"response"]objectAtIndex:0];
                 NSString *status=[coupon_dict valueForKey:@"status"];
                 NSString *coupon_status=[coupon_dict valueForKey:@"message"];
                 NSLog(@"Coupon Status %@",coupon_status);
                // [self Alert:coupon_status];
                 ALERT_DIALOG(@"Alert", coupon_status);
                 [self.view endEditing:YES];
             }
             else
             {
//                 UIAlertController * alert=   [UIAlertController
//                                               alertControllerWithTitle:@"Alert"
//                                               message:[NSString stringWithFormat:@"%@",[data valueForKey:@"error_message"]]
//                                               preferredStyle:UIAlertControllerStyleAlert];
//                 
//                 UIAlertAction* ok = [UIAlertAction
//                                      actionWithTitle:@"OK"
//                                      style:UIAlertActionStyleDefault
//                                      handler:^(UIAlertAction * action)
//                                      {
//                                          [alert dismissViewControllerAnimated:YES completion:nil];
//                                      }];
//                 [alert addAction:ok];
//                 [self presentViewController:alert animated:YES completion:nil];
             }
         }];
    }
}

- (void)keyboardWillHide:(NSNotification *)n
{
    NSDictionary* userInfo = [n userInfo];
    
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    // resize the scrollview
    CGRect viewFrame = self.cart_table.frame;
    // I'm also subtracting a constant kTabBarHeight because my UIScrollView was offset by the UITabBar so really only the portion of the keyboard that is leftover pass the UITabBar is obscuring my UIScrollView.
    viewFrame.size.height += (keyboardSize.height - 100);
    if (viewFrame.size.height < tableHeight) {
        viewFrame.size.height = tableHeight;
    }
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [self.cart_table setFrame:viewFrame];
    [UIView commitAnimations];
    
    keyboardIsShown = NO;
}

- (void)keyboardWillShow:(NSNotification *)n
{
    // This is an ivar I'm using to ensure that we do not do the frame size adjustment on the `UIScrollView` if the keyboard is already shown.  This can happen if the user, after fixing editing a `UITextField`, scrolls the resized `UIScrollView` to another `UITextField` and attempts to edit the next `UITextField`.  If we were to resize the `UIScrollView` again, it would be disastrous.  NOTE: The keyboard notification will fire even when the keyboard is already shown.
    if (keyboardIsShown)
    {
        return;
    }
    
    NSDictionary* userInfo = [n userInfo];
    
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    // resize the noteView
    CGRect viewFrame = self.cart_table.frame;
    // I'm also subtracting a constant kTabBarHeight because my UIScrollView was offset by the UITabBar so really only the portion of the keyboard that is leftover pass the UITabBar is obscuring my UIScrollView.
    viewFrame.size.height -= (keyboardSize.height - 100);
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [self.cart_table setFrame:viewFrame];
    [UIView commitAnimations];
    keyboardIsShown = YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)Continue_shopping:(id)sender
{
    NSArray *currentControllers = self.navigationController.viewControllers;
   

    BOOL ischeck = false;
    
    for (UIViewController *view in currentControllers ) {
        if ([view isKindOfClass:[HomeViewController class]]) {
            ischeck =YES;
        [self.navigationController popToViewController:view animated:YES];
            break;
            
        }
    }
    
    if (!ischeck) {
         HomeViewController *con = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
        /* Create a mutable array out of this array */
        NSMutableArray *newControllers = [NSMutableArray
                                          arrayWithArray:@[con]];
        
        /* Remove the last object from the array */
        
        
        /* Assign this array to the Navigation Controller */
        self.navigationController.viewControllers = newControllers;
        [self.navigationController pushViewController:con animated:YES];


    }
    
}

- (IBAction)PlaceOrder_click:(id)sender
{
    NSString * sellarName;
    if (Cart_Products_ary.count>0)
    {
        BOOL check = false;
        NSString *checkAv;
        
        for (int p=0; p<Cart_Products_ary.count; p++)
        {
            NSString *tonnes=[NSString stringWithFormat:@"%@",[[Cart_Products_ary valueForKey:@"quantityInTonnes"]objectAtIndex:p]];
            if ([tonnes isEqual:[NSNull null]]||[tonnes isEqualToString:@""]||[tonnes isEqualToString:@"0"])
            {
                check = YES;
                break;
            }
            
             checkAv=[NSString stringWithFormat:@"%@",[[Cart_Products_ary valueForKey:@"checkavilable"]objectAtIndex:p]];
            
            if ([checkAv isEqualToString:@"false"]) {
                
                sellarName = [NSString stringWithFormat:@"%@",[[Cart_Products_ary valueForKey:@"sellerName"]objectAtIndex:p]];
                break;
            }
            
        }
        
        if (![checkAv isEqualToString:@"false"]) {
            
            if (check) {
                [self Alert:@"Please enter tonnes quantity"];
            }
            else{
                BillingInfoViewController *define = [[BillingInfoViewController alloc]init];
                [self.navigationController pushViewController:define animated:YES];
            }
        }
        
        else
        {
            NSString *str = [NSString stringWithFormat:@"Please change quantity for %@",sellarName];
             [self Alert:str];
        }

        
        
    }
    else
        [self Alert:@"Add products to the Cart!"];
}




-(void)Alert:(NSString *)Msg
{
    NSDictionary *options = @{kCRToastNotificationTypeKey:@(CRToastTypeNavigationBar),
                              
                              kCRToastTextKey : Msg,
                              
                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                              
                              kCRToastBackgroundColorKey : [UIColor colorWithRed:44.0/255.0 green:52.0/255.0 blue:75.0/255.0 alpha:1],
                              kCRToastTimeIntervalKey: @(2),
                              //                              kCRToastFontKey:[UIFont fontWithName:@"PT Sans Narrow" size:23],
                              kCRToastInteractionRespondersKey:@[[CRToastInteractionResponder interactionResponderWithInteractionType:CRToastInteractionTypeSwipeUp
                                                                  
                                                                                                                 automaticallyDismiss:YES
                                                                  
                                                                                                                                block:^(CRToastInteractionType interactionType){
                                                                                                                                    
                                                                                                                                    NSLog(@"Dismissed with %@ interaction", NSStringFromCRToastInteractionType(interactionType));
                                                                                                                                }]],
                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeGravity),
                              
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeGravity),
                              
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                              
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop)
                              
                              };
    
    [CRToastManager showNotificationWithOptions:options
                                completionBlock:^{
                                    NSLog(@"Completed");
                                }];
}

-(void)Back_click
{
    if ([_From_check isEqualToString:@"login"])
    {
        NSArray *currentControllers = self.navigationController.viewControllers;
        
        
        BOOL ischeck = false;
        
        for (UIViewController *view in currentControllers ) {
            if ([view isKindOfClass:[HomeViewController class]]) {
                ischeck =YES;
                [self.navigationController popToViewController:view animated:YES];
                break;
                
            }
        }
        
        if (!ischeck) {
            HomeViewController *con = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
            /* Create a mutable array out of this array */
            NSMutableArray *newControllers = [NSMutableArray
                                              arrayWithArray:@[con]];
            
            /* Remove the last object from the array */
            
            
            /* Assign this array to the Navigation Controller */
            self.navigationController.viewControllers = newControllers;
            [self.navigationController pushViewController:con animated:YES];
            
            
        }    }
    else
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)call_tonnes_service
{
   // NSString *url_form=[NSString stringWithFormat:@"https://steeloncall.com/getcalculateqty?ids=%@",product_ids_str];
    
     NSString *url_form=[NSString stringWithFormat:@"http://stg.steeloncall.com/getcalculateqty?ids=%@",product_ids_str];
    
    NSString* urlEncoded = [url_form stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString: urlEncoded requestNumber:PRODUCT_TONNES showProgress:YES withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             NSLog(@" response %@",data);
             NSDictionary *res_data=data;
             for (int p=0; p<res_data.count; p++)
             {
                 [pieces_per_ton_ary addObject:[[res_data valueForKey:@"pieces"]objectAtIndex:p]];
             }
             
             
             
             
         }
         else
         {
//             UIAlertController * alert=   [UIAlertController
//                                           alertControllerWithTitle:@"Alert"
//                                           message:[NSString stringWithFormat:@"%@",[data valueForKey:@"error_message"]]
//                                           preferredStyle:UIAlertControllerStyleAlert];
//             
//             UIAlertAction* ok = [UIAlertAction
//                                  actionWithTitle:@"OK"
//                                  style:UIAlertActionStyleDefault
//                                  handler:^(UIAlertAction * action)
//                                  {
//                                      [alert dismissViewControllerAnimated:YES completion:nil];
//                                  }];
//             [alert addAction:ok];
//             [self presentViewController:alert animated:YES completion:nil];
         }
     }];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSLog(@"enterd text %@",newString);
    
    NSString *Home_tag_str=[NSString stringWithFormat:@"%ld",(long)textField.tag];
    
    NSString *TF_check = [NSString stringWithFormat:@"%c", [Home_tag_str characterAtIndex:0]];
    NSString *Tag = [Home_tag_str substringFromIndex:1];
    row=[Tag intValue];
    NSLog(@"tag %d",row);
    
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:row inSection:0];
    MyCart_Cell *tappedCell = (MyCart_Cell *)[_cart_table cellForRowAtIndexPath:indexpath];
    
    NSString *pieces_per_tonne=[pieces_per_ton_ary objectAtIndex:row];
    int pieces_per_tonne_int=[pieces_per_tonne intValue];
    
    if(![string isEqualToString:@"0"] || textField.text.length >0 || [string isEqualToString:@""]) {
        
        
        if ([TF_check isEqualToString:@"1"])//tonnes
        {
            NSArray *sep = [newString componentsSeparatedByString:@"."];
            //        NSString *lastStrng =[NSString stringWithFormat:@"%hu",[newString characterAtIndex:[newString length] - 1]];
            
            if([sep count] >= 2)
            {
                NSString *sepStr=[NSString stringWithFormat:@"%@",[sep objectAtIndex:1]];
                if (sepStr.length>3)
                {
                    return !([sepStr length]>3);
                }
                
                float tonnes_int=[newString floatValue];
                int total_pieces_int=tonnes_int*pieces_per_tonne_int;
                NSString *total_pieces_str;
                if (total_pieces_int >0) {
                    total_pieces_str=[NSString stringWithFormat:@"%d",total_pieces_int];
                    if (total_pieces_str.length>6) {
                        NSRange range = NSMakeRange(0,6);
                        NSString *trimmedString=[total_pieces_str substringWithRange:range];
                        total_pieces_str =trimmedString;
                    }
                }else{
                    newString = @"";
                    total_pieces_str = @"";
                }
                tappedCell.pieces.text=total_pieces_str;
                NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
                NSDictionary *oldDict = (NSDictionary *)[Cart_Products_ary objectAtIndex:row];
                [newDict addEntriesFromDictionary:oldDict];
                [newDict setObject:newString forKey:@"quantityInTonnes"];
                [newDict setObject:total_pieces_str forKey:@"quantityInPieces"];
                [Cart_Products_ary replaceObjectAtIndex:row withObject:newDict];
                
                tappedCell.pieces.text=total_pieces_str;
                [self change_tonnes_service:newString :row];
                
            }
            else
            {
                float tonnes_int=[newString floatValue];
                int total_pieces_int=tonnes_int*pieces_per_tonne_int;
                NSString *total_pieces_str;
                if (total_pieces_int >0)
                {
                    total_pieces_str=[NSString stringWithFormat:@"%d",total_pieces_int];
                    
                    if (total_pieces_str.length>6) {
                        NSRange range = NSMakeRange(0,6);
                        NSString *trimmedString=[total_pieces_str substringWithRange:range];
                        total_pieces_str =trimmedString;
                    }
                }else{
                    newString = @"";
                    total_pieces_str = @"";
                }
                tappedCell.pieces.text=total_pieces_str;
                NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
                NSDictionary *oldDict = (NSDictionary *)[Cart_Products_ary objectAtIndex:row];
                [newDict addEntriesFromDictionary:oldDict];
                [newDict setObject:newString forKey:@"quantityInTonnes"];
                [newDict setObject:total_pieces_str forKey:@"quantityInPieces"];
                [Cart_Products_ary replaceObjectAtIndex:row withObject:newDict];
                tappedCell.pieces.text=total_pieces_str;
                [self change_tonnes_service:newString :row];
                
                
            }
        }
        else if ([TF_check isEqualToString:@"2"])//pieces
        {
            float pieces_int=[newString floatValue];
            float total_tonnes_int=pieces_int/pieces_per_tonne_int;
            NSString *total_tonnes_str;
            NSArray *sep = [newString componentsSeparatedByString:@"."];
            if (sep.count == 3) {
                return false;
            }

            if (sep.count == 2) {
                return false;
            }
            if (total_tonnes_int >0) {
                total_tonnes_str=[NSString stringWithFormat:@"%.3f",total_tonnes_int];
                
                if (total_tonnes_str.length>6) {
                    NSRange range = NSMakeRange(0,6);
                    NSString *trimmedString=[total_tonnes_str substringWithRange:range];
                    total_tonnes_str =trimmedString;
                    
                }
            }else{
                newString = @"";
                total_tonnes_str = @"";
            }
            
            NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
            NSDictionary *oldDict = (NSDictionary *)[Cart_Products_ary objectAtIndex:row];
            [newDict addEntriesFromDictionary:oldDict];
            [newDict setObject:total_tonnes_str forKey:@"quantityInTonnes"];
            [newDict setObject:newString forKey:@"quantityInPieces"];
            [Cart_Products_ary replaceObjectAtIndex:row withObject:newDict];
            tappedCell.tonnnes.text=total_tonnes_str;
            [self change_tonnes_service:tappedCell.tonnnes.text :row];

            NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
            
            NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
            if (![string isEqualToString:filtered]) {
                return false;
            }
            
//            if (!string.length)
//                return YES;
//            NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string]; NSString *expression = @"^([0-9]+)?(\\.([0-9]{1,2})?)?$"; NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:nil];
//            NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
            
//            if (numberOfMatches == 0)
//                return YES;
        }
        //  [_cart_table reloadData];
        
    }else{
        return NO;
    }
    return YES;
}

-(void)change_tonnes_service:(NSString *)tonnes :(int )tag_int
{
    NSString *child_id=[[Cart_Products_ary valueForKey:@"childId"]objectAtIndex:tag_int];
    NSString *prod_id=[[Cart_Products_ary valueForKey:@"id"]objectAtIndex:tag_int];
    
    NSMutableArray *ary=[[NSMutableArray alloc]init];
    
    for (int i=0; i<Cart_Products_ary.count; i++)
    {
        NSString *d=[[Cart_Products_ary valueForKey:@"childId"]objectAtIndex:i];
        [ary addObject:d];
    }
    NSString * childs_ids = [ary componentsJoinedByString:@"_"];
    
    
    NSMutableArray *ary1=[[NSMutableArray alloc]init];
    for (int i=0; i<Cart_Products_ary.count; i++)
    {
        NSString *d=[[Cart_Products_ary valueForKey:@"id"]objectAtIndex:i];
        [ary1 addObject:d];
    }
    NSString * product_ids = [ary1 componentsJoinedByString:@"_"];
    
    
//    childs
//    product_ids
    
    NSString *url_form=[NSString stringWithFormat:@"changetons?id=%@&child_id=%@&ton=%@&customer_id=%@&childs=%@&product_ids=%@",prod_id,child_id,tonnes,user_id,childs_ids,product_ids];
    
    
    
    NSString* urlEncoded = [url_form stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString:urlEncoded requestNumber:Change_tons showProgress:YES withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             NSLog(@" response %@",data);
             if ([data count]>0)
             {
            NSDictionary *res_dict=[ data objectAtIndex:0];
             if (res_dict.count>0)
             {
                 NSString *status=[NSString stringWithFormat:@"%@",[res_dict valueForKey:@"status"]];
                 if ([status isEqualToString:@"0"]||[status isEqualToString:@"false"])
                 {
                     
                     NSIndexPath *indexpath = [NSIndexPath indexPathForRow:tag_int inSection:0];
                     MyCart_Cell *tappedCell1 = (MyCart_Cell *)[_cart_table cellForRowAtIndexPath:indexpath];
                     
                     
                     NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
                     NSDictionary *oldDict = (NSDictionary *)[Cart_Products_ary objectAtIndex:tag_int];
                     [newDict addEntriesFromDictionary:oldDict];
                     [newDict setObject:@"false" forKey:@"checkavilable"];
                     [Cart_Products_ary replaceObjectAtIndex:tag_int withObject:newDict];

                     
                     [self Alert:[res_dict valueForKey:@"message"]];
                    // checkAvilability = @"false";
                 }
                 else
                 {
                    // checkAvilability = @"true";
                     NSIndexPath *indexpath = [NSIndexPath indexPathForRow:tag_int inSection:0];
                     MyCart_Cell *tappedCell = (MyCart_Cell *)[_cart_table cellForRowAtIndexPath:indexpath];
                     discount_str=[NSString stringWithFormat:@"%@",[res_dict valueForKey:@"discount"]];
                     price_str=[NSString stringWithFormat:@"%@",[res_dict valueForKey:@"price"]];
                     
                     NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
                     NSDictionary *oldDict = (NSDictionary *)[Cart_Products_ary objectAtIndex:tag_int];
                     [newDict addEntriesFromDictionary:oldDict];
                     [newDict setObject:discount_str forKey:@"discount"];
                     [newDict setObject:price_str forKey:@"price"];
                     [newDict setObject:@"true" forKey:@"checkavilable"];
                     
                     [Cart_Products_ary replaceObjectAtIndex:tag_int withObject:newDict];
                     
                     if ([discount_str isEqualToString:@"0"]) {
                         tappedCell.dicountValueHeight.constant = 0.0;
                         tappedCell.discountLabelheight.constant = 0.0;
                     }
                     else
                     {
                     tappedCell.Discount_lbl.text=discount_str;
                     }
                     tappedCell.total_price.text=price_str;
                     
                     
                     Grand_total =0;
                     for (int d=0; d<Cart_Products_ary.count; d++)
                     {
                         NSString *pri_str=[[Cart_Products_ary valueForKey:@"price"]objectAtIndex:d];
                         float prict_int=[pri_str floatValue];
                         Grand_total=Grand_total+prict_int;
                         //[Grand_Total_Dict setObject:pri_str forKey:[NSString stringWithFormat:@"%d",d]];
                     }
                     
                     
                     _Grand_lbl.text=[NSString stringWithFormat:@"Rs. %.2f",Grand_total];
                     
                     
                 }
             }
         }
         }
         else
         {
             
         }
     }];
}

- (IBAction)doneClicked:(id)sender
{
    NSLog(@"Done Clicked.");
    [self.view endEditing:YES];
}

- (IBAction)cart_click:(id)sender
{
    My_Cart *define = [[My_Cart alloc]init];
    [self.navigationController pushViewController:define animated:YES];
}

- (IBAction)back_click:(id)sender
{
    if ([_From_check_cart isEqualToString:@"N"])
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate back_click];
    }
    else
    {
        
        if ([_From_check isEqualToString:@"login"])
        {
            NSArray *currentControllers = self.navigationController.viewControllers;
            
            
            BOOL ischeck = false;
            
            for (UIViewController *view in currentControllers ) {
                if ([view isKindOfClass:[HomeViewController class]]) {
                    ischeck =YES;
                    [self.navigationController popToViewController:view animated:YES];
                    break;
                    
                }
            }
            
            if (!ischeck) {
                HomeViewController *con = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
                /* Create a mutable array out of this array */
                NSMutableArray *newControllers = [NSMutableArray
                                                  arrayWithArray:@[con]];
                
                /* Remove the last object from the array */
                
                
                /* Assign this array to the Navigation Controller */
                self.navigationController.viewControllers = newControllers;
                [self.navigationController pushViewController:con animated:YES];
                
                
            }    }
        else
            [self.navigationController popViewControllerAnimated:YES];
    }
}
- (IBAction)goToHomeScreenAction:(id)sender {
    HomeViewController *con = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
    [self.navigationController pushViewController:con animated:YES];
    
    
}
@end
