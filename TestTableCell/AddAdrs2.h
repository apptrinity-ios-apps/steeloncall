//
//  AddAdrs2.h
//  SteelonCall
//
//  Created by S s Vali on 9/12/17.
//  Copyright © 2017 com.indobytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddAdrs2 : UITableViewCell
@property (strong, nonatomic) IBOutlet UITextField *strettAdrs;
@property (strong, nonatomic) IBOutlet UITextField *strtAdrs2;
@property (strong, nonatomic) IBOutlet UITextField *city;
@property (strong, nonatomic) IBOutlet UITextField *state;
@property (strong, nonatomic) IBOutlet UITextField *zipcode;
@property (strong, nonatomic) IBOutlet UITextField *mobile;
@property (strong, nonatomic) IBOutlet UITextField *pan;
@property (strong, nonatomic) IBOutlet UITextField *gstinNumber;

@end
