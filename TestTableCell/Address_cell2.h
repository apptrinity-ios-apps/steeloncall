//
//  Address_cell.h
//  SteelonCall
//
//  Created by INDOBYTES on 12/09/17.
//  Copyright © 2017 com.indobytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Address_cell2 : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UILabel *company;
@property (strong, nonatomic) IBOutlet UILabel *colony;
@property (strong, nonatomic) IBOutlet UILabel *address;
@property (strong, nonatomic) IBOutlet UILabel *country;
@property (strong, nonatomic) IBOutlet UILabel *tele;
@property (strong, nonatomic) IBOutlet UILabel *mobile;
@property (strong, nonatomic) IBOutlet UIButton *delete_addr;
@property (strong, nonatomic) IBOutlet UILabel *pan;
@property (strong, nonatomic) IBOutlet UIButton *edit_addr;
@property (strong, nonatomic) IBOutlet UILabel *cst;
@end
