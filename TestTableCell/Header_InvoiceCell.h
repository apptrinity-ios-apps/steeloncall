//
//  Header_InvoiceCell.h
//  SteelonCall
//
//  Created by INDOBYTES on 03/04/17.
//  Copyright © 2017 com.indobytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Header_InvoiceCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *shippNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *ShipAddressLbl;
@property (strong, nonatomic) IBOutlet UILabel *shipAddress2Lbl;


@property (strong, nonatomic) IBOutlet UILabel *shipTinLbl;
@property (strong, nonatomic) IBOutlet UILabel *shipCstLbl;
@property (strong, nonatomic) IBOutlet UILabel *shipPanLbl;
@property (strong, nonatomic) IBOutlet UILabel *shipPhoneLbl;
@property (strong, nonatomic) IBOutlet UILabel *ShipTeleLbl;

// billing

@property (strong, nonatomic) IBOutlet UILabel *billpNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *billAddressLbl;
@property (strong, nonatomic) IBOutlet UILabel *billAddress2Lbl;


@property (strong, nonatomic) IBOutlet UILabel *billTinLbl;
@property (strong, nonatomic) IBOutlet UILabel *billCstLbl;
@property (strong, nonatomic) IBOutlet UILabel *billPanLbl;
@property (strong, nonatomic) IBOutlet UILabel *billPhoneLbl;
@property (strong, nonatomic) IBOutlet UILabel *billTeleLbl;


@property (strong, nonatomic) IBOutlet UILabel *headerOrderId;
@property (strong, nonatomic) IBOutlet UILabel *headerOrderDate;

@property (strong, nonatomic) IBOutlet UILabel *shipingMethodLbl;
@property (strong, nonatomic) IBOutlet UILabel *paymentMethod_Lbl;
@property (strong, nonatomic) IBOutlet UILabel *sCompany;
@property (strong, nonatomic) IBOutlet UILabel *bCompany;


@end
