//
//  Home_LastCell.h
//  SteelonCall
//
//  Created by INDOBYTES on 29/07/17.
//  Copyright © 2017 com.indobytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STParsing.h"
#import "testimonialCell.h"
#import "blogCell.h"
#import "brandsCell.h"

@interface Home_LastCell : UITableViewCell<UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet UILabel *brandLabel;

@property (strong, nonatomic) IBOutlet UICollectionView *testimonialCollection;
@property (strong, nonatomic) IBOutlet UICollectionView *blogUpdateCollection;
@property (strong, nonatomic) IBOutlet UIView *testimonialView;
@property (strong, nonatomic) IBOutlet UIView *blogView;
@property (strong, nonatomic) IBOutlet UICollectionView *brandCollectionView;

- (IBAction)gotoLeftAction:(id)sender;
- (IBAction)gotoRightAction:(id)sender;

- (IBAction)blogleftAction:(id)sender;
- (IBAction)blogRightAction:(id)sender;

@end
