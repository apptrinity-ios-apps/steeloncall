//
//  YourOrdersViewController.h
//  SteelonCall
//
//  Created by Manoyadav on 06/12/16.
//  Copyright © 2016 com.way2online. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YourOrdersViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *yourOrdersArray;
    
}
@property (strong, nonatomic) IBOutlet UILabel *Cart_count;
@property (strong, nonatomic) IBOutlet UIView *cart_view;
@property (strong, nonatomic) IBOutlet UITableView *yourOrdersTableView;
@property (strong, nonatomic) IBOutlet UILabel *statusLbl;
@property (strong, nonatomic)  NSString *From;
- (IBAction)back_click:(id)sender;
- (IBAction)cart_click:(id)sender;



@end
