//
//  InVoiceListViewController.h
//  SteelonCall
//
//  Created by INDOBYTES on 03/04/17.
//  Copyright © 2017 com.indobytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"

@interface InVoiceListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    NSUserDefaults *user_data;
    NSString *user_id;
}
@property (strong, nonatomic) IBOutlet UITableView *invoiceTableview;
@property (strong, nonatomic)NSString *orderId;
@property (strong, nonatomic)NSString *orderDate;
@property (strong, nonatomic)NSString *orderStatus;
- (IBAction)BackClick:(id)sender;

- (IBAction)printPdfBtn:(id)sender;


@end
