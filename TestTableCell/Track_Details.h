//
//  Track_Details.h
//  SteelonCall
//
//  Created by INDOBYTES on 03/04/17.
//  Copyright © 2017 com.indobytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OP_Cell0.h"
#import "OP_Cell_2_pending.h"
#import "OP_Cell_2_invoice.h"
#import "STParsing.h"
#import "CRToastView.h"

@interface Track_Details : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    NSDictionary *Sellers_dict;
}
@property (strong, nonatomic) IBOutlet UITableView *table;
@property (strong, nonatomic) IBOutlet NSString *order_id;

@end
