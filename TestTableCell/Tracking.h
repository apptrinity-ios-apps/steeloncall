//
//  Tracking.h
//  SteelonCall
//
//  Created by INDOBYTES on 03/04/17.
//  Copyright © 2017 com.indobytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"
#import "Track_Details.h"

@interface Tracking : UIViewController<UITextFieldDelegate>
@property (strong, nonatomic)  NSString *From;
@property (strong, nonatomic) IBOutlet UIView *border_view;
- (IBAction)submit_click:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *order_tf;

@end
