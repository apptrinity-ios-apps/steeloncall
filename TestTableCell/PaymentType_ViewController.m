//
//  PaymentType_ViewController.m
//  steelonCallThree
//
//  Created by nagaraj  kumar on 03/12/16.
//  Copyright © 2016 nagaraj  kumar. All rights reserved.
//

#import "PaymentType_ViewController.h"
#import "Payment_ProductDetailsViewController.h"

@interface PaymentType_ViewController ()
{
    NSString *paymentTypeIs;
    NSString *subMethod;
}

@end

@implementation PaymentType_ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.payment_subview.hidden = YES;
    self.LC_view.layer.borderColor = [UIColor blackColor].CGColor;
    self.LC_view.layer.borderWidth=1;
    check_tag=NO;
    [[self.issuing_bank layer] setBorderColor:[[UIColor grayColor] CGColor]];
    [[self.issuing_bank layer] setBorderWidth:1];
   // [[self.issuing_bank layer] setCornerRadius:15];
    
    [[self.credit_availbale layer] setBorderColor:[[UIColor grayColor] CGColor]];
    [[self.credit_availbale layer] setBorderWidth:1];
    //[[self.credit_availbale layer] setCornerRadius:15];
    
    [[self.adv_bank layer] setBorderColor:[[UIColor grayColor] CGColor]];
    [[self.adv_bank layer] setBorderWidth:1];
  //  [[self.adv_bank layer] setCornerRadius:15];
    
    [[self.documents_req layer] setBorderColor:[[UIColor grayColor] CGColor]];
    [[self.documents_req layer] setBorderWidth:1];

    _shipment_to.editable=NO;
    _shipment_from.editable=NO;
    _documents_req.editable=NO;
    _bank_charges.editable=NO;
    _neg_bank.editable=NO;
    _credit_availbale.editable=NO;
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(view_tap:)];
    [self.view addGestureRecognizer:singleFingerTap];
    
    _payment_refenceTFT.delegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
    
}
//The event handling method
- (void)view_tap:(UITapGestureRecognizer *)recognizer
{
    CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    //Do stuff here...
    [self.view endEditing:YES];
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
   
    
    NSString *newString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    _credit_availbale.text=newString;
    //[self updateTextLabelsWithText: newString];
    return YES;
    
    
}

- (void)keyboardWillShow:(NSNotification *)n
{
    
    [self.view setFrame:CGRectMake(0,-110,self.view.frame.size.width,self.view.frame.size.height)];
    
}
- (void)keyboardWillHide:(NSNotification *)n
{
    
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    
    [self.view endEditing:YES];
    
    
    return YES;
    
}





//- (BOOL)textViewShouldReturn:(UITextView *)textField
//{
////    [self.questionAnswerTextField resignFirstResponder];
//    return YES;
//}
//
//-(void)textViewDidBeginEditing:(UITextView *)textField {
//    
//    if ([textField.text isEqualToString:@"Sorunun cevabını buraya yazınız!"]) {
//        textField.text = @"";
//    }
//}
//
//
//- (void)textViewDidEndEditing:(UITextView *)textField {
//    if ([textField.text isEqualToString:@""]) {
//        textField.text = @"Sorunun cevabını buraya yazınız!";
//    }
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden =YES;
    // self.automaticallyAdjustsScrollViewInsets = NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)credit_click:(id)sender
{
    paymentTypeIs = @"3";

    [_credit_btn setImage:[UIImage imageNamed:@"Radio_select"] forState:UIControlStateNormal];
    [_onlinePayBtn setImage:[UIImage imageNamed:@"Radio_Deselect"] forState:UIControlStateNormal];
    [_cashOnDeliveryBtn setImage:[UIImage imageNamed:@"Radio_Deselect"] forState:UIControlStateNormal];
    self.payment_subview.hidden = YES;
    _credit_height_constraint.constant=1500;
    
    
    
    
    
    [self LC_API];
//
//    _view_1.hidden=NO;
//
//    UIScrollView *scrollview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, 400)];
//    scrollview.contentSize = CGSizeMake(self.view.frame.size.width, 1000);
//
//    _view_1.frame=CGRectMake(1, 1, self.view.frame.size.width-19, 1000);
//     [scrollview addSubview:_view_1];
//     [_credit_view addSubview:scrollview];
    
    
    
}

-(void)LC_API
{
    user_data=[NSUserDefaults standardUserDefaults];
    user_id=[user_data valueForKey:@"user_id"];

    NSString *urlString = [NSString stringWithFormat:@"getCreditFacilityLCData/"];
    NSDictionary *params = @{@"customer_id":user_id};

     [[STParsing sharedWebServiceHelper]requesting_POST_ServiceWithString1:urlString parameters:params requestNumber:Save_Payment showProgress:YES withHandler:^(BOOL success, id data)
    //[[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString:urlString requestNumber:LC showProgress:YES withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             if (data)
             {
                 dict=data;
                
                 _type_lc.text=[NSString stringWithFormat:@"%@",[[dict valueForKey:@"credit"]valueForKey:@"type_lc"]];
                 _usance.text=[NSString stringWithFormat:@"%@",[[dict valueForKey:@"credit"]valueForKey:@"usance"]];
                 _exp_date.text=[NSString stringWithFormat:@"%@",[[dict valueForKey:@"credit"]valueForKey:@"expiry_date_format"]];
                 _ben_name.text=[NSString stringWithFormat:@"%@",[[dict valueForKey:@"credit"]valueForKey:@"benefiiary_name"]];
                 _buyer_name.text=[NSString stringWithFormat:@"%@",[[dict valueForKey:@"credit"]valueForKey:@"buyer_name"]];
                 _issuing_bank.text=[NSString stringWithFormat:@"%@",[[dict valueForKey:@"credit"]valueForKey:@"issuing_bank"]];
                 _credit_availbale.text=[NSString stringWithFormat:@"%@",[[dict valueForKey:@"credit"]valueForKey:@"credit_available_bank"]];
                 _amount.text=[NSString stringWithFormat:@"%@",[[dict valueForKey:@"credit"]valueForKey:@"amount_lc"]];
                  _neg_bank.text=[NSString stringWithFormat:@"%@",[[dict valueForKey:@"credit"]valueForKey:@"negotiating_bank"]];
                  _adv_bank.text=[NSString stringWithFormat:@"%@",[[dict valueForKey:@"credit"]valueForKey:@"advising_bank"]];
                  _bank_charges.text=[NSString stringWithFormat:@"%@",[[dict valueForKey:@"credit"]valueForKey:@"bank_charges"]];
                  _trans_shipment.text=[NSString stringWithFormat:@"%@",[[dict valueForKey:@"credit"]valueForKey:@"transhipment"]];
                 _partshipment.text=[NSString stringWithFormat:@"%@",[[dict valueForKey:@"credit"]valueForKey:@"partshipment"]];
                 _shipment_from.text=[NSString stringWithFormat:@"%@",[[dict valueForKey:@"credit"]valueForKey:@"shipment_from"]];
                 _shipment_to.text=[NSString stringWithFormat:@"%@",[[dict valueForKey:@"credit"]valueForKey:@"shipment_to"]];
                 _documents_req.text=[NSString stringWithFormat:@"%@",[[dict valueForKey:@"credit"]valueForKey:@"documents_required"]];
             }
             else
             {
                 
                 
             }
             
         }
         else
         {
             
         }
         
         
     }];
}

- (IBAction)Check_click_LC:(id)sender
{
    if (check_tag==NO)
    {
        check_tag=YES;
        [_LC_check setImage:[UIImage imageNamed:@"Radio_select1"] forState:UIControlStateNormal];
    }
    else
    {
        check_tag=NO;
        [_LC_check setImage:[UIImage imageNamed:@"Radio_Deselect1"] forState:UIControlStateNormal];

    }
}

- (IBAction)onlinePayBtnAction:(id)sender
{
    [_onlinePayBtn setImage:[UIImage imageNamed:@"Radio_select"] forState:UIControlStateNormal];
    [_cashOnDeliveryBtn setImage:[UIImage imageNamed:@"Radio_Deselect"] forState:UIControlStateNormal];
    [_credit_btn setImage:[UIImage imageNamed:@"Radio_Deselect"] forState:UIControlStateNormal];
    _view_1.hidden=YES;
    _credit_height_constraint.constant=0;

    self.payment_subview.hidden = YES;
    paymentTypeIs = @"1";
}

- (IBAction)cashOnDelivryBtnAction:(id)sender
{
    if ([_methodType isEqualToString:@"1"]) {
        
    }
    else
    {
    self.payment_subview.hidden = false;
    paymentTypeIs = @"21";
    }
    
    [_onlinePayBtn setImage:[UIImage imageNamed:@"Radio_Deselect"] forState:UIControlStateNormal];
    [_cashOnDeliveryBtn setImage:[UIImage imageNamed:@"Radio_select"] forState:UIControlStateNormal];
    [_credit_btn setImage:[UIImage imageNamed:@"Radio_Deselect"] forState:UIControlStateNormal];
    [_sub_OnlinePayBtn setImage:[UIImage imageNamed:@"Radio_select"] forState:UIControlStateNormal];
    [_sub_OflinePayBtn setImage:[UIImage imageNamed:@"Radio_Deselect"] forState:UIControlStateNormal];

    _view_1.hidden=YES;

    _credit_height_constraint.constant=0;

}

- (IBAction)sub_OnlinePayBtnAction:(id)sender {
    paymentTypeIs = @"21";
    
    [_onlinePayBtn setImage:[UIImage imageNamed:@"Radio_Deselect"] forState:UIControlStateNormal];
    [_cashOnDeliveryBtn setImage:[UIImage imageNamed:@"Radio_select"] forState:UIControlStateNormal];
    
    [_sub_OnlinePayBtn setImage:[UIImage imageNamed:@"Radio_select"] forState:UIControlStateNormal];
    [_sub_OflinePayBtn setImage:[UIImage imageNamed:@"Radio_Deselect"] forState:UIControlStateNormal];
    [_payment_refenceBtn setImage:[UIImage imageNamed:@"Radio_Deselect"] forState:UIControlStateNormal];
    _payment_refenceTFT.userInteractionEnabled = NO;

    
}

- (IBAction)sub_offlinePayBtnAction:(id)sender
{
    paymentTypeIs = @"22";
    [_onlinePayBtn setImage:[UIImage imageNamed:@"Radio_Deselect"] forState:UIControlStateNormal];
    [_cashOnDeliveryBtn setImage:[UIImage imageNamed:@"Radio_select"] forState:UIControlStateNormal];
    
    [_sub_OnlinePayBtn setImage:[UIImage imageNamed:@"Radio_Deselect"] forState:UIControlStateNormal];
    [_sub_OflinePayBtn setImage:[UIImage imageNamed:@"Radio_select"] forState:UIControlStateNormal];
    [_payment_refenceBtn setImage:[UIImage imageNamed:@"Radio_Deselect"] forState:UIControlStateNormal];
    _payment_refenceTFT.userInteractionEnabled = NO;

}

- (IBAction)payment_RefenceAction:(id)sender {
    
    
    paymentTypeIs = @"23";
    [_onlinePayBtn setImage:[UIImage imageNamed:@"Radio_select"] forState:UIControlStateNormal];
    [_cashOnDeliveryBtn setImage:[UIImage imageNamed:@"Radio_select"] forState:UIControlStateNormal];
    [_sub_OnlinePayBtn setImage:[UIImage imageNamed:@"Radio_Deselect"] forState:UIControlStateNormal];
    [_sub_OflinePayBtn setImage:[UIImage imageNamed:@"Radio_Deselect"] forState:UIControlStateNormal];
    [_payment_refenceBtn setImage:[UIImage imageNamed:@"Radio_select"] forState:UIControlStateNormal];
    _payment_refenceTFT.userInteractionEnabled = YES;
    
}



//
//else if (rb_cod_online.isChecked()) {
//    RequestParams requestParams = new RequestParams();
//    requestParams.put("customer_id", userId);
//    requestParams.put("method", "cashondelivery");
//    requestParams.put("submethod", "online");
//    new WebserviceHelper(this).postData(getString(R.string.base_url) + "calculate/service/savePaymentMethod", requestParams, "savePaymentMethod", this);
//    
//} else if (rb_cod_offline.isChecked()) {
//    RequestParams requestParams = new RequestParams();
//    requestParams.put("customer_id", userId);
//    requestParams.put("method", "cashondelivery");
//    requestParams.put("submethod", "offline");
//    new WebserviceHelper(this).postData(getString(R.string.base_url) + "calculate/service/savePaymentMethod", requestParams, "savePaymentMethod", this);
//    
//}
- (IBAction)makePaymentBtnActn:(id)sender
{
    if ([paymentTypeIs isEqualToString:@"1"])
    {
        subMethod = @"online";
        [self payment_service:@"ccavenue"];
    }
    else if ([paymentTypeIs isEqualToString:@"21"])
    {
        subMethod = @"online";

        [self payment_service:@"cashondelivery"];
           }
    else if ([paymentTypeIs isEqualToString:@"22"])
    {
        subMethod = @"offline";
        [self payment_service:@"cashondelivery"];
        
    }
    else if ([paymentTypeIs isEqualToString:@"23"])
    {
        subMethod = @"offline";
        [self payment_service:@"cashondelivery"];
        
    }
    else if ([paymentTypeIs isEqualToString:@"3"])//LC
    {
        if (check_tag==NO)
        {
            ALERT_DIALOG(@"ALert", @"Please Check the Terms & Conditions!");
        }
        else if ([_issuing_bank.text isEqualToString:@""]||[_issuing_bank.text isEqual:[NSNull null]])
        {
            ALERT_DIALOG(@"ALert", @"Issuing Bank Should not be empty!");
        }
        else if ([_adv_bank.text isEqualToString:@""]||[_adv_bank.text isEqual:[NSNull null]])
        {
            ALERT_DIALOG(@"ALert", @"Advising Bank Should not be empty!");
        }
        else
            [self payment_service:@"credit"];
    }
    else
    {
        [self payment_service:@"cashondelivery"];//newwww
    }
    
    
    
    
    
}

-(void)payment_service:(NSString *)Method
{
//    user_data=[NSUserDefaults standardUserDefaults];
//    [user_data setValue:@"0" forKey:@"cart_count"];
    
    user_data=[NSUserDefaults standardUserDefaults];
    user_id=[user_data valueForKey:@"user_id"];
    NSDictionary *params;
    if ([paymentTypeIs isEqualToString:@"3"])//LC
    {
        params = @{@"customer_id":user_id,@"method":Method,
                   @"negotiating_bank":_neg_bank.text,@"expiry_date":[NSString stringWithFormat:@"%@",[[dict valueForKey:@"credit"]valueForKey:@"expiry_date"]],
                   @"buyer_name":_buyer_name.text,@"shipment_to":_shipment_to.text,
                   @"submethod":@"LC",@"transhipment":_trans_shipment.text,
                   @"credit_available_bank":_credit_availbale.text,@"advising_bank":_adv_bank.text,
                   @"bank_charges":_bank_charges.text,@"amount_lc":[NSString stringWithFormat:@"%@",[[dict valueForKey:@"credit"]valueForKey:@"amount_lc"]],
                   @"shipment_from":_shipment_from.text,@"issuing_bank":_issuing_bank.text,
                   @"documents_required":_documents_req.text,@"type_lc":_type_lc.text,
                   @"benefiiary_name":_ben_name.text,@"partshipment":_partshipment.text
                   
                   };
    }
    else if ([paymentTypeIs isEqualToString:@"23"])//payment Refence NO
    {
        
        if ([_payment_refenceTFT.text  isEqual: @""]){
          
            ALERT_DIALOG(@"ALert", @"Payment Refernce No Should not be empty!");
        }else
        {
          params = @{@"customer_id":user_id,@"method":Method,@"submethod":subMethod,@"tranx_number":_payment_refenceTFT.text};
        }
        
        
       
    }
    
    
    else
    {
        params = @{@"customer_id":user_id,@"method":Method,@"submethod":subMethod};
    }
   
    [[STParsing sharedWebServiceHelper]requesting_POST_ServiceWithString1:@"savePaymentMethod?" parameters:params requestNumber:Save_Payment showProgress:YES withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             Payment_ProductDetailsViewController *payment_ProductDetailsViewController = [[Payment_ProductDetailsViewController alloc] initWithNibName:@"Payment_ProductDetailsViewController" bundle:nil];
             payment_ProductDetailsViewController.payType = paymentTypeIs;//
             payment_ProductDetailsViewController.shippingaddress =_shippingAddress;
             [self.navigationController pushViewController:payment_ProductDetailsViewController animated:YES];
             
         }
         else
         {
             ALERT_DIALOG(@"ALert", @"Something went wrong Please try again");
         }
     }];
    
//    NSString *url_form=[NSString stringWithFormat:@"savePaymentMethod?customer_id=%@&method=%@",user_id,Method];
//    NSString* urlEncoded = [url_form stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    [[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString:urlEncoded requestNumber:Save_Payment showProgress:YES withHandler:^(BOOL success, id data)
//     {
//         if (success)
//         {
//            Payment_ProductDetailsViewController *payment_ProductDetailsViewController = [[Payment_ProductDetailsViewController alloc] initWithNibName:@"Payment_ProductDetailsViewController" bundle:nil];
//            payment_ProductDetailsViewController.payType = paymentTypeIs;//
//             payment_ProductDetailsViewController.shippingaddress =_shippingAddress;
//                 [self.navigationController pushViewController:payment_ProductDetailsViewController animated:YES];
//         }
//         else
//         {
//             
//         }
//     }];
}

- (IBAction)backBtnActn:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)lc_click:(id)sender
{
    
}

- (IBAction)bg_click:(id)sender {
}

- (IBAction)goToHomeScreenAction:(id)sender {
    HomeViewController *con = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
    [self.navigationController pushViewController:con animated:YES];
    
    
}

@end
