//
//  OP_Cell0.h
//  Steeloncall_Seller
//
//  Created by INDOBYTES on 09/01/17.
//  Copyright © 2017 sai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OP_Cell0 : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *order__id;
@property (strong, nonatomic) IBOutlet UILabel *order_status;

@end
