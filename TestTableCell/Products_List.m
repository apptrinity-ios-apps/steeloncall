//
//  Products_List.m
//  SteelonCall
//
//  Created by Manoyadav on 01/12/16.
//  Copyright © 2016 com.way2online. All rights reserved.

#import "Products_List.h"
#import "searchPopupVIew.h"
#import "NewLoginVC.h"
#import "CategoryVC.h"
@interface Products_List ()
{
    NSString *C_Count;
    NSString *edited;
     searchPopupVIew *searchView;
      NSMutableArray *mainSearchArry;
    NSMutableArray *searchItemsArray;
    NSMutableArray *searchResultArray;
     NSDictionary *selectedDic;
}
@end

@implementation Products_List
-(void)viewWillAppear:(BOOL)animated
{
    AppDelegate *del = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [del setShouldRotate:NO];
    
    self.navigationController.navigationBar.hidden = YES;
    
   user_data=[NSUserDefaults standardUserDefaults];
    user_id=[NSString stringWithFormat:@"%@",[user_data valueForKey:@"user_id"]];
   
   C_Count=[user_data valueForKey:@"cart_count"];
    if ([C_Count isEqual:[NSNull null]]||[C_Count isEqualToString:@""]||C_Count ==nil||[C_Count isEqualToString:@"<nil>"]||[C_Count isEqualToString:@"null"]||[C_Count isEqualToString:@"<null>"]||[C_Count isEqualToString:@"(null)"]) {
        C_Count = @"0";
    }
    _Cart_lbl.text=C_Count;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
//    _kgTexfield.delegate=self;
//    _tonsTextfield.delegate=self;
    [self kartCntService:user_id];
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    NSString *Home_tag_str=[NSString stringWithFormat:@"%ld",(long)textField.tag];
    if (textField.tag == 100)
    {
        
        
    } else if (textField.tag == 150)
        
    {
    } else
    {
        if ([Home_tag_str isEqualToString:@"88888"])//pin
        {
        }else if ([Home_tag_str isEqualToString:@"99999"])//city
        {
            
        }else if ([Home_tag_str isEqualToString:@"252"])//city
        {
            
        }else{
            
            NSString *TF_check = [NSString stringWithFormat:@"%c", [Home_tag_str characterAtIndex:0]];
            
            if ([TF_check isEqualToString:@"1"])//tonnes
            {
                
                 NSInteger tagg = [Home_tag_str integerValue] - 10;
                NSIndexPath *indexpath = [NSIndexPath indexPathForRow:tagg inSection:0];
                
                List_cell *tappedCell = [_list_collection cellForItemAtIndexPath:indexpath];
                
                
                
                if([tappedCell.tonnes.text  isEqual: @""]){
                    
                    if ([Selected_ids_ary containsObject:[[Product_List_Dict valueForKey:@"id"] objectAtIndex:(long)tagg]]) {
                        [tones_dict removeObjectForKey:[NSString stringWithFormat:@"%ld", (long)tagg]];
                        [pieces_dict removeObjectForKey:[NSString stringWithFormat:@"%ld", (long)tagg]];
                        [Selected_ids_ary removeObject:[[Product_List_Dict valueForKey:@"id"] objectAtIndex:(long)tagg]];
                    }else{
                       
                    }
                    
                }else{
                    if ([Selected_ids_ary containsObject:[[Product_List_Dict valueForKey:@"id"] objectAtIndex:(long)tagg]]) {
                        
                        
                    }else{
                        [Selected_ids_ary addObject:[[Product_List_Dict valueForKey:@"id"] objectAtIndex:(long)tagg]];
                    }
                    
                }
              
            }else{
                NSInteger tagg = [Home_tag_str integerValue] - 20;
                NSIndexPath *indexpath = [NSIndexPath indexPathForRow:tagg inSection:0];
                
                List_cell *tappedCell = [_list_collection cellForItemAtIndexPath:indexpath];
                
                
                
                if([tappedCell.pieces.text  isEqual: @""]){
                    if ([Selected_ids_ary containsObject:[[Product_List_Dict valueForKey:@"id"] objectAtIndex:(long)tagg]]) {
                        [tones_dict removeObjectForKey:[NSString stringWithFormat:@"%ld", (long)tagg]];
                        [pieces_dict removeObjectForKey:[NSString stringWithFormat:@"%ld", (long)tagg]];
                        [Selected_ids_ary removeObject:[[Product_List_Dict valueForKey:@"id"] objectAtIndex:(long)tagg]];
                    }else{
                       
                    }
                }else{
                    if ([Selected_ids_ary containsObject:[[Product_List_Dict valueForKey:@"id"] objectAtIndex:(long)tagg]]) {
                        
                        
                    }else{
                        [Selected_ids_ary addObject:[[Product_List_Dict valueForKey:@"id"] objectAtIndex:(long)tagg]];
                    }
                }
            }
                //  [tick_check addObject:[NSString stringWithFormat:@"%ld", (long)tagg]];
            
        }
            
            
        
    }
    
    
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if(textField.tag == 100 || textField.tag == 150){
      //  _popup_top.constant = -250;
        //_proceedottomConstriant.constant = 0;
        
    }else{
        _conversionView.hidden = YES;
       
      //  _popup_top.constant = 20;
    }
    
    
}
-(void)searchItems:(UITextField *)textField{
    if(textField.text.length>0)
    {
        searchView.hidden =NO;
        if (mainSearchArry.count==0) {
            
            [self searchWebserviceCall];
            
        }else{
            
            NSPredicate *pred  = [NSPredicate predicateWithFormat:@"(name CONTAINS[c] %@)",textField.text];
            searchResultArray =[NSMutableArray arrayWithArray:[mainSearchArry filteredArrayUsingPredicate:pred]];
            
            if (searchResultArray.count==0) {
                // searchResultArray = mainSearchArry;
                
            }
            searchItemsArray = searchResultArray;
            [searchView relaodSearchTable];
            
            
        }
        
        
        
    }else{
        //  [searchItemsArray removeAllObjects];
        
        selectedDic = [[NSDictionary alloc]init];
        searchView.hidden =YES;
    }
}
-(void)searchTableViewSelected:(UITableView *)table IndexPath:(NSIndexPath * )indexPath
{
    selectedDic = [[NSDictionary alloc]init];
    selectedDic =[searchItemsArray objectAtIndex:indexPath.row];
    _searchTextfield.text =[selectedDic valueForKey:@"name"];
    searchView.hidden =YES;
}
- (IBAction)searchProductActionClicked:(id)sender {
    
    _searchTextfield.text= [_searchTextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (_searchTextfield.text.length == 0) {
        
        ALERT_DIALOG(@"alert",@"Please enter item name ");
    }
    else
    {
        if(selectedDic)
        {
          
            _product_id=[selectedDic valueForKey:@"id"];
            _product_type=[selectedDic valueForKey:@"name"];
            _type=[selectedDic valueForKey:@"type"];
            _from=@"search";
            
            
           
            _searchTextfield.text =@"";
            selectedDic = nil;
            searchView.hidden =YES;
              [self Call_Products_List_Service];
            
        }
        else
        {
            ALERT_DIALOG(@"alert",@"Please select an item ");
        }
    }
}
-(float)setHeighForSearchTable:(UITableView *)tableView{
    
    return 40;
}

-(NSInteger )numberOffRowsInSearchTableView:(UITableView *)tableView
{
    return searchItemsArray.count;
}

- (JMOTableViewCell *)cellforRowAtSearchINdex:(UITableView*)tableVIew viewAtIndex:(NSIndexPath *)index
{
    JMOTableViewCell *cell = [tableVIew dequeueReusableCellWithIdentifier:@"searchCell"];
    if (cell==nil)
    {
        [tableVIew registerNib:[UINib nibWithNibName:@"JMOTableViewCell" bundle:nil] forCellReuseIdentifier:@"searchCell"];
        cell = [tableVIew dequeueReusableCellWithIdentifier:@"searchCell"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.labelName.text =[[searchItemsArray objectAtIndex:index.row] valueForKey:@"name"];
    
    cell.contentView.backgroundColor = [UIColor colorWithRed:10/255.0f green:21/255.0f blue:80/255.0f alpha:1.0];
    cell.textLabel.backgroundColor = [UIColor colorWithRed:10/255.0f green:21/255.0f blue:80/255.0f alpha:1.0];    return cell;
}

-(void)searchWebserviceCall
{
    [[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString:@"search" requestNumber:WS_Search showProgress:YES withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             if (data) {
                 
                 searchItemsArray = [[NSMutableArray alloc]init];
                 mainSearchArry = [[NSMutableArray alloc]init];
                 [searchItemsArray  addObjectsFromArray:data];
                 [ mainSearchArry addObjectsFromArray:data];
                 
                 [searchView relaodSearchTable];
                 //archive
                 
             }
             
         }
         
         
     }];
    
}
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}

-(void)kartCntService:(NSString *)userId
{
    NSDictionary *params = @{@"customer_id":userId};
    
    [[STParsing sharedWebServiceHelper]requesting_POST_ServiceWithString1:@"getCartCountAfterLogin" parameters:params requestNumber:WS_Login showProgress:YES withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             NSDictionary *res_dict=data;
             NSString *cartCount=[NSString stringWithFormat:@"%@",[[[res_dict valueForKey:@"cart"] objectAtIndex:0] valueForKey:@"cart_count"]];
            prev_location=[NSString stringWithFormat:@"%@",[[[res_dict valueForKey:@"cart"] objectAtIndex:0] valueForKey:@"location"]];
            prev_pincode=[NSString stringWithFormat:@"%@",[[[res_dict valueForKey:@"cart"] objectAtIndex:0] valueForKey:@"pincode"]];

             if (cartCount == nil||[cartCount isEqualToString:@"<null>"])
             {
                 cartCount =@"0";
             }
             
             [user_data setValue:cartCount forKey:@"cart_count"];
         }
         else
         {
             [self Alert:@"Something went wrong"];
         }
     }];
}

-(void)viewDidAppear:(BOOL)animated
{
    tableHeight = _list_view.frame.size.height;
    self.navigationController.navigationBar.hidden = YES;
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
    {
        searchView = [[searchPopupVIew alloc]initWithFrame:CGRectMake(10,_searchTextfield.frame.origin.y+40 ,[UIScreen mainScreen].bounds.size.width-20,400)];
        
    }
    else
    {
        if ([UIScreen mainScreen].bounds.size.width == 320) {
            searchView = [[searchPopupVIew alloc]initWithFrame:CGRectMake(10,_searchTextfield.frame.origin.y+40 ,[UIScreen mainScreen].bounds.size.width-20,200)];
        }else{
            searchView = [[searchPopupVIew alloc]initWithFrame:CGRectMake(10,_searchTextfield.frame.origin.y+40 ,[UIScreen mainScreen].bounds.size.width-20,260)];
        }
    }
    
    searchView.delegate =self;
    searchView.hidden =YES;
    
    [self.view addSubview:searchView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    
    
    _pincode_tf.layer.sublayerTransform = CATransform3DMakeTranslation(20, 0, 0);
    
     _city_tf.layer.sublayerTransform = CATransform3DMakeTranslation(20, 0, 0);
    
     _searchTextfield.layer.sublayerTransform = CATransform3DMakeTranslation(20, 0, 0);
    _pincode_tf.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter Your Pincode" attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    _city_tf.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter Your City" attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    
    _location_table.layer.shadowRadius  = 1.5f;
    _location_table.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    _location_table.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    _location_table.layer.shadowOpacity = 0.9f;
    _location_table.layer.cornerRadius = 12;
    _location_table.layer.masksToBounds = NO;
    
    
    
//    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
//    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(_location_table.bounds, shadowInsets)];
//    _location_table.layer.shadowPath    = shadowPath.CGPath;
    
    
//    _pincode_tf.leftView = paddingView;
//    _pincode_tf.leftViewMode = UITextFieldViewModeAlways;
//
//    _city_tf.leftView = paddingView;
//    _city_tf.leftViewMode = UITextFieldViewModeAlways;
    
    
    
    self.pincode_tf.clipsToBounds = YES;
    self.pincode_tf.layer.cornerRadius = 23;
    _popup_view.layer.cornerRadius = 10;
    
    self.city_tf.clipsToBounds = YES;
    self.city_tf.layer.cornerRadius = 23;
    self.proceedBtn.layer.cornerRadius = 23;
    
    [_searchTextfield addTarget:self action:@selector(searchItems:) forControlEvents:UIControlEventEditingChanged];
   
    _searchTextfield.leftView = paddingView;
    _searchTextfield.leftViewMode = UITextFieldViewModeAlways;
    searchItemsArray = [[NSMutableArray alloc]init];
    searchResultArray = [[NSMutableArray alloc]init];
    mainSearchArry = [[NSMutableArray alloc]init];
   // _searchTextfield.delegate  = self;
    _productHeadingLabel.layer.cornerRadius = _productHeadingLabel.frame.size.height/2;
    _productHeadingLabel.clipsToBounds = YES;
    
    _categoriesLabel.layer.cornerRadius = _categoriesLabel.frame.size.height/2;
    _categoriesLabel.clipsToBounds = YES;
    
    _searchTextfield.layer.cornerRadius = _searchTextfield.frame.size.height/2;
    _searchTextfield.clipsToBounds = YES;
    self.conversionView.hidden = true;
    _location_table.dataSource = self;
    _location_table.delegate = self;
    _selectLabel.hidden = YES;
    _location_table.hidden=YES;
    self.list_view.userInteractionEnabled=YES;
    self.navigationController.navigationBar.hidden = YES;
   
    _tonsTextfield.delegate = self;
    _kgTexfield.delegate = self;
    _tonsTextfield.tag = 100;
    _kgTexfield.tag = 150;
    
    UIToolbar *keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStyleBordered target:self
                                                                  action:@selector(kg_doneClicked:)];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    _kgTexfield.inputAccessoryView = keyboardDoneButtonView;
    _tonsTextfield.inputAccessoryView = keyboardDoneButtonView;
    
    _location_table.allowsSelection=YES;

    tick_check=[[NSMutableArray alloc]init];
    tones_dict=[[NSMutableDictionary alloc]init];
    pieces_dict=[[NSMutableDictionary alloc]init];
    Product_List_Dict=[[NSDictionary alloc]init];
    pieces_per_ton_ary=[[NSMutableArray alloc]init];
    Selected_ids_ary=[[NSMutableArray alloc]init];
    tonnes_pieces_ary=[[NSMutableArray alloc]init];
    product_names=[[NSMutableArray alloc]init];
    [_pincode_tf setDelegate:self];
    [_city_tf setDelegate:self];

    _popup_view.layer.cornerRadius=8;
    table_tag=@"products";
    keyboardIsShown=NO;
    
    UINib *cellNib = [UINib nibWithNibName:@"List_cell" bundle:nil];

    [self.list_collection registerNib:cellNib forCellWithReuseIdentifier:@"List_cell"];

    
    //tap gesture
    singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.list_collection addGestureRecognizer:singleFingerTap];
    
    _list_view.separatorStyle = UITableViewCellSeparatorStyleNone;
    for (int t=0; t<Product_List_Dict.count; t++)
    {
        [tones_dict setObject:@"" forKey:[NSString stringWithFormat:@"%d",t]];
        [pieces_dict setObject:@"" forKey:[NSString stringWithFormat:@"%d",t]];
    }
    [self Call_Products_List_Service];
}

-(void)call_tonnes_service
{
   // NSString *url_form=[NSString stringWithFormat:@"https://steeloncall.com/getcalculateqty?ids=%@",product_ids_str];
    
     NSString *url_form=[NSString stringWithFormat:@"http://stg.steeloncall.com/getcalculateqty?ids=%@",product_ids_str];
    
     NSString* urlEncoded = [url_form stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString: urlEncoded requestNumber:PRODUCT_TONNES showProgress:YES withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             NSLog(@" response %@",data);
             NSDictionary *res_data=data;
             for (int p=0; p<res_data.count; p++)
             {
                 NSString *pieces=[[res_data valueForKey:@"pieces"]objectAtIndex:p];
                 if ([pieces isEqual:[NSNull null]]||[pieces isEqualToString:@""])
                 {
                     [pieces_per_ton_ary addObject:@"0"];
                 }
                 else
                 [pieces_per_ton_ary addObject:pieces];
             }
        }
         else
         {
         }
     }];
}

-(void)Call_Products_List_Service
{
    NSString *url_form;
    if ([_type isEqualToString:@"product"])
    {
         url_form=[NSString stringWithFormat:@"getsearchresults?id=%@&type=%@",_product_id,_type];
    }
    else
    {
      url_form=[NSString stringWithFormat:@"getcategory/id/%@",_product_id];
    }
    if ([_product_id isEqualToString:@"18"]) {
      //  self.listViewTopHeight.constant = 55;
        
        _conversionView.hidden = false;
    }
    else
    {
        self.listViewTopHeight.constant = 0;
    }
    NSString* urlEncoded = [url_form stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString:urlEncoded requestNumber:PRODUCT_LIST showProgress:YES withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             NSLog(@" response %@",data);
             Product_List_Dict=data;
             // [_list_view reloadData];
             
             _list_collection.delegate=self;
             _list_collection.dataSource=self;
             
             [_list_collection reloadData];
             
             for (int l=0; l<Product_List_Dict.count; l++)
             {
                 if (l==0)
                 {
                     product_ids_str=[[Product_List_Dict valueForKey:@"id"]objectAtIndex:l];
                 }
                 else
                 product_ids_str=[NSString stringWithFormat:@"%@_%@",product_ids_str,[[Product_List_Dict valueForKey:@"id"]objectAtIndex:l]];
             }
             NSLog(@"ids string %@",product_ids_str);
             
             [self call_tonnes_service];
             
             
         }
         else
         {
//             UIAlertController * alert=   [UIAlertController
//                                           alertControllerWithTitle:@"Alert"
//                                           message:[NSString stringWithFormat:@"%@",[data valueForKey:@"error_message"]]
//                                           preferredStyle:UIAlertControllerStyleAlert];
//             
//             UIAlertAction* ok = [UIAlertAction
//                                  actionWithTitle:@"OK"
//                                  style:UIAlertActionStyleDefault
//                                  handler:^(UIAlertAction * action)
//                                  {
//                                      [alert dismissViewControllerAnimated:YES completion:nil];
//                                  }];
//             [alert addAction:ok];
//             [self presentViewController:alert animated:YES completion:nil];
         }
     }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == _list_view)
    {
        return Product_List_Dict.count;
    }
    else
        return locations_array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _list_view)
    {
    static NSString *simpleTableIdentifier = @"Product_list_cell";
    cell = (Product_list_cell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"Product_list_cell" owner:self options:nil];
        cell = (Product_list_cell *)[nib objectAtIndex:0];
    }
    cell.checkbox.tag = indexPath.row;
        
    NSString *Home_tag=[NSString stringWithFormat:@"1%ld",(long)indexPath.row];
    int Home_tag_int=[Home_tag intValue];
    NSString *Away_tag=[NSString stringWithFormat:@"2%ld",(long)indexPath.row];
    int Away_tag_int=[Away_tag intValue];
    
    cell.tonnes.tag = Home_tag_int;
    cell.pieces.tag = Away_tag_int;
        
    cell.tonnes.delegate = self;
    cell.pieces.delegate = self;
        
        UIToolbar *keyboardDoneButtonView = [[UIToolbar alloc] init];
        [keyboardDoneButtonView sizeToFit];
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                       style:UIBarButtonItemStyleBordered target:self
                                                                      action:@selector(doneClicked:)];
        [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
        cell.tonnes.inputAccessoryView = keyboardDoneButtonView;
        cell.pieces.inputAccessoryView = keyboardDoneButtonView;

  //  UIColor *color = [UIColor grayColor];
    //    cell.tonnes.attributedPlaceholder =
     //   [[NSAttributedString alloc] initWithString:@"Enter Ton"
       //                                 attributes:@{
       //                                              NSForegroundColorAttributeName: color,
       //                                              NSFontAttributeName : [UIFont fontWithName:@"Roboto" size:12.0]
       //                                              }
       //  ];
      //  cell.pieces.attributedPlaceholder =
      //  [[NSAttributedString alloc] initWithString:@"Enter Pieces"
      //                                  attributes:@{
     //                                                NSForegroundColorAttributeName: color,
     //                                                NSFontAttributeName : [UIFont fontWithName:@"Roboto" //size:12.0]
          //                                           }
       //  ];

    [cell.checkbox addTarget:self action:@selector(Check_Click:) forControlEvents:UIControlEventTouchUpInside];
    cell.tonnes.text=@"";
    cell.pieces.text=@"";
    
//    if ([tick_check containsObject:[NSString stringWithFormat:@"%ld", (long)indexPath.row]])
//    {
//        [cell.checkbox setImage:[UIImage imageNamed:@"Check_Select"] forState:UIControlStateNormal];
//        cell.tonnes.enabled=YES;
//        cell.pieces.enabled=YES;
//    }
//    else
//    {
//        [cell.checkbox setImage:[UIImage imageNamed:@"Check_Deselect"] forState:UIControlStateNormal];
//        cell.tonnes.enabled=NO;
//        cell.pieces.enabled=NO;
//    }
        
    cell.tonnes.text=[tones_dict valueForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
    cell.pieces.text=[pieces_dict valueForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
    
    cell.name.text=[[Product_List_Dict valueForKey:@"name"]objectAtIndex:indexPath.row];
    cell.price.text=[NSString stringWithFormat:@"From Rs. %@", [[Product_List_Dict valueForKey:@"price"]objectAtIndex:indexPath.row]];

        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        NSURL *imageURL = [NSURL URLWithString:[[Product_List_Dict valueForKey:@"img"]objectAtIndex:indexPath.row]];
        
        [manager downloadImageWithURL: imageURL options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize)
         {
         } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
             
             if(image)
             {
                    cell.product_img.image = image;
             }
         }];

    return cell;
    }
    else
    {
            static NSString *CellIdentifier = @"Cell";
            
            UITableViewCell *cell_loc = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell_loc == nil)
            {
                cell_loc = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                cell_loc.textLabel.font = [UIFont systemFontOfSize:13];
                cell_loc.textLabel.textAlignment = UITextAlignmentCenter;
            }
            cell_loc.textLabel.textColor = [UIColor blackColor];
        NSString *city=[[locations_array objectAtIndex:indexPath.row] valueForKey:@"city"];
        NSString *Dist=[[locations_array objectAtIndex:indexPath.row] valueForKey:@"district"];
        
            cell_loc.textLabel.text=[NSString stringWithFormat:@"%@-%@",city,Dist];
        _selectLabel.hidden = NO;
            _location_table.hidden=NO;
            [self.view bringSubviewToFront:_location_table];
            return cell_loc;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([table_tag isEqualToString:@"products"])
    {
    return 200;
    }
    else
        return 40;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([table_tag isEqualToString:@"Location"])
    {
        edited = @"no";
       Product_list_cell *tappedCell = (Product_list_cell *)[_location_table cellForRowAtIndexPath:indexPath];
        NSLog(@"location clicked");
        _selectLabel.hidden = YES;
        _location_table.hidden=YES;
        _city_tf.text=[NSString stringWithFormat:@"%@",tappedCell.textLabel.text];
        _pincode_tf.text=[NSString stringWithFormat:@"%@",[[locations_array objectAtIndex:indexPath.row]valueForKey:@"pincode"]];
        [self.view endEditing:YES];
    }
}
#pragma mark collection view
#pragma collection view delegate methods
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [Product_List_Dict count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
   // List_cell * coll_cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"List_cell" forIndexPath:indexPath];
    static NSString *identifier = @"List_cell";
    List_cell *coll_cell = (List_cell*) [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    coll_cell.layer.shadowRadius  = 0.5f;
    coll_cell.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    coll_cell.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    coll_cell.layer.shadowOpacity = 0.3f;
    coll_cell.bgView.layer.cornerRadius = 3;
    coll_cell.layer.masksToBounds = NO;

    coll_cell.checkbox.tag = indexPath.row;
    
    NSString *Home_tag=[NSString stringWithFormat:@"1%ld",(long)indexPath.row];
    int Home_tag_int=[Home_tag intValue];
    NSString *Away_tag=[NSString stringWithFormat:@"2%ld",(long)indexPath.row];
    int Away_tag_int=[Away_tag intValue];
    coll_cell.tonnes.tag = Home_tag_int;
    coll_cell.pieces.tag = Away_tag_int;
    coll_cell.tonnes.delegate = self;
    coll_cell.pieces.delegate = self;
//    UIToolbar *keyboardDoneButtonView = [[UIToolbar alloc] init];
//    [keyboardDoneButtonView sizeToFit];
    
   // keyboardDoneButtonView.tintColor = [UIColor redColor];
   // UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
//                                                                   style:UIBarButtonItemStyleBordered target:self
//                                                                  action:@selector(doneClicked:)];
    
   // UIButton *doneBtn ;
    //  [doneBtn setBackgroundColor:[UIColor colorWithRed:7/255.0 green:21/255.0 blue:82/255.0 alpha:1.0]];
    
   // [doneBtn setTitle:@"Proceed" forState:UIControlStateNormal];
   // [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneBtn, nil]];
    
    [ coll_cell.tonnes addTarget:self
                  action:@selector(editingChanged:)
        forControlEvents:UIControlEventEditingChanged];
//    coll_cell.tonnes.inputAccessoryView = keyboardDoneButtonView;
//    coll_cell.pieces.inputAccessoryView = keyboardDoneButtonView;
            coll_cell.tonnes.enabled=YES;
            coll_cell.pieces.enabled=YES;
    UIColor *color = [UIColor grayColor];
    //coll_cell.tonnes.attributedPlaceholder =
//    [[NSAttributedString alloc] initWithString:@"Enter Ton"
//                                    attributes:@{
//                                                 NSForegroundColorAttributeName: color,
//                                                 NSFontAttributeName : [UIFont fontWithName:@"Arial" size:11.0]
//                                                 }
//     ];
//    coll_cell.pieces.attributedPlaceholder =
//    [[NSAttributedString alloc] initWithString:@"Enter Pieces"
//                                    attributes:@{
//                                                 NSForegroundColorAttributeName: color,
//                                                 NSFontAttributeName : [UIFont fontWithName:@"Arial" size:11.0]
//                                                 }
//     ];
    //coll_cell.pieces.tag = indexPath.item;
    [coll_cell.checkbox addTarget:self action:@selector(Check_Click:) forControlEvents:UIControlEventTouchUpInside];
    coll_cell.tonnes.text=@"";
    coll_cell.pieces.text=@"";
    
    
    
//    if ([tick_check containsObject:[NSString stringWithFormat:@"%ld", (long)indexPath.row]])
//    {
//        [coll_cell.checkbox setImage:[UIImage imageNamed:@"Check_Select"] forState:UIControlStateNormal];
//        coll_cell.tonnes.enabled=YES;
//        coll_cell.pieces.enabled=YES;
//    }
//    else
//    {
//        [coll_cell.checkbox setImage:[UIImage imageNamed:@"Check_Deselect"] forState:UIControlStateNormal];
//        coll_cell.tonnes.enabled=NO;
//        coll_cell.pieces.enabled=NO;
//    }
    coll_cell.pieces.keyboardType=UIKeyboardTypePhonePad;
    
    coll_cell.tonnes.text=[tones_dict valueForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
    coll_cell.pieces.text=[pieces_dict valueForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
    
    coll_cell.name.text=[[Product_List_Dict valueForKey:@"name"]objectAtIndex:indexPath.row];
    coll_cell.price.text=[NSString stringWithFormat:@"₹ %@", [[Product_List_Dict valueForKey:@"price"]objectAtIndex:indexPath.row]];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    NSURL *imageURL = [NSURL URLWithString:[[Product_List_Dict valueForKey:@"img"]objectAtIndex:indexPath.row]];
    
    [manager downloadImageWithURL: imageURL options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize)
     {
     } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
         
         if(image)
         {
             coll_cell.product_img.image = image;
         }
     }];

    return coll_cell;
}

-(void) editingChanged:(id)sender {
    // your code
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger width = collectionView.frame.size.width-16;
    return CGSizeMake((width/2), 230);
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 5, 0, 0); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 10;
}

-(NSString *)proceed_validation
{
    for (int c=0; c<Selected_ids_ary.count; c++)
    {
        int index_int=[[Selected_ids_ary objectAtIndex:c]intValue];
        NSIndexPath *indexpath = [NSIndexPath indexPathForRow:index_int inSection:0];
        //Product_list_cell *tappedCell = (Product_list_cell *)[_list_view cellForRowAtIndexPath:indexpath];
        List_cell *tappedCell = [_list_collection cellForItemAtIndexPath:indexpath];

        NSString *tonnes_str=tappedCell.tonnes.text;
        NSString *pieces_str=tappedCell.pieces.text;
        if ([tonnes_str isEqualToString:@""]||[tonnes_str isEqual:[NSNull null]]||[tonnes_str isEqualToString:@"<nil>"]||[tonnes_str isEqualToString:@"0"])
        {
           
            return @"tonnes";
            break;
        }
        else if ([pieces_str isEqualToString:@""]||[pieces_str isEqual:[NSNull null]]||[pieces_str isEqualToString:@"<nil>"]||[pieces_str isEqualToString:@"0"])
        {
            return @"pieces";
            break;
        }else{
             return @"YES";
        }
    }
    if (Selected_ids_ary.count ==0)
    {
        return @"select";
    }
    else
        return @"YES";
}

-(BOOL)Popup_proceed_validation
{
    if (_pincode_tf.text.length !=6 )
    {
        [self Alert:@"Invalid Pincode"];
        return NO;
    }
    else if ([_city_tf.text isEqual:[NSNull null]]||[_city_tf.text isEqualToString:@""]||[_city_tf.text isEqualToString:@"<nil>"])
    {
        [self Alert:@"Invalid City"];

        return NO;
    }
    return YES;
}

- (IBAction)popup_proceed_click:(id)sender
{
    if ([edited isEqualToString:@"yes"]) {
        [self Alert:@"Please select Valid address from the list given"];
    }
    
    else{
    [self local_notif];

    if ([self Popup_proceed_validation] == YES)
    {
       
        [tonnes_pieces_ary removeAllObjects];
        _popup_view.hidden=YES;
        _hideView.hidden = YES;
        self.list_view.userInteractionEnabled=YES;
//        for (int i = 0; i<tones_dict.count; i++) {
//            if () {
//                
//            }
//        }
        NSArray *selected_keys=[tones_dict allKeys];
        
        for (int c=0; c<Product_List_Dict.count; c++)
        {
            NSString *key=[NSString stringWithFormat:@"%d",c];
            
            if ([selected_keys containsObject:key])
            {
                    NSMutableDictionary  *Tonnes_Pieces_Dict=[[NSMutableDictionary alloc]init];
                    [Tonnes_Pieces_Dict setObject:[tones_dict valueForKey:key] forKey:@"tonnes"];
                    [Tonnes_Pieces_Dict setObject:[pieces_dict valueForKey:key] forKey:@"pieces"];
                    [Tonnes_Pieces_Dict setObject:[[Product_List_Dict valueForKey:@"name"]objectAtIndex:c] forKey:@"name"] ;
                    [Tonnes_Pieces_Dict setObject:[[Product_List_Dict valueForKey:@"img"]objectAtIndex:c] forKey:@"img"] ;
                    [tonnes_pieces_ary addObject:Tonnes_Pieces_Dict];
            }
        }
        NSMutableArray *selected_tonnes=[[NSMutableArray alloc]init];
        
        for (int t=0; t<tonnes_pieces_ary.count; t++)
        {
            [selected_tonnes addObject:[[tonnes_pieces_ary valueForKey:@"tonnes"]objectAtIndex:t]];
        }
        NSString *ids_str = [Selected_ids_ary componentsJoinedByString:@"_"];
        NSString *tonnes_str = [selected_tonnes componentsJoinedByString:@"_"];

        product_defined *myControllerHastag = [[product_defined alloc]init];
        myControllerHastag.Pin_code =_pincode_tf.text;
        myControllerHastag.Location =_city_tf.text;
        myControllerHastag.selected_ids =ids_str;
        myControllerHastag.product_type =_product_type;
        myControllerHastag.selected_qty =tonnes_str;
        myControllerHastag.tonne_piece_ary=tonnes_pieces_ary;
        myControllerHastag.pincode_default = self.pincode_tf.text;
        myControllerHastag.city_default = self.city_tf.text;
        _pincode_tf.text=@"";
        _city_tf.text=@"";
        [self.navigationController pushViewController:myControllerHastag animated:YES];
    }
    else
    {
        
    }
 }
}

-(void)local_notif
{
    NSLog(@"startLocalNotification");
    
//    UILocalNotification *notification = [[UILocalNotification alloc] init];
//    notification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
//    notification.alertBody = @"Your Products Waiting for you in Cart!";
//    notification.timeZone = [NSTimeZone defaultTimeZone];
//    notification.soundName = UILocalNotificationDefaultSoundName;
//    notification.applicationIconBadgeNumber = 0;
//    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
//
//    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
}

-(void)Pincode_To_City
{
    NSString *url_form=[NSString stringWithFormat:@"getLocationsBasedOnPincode?pincode=%@",newString];
     NSString* urlEncoded = [url_form stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
     [self.view endEditing:YES];
    [[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString:urlEncoded requestNumber:Get_Country showProgress:YES withHandler:^(BOOL success, id data)
         {
             if (success)
             {
                 if (data) {
                     
                     locations_array = [[NSArray alloc]init];
                     NSLog(@" response %@",data);
                     locations_array =[data valueForKey:@"Locations"];
                     NSLog(@"countries list %@",Countries_dict);
                     table_tag=@"Location";
                     if (locations_array.count==0)
                     {
                         _city_tf.text =@"";
                     }
                     int tab_height= 301;//locations_array.count*50;
                     if (tab_height<=300)
                     {
                         CGRect frame = _location_table.frame;
                         frame.size.height = tab_height;
                         _location_table.frame = frame;
                     }
                     [_location_table reloadData];
                 }
             }
             else
             {
//                 UIAlertController * alert=   [UIAlertController
//                                               alertControllerWithTitle:@"Alert"
//                                               message:[NSString stringWithFormat:@"%@",[data valueForKey:@"error_message"]]
//                                               preferredStyle:UIAlertControllerStyleAlert];
//                 
//                 UIAlertAction* ok = [UIAlertAction
//                                      actionWithTitle:@"OK"
//                                      style:UIAlertActionStyleDefault
//                                      handler:^(UIAlertAction * action)
//                                      {
//                                          [alert dismissViewControllerAnimated:YES completion:nil];
//                                      }];
//                 [alert addAction:ok];
//                 [self presentViewController:alert animated:YES completion:nil];
             }
         }];
}

-(void)City_To_Pincode
{
    NSString *url_form=[NSString stringWithFormat:@"getLocations?loc=%@",newString];
    NSString* urlEncoded = [url_form stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [[STParsing sharedWebServiceHelper]requesting_GET_ServiceWithString:urlEncoded requestNumber:Get_Country showProgress:YES withHandler:^(BOOL success, id data)
     {
         if (success)
         {
             NSLog(@" response %@",data);
             if (data)
             {
                 locations_array = [[NSArray alloc]init];
                 NSLog(@" response %@",data);
                 locations_array =[data valueForKey:@"Locations"];
                 NSLog(@"countries list %@",Countries_dict);
                 table_tag=@"Location";
                 
                long int tab_height=locations_array.count*120;
                 if (tab_height<300)
                 {
                     CGRect frame = _location_table.frame;
                     frame.size.height = tab_height;
                     _location_table.hidden = false;
                     //_location_table.frame = frame;
                     [self.location_table setFrame:CGRectMake(_city_tf.frame.origin.x,_city_tf.frame.origin.y +30,self.city_tf.frame.size.width,200)];
                     
                 }
                 
                 [_location_table reloadData];
             }
         }
         else
         {
//             UIAlertController * alert=   [UIAlertController
//                                           alertControllerWithTitle:@"Alert"
//                                           message:[NSString stringWithFormat:@"%@",[data valueForKey:@"error_message"]]
//                                           preferredStyle:UIAlertControllerStyleAlert];
//             UIAlertAction* ok = [UIAlertAction
//                                  actionWithTitle:@"OK"
//                                  style:UIAlertActionStyleDefault
//                                  handler:^(UIAlertAction * action)
//                                  {
//                                      [alert dismissViewControllerAnimated:YES completion:nil];
//                                  }];
//             [alert addAction:ok];
//             [self presentViewController:alert animated:YES completion:nil];
         }
     }];
}

- (IBAction)popup_decline_click:(id)sender
{
    _popup_view.hidden=YES;
    _hideView.hidden=YES;
    
    _selectLabel.hidden = YES;
    _location_table.hidden=YES;
    self.list_view.userInteractionEnabled=YES;
    _pincode_tf.text=@"";
    _city_tf.text=@"";

    table_tag=@"products";
}

- (IBAction)Proceed_click:(id)sender
{
    [_city_tf resignFirstResponder];
    [self.view endEditing:YES];
        
    if ([[self proceed_validation] isEqualToString:@"YES"])
    {
        if ([C_Count isEqualToString:@"0"])
        {
            self.pincode_tf.userInteractionEnabled = true;
            self.city_tf.userInteractionEnabled = true;
            self.noteLabel.hidden = true;
        }
        else
        {
            if (prev_pincode ==nil||[prev_pincode isEqualToString:@"<null>"])
            {
                prev_pincode=@"";
            }
            if (prev_location ==nil||[prev_location isEqualToString:@"<null>"])
            {
                prev_location=@"";
            }

        NSString *pincode=prev_pincode;
        NSString *city=prev_location;
        
        if ([pincode isEqual:[NSNull null]]||[pincode isEqualToString:@"(null)"])
        {
            self.pincode_tf.userInteractionEnabled = true;
            self.city_tf.userInteractionEnabled = true;
            self.noteLabel.hidden = true;
        }
        else
        {
        self.pincode_tf.text = pincode;
         self.city_tf.text = city;
//        self.pincode_tf.userInteractionEnabled = false;
//         self.city_tf.userInteractionEnabled = false;
         self.noteLabel.hidden = false;
        }
        }
        
        
        
    //_popup_top.constant = self.view.frame.size.height/2-177;
      //  _popup_top.constant = 20;

    [UIView animateWithDuration:0.5
                     animations:^{
                         [self.view layoutIfNeeded]; // Called on parent view
                     }];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationDelegate:self];
    _popup_view.hidden=NO;
     _hideView.hidden=NO;
        
        _selectLabel.hidden = YES;
    _location_table.hidden=YES;
    self.list_view.userInteractionEnabled=NO;

    table_tag=@"products";
    _popup_view.alpha = 1.5;
    [self.view addSubview:_popup_view];
    singleFingerTap.enabled=YES;
    [UIView commitAnimations];
    }
    
    else if ([[self proceed_validation] isEqualToString:@"tonnes"])
    {
        NSLog(@"enter tonnes");
        [self Alert:@"Please Enter Tonnes"];
    }
    else if ([[self proceed_validation] isEqualToString:@"pieces"])
    {
        NSLog(@"enter pieces");
        [self Alert:@"Please Enter Pieces"];

    }
    else if ([[self proceed_validation] isEqualToString:@"select"])
    {
        NSLog(@"Select atleast single product");
        [self Alert:@"Please Select Product"];
    }
 
}

- (IBAction)conversionAction:(id)sender {
    self.tonsTextfield.text = @"";
    self.kgTexfield.text = @"";
    self.conversion_top.constant = 50;
    self.conversionView.hidden = false;
}

-(void)Alert:(NSString *)Msg
{
    NSDictionary *options = @{kCRToastNotificationTypeKey:@(CRToastTypeNavigationBar),
                              
                              kCRToastTextKey : Msg,
                              
                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                              
                              kCRToastBackgroundColorKey : [UIColor colorWithRed:44.0/255.0 green:52.0/255.0 blue:75.0/255.0 alpha:1],
                              kCRToastTimeIntervalKey: @(2),
//                              kCRToastFontKey:[UIFont fontWithName:@"PT Sans Narrow" size:23],
                              kCRToastInteractionRespondersKey:@[[CRToastInteractionResponder interactionResponderWithInteractionType:CRToastInteractionTypeSwipeUp
                                                                  
                                                                                                                 automaticallyDismiss:YES
                                                                  
                                                                                                                                block:^(CRToastInteractionType interactionType){
                                                                                                                                    NSLog(@"Dismissed with %@ interaction", NSStringFromCRToastInteractionType(interactionType));
                                                                                                                                }]],
                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeGravity),
                              
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeGravity),
                              
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                              
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop)
                              
                              };
    
    [CRToastManager showNotificationWithOptions:options
     
                                completionBlock:^{
                                    
                                    NSLog(@"Completed");
                                    
                                }];
}



- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    //CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    [self.view endEditing:YES];
    _popup_view.hidden=YES;
   
    
  //  _popup_top.constant = 400;
    _selectLabel.hidden = YES;
    _location_table.hidden=YES;
    self.list_view.userInteractionEnabled=YES;
    _conversionView.hidden = true;
    table_tag=@"products";
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField.tag == 100) {
       self.conversion_top.constant = 300;
    }
    else if (textField.tag == 150)
        
    {
        self.conversion_top.constant = 300;
    }
    else
    {
        self.conversion_top.constant = 0;
    }
    return YES;
}

#define ACCEPTABLE_CHARACTERS @"0123456789."
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    
     if (textField.tag == 100 || textField.tag == 150)
     {
    if (![string isEqualToString:filtered])
    {
        return false;
    }
     }

    
    if (textField.tag == 100)
    {
     //   self.conversion_top.constant = 100;
        NSString *tonValue;
        
        if ([string isEqualToString:@""]) {
            
            tonValue = [_tonsTextfield.text substringToIndex:_tonsTextfield.text.length-1];
            
        }
        
        else{
            
            tonValue =[NSString stringWithFormat:@"%@%@",_tonsTextfield.text,string];
            
        }
        
        NSArray *sep = [tonValue componentsSeparatedByString:@"."];
        
        //        NSString *lastStrng =[NSString stringWithFormat:@"%hu",[newString characterAtIndex:[newString length] - 1]];
        
        if (sep.count == 3) {
            
            return false;
            
        }
        
        if([sep count] >= 2)
            
        {
            
            NSString *sepStr=[NSString stringWithFormat:@"%@",[sep objectAtIndex:1]];
            
            if (sepStr.length>3)
                
            {
                
                return !([sepStr length]>3);
                
            }
            
        }
        
        float kgs = [tonValue floatValue]*1000;
        
        _kgTexfield.text = [NSString stringWithFormat:@"%.3f",kgs];
        
    }
    
    else if (textField.tag == 150)
        
    {
      //  self.conversion_top.constant = 100;
        NSString *kgValue;
        
        if ([string isEqualToString:@""]) {
            
            kgValue = [_kgTexfield.text substringToIndex:_kgTexfield.text.length-1];
            
        }
        
        else{
            
            kgValue =[NSString stringWithFormat:@"%@%@",_kgTexfield.text,string];
            
        }
        
        NSArray *sep = [kgValue componentsSeparatedByString:@"."];
        
        //        NSString *lastStrng =[NSString stringWithFormat:@"%hu",[newString characterAtIndex:[newString length] - 1]];
        
        if (sep.count == 3) {
            
            return false;
            
        }
        
        if([sep count] >= 2)
            
        {
            
            NSString *sepStr=[NSString stringWithFormat:@"%@",[sep objectAtIndex:1]];
            
            if (sepStr.length>3)
                
            {
                
                return !([sepStr length]>3);
                
            }
            
        }
        
        //  NSString *kgValue = [NSString stringWithFormat:@"%@%@",_kgTexfield.text,string];
        
        float tons = [kgValue floatValue]/1000.0;
        _tonsTextfield.text = [NSString stringWithFormat:@" %.3f",tons];
        
    }
    

    else
    {
    
    newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    NSLog(@"enterd text %@",newString);
    
    NSString *Home_tag_str=[NSString stringWithFormat:@"%ld",(long)textField.tag];
    
    if ([Home_tag_str isEqualToString:@"88888"])//pin
    {
        
        _proceedottomConstriant.constant = 0;
         edited = @"yes";
        if (newString.length == 6)
        {
            _pincode_tf.text=newString;
            [self Pincode_To_City];
           
        }
    }
    else if ([Home_tag_str isEqualToString:@"99999"])//city
    {
         _proceedottomConstriant.constant = 0;
         edited = @"yes";
        if ([newString isEqualToString:@""]||[newString isEqual:[NSNull null]])
        {
        }
        else
        {
            if (newString.length>3) {
                
                 [self City_To_Pincode];
            }
           
        }
    }else if ([Home_tag_str isEqualToString:@"252"])//city
    {
        NSLog(@"tag is 252");
    }
    else
    {
      
        if(![string isEqualToString:@"0"] || textField.text.length>0  || [string isEqualToString:@""]){
        
            NSString *TF_check = [NSString stringWithFormat:@"%c", [Home_tag_str characterAtIndex:0]];
            NSString *Tag = [Home_tag_str substringFromIndex:1];
            int row=[Tag intValue];
            NSLog(@"tag %d",row);
            
            NSIndexPath *indexpath = [NSIndexPath indexPathForRow:row inSection:0];
            // Product_list_cell *tappedCell = (Product_list_cell *)[_list_view cellForRowAtIndexPath:indexpath];
            List_cell *tappedCell = [_list_collection cellForItemAtIndexPath:indexpath];
            
            if (pieces_per_ton_ary.count>0)
            {//httpss
                NSString *pieces_per_tonne=[pieces_per_ton_ary objectAtIndex:row];
                int pieces_per_tonne_int=[pieces_per_tonne intValue];
                
                if ([TF_check isEqualToString:@"1"])//tonnes
                {
                    NSArray *sep = [newString componentsSeparatedByString:@"."];
                    //        NSString *lastStrng =[NSString stringWithFormat:@"%hu",[newString characterAtIndex:[newString length] - 1]];
                    if (sep.count == 3) {
                        return false;
                    }
                    if([sep count] >= 2)
                    {
                        NSString *sepStr=[NSString stringWithFormat:@"%@",[sep objectAtIndex:1]];
                        if (sepStr.length>3)
                        {
                            return !([sepStr length]>3);
                        }
                        
                        float tonnes_int=[newString floatValue];
                        int total_pieces_int=tonnes_int*pieces_per_tonne_int;
                        NSString *total_pieces_str;
                        if (total_pieces_int >0) {
                            
                            total_pieces_str=[NSString stringWithFormat:@"%d",total_pieces_int];
                            
                            if (total_pieces_str.length>6) {
                                NSRange range = NSMakeRange(0,6);
                                NSString *trimmedString=[total_pieces_str substringWithRange:range];
                                total_pieces_str =trimmedString;
                            }
                        }
                        else{
                            newString = @"";
                            total_pieces_str = @"";
                        }
                        tappedCell.pieces.text=total_pieces_str;
                        
                        [tones_dict setObject:newString forKey:[NSString stringWithFormat:@"%d",row]];
                        [pieces_dict setObject:total_pieces_str forKey:[NSString stringWithFormat:@"%d",row]];
                    }
                    else
                    {
                        float tonnes_int=[newString floatValue];
                        int total_pieces_int=tonnes_int*pieces_per_tonne_int;
                        NSString *total_pieces_str;
                        if (total_pieces_int >0)
                        {
                            total_pieces_str=[NSString stringWithFormat:@"%d",total_pieces_int];
                            
                            if (total_pieces_str.length>6) {
                                NSRange range = NSMakeRange(0,6);
                                NSString *trimmedString=[total_pieces_str substringWithRange:range];
                                total_pieces_str =trimmedString;
                            }
                        }else{
                            newString = @"";
                            total_pieces_str = @"";
                        }
                        tappedCell.pieces.text=total_pieces_str;
                        
                        [tones_dict setObject:newString forKey:[NSString stringWithFormat:@"%d",row]];
                        [pieces_dict setObject:total_pieces_str forKey:[NSString stringWithFormat:@"%d",row]];
                    }
                }
                else if ([TF_check isEqualToString:@"2"])//pieces
                {
                    float pieces_int=[newString integerValue];
                    float total_tonnes_int=pieces_int/pieces_per_tonne_int;
                    NSString *total_tonnes_str;
                    NSArray *sep = [newString componentsSeparatedByString:@"."];
                    if (sep.count == 2) {
                        return false;
                    }
                    if (total_tonnes_int >0)
                    {
                        total_tonnes_str=[NSString stringWithFormat:@"%.3f",total_tonnes_int];
                        if (total_tonnes_str.length>6)
                        {
                            NSRange range = NSMakeRange(0,6);
                            NSString *trimmedString=[total_tonnes_str substringWithRange:range];
                            total_tonnes_str =trimmedString;
                        }
                    }
                    else
                    {
                        newString = @"";
                        total_tonnes_str = @"";
                    }
                    tappedCell.tonnes.text=total_tonnes_str;
                    [pieces_dict setObject:newString forKey:[NSString stringWithFormat:@"%d",row]];
                    [tones_dict setObject:total_tonnes_str forKey:[NSString stringWithFormat:@"%d",row]];
                    
                    if (!string.length) return YES;  NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string]; NSString *expression = @"^([0-9]+)?(\\.([0-9]{1,2})?)?$"; NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:nil]; NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])]; if (numberOfMatches == 0) return NO;
                }
                
                NSLog(@"tonnes dictionay %@",tones_dict);
                NSLog(@"pieces dictionay %@",pieces_dict);
            }else{
                newString =@"";
                return NO;
            }
        }
    }
    }
    
    return YES;
}
- (void)keyboardWillHide:(NSNotification *)n
{
     _proceedottomConstriant.constant = 0 ;
    
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
//    NSDictionary* userInfo = [n userInfo];
    
    
    // get the size of the keyboard
//    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
//
//    // resize the scrollview
//    CGRect viewFrame = self.list_view.frame;
//    // I'm also subtracting a constant kTabBarHeight because my UIScrollView was offset by the UITabBar so really only the portion of the keyboard that is leftover pass the UITabBar is obscuring my UIScrollView.
//    viewFrame.size.height += (keyboardSize.height - 50);
//    if (viewFrame.size.height < tableHeight) {
//        viewFrame.size.height = tableHeight;
//    }
//    [UIView beginAnimations:nil context:NULL];
//    [UIView setAnimationBeginsFromCurrentState:YES];
//    [self.list_view setFrame:viewFrame];
//    [UIView commitAnimations];
//    keyboardIsShown = NO;
}

- (void)keyboardWillShow:(NSNotification *)n
{
    
    NSInteger height = [n.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
   
     [self.view setFrame:CGRectMake(0,-110,self.view.frame.size.width,self.view.frame.size.height)];
    _proceedottomConstriant.constant = height-110;
    
    
    
    // This is an ivar I'm using to ensure that we do not do the frame size adjustment on the `UIScrollView` if the keyboard is already shown.  This can happen if the user, after fixing editing a `UITextField`, scrolls the resized `UIScrollView` to another `UITextField` and attempts to edit the next `UITextField`.  If we were to resize the `UIScrollView` again, it would be disastrous.  NOTE: The keyboard notification will fire even when the keyboard is already shown.
//    if (keyboardIsShown)
//    {
//        return;
//    }
//
//    NSDictionary* userInfo = [n userInfo];
//
//    // get the size of the keyboard
//    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
//
//    // resize the noteView
//    CGRect viewFrame = self.list_view.frame;
//    // I'm also subtracting a constant kTabBarHeight because my UIScrollView was offset by the UITabBar so really only the portion of the keyboard that is leftover pass the UITabBar is obscuring my UIScrollView.
//    viewFrame.size.height -= (keyboardSize.height - 50);
//
//    [UIView beginAnimations:nil context:NULL];
//    [UIView setAnimationBeginsFromCurrentState:YES];
//    [self.list_view setFrame:viewFrame];
//    [UIView commitAnimations];
//    keyboardIsShown = YES;
    
    
    
    
    
    
    
    
}
- (IBAction)Cart_click:(id)sender
{
    if ([user_id isEqualToString:@""]||[user_id isEqual:[NSNull null]]||[user_id isEqualToString:@"<nil>"]||user_id == nil||[user_id isEqualToString:@"0"]||[user_id isEqualToString:@"(null)"])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        NewLoginVC *myNewVC = (NewLoginVC *)[storyboard instantiateViewControllerWithIdentifier:@"NewLoginVC"];
        [self.navigationController pushViewController:myNewVC animated:YES];
    }
    else//go to cart page
    {
        My_Cart *define = [[My_Cart alloc]init];
        [self.navigationController pushViewController:define animated:YES];
    }
}

//-(void)Cart_click
//{
//    if ([user_id isEqualToString:@""]||[user_id isEqual:[NSNull null]]||[user_id isEqualToString:@"<nil>"]||user_id == nil||[user_id isEqualToString:@"0"])
//    {
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        LoginViewController *myNewVC = (LoginViewController *)[storyboard instantiateViewControllerWithIdentifier:@"LoginViewControllerID"];
//        [self.navigationController pushViewController:myNewVC animated:YES];
//    }
//    else//go to cart page
//    {
//        My_Cart *define = [[My_Cart alloc]init];
//        [self.navigationController pushViewController:define animated:YES];
//    }
//}

- (IBAction)back_Click:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

//-(void)Back_click
//{
//    
//}

- (IBAction)doneClicked:(id)sender
{
    NSLog(@"Done Clicked.");
    [self.view endEditing:YES];
//[_conversionView setHidden:YES];
}

- (IBAction)kg_doneClicked:(id)sender
{
    NSLog(@"Done Clicked.");
    [self.view endEditing:YES];
 //  [_conversionView setHidden:YES];
}

- (IBAction)goToHomeScreenAction:(id)sender
{
    HomeViewController *con = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
    [self.navigationController pushViewController:con animated:YES];
    
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (IBAction)categories_Action:(id)sender {
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CategoryVC *myNewVC = (CategoryVC *)[storyboard instantiateViewControllerWithIdentifier:@"CategoryVC"];
    [self.navigationController pushViewController:myNewVC animated:YES];
    
    
}


@end
