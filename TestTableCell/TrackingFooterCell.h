//
//  TrackingFooterCell.h
//  SteelonCall
//
//  Created by nagaraj  kumar on 14/11/18.
//  Copyright © 2018 com.indobytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrackingFooterCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *trackProgress_Btn;
@property (weak, nonatomic) IBOutlet UIImageView *arrow_Image;

@property (weak, nonatomic) IBOutlet UIImageView *Status_Image;
@property (weak, nonatomic) IBOutlet UILabel *order_Lbl1;
@property (weak, nonatomic) IBOutlet UILabel *order_Lbl2;

@property (weak, nonatomic) IBOutlet UILabel *processing_Lbl1;
@property (weak, nonatomic) IBOutlet UILabel *processing_Lbl2;
@property (weak, nonatomic) IBOutlet UILabel *shipping_Lbl1;
@property (weak, nonatomic) IBOutlet UILabel *shipping_Lbl2;
@property (weak, nonatomic) IBOutlet UILabel *delivery_Lbl1;

@property (weak, nonatomic) IBOutlet UILabel *delivery_Lbl2;
@property (weak, nonatomic) IBOutlet UILabel *estimatedLbl;

@end
