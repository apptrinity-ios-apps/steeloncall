//
//  OP_Cell_2.h
//  Steeloncall_Seller
//
//  Created by INDOBYTES on 21/12/16.
//  Copyright © 2016 sai. All rights reserved.

#import <UIKit/UIKit.h>

@interface OP_Cell_2_invoice : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *location;
@property (strong, nonatomic) IBOutlet UILabel *details;
@property (strong, nonatomic) IBOutlet UILabel *status;
@property (strong, nonatomic) IBOutlet UILabel *date;

@property (strong, nonatomic) IBOutlet UILabel *time;

@end
