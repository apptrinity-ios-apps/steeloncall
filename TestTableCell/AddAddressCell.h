//
//  AddAddressCell.h
//  SteelonCall
//
//  Created by S s Vali on 9/12/17.
//  Copyright © 2017 com.indobytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddAddressCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *headingLabel;
@property (strong, nonatomic) IBOutlet UITextField *dataLabel;
@property (strong, nonatomic) IBOutlet UITextField *lastName;
@property (strong, nonatomic) IBOutlet UITextField *company;
@property (strong, nonatomic) IBOutlet UITextField *telephoneLabel;
@end
